function preloderFunction() {
  $(window).load(function () {
    setTimeout(function () {
      document.getElementById("page-top").scrollIntoView(),
        $("#ctn-preloader").addClass("loaded"),
        $("body").removeClass("no-scroll-y"),
        $(".fullscreen-bg__video").addClass("image"),
        $(".center_cover").addClass("text1"),
        $("#ctn-preloader").hasClass("loaded") &&
          $("#preloader")
            .delay(1e3)
            .queue(function () {
              $(this).remove(), afterLoad();
            });
    }, 1500);
  });
}
function afterLoad() {}
!(function (t, e, i) {
  function n(t, e) {
    return typeof t === e;
  }
  function s(t) {
    var e = w.className,
      i = _._config.classPrefix || "";
    if ((b && (e = e.baseVal), _._config.enableJSClass)) {
      var n = new RegExp("(^|\\s)" + i + "no-js(\\s|$)");
      e = e.replace(n, "$1" + i + "js$2");
    }
    _._config.enableClasses &&
      ((e += " " + i + t.join(" " + i)),
      b ? (w.className.baseVal = e) : (w.className = e));
  }
  function o(t, e) {
    if ("object" == typeof t) for (var i in t) D(t, i) && o(i, t[i]);
    else {
      var n = (t = t.toLowerCase()).split("."),
        r = _[n[0]];
      if ((2 == n.length && (r = r[n[1]]), void 0 !== r)) return _;
      (e = "function" == typeof e ? e() : e),
        1 == n.length
          ? (_[n[0]] = e)
          : (!_[n[0]] ||
              _[n[0]] instanceof Boolean ||
              (_[n[0]] = new Boolean(_[n[0]])),
            (_[n[0]][n[1]] = e)),
        s([(e && 0 != e ? "" : "no-") + n.join("-")]),
        _._trigger(t, e);
    }
    return _;
  }
  function r() {
    return "function" != typeof e.createElement
      ? e.createElement(arguments[0])
      : b
      ? e.createElementNS.call(e, "http://www.w3.org/2000/svg", arguments[0])
      : e.createElement.apply(e, arguments);
  }
  function a(t, i, n, s) {
    var o,
      a,
      l,
      c,
      d = "modernizr",
      h = r("div"),
      u = (function () {
        var t = e.body;
        return t || ((t = r(b ? "svg" : "body")).fake = !0), t;
      })();
    if (parseInt(n, 10))
      for (; n--; )
        ((l = r("div")).id = s ? s[n] : d + (n + 1)), h.appendChild(l);
    return (
      ((o = r("style")).type = "text/css"),
      (o.id = "s" + d),
      (u.fake ? u : h).appendChild(o),
      u.appendChild(h),
      o.styleSheet
        ? (o.styleSheet.cssText = t)
        : o.appendChild(e.createTextNode(t)),
      (h.id = d),
      u.fake &&
        ((u.style.background = ""),
        (u.style.overflow = "hidden"),
        (c = w.style.overflow),
        (w.style.overflow = "hidden"),
        w.appendChild(u)),
      (a = i(h, t)),
      u.fake
        ? (u.parentNode.removeChild(u), (w.style.overflow = c), w.offsetHeight)
        : h.parentNode.removeChild(h),
      !!a
    );
  }
  function l(t, e) {
    return !!~("" + t).indexOf(e);
  }
  function c(t) {
    return t
      .replace(/([A-Z])/g, function (t, e) {
        return "-" + e.toLowerCase();
      })
      .replace(/^ms-/, "-ms-");
  }
  function d(e, i, n) {
    var s;
    if ("getComputedStyle" in t) {
      s = getComputedStyle.call(t, e, i);
      var o = t.console;
      null !== s
        ? n && (s = s.getPropertyValue(n))
        : o &&
          o[o.error ? "error" : "log"].call(
            o,
            "getComputedStyle returning null, its possible modernizr test results are inaccurate"
          );
    } else s = !i && e.currentStyle && e.currentStyle[n];
    return s;
  }
  function h(e, n) {
    var s = e.length;
    if ("CSS" in t && "supports" in t.CSS) {
      for (; s--; ) if (t.CSS.supports(c(e[s]), n)) return !0;
      return !1;
    }
    if ("CSSSupportsRule" in t) {
      for (var o = []; s--; ) o.push("(" + c(e[s]) + ":" + n + ")");
      return a(
        "@supports (" +
          (o = o.join(" or ")) +
          ") { #modernizr { position: absolute; } }",
        function (t) {
          return "absolute" == d(t, null, "position");
        }
      );
    }
    return i;
  }
  function u(t) {
    return t
      .replace(/([a-z])-([a-z])/g, function (t, e, i) {
        return e + i.toUpperCase();
      })
      .replace(/^-/, "");
  }
  function p(t, e, s, o) {
    function a() {
      d && (delete M.style, delete M.modElem);
    }
    if (((o = !n(o, "undefined") && o), !n(s, "undefined"))) {
      var c = h(t, s);
      if (!n(c, "undefined")) return c;
    }
    for (
      var d, p, f, m, g, v = ["modernizr", "tspan", "samp"];
      !M.style && v.length;

    )
      (d = !0), (M.modElem = r(v.shift())), (M.style = M.modElem.style);
    for (f = t.length, p = 0; f > p; p++)
      if (
        ((m = t[p]),
        (g = M.style[m]),
        l(m, "-") && (m = u(m)),
        M.style[m] !== i)
      ) {
        if (o || n(s, "undefined")) return a(), "pfx" != e || m;
        try {
          M.style[m] = s;
        } catch (t) {}
        if (M.style[m] != g) return a(), "pfx" != e || m;
      }
    return a(), !1;
  }
  function f(t, e) {
    return function () {
      return t.apply(e, arguments);
    };
  }
  function m(t, e, i, s, o) {
    var r = t.charAt(0).toUpperCase() + t.slice(1),
      a = (t + " " + E.join(r + " ") + r).split(" ");
    return n(e, "string") || n(e, "undefined")
      ? p(a, e, s, o)
      : (function (t, e, i) {
          var s;
          for (var o in t)
            if (t[o] in e)
              return !1 === i
                ? t[o]
                : n((s = e[t[o]]), "function")
                ? f(s, i || e)
                : s;
          return !1;
        })((a = (t + " " + k.join(r + " ") + r).split(" ")), e, i);
  }
  function g(t, e, n) {
    return m(t, i, i, e, n);
  }
  var v = [],
    y = {
      _version: "3.5.0",
      _config: {
        classPrefix: "",
        enableClasses: !0,
        enableJSClass: !0,
        usePrefixes: !0,
      },
      _q: [],
      on: function (t, e) {
        var i = this;
        setTimeout(function () {
          e(i[t]);
        }, 0);
      },
      addTest: function (t, e, i) {
        v.push({ name: t, fn: e, options: i });
      },
      addAsyncTest: function (t) {
        v.push({ name: null, fn: t });
      },
    },
    _ = function () {};
  (_.prototype = y), (_ = new _());
  var w = e.documentElement,
    b = "svg" === w.nodeName.toLowerCase(),
    C = "Moz O ms Webkit",
    k = y._config.usePrefixes ? C.toLowerCase().split(" ") : [];
  y._domPrefixes = k;
  var D,
    T = y._config.usePrefixes
      ? " -webkit- -moz- -o- -ms- ".split(" ")
      : ["", ""];
  (y._prefixes = T),
    (function () {
      var t = {}.hasOwnProperty;
      D =
        n(t, "undefined") || n(t.call, "undefined")
          ? function (t, e) {
              return e in t && n(t.constructor.prototype[e], "undefined");
            }
          : function (e, i) {
              return t.call(e, i);
            };
    })(),
    (y._l = {}),
    (y.on = function (t, e) {
      this._l[t] || (this._l[t] = []),
        this._l[t].push(e),
        _.hasOwnProperty(t) &&
          setTimeout(function () {
            _._trigger(t, _[t]);
          }, 0);
    }),
    (y._trigger = function (t, e) {
      if (this._l[t]) {
        var i = this._l[t];
        setTimeout(function () {
          var t;
          for (t = 0; t < i.length; t++) (0, i[t])(e);
        }, 0),
          delete this._l[t];
      }
    }),
    _._q.push(function () {
      y.addTest = o;
    });
  var x = (function () {
    var t = !("onblur" in e.documentElement);
    return function (e, n) {
      var s;
      return (
        !!e &&
        ((n && "string" != typeof n) || (n = r(n || "div")),
        !(s = (e = "on" + e) in n) &&
          t &&
          (n.setAttribute || (n = r("div")),
          n.setAttribute(e, ""),
          (s = "function" == typeof n[e]),
          n[e] !== i && (n[e] = i),
          n.removeAttribute(e)),
        s)
      );
    };
  })();
  y.hasEvent = x;
  var S = (function () {
    var e = t.matchMedia || t.msMatchMedia;
    return e
      ? function (t) {
          var i = e(t);
          return (i && i.matches) || !1;
        }
      : function (e) {
          var i = !1;
          return (
            a(
              "@media " + e + " { #modernizr { position: absolute; } }",
              function (e) {
                i =
                  "absolute" ==
                  (t.getComputedStyle
                    ? t.getComputedStyle(e, null)
                    : e.currentStyle
                  ).position;
              }
            ),
            i
          );
        };
  })();
  (y.mq = S),
    (y.prefixedCSSValue = function (t, e) {
      var i = !1,
        n = r("div").style;
      if (t in n) {
        var s = k.length;
        for (n[t] = e, i = n[t]; s-- && !i; )
          (n[t] = "-" + k[s] + "-" + e), (i = n[t]);
      }
      return "" === i && (i = !1), i;
    });
  var E = y._config.usePrefixes ? C.split(" ") : [];
  y._cssomPrefixes = E;
  var A = { elem: r("modernizr") };
  _._q.push(function () {
    delete A.elem;
  });
  var M = { style: A.elem.style };
  _._q.unshift(function () {
    delete M.style;
  }),
    (y.testAllProps = m),
    (y.testAllProps = g),
    (y.testProp = function (t, e, n) {
      return p([t], i, e, n);
    }),
    (y.testStyles = a),
    _.addTest("", "" in t),
    _.addTest("", function () {
      var e = navigator.userAgent;
      return (
        ((-1 === e.indexOf("Android 2.") && -1 === e.indexOf("Android 4.0")) ||
          -1 === e.indexOf("Mobile Safari") ||
          -1 !== e.indexOf("Chrome") ||
          -1 !== e.indexOf("Windows Phone") ||
          "file:" === location.protocol) &&
        t.history &&
        "pushState" in t.history
      );
    }),
    _.addTest("", function () {
      var t = !1,
        e = k.length;
      for (t = _.hasEvent("pointerdown"); e-- && !t; )
        x(k[e] + "pointerdown") && (t = !0);
      return t;
    }),
    _.addTest("", "" in t),
    _.addTest("", function () {
      var e = r("canvas"),
        i =
          "probablySupportsContext" in e
            ? "probablySupportsContext"
            : "supportsContext";
      return i in e
        ? e[i]("webgl") || e[i]("experimental-webgl")
        : "WebGLRenderingContext" in t;
    });
  var $ = !1;
  try {
    $ = "WebSocket" in t && 2 === t.WebSocket.CLOSING;
  } catch (t) {}
  _.addTest("", $),
    _.addTest("", g("animationName", "a", !0)),
    (function () {
      _.addTest("", function () {
        var t = !1,
          e = g("columnCount");
        try {
          (t = !!e) && (t = new Boolean(t));
        } catch (t) {}
        return t;
      });
      for (
        var t,
          e,
          i = [
            "Width",
            "Span",
            "Fill",
            "Gap",
            "Rule",
            "RuleColor",
            "RuleStyle",
            "RuleWidth",
            "BreakBefore",
            "BreakAfter",
            "BreakInside",
          ],
          n = 0;
        n < i.length;
        n++
      )
        (t = i[n].toLowerCase()),
          (e = g("column" + i[n])),
          ("breakbefore" === t || "breakafter" === t || "breakinside" == t) &&
            (e = e || g(i[n])),
          _.addTest("csscolumns." + t, e);
    })(),
    _.addTest("", g("flexBasis", "1px", !0)),
    _.addTest("", "" in t),
    _.addAsyncTest(function () {
      var t,
        e,
        i = r("img"),
        n = "sizes" in i;
      !n && "srcset" in i
        ? ((t =
            "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="),
          (e = function () {
            o("sizes", 2 == i.width);
          }),
          (i.onload = e),
          (i.onerror = e),
          i.setAttribute("sizes", "9px"),
          (i.srcset =
            t +
            " 1w,data:image/gif;base64,R0lGODlhAgABAPAAAP///wAAACH5BAAAAAAALAAAAAACAAEAAAICBAoAOw== 8w"),
          (i.src = t))
        : o("sizes", n);
    }),
    _.addTest("", "" in r("img")),
    _.addTest("", "" in t),
    s([]),
    delete y.addTest,
    delete y.addAsyncTest;
  for (var O = 0; O < _._q.length; O++) _._q[O]();
  t.Modernizr = _;
})(window, document),
  (function (t, e) {
    "object" == typeof module && "object" == typeof module.exports
      ? (module.exports = t.document
          ? e(t, !0)
          : function (t) {
              if (!t.document)
                throw new Error("jQuery requires a window with a document");
              return e(t);
            })
      : e(t);
  })("undefined" != typeof window ? window : this, function (t, e) {
    var i = [],
      n = t.document,
      s = i.slice,
      o = i.concat,
      r = i.push,
      a = i.indexOf,
      l = {},
      c = l.toString,
      d = l.hasOwnProperty,
      h = {},
      u = "1.12.4",
      p = function (t, e) {
        return new p.fn.init(t, e);
      },
      f = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
      m = /^-ms-/,
      g = /-([\da-z])/gi,
      v = function (t, e) {
        return e.toUpperCase();
      };
    function y(t) {
      var e = !!t && "length" in t && t.length,
        i = p.type(t);
      return (
        "function" !== i &&
        !p.isWindow(t) &&
        ("array" === i ||
          0 === e ||
          ("number" == typeof e && e > 0 && e - 1 in t))
      );
    }
    (p.fn = p.prototype =
      {
        jquery: u,
        constructor: p,
        selector: "",
        length: 0,
        toArray: function () {
          return s.call(this);
        },
        get: function (t) {
          return null != t
            ? 0 > t
              ? this[t + this.length]
              : this[t]
            : s.call(this);
        },
        pushStack: function (t) {
          var e = p.merge(this.constructor(), t);
          return (e.prevObject = this), (e.context = this.context), e;
        },
        each: function (t) {
          return p.each(this, t);
        },
        map: function (t) {
          return this.pushStack(
            p.map(this, function (e, i) {
              return t.call(e, i, e);
            })
          );
        },
        slice: function () {
          return this.pushStack(s.apply(this, arguments));
        },
        first: function () {
          return this.eq(0);
        },
        last: function () {
          return this.eq(-1);
        },
        eq: function (t) {
          var e = this.length,
            i = +t + (0 > t ? e : 0);
          return this.pushStack(i >= 0 && e > i ? [this[i]] : []);
        },
        end: function () {
          return this.prevObject || this.constructor();
        },
        push: r,
        sort: i.sort,
        splice: i.splice,
      }),
      (p.extend = p.fn.extend =
        function () {
          var t,
            e,
            i,
            n,
            s,
            o,
            r = arguments[0] || {},
            a = 1,
            l = arguments.length,
            c = !1;
          for (
            "boolean" == typeof r && ((c = r), (r = arguments[a] || {}), a++),
              "object" == typeof r || p.isFunction(r) || (r = {}),
              a === l && ((r = this), a--);
            l > a;
            a++
          )
            if (null != (s = arguments[a]))
              for (n in s)
                (t = r[n]),
                  r !== (i = s[n]) &&
                    (c && i && (p.isPlainObject(i) || (e = p.isArray(i)))
                      ? (e
                          ? ((e = !1), (o = t && p.isArray(t) ? t : []))
                          : (o = t && p.isPlainObject(t) ? t : {}),
                        (r[n] = p.extend(c, o, i)))
                      : void 0 !== i && (r[n] = i));
          return r;
        }),
      p.extend({
        expando: "jQuery" + (u + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function (t) {
          throw new Error(t);
        },
        noop: function () {},
        isFunction: function (t) {
          return "function" === p.type(t);
        },
        isArray:
          Array.isArray ||
          function (t) {
            return "array" === p.type(t);
          },
        isWindow: function (t) {
          return null != t && t == t.window;
        },
        isNumeric: function (t) {
          var e = t && t.toString();
          return !p.isArray(t) && e - parseFloat(e) + 1 >= 0;
        },
        isEmptyObject: function (t) {
          var e;
          for (e in t) return !1;
          return !0;
        },
        isPlainObject: function (t) {
          var e;
          if (!t || "object" !== p.type(t) || t.nodeType || p.isWindow(t))
            return !1;
          try {
            if (
              t.constructor &&
              !d.call(t, "constructor") &&
              !d.call(t.constructor.prototype, "isPrototypeOf")
            )
              return !1;
          } catch (t) {
            return !1;
          }
          if (!h.ownFirst) for (e in t) return d.call(t, e);
          for (e in t);
          return void 0 === e || d.call(t, e);
        },
        type: function (t) {
          return null == t
            ? t + ""
            : "object" == typeof t || "function" == typeof t
            ? l[c.call(t)] || "object"
            : typeof t;
        },
        globalEval: function (e) {
          e &&
            p.trim(e) &&
            (
              t.execScript ||
              function (e) {
                t.eval.call(t, e);
              }
            )(e);
        },
        camelCase: function (t) {
          return t.replace(m, "ms-").replace(g, v);
        },
        nodeName: function (t, e) {
          return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase();
        },
        each: function (t, e) {
          var i,
            n = 0;
          if (y(t))
            for (i = t.length; i > n && !1 !== e.call(t[n], n, t[n]); n++);
          else for (n in t) if (!1 === e.call(t[n], n, t[n])) break;
          return t;
        },
        trim: function (t) {
          return null == t ? "" : (t + "").replace(f, "");
        },
        makeArray: function (t, e) {
          var i = e || [];
          return (
            null != t &&
              (y(Object(t))
                ? p.merge(i, "string" == typeof t ? [t] : t)
                : r.call(i, t)),
            i
          );
        },
        inArray: function (t, e, i) {
          var n;
          if (e) {
            if (a) return a.call(e, t, i);
            for (
              n = e.length, i = i ? (0 > i ? Math.max(0, n + i) : i) : 0;
              n > i;
              i++
            )
              if (i in e && e[i] === t) return i;
          }
          return -1;
        },
        merge: function (t, e) {
          for (var i = +e.length, n = 0, s = t.length; i > n; ) t[s++] = e[n++];
          if (i != i) for (; void 0 !== e[n]; ) t[s++] = e[n++];
          return (t.length = s), t;
        },
        grep: function (t, e, i) {
          for (var n = [], s = 0, o = t.length, r = !i; o > s; s++)
            !e(t[s], s) !== r && n.push(t[s]);
          return n;
        },
        map: function (t, e, i) {
          var n,
            s,
            r = 0,
            a = [];
          if (y(t))
            for (n = t.length; n > r; r++)
              null != (s = e(t[r], r, i)) && a.push(s);
          else for (r in t) null != (s = e(t[r], r, i)) && a.push(s);
          return o.apply([], a);
        },
        guid: 1,
        proxy: function (t, e) {
          var i, n, o;
          return (
            "string" == typeof e && ((o = t[e]), (e = t), (t = o)),
            p.isFunction(t)
              ? ((i = s.call(arguments, 2)),
                ((n = function () {
                  return t.apply(e || this, i.concat(s.call(arguments)));
                }).guid = t.guid =
                  t.guid || p.guid++),
                n)
              : void 0
          );
        },
        now: function () {
          return +new Date();
        },
        support: h,
      }),
      "function" == typeof Symbol &&
        (p.fn[Symbol.iterator] = i[Symbol.iterator]),
      p.each(
        "Boolean Number String Function Array Date RegExp Object Error Symbol".split(
          " "
        ),
        function (t, e) {
          l["[object " + e + "]"] = e.toLowerCase();
        }
      );
    var _ = (function (t) {
      var e,
        i,
        n,
        s,
        o,
        r,
        a,
        l,
        c,
        d,
        h,
        u,
        p,
        f,
        m,
        g,
        v,
        y,
        _,
        w = "sizzle" + 1 * new Date(),
        b = t.document,
        C = 0,
        k = 0,
        D = ot(),
        T = ot(),
        x = ot(),
        S = function (t, e) {
          return t === e && (h = !0), 0;
        },
        E = 1 << 31,
        A = {}.hasOwnProperty,
        M = [],
        $ = M.pop,
        O = M.push,
        N = M.push,
        I = M.slice,
        P = function (t, e) {
          for (var i = 0, n = t.length; n > i; i++) if (t[i] === e) return i;
          return -1;
        },
        L =
          "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
        Y = "[\\x20\\t\\r\\n\\f]",
        H = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
        j =
          "\\[" +
          Y +
          "*(" +
          H +
          ")(?:" +
          Y +
          "*([*^$|!~]?=)" +
          Y +
          "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" +
          H +
          "))|)" +
          Y +
          "*\\]",
        W =
          ":(" +
          H +
          ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" +
          j +
          ")*)|.*)\\)|)",
        R = new RegExp(Y + "+", "g"),
        z = new RegExp("^" + Y + "+|((?:^|[^\\\\])(?:\\\\.)*)" + Y + "+$", "g"),
        F = new RegExp("^" + Y + "*," + Y + "*"),
        U = new RegExp("^" + Y + "*([>+~]|" + Y + ")" + Y + "*"),
        B = new RegExp("=" + Y + "*([^\\]'\"]*?)" + Y + "*\\]", "g"),
        q = new RegExp(W),
        V = new RegExp("^" + H + "$"),
        G = {
          ID: new RegExp("^#(" + H + ")"),
          CLASS: new RegExp("^\\.(" + H + ")"),
          TAG: new RegExp("^(" + H + "|[*])"),
          ATTR: new RegExp("^" + j),
          PSEUDO: new RegExp("^" + W),
          CHILD: new RegExp(
            "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" +
              Y +
              "*(even|odd|(([+-]|)(\\d*)n|)" +
              Y +
              "*(?:([+-]|)" +
              Y +
              "*(\\d+)|))" +
              Y +
              "*\\)|)",
            "i"
          ),
          bool: new RegExp("^(?:" + L + ")$", "i"),
          needsContext: new RegExp(
            "^" +
              Y +
              "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
              Y +
              "*((?:-\\d)?\\d*)" +
              Y +
              "*\\)|)(?=[^-]|$)",
            "i"
          ),
        },
        Q = /^(?:input|select|textarea|button)$/i,
        X = /^h\d$/i,
        K = /^[^{]+\{\s*\[native \w/,
        Z = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
        J = /[+~]/,
        tt = /'|\\/g,
        et = new RegExp("\\\\([\\da-f]{1,6}" + Y + "?|(" + Y + ")|.)", "ig"),
        it = function (t, e, i) {
          var n = "0x" + e - 65536;
          return n != n || i
            ? e
            : 0 > n
            ? String.fromCharCode(n + 65536)
            : String.fromCharCode((n >> 10) | 55296, (1023 & n) | 56320);
        },
        nt = function () {
          u();
        };
      try {
        N.apply((M = I.call(b.childNodes)), b.childNodes),
          M[b.childNodes.length].nodeType;
      } catch (t) {
        N = {
          apply: M.length
            ? function (t, e) {
                O.apply(t, I.call(e));
              }
            : function (t, e) {
                for (var i = t.length, n = 0; (t[i++] = e[n++]); );
                t.length = i - 1;
              },
        };
      }
      function st(t, e, n, s) {
        var o,
          a,
          c,
          d,
          h,
          f,
          v,
          y,
          C = e && e.ownerDocument,
          k = e ? e.nodeType : 9;
        if (
          ((n = n || []),
          "string" != typeof t || !t || (1 !== k && 9 !== k && 11 !== k))
        )
          return n;
        if (
          !s &&
          ((e ? e.ownerDocument || e : b) !== p && u(e), (e = e || p), m)
        ) {
          if (11 !== k && (f = Z.exec(t)))
            if ((o = f[1])) {
              if (9 === k) {
                if (!(c = e.getElementById(o))) return n;
                if (c.id === o) return n.push(c), n;
              } else if (
                C &&
                (c = C.getElementById(o)) &&
                _(e, c) &&
                c.id === o
              )
                return n.push(c), n;
            } else {
              if (f[2]) return N.apply(n, e.getElementsByTagName(t)), n;
              if (
                (o = f[3]) &&
                i.getElementsByClassName &&
                e.getElementsByClassName
              )
                return N.apply(n, e.getElementsByClassName(o)), n;
            }
          if (i.qsa && !x[t + " "] && (!g || !g.test(t))) {
            if (1 !== k) (C = e), (y = t);
            else if ("object" !== e.nodeName.toLowerCase()) {
              for (
                (d = e.getAttribute("id"))
                  ? (d = d.replace(tt, "\\$&"))
                  : e.setAttribute("id", (d = w)),
                  a = (v = r(t)).length,
                  h = V.test(d) ? "#" + d : "[id='" + d + "']";
                a--;

              )
                v[a] = h + " " + mt(v[a]);
              (y = v.join(",")), (C = (J.test(t) && pt(e.parentNode)) || e);
            }
            if (y)
              try {
                return N.apply(n, C.querySelectorAll(y)), n;
              } catch (t) {
              } finally {
                d === w && e.removeAttribute("id");
              }
          }
        }
        return l(t.replace(z, "$1"), e, n, s);
      }
      function ot() {
        var t = [];
        return function e(i, s) {
          return (
            t.push(i + " ") > n.cacheLength && delete e[t.shift()],
            (e[i + " "] = s)
          );
        };
      }
      function rt(t) {
        return (t[w] = !0), t;
      }
      function at(t) {
        var e = p.createElement("div");
        try {
          return !!t(e);
        } catch (t) {
          return !1;
        } finally {
          e.parentNode && e.parentNode.removeChild(e), (e = null);
        }
      }
      function lt(t, e) {
        for (var i = t.split("|"), s = i.length; s--; ) n.attrHandle[i[s]] = e;
      }
      function ct(t, e) {
        var i = e && t,
          n =
            i &&
            1 === t.nodeType &&
            1 === e.nodeType &&
            (~e.sourceIndex || E) - (~t.sourceIndex || E);
        if (n) return n;
        if (i) for (; (i = i.nextSibling); ) if (i === e) return -1;
        return t ? 1 : -1;
      }
      function dt(t) {
        return function (e) {
          return "input" === e.nodeName.toLowerCase() && e.type === t;
        };
      }
      function ht(t) {
        return function (e) {
          var i = e.nodeName.toLowerCase();
          return ("input" === i || "button" === i) && e.type === t;
        };
      }
      function ut(t) {
        return rt(function (e) {
          return (
            (e = +e),
            rt(function (i, n) {
              for (var s, o = t([], i.length, e), r = o.length; r--; )
                i[(s = o[r])] && (i[s] = !(n[s] = i[s]));
            })
          );
        });
      }
      function pt(t) {
        return t && void 0 !== t.getElementsByTagName && t;
      }
      for (e in ((i = st.support = {}),
      (o = st.isXML =
        function (t) {
          var e = t && (t.ownerDocument || t).documentElement;
          return !!e && "HTML" !== e.nodeName;
        }),
      (u = st.setDocument =
        function (t) {
          var e,
            s,
            r = t ? t.ownerDocument || t : b;
          return r !== p && 9 === r.nodeType && r.documentElement
            ? ((f = (p = r).documentElement),
              (m = !o(p)),
              (s = p.defaultView) &&
                s.top !== s &&
                (s.addEventListener
                  ? s.addEventListener("unload", nt, !1)
                  : s.attachEvent && s.attachEvent("onunload", nt)),
              (i.attributes = at(function (t) {
                return (t.className = "i"), !t.getAttribute("className");
              })),
              (i.getElementsByTagName = at(function (t) {
                return (
                  t.appendChild(p.createComment("")),
                  !t.getElementsByTagName("*").length
                );
              })),
              (i.getElementsByClassName = K.test(p.getElementsByClassName)),
              (i.getById = at(function (t) {
                return (
                  (f.appendChild(t).id = w),
                  !p.getElementsByName || !p.getElementsByName(w).length
                );
              })),
              i.getById
                ? ((n.find.ID = function (t, e) {
                    if (void 0 !== e.getElementById && m) {
                      var i = e.getElementById(t);
                      return i ? [i] : [];
                    }
                  }),
                  (n.filter.ID = function (t) {
                    var e = t.replace(et, it);
                    return function (t) {
                      return t.getAttribute("id") === e;
                    };
                  }))
                : (delete n.find.ID,
                  (n.filter.ID = function (t) {
                    var e = t.replace(et, it);
                    return function (t) {
                      var i =
                        void 0 !== t.getAttributeNode &&
                        t.getAttributeNode("id");
                      return i && i.value === e;
                    };
                  })),
              (n.find.TAG = i.getElementsByTagName
                ? function (t, e) {
                    return void 0 !== e.getElementsByTagName
                      ? e.getElementsByTagName(t)
                      : i.qsa
                      ? e.querySelectorAll(t)
                      : void 0;
                  }
                : function (t, e) {
                    var i,
                      n = [],
                      s = 0,
                      o = e.getElementsByTagName(t);
                    if ("*" === t) {
                      for (; (i = o[s++]); ) 1 === i.nodeType && n.push(i);
                      return n;
                    }
                    return o;
                  }),
              (n.find.CLASS =
                i.getElementsByClassName &&
                function (t, e) {
                  return void 0 !== e.getElementsByClassName && m
                    ? e.getElementsByClassName(t)
                    : void 0;
                }),
              (v = []),
              (g = []),
              (i.qsa = K.test(p.querySelectorAll)) &&
                (at(function (t) {
                  (f.appendChild(t).innerHTML =
                    "<a id='" +
                    w +
                    "'></a><select id='" +
                    w +
                    "-\r\\' msallowcapture=''><option selected=''></option></select>"),
                    t.querySelectorAll("[msallowcapture^='']").length &&
                      g.push("[*^$]=" + Y + "*(?:''|\"\")"),
                    t.querySelectorAll("[selected]").length ||
                      g.push("\\[" + Y + "*(?:value|" + L + ")"),
                    t.querySelectorAll("[id~=" + w + "-]").length ||
                      g.push("~="),
                    t.querySelectorAll(":checked").length || g.push(":checked"),
                    t.querySelectorAll("a#" + w + "+*").length ||
                      g.push(".#.+[+~]");
                }),
                at(function (t) {
                  var e = p.createElement("input");
                  e.setAttribute("type", "hidden"),
                    t.appendChild(e).setAttribute("name", "D"),
                    t.querySelectorAll("[name=d]").length &&
                      g.push("name" + Y + "*[*^$|!~]?="),
                    t.querySelectorAll(":enabled").length ||
                      g.push(":enabled", ":disabled"),
                    t.querySelectorAll("*,:x"),
                    g.push(",.*:");
                })),
              (i.matchesSelector = K.test(
                (y =
                  f.matches ||
                  f.webkitMatchesSelector ||
                  f.mozMatchesSelector ||
                  f.oMatchesSelector ||
                  f.msMatchesSelector)
              )) &&
                at(function (t) {
                  (i.disconnectedMatch = y.call(t, "div")),
                    y.call(t, "[s!='']:x"),
                    v.push("!=", W);
                }),
              (g = g.length && new RegExp(g.join("|"))),
              (v = v.length && new RegExp(v.join("|"))),
              (e = K.test(f.compareDocumentPosition)),
              (_ =
                e || K.test(f.contains)
                  ? function (t, e) {
                      var i = 9 === t.nodeType ? t.documentElement : t,
                        n = e && e.parentNode;
                      return (
                        t === n ||
                        !(
                          !n ||
                          1 !== n.nodeType ||
                          !(i.contains
                            ? i.contains(n)
                            : t.compareDocumentPosition &&
                              16 & t.compareDocumentPosition(n))
                        )
                      );
                    }
                  : function (t, e) {
                      if (e)
                        for (; (e = e.parentNode); ) if (e === t) return !0;
                      return !1;
                    }),
              (S = e
                ? function (t, e) {
                    if (t === e) return (h = !0), 0;
                    var n =
                      !t.compareDocumentPosition - !e.compareDocumentPosition;
                    return (
                      n ||
                      (1 &
                        (n =
                          (t.ownerDocument || t) === (e.ownerDocument || e)
                            ? t.compareDocumentPosition(e)
                            : 1) ||
                      (!i.sortDetached && e.compareDocumentPosition(t) === n)
                        ? t === p || (t.ownerDocument === b && _(b, t))
                          ? -1
                          : e === p || (e.ownerDocument === b && _(b, e))
                          ? 1
                          : d
                          ? P(d, t) - P(d, e)
                          : 0
                        : 4 & n
                        ? -1
                        : 1)
                    );
                  }
                : function (t, e) {
                    if (t === e) return (h = !0), 0;
                    var i,
                      n = 0,
                      s = t.parentNode,
                      o = e.parentNode,
                      r = [t],
                      a = [e];
                    if (!s || !o)
                      return t === p
                        ? -1
                        : e === p
                        ? 1
                        : s
                        ? -1
                        : o
                        ? 1
                        : d
                        ? P(d, t) - P(d, e)
                        : 0;
                    if (s === o) return ct(t, e);
                    for (i = t; (i = i.parentNode); ) r.unshift(i);
                    for (i = e; (i = i.parentNode); ) a.unshift(i);
                    for (; r[n] === a[n]; ) n++;
                    return n
                      ? ct(r[n], a[n])
                      : r[n] === b
                      ? -1
                      : a[n] === b
                      ? 1
                      : 0;
                  }),
              p)
            : p;
        }),
      (st.matches = function (t, e) {
        return st(t, null, null, e);
      }),
      (st.matchesSelector = function (t, e) {
        if (
          ((t.ownerDocument || t) !== p && u(t),
          (e = e.replace(B, "='$1']")),
          i.matchesSelector &&
            m &&
            !x[e + " "] &&
            (!v || !v.test(e)) &&
            (!g || !g.test(e)))
        )
          try {
            var n = y.call(t, e);
            if (
              n ||
              i.disconnectedMatch ||
              (t.document && 11 !== t.document.nodeType)
            )
              return n;
          } catch (t) {}
        return st(e, p, null, [t]).length > 0;
      }),
      (st.contains = function (t, e) {
        return (t.ownerDocument || t) !== p && u(t), _(t, e);
      }),
      (st.attr = function (t, e) {
        (t.ownerDocument || t) !== p && u(t);
        var s = n.attrHandle[e.toLowerCase()],
          o = s && A.call(n.attrHandle, e.toLowerCase()) ? s(t, e, !m) : void 0;
        return void 0 !== o
          ? o
          : i.attributes || !m
          ? t.getAttribute(e)
          : (o = t.getAttributeNode(e)) && o.specified
          ? o.value
          : null;
      }),
      (st.error = function (t) {
        throw new Error("Syntax error, unrecognized expression: " + t);
      }),
      (st.uniqueSort = function (t) {
        var e,
          n = [],
          s = 0,
          o = 0;
        if (
          ((h = !i.detectDuplicates),
          (d = !i.sortStable && t.slice(0)),
          t.sort(S),
          h)
        ) {
          for (; (e = t[o++]); ) e === t[o] && (s = n.push(o));
          for (; s--; ) t.splice(n[s], 1);
        }
        return (d = null), t;
      }),
      (s = st.getText =
        function (t) {
          var e,
            i = "",
            n = 0,
            o = t.nodeType;
          if (o) {
            if (1 === o || 9 === o || 11 === o) {
              if ("string" == typeof t.textContent) return t.textContent;
              for (t = t.firstChild; t; t = t.nextSibling) i += s(t);
            } else if (3 === o || 4 === o) return t.nodeValue;
          } else for (; (e = t[n++]); ) i += s(e);
          return i;
        }),
      ((n = st.selectors =
        {
          cacheLength: 50,
          createPseudo: rt,
          match: G,
          attrHandle: {},
          find: {},
          relative: {
            ">": { dir: "parentNode", first: !0 },
            " ": { dir: "parentNode" },
            "+": { dir: "previousSibling", first: !0 },
            "~": { dir: "previousSibling" },
          },
          preFilter: {
            ATTR: function (t) {
              return (
                (t[1] = t[1].replace(et, it)),
                (t[3] = (t[3] || t[4] || t[5] || "").replace(et, it)),
                "~=" === t[2] && (t[3] = " " + t[3] + " "),
                t.slice(0, 4)
              );
            },
            CHILD: function (t) {
              return (
                (t[1] = t[1].toLowerCase()),
                "nth" === t[1].slice(0, 3)
                  ? (t[3] || st.error(t[0]),
                    (t[4] = +(t[4]
                      ? t[5] + (t[6] || 1)
                      : 2 * ("even" === t[3] || "odd" === t[3]))),
                    (t[5] = +(t[7] + t[8] || "odd" === t[3])))
                  : t[3] && st.error(t[0]),
                t
              );
            },
            PSEUDO: function (t) {
              var e,
                i = !t[6] && t[2];
              return G.CHILD.test(t[0])
                ? null
                : (t[3]
                    ? (t[2] = t[4] || t[5] || "")
                    : i &&
                      q.test(i) &&
                      (e = r(i, !0)) &&
                      (e = i.indexOf(")", i.length - e) - i.length) &&
                      ((t[0] = t[0].slice(0, e)), (t[2] = i.slice(0, e))),
                  t.slice(0, 3));
            },
          },
          filter: {
            TAG: function (t) {
              var e = t.replace(et, it).toLowerCase();
              return "*" === t
                ? function () {
                    return !0;
                  }
                : function (t) {
                    return t.nodeName && t.nodeName.toLowerCase() === e;
                  };
            },
            CLASS: function (t) {
              var e = D[t + " "];
              return (
                e ||
                ((e = new RegExp("(^|" + Y + ")" + t + "(" + Y + "|$)")) &&
                  D(t, function (t) {
                    return e.test(
                      ("string" == typeof t.className && t.className) ||
                        (void 0 !== t.getAttribute &&
                          t.getAttribute("class")) ||
                        ""
                    );
                  }))
              );
            },
            ATTR: function (t, e, i) {
              return function (n) {
                var s = st.attr(n, t);
                return null == s
                  ? "!=" === e
                  : !e ||
                      ((s += ""),
                      "=" === e
                        ? s === i
                        : "!=" === e
                        ? s !== i
                        : "^=" === e
                        ? i && 0 === s.indexOf(i)
                        : "*=" === e
                        ? i && s.indexOf(i) > -1
                        : "$=" === e
                        ? i && s.slice(-i.length) === i
                        : "~=" === e
                        ? (" " + s.replace(R, " ") + " ").indexOf(i) > -1
                        : "|=" === e &&
                          (s === i || s.slice(0, i.length + 1) === i + "-"));
              };
            },
            CHILD: function (t, e, i, n, s) {
              var o = "nth" !== t.slice(0, 3),
                r = "last" !== t.slice(-4),
                a = "of-type" === e;
              return 1 === n && 0 === s
                ? function (t) {
                    return !!t.parentNode;
                  }
                : function (e, i, l) {
                    var c,
                      d,
                      h,
                      u,
                      p,
                      f,
                      m = o !== r ? "nextSibling" : "previousSibling",
                      g = e.parentNode,
                      v = a && e.nodeName.toLowerCase(),
                      y = !l && !a,
                      _ = !1;
                    if (g) {
                      if (o) {
                        for (; m; ) {
                          for (u = e; (u = u[m]); )
                            if (
                              a
                                ? u.nodeName.toLowerCase() === v
                                : 1 === u.nodeType
                            )
                              return !1;
                          f = m = "only" === t && !f && "nextSibling";
                        }
                        return !0;
                      }
                      if (((f = [r ? g.firstChild : g.lastChild]), r && y)) {
                        for (
                          _ =
                            (p =
                              (c =
                                (d =
                                  (h = (u = g)[w] || (u[w] = {}))[u.uniqueID] ||
                                  (h[u.uniqueID] = {}))[t] || [])[0] === C &&
                              c[1]) && c[2],
                            u = p && g.childNodes[p];
                          (u = (++p && u && u[m]) || (_ = p = 0) || f.pop());

                        )
                          if (1 === u.nodeType && ++_ && u === e) {
                            d[t] = [C, p, _];
                            break;
                          }
                      } else if (
                        (y &&
                          (_ = p =
                            (c =
                              (d =
                                (h = (u = e)[w] || (u[w] = {}))[u.uniqueID] ||
                                (h[u.uniqueID] = {}))[t] || [])[0] === C &&
                            c[1]),
                        !1 === _)
                      )
                        for (
                          ;
                          (u = (++p && u && u[m]) || (_ = p = 0) || f.pop()) &&
                          ((a
                            ? u.nodeName.toLowerCase() !== v
                            : 1 !== u.nodeType) ||
                            !++_ ||
                            (y &&
                              ((d =
                                (h = u[w] || (u[w] = {}))[u.uniqueID] ||
                                (h[u.uniqueID] = {}))[t] = [C, _]),
                            u !== e));

                        );
                      return (_ -= s) === n || (_ % n == 0 && _ / n >= 0);
                    }
                  };
            },
            PSEUDO: function (t, e) {
              var i,
                s =
                  n.pseudos[t] ||
                  n.setFilters[t.toLowerCase()] ||
                  st.error("unsupported pseudo: " + t);
              return s[w]
                ? s(e)
                : s.length > 1
                ? ((i = [t, t, "", e]),
                  n.setFilters.hasOwnProperty(t.toLowerCase())
                    ? rt(function (t, i) {
                        for (var n, o = s(t, e), r = o.length; r--; )
                          t[(n = P(t, o[r]))] = !(i[n] = o[r]);
                      })
                    : function (t) {
                        return s(t, 0, i);
                      })
                : s;
            },
          },
          pseudos: {
            not: rt(function (t) {
              var e = [],
                i = [],
                n = a(t.replace(z, "$1"));
              return n[w]
                ? rt(function (t, e, i, s) {
                    for (var o, r = n(t, null, s, []), a = t.length; a--; )
                      (o = r[a]) && (t[a] = !(e[a] = o));
                  })
                : function (t, s, o) {
                    return (
                      (e[0] = t), n(e, null, o, i), (e[0] = null), !i.pop()
                    );
                  };
            }),
            has: rt(function (t) {
              return function (e) {
                return st(t, e).length > 0;
              };
            }),
            contains: rt(function (t) {
              return (
                (t = t.replace(et, it)),
                function (e) {
                  return (e.textContent || e.innerText || s(e)).indexOf(t) > -1;
                }
              );
            }),
            lang: rt(function (t) {
              return (
                V.test(t || "") || st.error("unsupported lang: " + t),
                (t = t.replace(et, it).toLowerCase()),
                function (e) {
                  var i;
                  do {
                    if (
                      (i = m
                        ? e.lang
                        : e.getAttribute("xml:lang") || e.getAttribute("lang"))
                    )
                      return (
                        (i = i.toLowerCase()) === t || 0 === i.indexOf(t + "-")
                      );
                  } while ((e = e.parentNode) && 1 === e.nodeType);
                  return !1;
                }
              );
            }),
            target: function (e) {
              var i = t.location && t.location.hash;
              return i && i.slice(1) === e.id;
            },
            root: function (t) {
              return t === f;
            },
            focus: function (t) {
              return (
                t === p.activeElement &&
                (!p.hasFocus || p.hasFocus()) &&
                !!(t.type || t.href || ~t.tabIndex)
              );
            },
            enabled: function (t) {
              return !1 === t.disabled;
            },
            disabled: function (t) {
              return !0 === t.disabled;
            },
            checked: function (t) {
              var e = t.nodeName.toLowerCase();
              return (
                ("input" === e && !!t.checked) ||
                ("option" === e && !!t.selected)
              );
            },
            selected: function (t) {
              return (
                t.parentNode && t.parentNode.selectedIndex, !0 === t.selected
              );
            },
            empty: function (t) {
              for (t = t.firstChild; t; t = t.nextSibling)
                if (t.nodeType < 6) return !1;
              return !0;
            },
            parent: function (t) {
              return !n.pseudos.empty(t);
            },
            header: function (t) {
              return X.test(t.nodeName);
            },
            input: function (t) {
              return Q.test(t.nodeName);
            },
            button: function (t) {
              var e = t.nodeName.toLowerCase();
              return ("input" === e && "button" === t.type) || "button" === e;
            },
            text: function (t) {
              var e;
              return (
                "input" === t.nodeName.toLowerCase() &&
                "text" === t.type &&
                (null == (e = t.getAttribute("type")) ||
                  "text" === e.toLowerCase())
              );
            },
            first: ut(function () {
              return [0];
            }),
            last: ut(function (t, e) {
              return [e - 1];
            }),
            eq: ut(function (t, e, i) {
              return [0 > i ? i + e : i];
            }),
            even: ut(function (t, e) {
              for (var i = 0; e > i; i += 2) t.push(i);
              return t;
            }),
            odd: ut(function (t, e) {
              for (var i = 1; e > i; i += 2) t.push(i);
              return t;
            }),
            lt: ut(function (t, e, i) {
              for (var n = 0 > i ? i + e : i; --n >= 0; ) t.push(n);
              return t;
            }),
            gt: ut(function (t, e, i) {
              for (var n = 0 > i ? i + e : i; ++n < e; ) t.push(n);
              return t;
            }),
          },
        }).pseudos.nth = n.pseudos.eq),
      { radio: !0, checkbox: !0, file: !0, password: !0, image: !0 }))
        n.pseudos[e] = dt(e);
      for (e in { submit: !0, reset: !0 }) n.pseudos[e] = ht(e);
      function ft() {}
      function mt(t) {
        for (var e = 0, i = t.length, n = ""; i > e; e++) n += t[e].value;
        return n;
      }
      function gt(t, e, i) {
        var n = e.dir,
          s = i && "parentNode" === n,
          o = k++;
        return e.first
          ? function (e, i, o) {
              for (; (e = e[n]); ) if (1 === e.nodeType || s) return t(e, i, o);
            }
          : function (e, i, r) {
              var a,
                l,
                c,
                d = [C, o];
              if (r) {
                for (; (e = e[n]); )
                  if ((1 === e.nodeType || s) && t(e, i, r)) return !0;
              } else
                for (; (e = e[n]); )
                  if (1 === e.nodeType || s) {
                    if (
                      (a = (l =
                        (c = e[w] || (e[w] = {}))[e.uniqueID] ||
                        (c[e.uniqueID] = {}))[n]) &&
                      a[0] === C &&
                      a[1] === o
                    )
                      return (d[2] = a[2]);
                    if (((l[n] = d), (d[2] = t(e, i, r)))) return !0;
                  }
            };
      }
      function vt(t) {
        return t.length > 1
          ? function (e, i, n) {
              for (var s = t.length; s--; ) if (!t[s](e, i, n)) return !1;
              return !0;
            }
          : t[0];
      }
      function yt(t, e, i, n, s) {
        for (var o, r = [], a = 0, l = t.length, c = null != e; l > a; a++)
          (o = t[a]) && ((i && !i(o, n, s)) || (r.push(o), c && e.push(a)));
        return r;
      }
      function _t(t, e, i, n, s, o) {
        return (
          n && !n[w] && (n = _t(n)),
          s && !s[w] && (s = _t(s, o)),
          rt(function (o, r, a, l) {
            var c,
              d,
              h,
              u = [],
              p = [],
              f = r.length,
              m =
                o ||
                (function (t, e, i) {
                  for (var n = 0, s = e.length; s > n; n++) st(t, e[n], i);
                  return i;
                })(e || "*", a.nodeType ? [a] : a, []),
              g = !t || (!o && e) ? m : yt(m, u, t, a, l),
              v = i ? (s || (o ? t : f || n) ? [] : r) : g;
            if ((i && i(g, v, a, l), n))
              for (c = yt(v, p), n(c, [], a, l), d = c.length; d--; )
                (h = c[d]) && (v[p[d]] = !(g[p[d]] = h));
            if (o) {
              if (s || t) {
                if (s) {
                  for (c = [], d = v.length; d--; )
                    (h = v[d]) && c.push((g[d] = h));
                  s(null, (v = []), c, l);
                }
                for (d = v.length; d--; )
                  (h = v[d]) &&
                    (c = s ? P(o, h) : u[d]) > -1 &&
                    (o[c] = !(r[c] = h));
              }
            } else (v = yt(v === r ? v.splice(f, v.length) : v)), s ? s(null, r, v, l) : N.apply(r, v);
          })
        );
      }
      function wt(t) {
        for (
          var e,
            i,
            s,
            o = t.length,
            r = n.relative[t[0].type],
            a = r || n.relative[" "],
            l = r ? 1 : 0,
            d = gt(
              function (t) {
                return t === e;
              },
              a,
              !0
            ),
            h = gt(
              function (t) {
                return P(e, t) > -1;
              },
              a,
              !0
            ),
            u = [
              function (t, i, n) {
                var s =
                  (!r && (n || i !== c)) ||
                  ((e = i).nodeType ? d(t, i, n) : h(t, i, n));
                return (e = null), s;
              },
            ];
          o > l;
          l++
        )
          if ((i = n.relative[t[l].type])) u = [gt(vt(u), i)];
          else {
            if ((i = n.filter[t[l].type].apply(null, t[l].matches))[w]) {
              for (s = ++l; o > s && !n.relative[t[s].type]; s++);
              return _t(
                l > 1 && vt(u),
                l > 1 &&
                  mt(
                    t
                      .slice(0, l - 1)
                      .concat({ value: " " === t[l - 2].type ? "*" : "" })
                  ).replace(z, "$1"),
                i,
                s > l && wt(t.slice(l, s)),
                o > s && wt((t = t.slice(s))),
                o > s && mt(t)
              );
            }
            u.push(i);
          }
        return vt(u);
      }
      function bt(t, e) {
        var i = e.length > 0,
          s = t.length > 0,
          o = function (o, r, a, l, d) {
            var h,
              f,
              g,
              v = 0,
              y = "0",
              _ = o && [],
              w = [],
              b = c,
              k = o || (s && n.find.TAG("*", d)),
              D = (C += null == b ? 1 : Math.random() || 0.1),
              T = k.length;
            for (
              d && (c = r === p || r || d);
              y !== T && null != (h = k[y]);
              y++
            ) {
              if (s && h) {
                for (
                  f = 0, r || h.ownerDocument === p || (u(h), (a = !m));
                  (g = t[f++]);

                )
                  if (g(h, r || p, a)) {
                    l.push(h);
                    break;
                  }
                d && (C = D);
              }
              i && ((h = !g && h) && v--, o && _.push(h));
            }
            if (((v += y), i && y !== v)) {
              for (f = 0; (g = e[f++]); ) g(_, w, r, a);
              if (o) {
                if (v > 0) for (; y--; ) _[y] || w[y] || (w[y] = $.call(l));
                w = yt(w);
              }
              N.apply(l, w),
                d && !o && w.length > 0 && v + e.length > 1 && st.uniqueSort(l);
            }
            return d && ((C = D), (c = b)), _;
          };
        return i ? rt(o) : o;
      }
      return (
        (ft.prototype = n.filters = n.pseudos),
        (n.setFilters = new ft()),
        (r = st.tokenize =
          function (t, e) {
            var i,
              s,
              o,
              r,
              a,
              l,
              c,
              d = T[t + " "];
            if (d) return e ? 0 : d.slice(0);
            for (a = t, l = [], c = n.preFilter; a; ) {
              for (r in ((i && !(s = F.exec(a))) ||
                (s && (a = a.slice(s[0].length) || a), l.push((o = []))),
              (i = !1),
              (s = U.exec(a)) &&
                ((i = s.shift()),
                o.push({ value: i, type: s[0].replace(z, " ") }),
                (a = a.slice(i.length))),
              n.filter))
                !(s = G[r].exec(a)) ||
                  (c[r] && !(s = c[r](s))) ||
                  ((i = s.shift()),
                  o.push({ value: i, type: r, matches: s }),
                  (a = a.slice(i.length)));
              if (!i) break;
            }
            return e ? a.length : a ? st.error(t) : T(t, l).slice(0);
          }),
        (a = st.compile =
          function (t, e) {
            var i,
              n = [],
              s = [],
              o = x[t + " "];
            if (!o) {
              for (e || (e = r(t)), i = e.length; i--; )
                (o = wt(e[i]))[w] ? n.push(o) : s.push(o);
              (o = x(t, bt(s, n))).selector = t;
            }
            return o;
          }),
        (l = st.select =
          function (t, e, s, o) {
            var l,
              c,
              d,
              h,
              u,
              p = "function" == typeof t && t,
              f = !o && r((t = p.selector || t));
            if (((s = s || []), 1 === f.length)) {
              if (
                (c = f[0] = f[0].slice(0)).length > 2 &&
                "ID" === (d = c[0]).type &&
                i.getById &&
                9 === e.nodeType &&
                m &&
                n.relative[c[1].type]
              ) {
                if (
                  !(e = (n.find.ID(d.matches[0].replace(et, it), e) || [])[0])
                )
                  return s;
                p && (e = e.parentNode), (t = t.slice(c.shift().value.length));
              }
              for (
                l = G.needsContext.test(t) ? 0 : c.length;
                l-- && ((d = c[l]), !n.relative[(h = d.type)]);

              )
                if (
                  (u = n.find[h]) &&
                  (o = u(
                    d.matches[0].replace(et, it),
                    (J.test(c[0].type) && pt(e.parentNode)) || e
                  ))
                ) {
                  if ((c.splice(l, 1), !(t = o.length && mt(c))))
                    return N.apply(s, o), s;
                  break;
                }
            }
            return (
              (p || a(t, f))(
                o,
                e,
                !m,
                s,
                !e || (J.test(t) && pt(e.parentNode)) || e
              ),
              s
            );
          }),
        (i.sortStable = w.split("").sort(S).join("") === w),
        (i.detectDuplicates = !!h),
        u(),
        (i.sortDetached = at(function (t) {
          return 1 & t.compareDocumentPosition(p.createElement("div"));
        })),
        at(function (t) {
          return (
            (t.innerHTML = "<a href='#'></a>"),
            "#" === t.firstChild.getAttribute("href")
          );
        }) ||
          lt("type|href|height|width", function (t, e, i) {
            return i
              ? void 0
              : t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2);
          }),
        (i.attributes &&
          at(function (t) {
            return (
              (t.innerHTML = "<input/>"),
              t.firstChild.setAttribute("value", ""),
              "" === t.firstChild.getAttribute("value")
            );
          })) ||
          lt("value", function (t, e, i) {
            return i || "input" !== t.nodeName.toLowerCase()
              ? void 0
              : t.defaultValue;
          }),
        at(function (t) {
          return null == t.getAttribute("disabled");
        }) ||
          lt(L, function (t, e, i) {
            var n;
            return i
              ? void 0
              : !0 === t[e]
              ? e.toLowerCase()
              : (n = t.getAttributeNode(e)) && n.specified
              ? n.value
              : null;
          }),
        st
      );
    })(t);
    (p.find = _),
      (p.expr = _.selectors),
      (p.expr[":"] = p.expr.pseudos),
      (p.uniqueSort = p.unique = _.uniqueSort),
      (p.text = _.getText),
      (p.isXMLDoc = _.isXML),
      (p.contains = _.contains);
    var w = function (t, e, i) {
        for (var n = [], s = void 0 !== i; (t = t[e]) && 9 !== t.nodeType; )
          if (1 === t.nodeType) {
            if (s && p(t).is(i)) break;
            n.push(t);
          }
        return n;
      },
      b = function (t, e) {
        for (var i = []; t; t = t.nextSibling)
          1 === t.nodeType && t !== e && i.push(t);
        return i;
      },
      C = p.expr.match.needsContext,
      k = /^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,
      D = /^.[^:#\[\.,]*$/;
    function T(t, e, i) {
      if (p.isFunction(e))
        return p.grep(t, function (t, n) {
          return !!e.call(t, n, t) !== i;
        });
      if (e.nodeType)
        return p.grep(t, function (t) {
          return (t === e) !== i;
        });
      if ("string" == typeof e) {
        if (D.test(e)) return p.filter(e, t, i);
        e = p.filter(e, t);
      }
      return p.grep(t, function (t) {
        return p.inArray(t, e) > -1 !== i;
      });
    }
    (p.filter = function (t, e, i) {
      var n = e[0];
      return (
        i && (t = ":not(" + t + ")"),
        1 === e.length && 1 === n.nodeType
          ? p.find.matchesSelector(n, t)
            ? [n]
            : []
          : p.find.matches(
              t,
              p.grep(e, function (t) {
                return 1 === t.nodeType;
              })
            )
      );
    }),
      p.fn.extend({
        find: function (t) {
          var e,
            i = [],
            n = this,
            s = n.length;
          if ("string" != typeof t)
            return this.pushStack(
              p(t).filter(function () {
                for (e = 0; s > e; e++) if (p.contains(n[e], this)) return !0;
              })
            );
          for (e = 0; s > e; e++) p.find(t, n[e], i);
          return (
            ((i = this.pushStack(s > 1 ? p.unique(i) : i)).selector = this
              .selector
              ? this.selector + " " + t
              : t),
            i
          );
        },
        filter: function (t) {
          return this.pushStack(T(this, t || [], !1));
        },
        not: function (t) {
          return this.pushStack(T(this, t || [], !0));
        },
        is: function (t) {
          return !!T(
            this,
            "string" == typeof t && C.test(t) ? p(t) : t || [],
            !1
          ).length;
        },
      });
    var x,
      S = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/;
    ((p.fn.init = function (t, e, i) {
      var s, o;
      if (!t) return this;
      if (((i = i || x), "string" == typeof t)) {
        if (
          !(s =
            "<" === t.charAt(0) &&
            ">" === t.charAt(t.length - 1) &&
            t.length >= 3
              ? [null, t, null]
              : S.exec(t)) ||
          (!s[1] && e)
        )
          return !e || e.jquery
            ? (e || i).find(t)
            : this.constructor(e).find(t);
        if (s[1]) {
          if (
            ((e = e instanceof p ? e[0] : e),
            p.merge(
              this,
              p.parseHTML(s[1], e && e.nodeType ? e.ownerDocument || e : n, !0)
            ),
            k.test(s[1]) && p.isPlainObject(e))
          )
            for (s in e)
              p.isFunction(this[s]) ? this[s](e[s]) : this.attr(s, e[s]);
          return this;
        }
        if ((o = n.getElementById(s[2])) && o.parentNode) {
          if (o.id !== s[2]) return x.find(t);
          (this.length = 1), (this[0] = o);
        }
        return (this.context = n), (this.selector = t), this;
      }
      return t.nodeType
        ? ((this.context = this[0] = t), (this.length = 1), this)
        : p.isFunction(t)
        ? void 0 !== i.ready
          ? i.ready(t)
          : t(p)
        : (void 0 !== t.selector &&
            ((this.selector = t.selector), (this.context = t.context)),
          p.makeArray(t, this));
    }).prototype = p.fn),
      (x = p(n));
    var E = /^(?:parents|prev(?:Until|All))/,
      A = { children: !0, contents: !0, next: !0, prev: !0 };
    function M(t, e) {
      do {
        t = t[e];
      } while (t && 1 !== t.nodeType);
      return t;
    }
    p.fn.extend({
      has: function (t) {
        var e,
          i = p(t, this),
          n = i.length;
        return this.filter(function () {
          for (e = 0; n > e; e++) if (p.contains(this, i[e])) return !0;
        });
      },
      closest: function (t, e) {
        for (
          var i,
            n = 0,
            s = this.length,
            o = [],
            r = C.test(t) || "string" != typeof t ? p(t, e || this.context) : 0;
          s > n;
          n++
        )
          for (i = this[n]; i && i !== e; i = i.parentNode)
            if (
              i.nodeType < 11 &&
              (r
                ? r.index(i) > -1
                : 1 === i.nodeType && p.find.matchesSelector(i, t))
            ) {
              o.push(i);
              break;
            }
        return this.pushStack(o.length > 1 ? p.uniqueSort(o) : o);
      },
      index: function (t) {
        return t
          ? "string" == typeof t
            ? p.inArray(this[0], p(t))
            : p.inArray(t.jquery ? t[0] : t, this)
          : this[0] && this[0].parentNode
          ? this.first().prevAll().length
          : -1;
      },
      add: function (t, e) {
        return this.pushStack(p.uniqueSort(p.merge(this.get(), p(t, e))));
      },
      addBack: function (t) {
        return this.add(
          null == t ? this.prevObject : this.prevObject.filter(t)
        );
      },
    }),
      p.each(
        {
          parent: function (t) {
            var e = t.parentNode;
            return e && 11 !== e.nodeType ? e : null;
          },
          parents: function (t) {
            return w(t, "parentNode");
          },
          parentsUntil: function (t, e, i) {
            return w(t, "parentNode", i);
          },
          next: function (t) {
            return M(t, "nextSibling");
          },
          prev: function (t) {
            return M(t, "previousSibling");
          },
          nextAll: function (t) {
            return w(t, "nextSibling");
          },
          prevAll: function (t) {
            return w(t, "previousSibling");
          },
          nextUntil: function (t, e, i) {
            return w(t, "nextSibling", i);
          },
          prevUntil: function (t, e, i) {
            return w(t, "previousSibling", i);
          },
          siblings: function (t) {
            return b((t.parentNode || {}).firstChild, t);
          },
          children: function (t) {
            return b(t.firstChild);
          },
          contents: function (t) {
            return p.nodeName(t, "iframe")
              ? t.contentDocument || t.contentWindow.document
              : p.merge([], t.childNodes);
          },
        },
        function (t, e) {
          p.fn[t] = function (i, n) {
            var s = p.map(this, e, i);
            return (
              "Until" !== t.slice(-5) && (n = i),
              n && "string" == typeof n && (s = p.filter(n, s)),
              this.length > 1 &&
                (A[t] || (s = p.uniqueSort(s)), E.test(t) && (s = s.reverse())),
              this.pushStack(s)
            );
          };
        }
      );
    var $,
      O,
      N = /\S+/g;
    function I() {
      n.addEventListener
        ? (n.removeEventListener("DOMContentLoaded", P),
          t.removeEventListener("load", P))
        : (n.detachEvent("onreadystatechange", P), t.detachEvent("onload", P));
    }
    function P() {
      (n.addEventListener ||
        "load" === t.event.type ||
        "complete" === n.readyState) &&
        (I(), p.ready());
    }
    for (O in ((p.Callbacks = function (t) {
      t =
        "string" == typeof t
          ? (function (t) {
              var e = {};
              return (
                p.each(t.match(N) || [], function (t, i) {
                  e[i] = !0;
                }),
                e
              );
            })(t)
          : p.extend({}, t);
      var e,
        i,
        n,
        s,
        o = [],
        r = [],
        a = -1,
        l = function () {
          for (s = t.once, n = e = !0; r.length; a = -1)
            for (i = r.shift(); ++a < o.length; )
              !1 === o[a].apply(i[0], i[1]) &&
                t.stopOnFalse &&
                ((a = o.length), (i = !1));
          t.memory || (i = !1), (e = !1), s && (o = i ? [] : "");
        },
        c = {
          add: function () {
            return (
              o &&
                (i && !e && ((a = o.length - 1), r.push(i)),
                (function e(i) {
                  p.each(i, function (i, n) {
                    p.isFunction(n)
                      ? (t.unique && c.has(n)) || o.push(n)
                      : n && n.length && "string" !== p.type(n) && e(n);
                  });
                })(arguments),
                i && !e && l()),
              this
            );
          },
          remove: function () {
            return (
              p.each(arguments, function (t, e) {
                for (var i; (i = p.inArray(e, o, i)) > -1; )
                  o.splice(i, 1), a >= i && a--;
              }),
              this
            );
          },
          has: function (t) {
            return t ? p.inArray(t, o) > -1 : o.length > 0;
          },
          empty: function () {
            return o && (o = []), this;
          },
          disable: function () {
            return (s = r = []), (o = i = ""), this;
          },
          disabled: function () {
            return !o;
          },
          lock: function () {
            return (s = !0), i || c.disable(), this;
          },
          locked: function () {
            return !!s;
          },
          fireWith: function (t, i) {
            return (
              s ||
                ((i = [t, (i = i || []).slice ? i.slice() : i]),
                r.push(i),
                e || l()),
              this
            );
          },
          fire: function () {
            return c.fireWith(this, arguments), this;
          },
          fired: function () {
            return !!n;
          },
        };
      return c;
    }),
    p.extend({
      Deferred: function (t) {
        var e = [
            ["resolve", "done", p.Callbacks("once memory"), "resolved"],
            ["reject", "fail", p.Callbacks("once memory"), "rejected"],
            ["notify", "progress", p.Callbacks("memory")],
          ],
          i = "pending",
          n = {
            state: function () {
              return i;
            },
            always: function () {
              return s.done(arguments).fail(arguments), this;
            },
            then: function () {
              var t = arguments;
              return p
                .Deferred(function (i) {
                  p.each(e, function (e, o) {
                    var r = p.isFunction(t[e]) && t[e];
                    s[o[1]](function () {
                      var t = r && r.apply(this, arguments);
                      t && p.isFunction(t.promise)
                        ? t
                            .promise()
                            .progress(i.notify)
                            .done(i.resolve)
                            .fail(i.reject)
                        : i[o[0] + "With"](
                            this === n ? i.promise() : this,
                            r ? [t] : arguments
                          );
                    });
                  }),
                    (t = null);
                })
                .promise();
            },
            promise: function (t) {
              return null != t ? p.extend(t, n) : n;
            },
          },
          s = {};
        return (
          (n.pipe = n.then),
          p.each(e, function (t, o) {
            var r = o[2],
              a = o[3];
            (n[o[1]] = r.add),
              a &&
                r.add(
                  function () {
                    i = a;
                  },
                  e[1 ^ t][2].disable,
                  e[2][2].lock
                ),
              (s[o[0]] = function () {
                return s[o[0] + "With"](this === s ? n : this, arguments), this;
              }),
              (s[o[0] + "With"] = r.fireWith);
          }),
          n.promise(s),
          t && t.call(s, s),
          s
        );
      },
      when: function (t) {
        var e,
          i,
          n,
          o = 0,
          r = s.call(arguments),
          a = r.length,
          l = 1 !== a || (t && p.isFunction(t.promise)) ? a : 0,
          c = 1 === l ? t : p.Deferred(),
          d = function (t, i, n) {
            return function (o) {
              (i[t] = this),
                (n[t] = arguments.length > 1 ? s.call(arguments) : o),
                n === e ? c.notifyWith(i, n) : --l || c.resolveWith(i, n);
            };
          };
        if (a > 1)
          for (e = new Array(a), i = new Array(a), n = new Array(a); a > o; o++)
            r[o] && p.isFunction(r[o].promise)
              ? r[o]
                  .promise()
                  .progress(d(o, i, e))
                  .done(d(o, n, r))
                  .fail(c.reject)
              : --l;
        return l || c.resolveWith(n, r), c.promise();
      },
    }),
    (p.fn.ready = function (t) {
      return p.ready.promise().done(t), this;
    }),
    p.extend({
      isReady: !1,
      readyWait: 1,
      holdReady: function (t) {
        t ? p.readyWait++ : p.ready(!0);
      },
      ready: function (t) {
        (!0 === t ? --p.readyWait : p.isReady) ||
          ((p.isReady = !0),
          (!0 !== t && --p.readyWait > 0) ||
            ($.resolveWith(n, [p]),
            p.fn.triggerHandler &&
              (p(n).triggerHandler("ready"), p(n).off("ready"))));
      },
    }),
    (p.ready.promise = function (e) {
      if (!$)
        if (
          (($ = p.Deferred()),
          "complete" === n.readyState ||
            ("loading" !== n.readyState && !n.documentElement.doScroll))
        )
          t.setTimeout(p.ready);
        else if (n.addEventListener)
          n.addEventListener("DOMContentLoaded", P),
            t.addEventListener("load", P);
        else {
          n.attachEvent("onreadystatechange", P), t.attachEvent("onload", P);
          var i = !1;
          try {
            i = null == t.frameElement && n.documentElement;
          } catch (t) {}
          i &&
            i.doScroll &&
            (function e() {
              if (!p.isReady) {
                try {
                  i.doScroll("left");
                } catch (i) {
                  return t.setTimeout(e, 50);
                }
                I(), p.ready();
              }
            })();
        }
      return $.promise(e);
    }),
    p.ready.promise(),
    p(h)))
      break;
    (h.ownFirst = "0" === O),
      (h.inlineBlockNeedsLayout = !1),
      p(function () {
        var t, e, i, s;
        (i = n.getElementsByTagName("body")[0]) &&
          i.style &&
          ((e = n.createElement("div")),
          ((s = n.createElement("div")).style.cssText =
            "position:absolute;border:0;width:0;height:0;top:0;left:-9999px"),
          i.appendChild(s).appendChild(e),
          void 0 !== e.style.zoom &&
            ((e.style.cssText =
              "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1"),
            (h.inlineBlockNeedsLayout = t = 3 === e.offsetWidth),
            t && (i.style.zoom = 1)),
          i.removeChild(s));
      }),
      (function () {
        var t = n.createElement("div");
        h.deleteExpando = !0;
        try {
          delete t.test;
        } catch (t) {
          h.deleteExpando = !1;
        }
        t = null;
      })();
    var L = function (t) {
        var e = p.noData[(t.nodeName + " ").toLowerCase()],
          i = +t.nodeType || 1;
        return (
          (1 === i || 9 === i) &&
          (!e || (!0 !== e && t.getAttribute("classid") === e))
        );
      },
      Y = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
      H = /([A-Z])/g;
    function j(t, e, i) {
      if (void 0 === i && 1 === t.nodeType) {
        var n = "data-" + e.replace(H, "-$1").toLowerCase();
        if ("string" == typeof (i = t.getAttribute(n))) {
          try {
            i =
              "true" === i ||
              ("false" !== i &&
                ("null" === i
                  ? null
                  : +i + "" === i
                  ? +i
                  : Y.test(i)
                  ? p.parseJSON(i)
                  : i));
          } catch (t) {}
          p.data(t, e, i);
        } else i = void 0;
      }
      return i;
    }
    function W(t) {
      var e;
      for (e in t)
        if (("data" !== e || !p.isEmptyObject(t[e])) && "toJSON" !== e)
          return !1;
      return !0;
    }
    function R(t, e, n, s) {
      if (L(t)) {
        var o,
          r,
          a = p.expando,
          l = t.nodeType,
          c = l ? p.cache : t,
          d = l ? t[a] : t[a] && a;
        if (
          (d && c[d] && (s || c[d].data)) ||
          void 0 !== n ||
          "string" != typeof e
        )
          return (
            d || (d = l ? (t[a] = i.pop() || p.guid++) : a),
            c[d] || (c[d] = l ? {} : { toJSON: p.noop }),
            ("object" != typeof e && "function" != typeof e) ||
              (s
                ? (c[d] = p.extend(c[d], e))
                : (c[d].data = p.extend(c[d].data, e))),
            (r = c[d]),
            s || (r.data || (r.data = {}), (r = r.data)),
            void 0 !== n && (r[p.camelCase(e)] = n),
            "string" == typeof e
              ? null == (o = r[e]) && (o = r[p.camelCase(e)])
              : (o = r),
            o
          );
      }
    }
    function z(t, e, i) {
      if (L(t)) {
        var n,
          s,
          o = t.nodeType,
          r = o ? p.cache : t,
          a = o ? t[p.expando] : p.expando;
        if (r[a]) {
          if (e && (n = i ? r[a] : r[a].data)) {
            s = (e = p.isArray(e)
              ? e.concat(p.map(e, p.camelCase))
              : e in n
              ? [e]
              : (e = p.camelCase(e)) in n
              ? [e]
              : e.split(" ")).length;
            for (; s--; ) delete n[e[s]];
            if (i ? !W(n) : !p.isEmptyObject(n)) return;
          }
          (i || (delete r[a].data, W(r[a]))) &&
            (o
              ? p.cleanData([t], !0)
              : h.deleteExpando || r != r.window
              ? delete r[a]
              : (r[a] = void 0));
        }
      }
    }
    p.extend({
      cache: {},
      noData: {
        "applet ": !0,
        "embed ": !0,
        "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
      },
      hasData: function (t) {
        return (
          !!(t = t.nodeType ? p.cache[t[p.expando]] : t[p.expando]) && !W(t)
        );
      },
      data: function (t, e, i) {
        return R(t, e, i);
      },
      removeData: function (t, e) {
        return z(t, e);
      },
      _data: function (t, e, i) {
        return R(t, e, i, !0);
      },
      _removeData: function (t, e) {
        return z(t, e, !0);
      },
    }),
      p.fn.extend({
        data: function (t, e) {
          var i,
            n,
            s,
            o = this[0],
            r = o && o.attributes;
          if (void 0 === t) {
            if (
              this.length &&
              ((s = p.data(o)), 1 === o.nodeType && !p._data(o, "parsedAttrs"))
            ) {
              for (i = r.length; i--; )
                r[i] &&
                  0 === (n = r[i].name).indexOf("data-") &&
                  j(o, (n = p.camelCase(n.slice(5))), s[n]);
              p._data(o, "parsedAttrs", !0);
            }
            return s;
          }
          return "object" == typeof t
            ? this.each(function () {
                p.data(this, t);
              })
            : arguments.length > 1
            ? this.each(function () {
                p.data(this, t, e);
              })
            : o
            ? j(o, t, p.data(o, t))
            : void 0;
        },
        removeData: function (t) {
          return this.each(function () {
            p.removeData(this, t);
          });
        },
      }),
      p.extend({
        queue: function (t, e, i) {
          var n;
          return t
            ? ((e = (e || "fx") + "queue"),
              (n = p._data(t, e)),
              i &&
                (!n || p.isArray(i)
                  ? (n = p._data(t, e, p.makeArray(i)))
                  : n.push(i)),
              n || [])
            : void 0;
        },
        dequeue: function (t, e) {
          e = e || "fx";
          var i = p.queue(t, e),
            n = i.length,
            s = i.shift(),
            o = p._queueHooks(t, e);
          "inprogress" === s && ((s = i.shift()), n--),
            s &&
              ("fx" === e && i.unshift("inprogress"),
              delete o.stop,
              s.call(
                t,
                function () {
                  p.dequeue(t, e);
                },
                o
              )),
            !n && o && o.empty.fire();
        },
        _queueHooks: function (t, e) {
          var i = e + "queueHooks";
          return (
            p._data(t, i) ||
            p._data(t, i, {
              empty: p.Callbacks("once memory").add(function () {
                p._removeData(t, e + "queue"), p._removeData(t, i);
              }),
            })
          );
        },
      }),
      p.fn.extend({
        queue: function (t, e) {
          var i = 2;
          return (
            "string" != typeof t && ((e = t), (t = "fx"), i--),
            arguments.length < i
              ? p.queue(this[0], t)
              : void 0 === e
              ? this
              : this.each(function () {
                  var i = p.queue(this, t, e);
                  p._queueHooks(this, t),
                    "fx" === t && "inprogress" !== i[0] && p.dequeue(this, t);
                })
          );
        },
        dequeue: function (t) {
          return this.each(function () {
            p.dequeue(this, t);
          });
        },
        clearQueue: function (t) {
          return this.queue(t || "fx", []);
        },
        promise: function (t, e) {
          var i,
            n = 1,
            s = p.Deferred(),
            o = this,
            r = this.length,
            a = function () {
              --n || s.resolveWith(o, [o]);
            };
          for (
            "string" != typeof t && ((e = t), (t = void 0)), t = t || "fx";
            r--;

          )
            (i = p._data(o[r], t + "queueHooks")) &&
              i.empty &&
              (n++, i.empty.add(a));
          return a(), s.promise(e);
        },
      }),
      (function () {
        var t;
        h.shrinkWrapBlocks = function () {
          return null != t
            ? t
            : ((t = !1),
              (i = n.getElementsByTagName("body")[0]) && i.style
                ? ((e = n.createElement("div")),
                  ((s = n.createElement("div")).style.cssText =
                    "position:absolute;border:0;width:0;height:0;top:0;left:-9999px"),
                  i.appendChild(s).appendChild(e),
                  void 0 !== e.style.zoom &&
                    ((e.style.cssText =
                      "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1"),
                    (e.appendChild(n.createElement("div")).style.width = "5px"),
                    (t = 3 !== e.offsetWidth)),
                  i.removeChild(s),
                  t)
                : void 0);
          var e, i, s;
        };
      })();
    var F = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
      U = new RegExp("^(?:([+-])=|)(" + F + ")([a-z%]*)$", "i"),
      B = ["Top", "Right", "Bottom", "Left"],
      q = function (t, e) {
        return (
          (t = e || t),
          "none" === p.css(t, "display") || !p.contains(t.ownerDocument, t)
        );
      };
    function V(t, e, i, n) {
      var s,
        o = 1,
        r = 20,
        a = n
          ? function () {
              return n.cur();
            }
          : function () {
              return p.css(t, e, "");
            },
        l = a(),
        c = (i && i[3]) || (p.cssNumber[e] ? "" : "px"),
        d = (p.cssNumber[e] || ("px" !== c && +l)) && U.exec(p.css(t, e));
      if (d && d[3] !== c) {
        (c = c || d[3]), (i = i || []), (d = +l || 1);
        do {
          (d /= o = o || ".5"), p.style(t, e, d + c);
        } while (o !== (o = a() / l) && 1 !== o && --r);
      }
      return (
        i &&
          ((d = +d || +l || 0),
          (s = i[1] ? d + (i[1] + 1) * i[2] : +i[2]),
          n && ((n.unit = c), (n.start = d), (n.end = s))),
        s
      );
    }
    var G = function (t, e, i, n, s, o, r) {
        var a = 0,
          l = t.length,
          c = null == i;
        if ("object" === p.type(i))
          for (a in ((s = !0), i)) G(t, e, a, i[a], !0, o, r);
        else if (
          void 0 !== n &&
          ((s = !0),
          p.isFunction(n) || (r = !0),
          c &&
            (r
              ? (e.call(t, n), (e = null))
              : ((c = e),
                (e = function (t, e, i) {
                  return c.call(p(t), i);
                }))),
          e)
        )
          for (; l > a; a++) e(t[a], i, r ? n : n.call(t[a], a, e(t[a], i)));
        return s ? t : c ? e.call(t) : l ? e(t[0], i) : o;
      },
      Q = /^(?:checkbox|radio)$/i,
      X = /<([\w:-]+)/,
      K = /^$|\/(?:java|ecma)script/i,
      Z = /^\s+/,
      J =
        "abbr|article|aside|audio|bdi|canvas|data|datalist|details|dialog|figcaption|figure|footer|header|hgroup|main|mark|meter|nav|output|picture|progress|section|summary|template|time|video";
    function tt(t) {
      var e = J.split("|"),
        i = t.createDocumentFragment();
      if (i.createElement) for (; e.length; ) i.createElement(e.pop());
      return i;
    }
    !(function () {
      var t = n.createElement("div"),
        e = n.createDocumentFragment(),
        i = n.createElement("input");
      (t.innerHTML =
        "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>"),
        (h.leadingWhitespace = 3 === t.firstChild.nodeType),
        (h.tbody = !t.getElementsByTagName("tbody").length),
        (h.htmlSerialize = !!t.getElementsByTagName("link").length),
        (h.html5Clone =
          "<:nav></:nav>" !== n.createElement("nav").cloneNode(!0).outerHTML),
        (i.type = "checkbox"),
        (i.checked = !0),
        e.appendChild(i),
        (h.appendChecked = i.checked),
        (t.innerHTML = "<textarea>x</textarea>"),
        (h.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue),
        e.appendChild(t),
        (i = n.createElement("input")).setAttribute("type", "radio"),
        i.setAttribute("checked", "checked"),
        i.setAttribute("name", "t"),
        t.appendChild(i),
        (h.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked),
        (h.noCloneEvent = !!t.addEventListener),
        (t[p.expando] = 1),
        (h.attributes = !t.getAttribute(p.expando));
    })();
    var et = {
      option: [1, "<select multiple='multiple'>", "</select>"],
      legend: [1, "<fieldset>", "</fieldset>"],
      area: [1, "<map>", "</map>"],
      param: [1, "<object>", "</object>"],
      thead: [1, "<table>", "</table>"],
      tr: [2, "<table><tbody>", "</tbody></table>"],
      col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
      td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
      _default: h.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"],
    };
    function it(t, e) {
      var i,
        n,
        s = 0,
        o =
          void 0 !== t.getElementsByTagName
            ? t.getElementsByTagName(e || "*")
            : void 0 !== t.querySelectorAll
            ? t.querySelectorAll(e || "*")
            : void 0;
      if (!o)
        for (o = [], i = t.childNodes || t; null != (n = i[s]); s++)
          !e || p.nodeName(n, e) ? o.push(n) : p.merge(o, it(n, e));
      return void 0 === e || (e && p.nodeName(t, e)) ? p.merge([t], o) : o;
    }
    function nt(t, e) {
      for (var i, n = 0; null != (i = t[n]); n++)
        p._data(i, "globalEval", !e || p._data(e[n], "globalEval"));
    }
    (et.optgroup = et.option),
      (et.tbody = et.tfoot = et.colgroup = et.caption = et.thead),
      (et.th = et.td);
    var st = /<|&#?\w+;/,
      ot = /<tbody/i;
    function rt(t) {
      Q.test(t.type) && (t.defaultChecked = t.checked);
    }
    function at(t, e, i, n, s) {
      for (
        var o, r, a, l, c, d, u, f = t.length, m = tt(e), g = [], v = 0;
        f > v;
        v++
      )
        if ((r = t[v]) || 0 === r)
          if ("object" === p.type(r)) p.merge(g, r.nodeType ? [r] : r);
          else if (st.test(r)) {
            for (
              l = l || m.appendChild(e.createElement("div")),
                c = (X.exec(r) || ["", ""])[1].toLowerCase(),
                u = et[c] || et._default,
                l.innerHTML = u[1] + p.htmlPrefilter(r) + u[2],
                o = u[0];
              o--;

            )
              l = l.lastChild;
            if (
              (!h.leadingWhitespace &&
                Z.test(r) &&
                g.push(e.createTextNode(Z.exec(r)[0])),
              !h.tbody)
            )
              for (
                o =
                  (r =
                    "table" !== c || ot.test(r)
                      ? "<table>" !== u[1] || ot.test(r)
                        ? 0
                        : l
                      : l.firstChild) && r.childNodes.length;
                o--;

              )
                p.nodeName((d = r.childNodes[o]), "tbody") &&
                  !d.childNodes.length &&
                  r.removeChild(d);
            for (p.merge(g, l.childNodes), l.textContent = ""; l.firstChild; )
              l.removeChild(l.firstChild);
            l = m.lastChild;
          } else g.push(e.createTextNode(r));
      for (
        l && m.removeChild(l),
          h.appendChecked || p.grep(it(g, "input"), rt),
          v = 0;
        (r = g[v++]);

      )
        if (n && p.inArray(r, n) > -1) s && s.push(r);
        else if (
          ((a = p.contains(r.ownerDocument, r)),
          (l = it(m.appendChild(r), "script")),
          a && nt(l),
          i)
        )
          for (o = 0; (r = l[o++]); ) K.test(r.type || "") && i.push(r);
      return (l = null), m;
    }
    !(function () {
      var e,
        i,
        s = n.createElement("div");
      for (e in { submit: !0, change: !0, focusin: !0 })
        (i = "on" + e),
          (h[e] = i in t) ||
            (s.setAttribute(i, "t"), (h[e] = !1 === s.attributes[i].expando));
      s = null;
    })();
    var lt = /^(?:input|select|textarea)$/i,
      ct = /^key/,
      dt = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
      ht = /^(?:focusinfocus|focusoutblur)$/,
      ut = /^([^.]*)(?:\.(.+)|)/;
    function pt() {
      return !0;
    }
    function ft() {
      return !1;
    }
    function mt() {
      try {
        return n.activeElement;
      } catch (t) {}
    }
    function gt(t, e, i, n, s, o) {
      var r, a;
      if ("object" == typeof e) {
        for (a in ("string" != typeof i && ((n = n || i), (i = void 0)), e))
          gt(t, a, i, n, e[a], o);
        return t;
      }
      if (
        (null == n && null == s
          ? ((s = i), (n = i = void 0))
          : null == s &&
            ("string" == typeof i
              ? ((s = n), (n = void 0))
              : ((s = n), (n = i), (i = void 0))),
        !1 === s)
      )
        s = ft;
      else if (!s) return t;
      return (
        1 === o &&
          ((r = s),
          ((s = function (t) {
            return p().off(t), r.apply(this, arguments);
          }).guid = r.guid || (r.guid = p.guid++))),
        t.each(function () {
          p.event.add(this, e, s, n, i);
        })
      );
    }
    (p.event = {
      global: {},
      add: function (t, e, i, n, s) {
        var o,
          r,
          a,
          l,
          c,
          d,
          h,
          u,
          f,
          m,
          g,
          v = p._data(t);
        if (v) {
          for (
            i.handler && ((i = (l = i).handler), (s = l.selector)),
              i.guid || (i.guid = p.guid++),
              (r = v.events) || (r = v.events = {}),
              (d = v.handle) ||
                ((d = v.handle =
                  function (t) {
                    return void 0 === p || (t && p.event.triggered === t.type)
                      ? void 0
                      : p.event.dispatch.apply(d.elem, arguments);
                  }).elem = t),
              a = (e = (e || "").match(N) || [""]).length;
            a--;

          )
            (f = g = (o = ut.exec(e[a]) || [])[1]),
              (m = (o[2] || "").split(".").sort()),
              f &&
                ((c = p.event.special[f] || {}),
                (f = (s ? c.delegateType : c.bindType) || f),
                (c = p.event.special[f] || {}),
                (h = p.extend(
                  {
                    type: f,
                    origType: g,
                    data: n,
                    handler: i,
                    guid: i.guid,
                    selector: s,
                    needsContext: s && p.expr.match.needsContext.test(s),
                    namespace: m.join("."),
                  },
                  l
                )),
                (u = r[f]) ||
                  (((u = r[f] = []).delegateCount = 0),
                  (c.setup && !1 !== c.setup.call(t, n, m, d)) ||
                    (t.addEventListener
                      ? t.addEventListener(f, d, !1)
                      : t.attachEvent && t.attachEvent("on" + f, d))),
                c.add &&
                  (c.add.call(t, h),
                  h.handler.guid || (h.handler.guid = i.guid)),
                s ? u.splice(u.delegateCount++, 0, h) : u.push(h),
                (p.event.global[f] = !0));
          t = null;
        }
      },
      remove: function (t, e, i, n, s) {
        var o,
          r,
          a,
          l,
          c,
          d,
          h,
          u,
          f,
          m,
          g,
          v = p.hasData(t) && p._data(t);
        if (v && (d = v.events)) {
          for (c = (e = (e || "").match(N) || [""]).length; c--; )
            if (
              ((f = g = (a = ut.exec(e[c]) || [])[1]),
              (m = (a[2] || "").split(".").sort()),
              f)
            ) {
              for (
                h = p.event.special[f] || {},
                  u = d[(f = (n ? h.delegateType : h.bindType) || f)] || [],
                  a =
                    a[2] &&
                    new RegExp("(^|\\.)" + m.join("\\.(?:.*\\.|)") + "(\\.|$)"),
                  l = o = u.length;
                o--;

              )
                (r = u[o]),
                  (!s && g !== r.origType) ||
                    (i && i.guid !== r.guid) ||
                    (a && !a.test(r.namespace)) ||
                    (n && n !== r.selector && ("**" !== n || !r.selector)) ||
                    (u.splice(o, 1),
                    r.selector && u.delegateCount--,
                    h.remove && h.remove.call(t, r));
              l &&
                !u.length &&
                ((h.teardown && !1 !== h.teardown.call(t, m, v.handle)) ||
                  p.removeEvent(t, f, v.handle),
                delete d[f]);
            } else for (f in d) p.event.remove(t, f + e[c], i, n, !0);
          p.isEmptyObject(d) && (delete v.handle, p._removeData(t, "events"));
        }
      },
      trigger: function (e, i, s, o) {
        var r,
          a,
          l,
          c,
          h,
          u,
          f,
          m = [s || n],
          g = d.call(e, "type") ? e.type : e,
          v = d.call(e, "namespace") ? e.namespace.split(".") : [];
        if (
          ((l = u = s = s || n),
          3 !== s.nodeType &&
            8 !== s.nodeType &&
            !ht.test(g + p.event.triggered) &&
            (g.indexOf(".") > -1 &&
              ((v = g.split(".")), (g = v.shift()), v.sort()),
            (a = g.indexOf(":") < 0 && "on" + g),
            ((e = e[p.expando]
              ? e
              : new p.Event(g, "object" == typeof e && e)).isTrigger = o
              ? 2
              : 3),
            (e.namespace = v.join(".")),
            (e.rnamespace = e.namespace
              ? new RegExp("(^|\\.)" + v.join("\\.(?:.*\\.|)") + "(\\.|$)")
              : null),
            (e.result = void 0),
            e.target || (e.target = s),
            (i = null == i ? [e] : p.makeArray(i, [e])),
            (h = p.event.special[g] || {}),
            o || !h.trigger || !1 !== h.trigger.apply(s, i)))
        ) {
          if (!o && !h.noBubble && !p.isWindow(s)) {
            for (
              c = h.delegateType || g, ht.test(c + g) || (l = l.parentNode);
              l;
              l = l.parentNode
            )
              m.push(l), (u = l);
            u === (s.ownerDocument || n) &&
              m.push(u.defaultView || u.parentWindow || t);
          }
          for (f = 0; (l = m[f++]) && !e.isPropagationStopped(); )
            (e.type = f > 1 ? c : h.bindType || g),
              (r =
                (p._data(l, "events") || {})[e.type] && p._data(l, "handle")) &&
                r.apply(l, i),
              (r = a && l[a]) &&
                r.apply &&
                L(l) &&
                ((e.result = r.apply(l, i)),
                !1 === e.result && e.preventDefault());
          if (
            ((e.type = g),
            !o &&
              !e.isDefaultPrevented() &&
              (!h._default || !1 === h._default.apply(m.pop(), i)) &&
              L(s) &&
              a &&
              s[g] &&
              !p.isWindow(s))
          ) {
            (u = s[a]) && (s[a] = null), (p.event.triggered = g);
            try {
              s[g]();
            } catch (t) {}
            (p.event.triggered = void 0), u && (s[a] = u);
          }
          return e.result;
        }
      },
      dispatch: function (t) {
        t = p.event.fix(t);
        var e,
          i,
          n,
          o,
          r,
          a = [],
          l = s.call(arguments),
          c = (p._data(this, "events") || {})[t.type] || [],
          d = p.event.special[t.type] || {};
        if (
          ((l[0] = t),
          (t.delegateTarget = this),
          !d.preDispatch || !1 !== d.preDispatch.call(this, t))
        ) {
          for (
            a = p.event.handlers.call(this, t, c), e = 0;
            (o = a[e++]) && !t.isPropagationStopped();

          )
            for (
              t.currentTarget = o.elem, i = 0;
              (r = o.handlers[i++]) && !t.isImmediatePropagationStopped();

            )
              (t.rnamespace && !t.rnamespace.test(r.namespace)) ||
                ((t.handleObj = r),
                (t.data = r.data),
                void 0 !==
                  (n = (
                    (p.event.special[r.origType] || {}).handle || r.handler
                  ).apply(o.elem, l)) &&
                  !1 === (t.result = n) &&
                  (t.preventDefault(), t.stopPropagation()));
          return d.postDispatch && d.postDispatch.call(this, t), t.result;
        }
      },
      handlers: function (t, e) {
        var i,
          n,
          s,
          o,
          r = [],
          a = e.delegateCount,
          l = t.target;
        if (
          a &&
          l.nodeType &&
          ("click" !== t.type || isNaN(t.button) || t.button < 1)
        )
          for (; l != this; l = l.parentNode || this)
            if (1 === l.nodeType && (!0 !== l.disabled || "click" !== t.type)) {
              for (n = [], i = 0; a > i; i++)
                void 0 === n[(s = (o = e[i]).selector + " ")] &&
                  (n[s] = o.needsContext
                    ? p(s, this).index(l) > -1
                    : p.find(s, this, null, [l]).length),
                  n[s] && n.push(o);
              n.length && r.push({ elem: l, handlers: n });
            }
        return a < e.length && r.push({ elem: this, handlers: e.slice(a) }), r;
      },
      fix: function (t) {
        if (t[p.expando]) return t;
        var e,
          i,
          s,
          o = t.type,
          r = t,
          a = this.fixHooks[o];
        for (
          a ||
            (this.fixHooks[o] = a =
              dt.test(o) ? this.mouseHooks : ct.test(o) ? this.keyHooks : {}),
            s = a.props ? this.props.concat(a.props) : this.props,
            t = new p.Event(r),
            e = s.length;
          e--;

        )
          t[(i = s[e])] = r[i];
        return (
          t.target || (t.target = r.srcElement || n),
          3 === t.target.nodeType && (t.target = t.target.parentNode),
          (t.metaKey = !!t.metaKey),
          a.filter ? a.filter(t, r) : t
        );
      },
      props:
        "altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(
          " "
        ),
      fixHooks: {},
      keyHooks: {
        props: "char charCode key keyCode".split(" "),
        filter: function (t, e) {
          return (
            null == t.which &&
              (t.which = null != e.charCode ? e.charCode : e.keyCode),
            t
          );
        },
      },
      mouseHooks: {
        props:
          "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(
            " "
          ),
        filter: function (t, e) {
          var i,
            s,
            o,
            r = e.button,
            a = e.fromElement;
          return (
            null == t.pageX &&
              null != e.clientX &&
              ((o = (s = t.target.ownerDocument || n).documentElement),
              (i = s.body),
              (t.pageX =
                e.clientX +
                ((o && o.scrollLeft) || (i && i.scrollLeft) || 0) -
                ((o && o.clientLeft) || (i && i.clientLeft) || 0)),
              (t.pageY =
                e.clientY +
                ((o && o.scrollTop) || (i && i.scrollTop) || 0) -
                ((o && o.clientTop) || (i && i.clientTop) || 0))),
            !t.relatedTarget &&
              a &&
              (t.relatedTarget = a === t.target ? e.toElement : a),
            t.which ||
              void 0 === r ||
              (t.which = 1 & r ? 1 : 2 & r ? 3 : 4 & r ? 2 : 0),
            t
          );
        },
      },
      special: {
        load: { noBubble: !0 },
        focus: {
          trigger: function () {
            if (this !== mt() && this.focus)
              try {
                return this.focus(), !1;
              } catch (t) {}
          },
          delegateType: "focusin",
        },
        blur: {
          trigger: function () {
            return this === mt() && this.blur ? (this.blur(), !1) : void 0;
          },
          delegateType: "focusout",
        },
        click: {
          trigger: function () {
            return p.nodeName(this, "input") &&
              "checkbox" === this.type &&
              this.click
              ? (this.click(), !1)
              : void 0;
          },
          _default: function (t) {
            return p.nodeName(t.target, "a");
          },
        },
        beforeunload: {
          postDispatch: function (t) {
            void 0 !== t.result &&
              t.originalEvent &&
              (t.originalEvent.returnValue = t.result);
          },
        },
      },
      simulate: function (t, e, i) {
        var n = p.extend(new p.Event(), i, { type: t, isSimulated: !0 });
        p.event.trigger(n, null, e),
          n.isDefaultPrevented() && i.preventDefault();
      },
    }),
      (p.removeEvent = n.removeEventListener
        ? function (t, e, i) {
            t.removeEventListener && t.removeEventListener(e, i);
          }
        : function (t, e, i) {
            var n = "on" + e;
            t.detachEvent &&
              (void 0 === t[n] && (t[n] = null), t.detachEvent(n, i));
          }),
      (p.Event = function (t, e) {
        return this instanceof p.Event
          ? (t && t.type
              ? ((this.originalEvent = t),
                (this.type = t.type),
                (this.isDefaultPrevented =
                  t.defaultPrevented ||
                  (void 0 === t.defaultPrevented && !1 === t.returnValue)
                    ? pt
                    : ft))
              : (this.type = t),
            e && p.extend(this, e),
            (this.timeStamp = (t && t.timeStamp) || p.now()),
            void (this[p.expando] = !0))
          : new p.Event(t, e);
      }),
      (p.Event.prototype = {
        constructor: p.Event,
        isDefaultPrevented: ft,
        isPropagationStopped: ft,
        isImmediatePropagationStopped: ft,
        preventDefault: function () {
          var t = this.originalEvent;
          (this.isDefaultPrevented = pt),
            t && (t.preventDefault ? t.preventDefault() : (t.returnValue = !1));
        },
        stopPropagation: function () {
          var t = this.originalEvent;
          (this.isPropagationStopped = pt),
            t &&
              !this.isSimulated &&
              (t.stopPropagation && t.stopPropagation(), (t.cancelBubble = !0));
        },
        stopImmediatePropagation: function () {
          var t = this.originalEvent;
          (this.isImmediatePropagationStopped = pt),
            t && t.stopImmediatePropagation && t.stopImmediatePropagation(),
            this.stopPropagation();
        },
      }),
      p.each(
        {
          mouseenter: "mouseover",
          mouseleave: "mouseout",
          pointerenter: "pointerover",
          pointerleave: "pointerout",
        },
        function (t, e) {
          p.event.special[t] = {
            delegateType: e,
            bindType: e,
            handle: function (t) {
              var i,
                n = t.relatedTarget,
                s = t.handleObj;
              return (
                (n && (n === this || p.contains(this, n))) ||
                  ((t.type = s.origType),
                  (i = s.handler.apply(this, arguments)),
                  (t.type = e)),
                i
              );
            },
          };
        }
      ),
      h.submit ||
        (p.event.special.submit = {
          setup: function () {
            return (
              !p.nodeName(this, "form") &&
              void p.event.add(
                this,
                "click._submit keypress._submit",
                function (t) {
                  var e = t.target,
                    i =
                      p.nodeName(e, "input") || p.nodeName(e, "button")
                        ? p.prop(e, "form")
                        : void 0;
                  i &&
                    !p._data(i, "submit") &&
                    (p.event.add(i, "submit._submit", function (t) {
                      t._submitBubble = !0;
                    }),
                    p._data(i, "submit", !0));
                }
              )
            );
          },
          postDispatch: function (t) {
            t._submitBubble &&
              (delete t._submitBubble,
              this.parentNode &&
                !t.isTrigger &&
                p.event.simulate("submit", this.parentNode, t));
          },
          teardown: function () {
            return (
              !p.nodeName(this, "form") && void p.event.remove(this, "._submit")
            );
          },
        }),
      h.change ||
        (p.event.special.change = {
          setup: function () {
            return lt.test(this.nodeName)
              ? (("checkbox" !== this.type && "radio" !== this.type) ||
                  (p.event.add(this, "propertychange._change", function (t) {
                    "checked" === t.originalEvent.propertyName &&
                      (this._justChanged = !0);
                  }),
                  p.event.add(this, "click._change", function (t) {
                    this._justChanged &&
                      !t.isTrigger &&
                      (this._justChanged = !1),
                      p.event.simulate("change", this, t);
                  })),
                !1)
              : void p.event.add(this, "beforeactivate._change", function (t) {
                  var e = t.target;
                  lt.test(e.nodeName) &&
                    !p._data(e, "change") &&
                    (p.event.add(e, "change._change", function (t) {
                      !this.parentNode ||
                        t.isSimulated ||
                        t.isTrigger ||
                        p.event.simulate("change", this.parentNode, t);
                    }),
                    p._data(e, "change", !0));
                });
          },
          handle: function (t) {
            var e = t.target;
            return this !== e ||
              t.isSimulated ||
              t.isTrigger ||
              ("radio" !== e.type && "checkbox" !== e.type)
              ? t.handleObj.handler.apply(this, arguments)
              : void 0;
          },
          teardown: function () {
            return p.event.remove(this, "._change"), !lt.test(this.nodeName);
          },
        }),
      h.focusin ||
        p.each({ focus: "focusin", blur: "focusout" }, function (t, e) {
          var i = function (t) {
            p.event.simulate(e, t.target, p.event.fix(t));
          };
          p.event.special[e] = {
            setup: function () {
              var n = this.ownerDocument || this,
                s = p._data(n, e);
              s || n.addEventListener(t, i, !0), p._data(n, e, (s || 0) + 1);
            },
            teardown: function () {
              var n = this.ownerDocument || this,
                s = p._data(n, e) - 1;
              s
                ? p._data(n, e, s)
                : (n.removeEventListener(t, i, !0), p._removeData(n, e));
            },
          };
        }),
      p.fn.extend({
        on: function (t, e, i, n) {
          return gt(this, t, e, i, n);
        },
        one: function (t, e, i, n) {
          return gt(this, t, e, i, n, 1);
        },
        off: function (t, e, i) {
          var n, s;
          if (t && t.preventDefault && t.handleObj)
            return (
              (n = t.handleObj),
              p(t.delegateTarget).off(
                n.namespace ? n.origType + "." + n.namespace : n.origType,
                n.selector,
                n.handler
              ),
              this
            );
          if ("object" == typeof t) {
            for (s in t) this.off(s, e, t[s]);
            return this;
          }
          return (
            (!1 !== e && "function" != typeof e) || ((i = e), (e = void 0)),
            !1 === i && (i = ft),
            this.each(function () {
              p.event.remove(this, t, i, e);
            })
          );
        },
        trigger: function (t, e) {
          return this.each(function () {
            p.event.trigger(t, e, this);
          });
        },
        triggerHandler: function (t, e) {
          var i = this[0];
          return i ? p.event.trigger(t, e, i, !0) : void 0;
        },
      });
    var vt = / jQuery\d+="(?:null|\d+)"/g,
      yt = new RegExp("<(?:" + J + ")[\\s/>]", "i"),
      _t =
        /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,
      wt = /<script|<style|<link/i,
      bt = /checked\s*(?:[^=]|=\s*.checked.)/i,
      Ct = /^true\/(.*)/,
      kt = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
      Dt = tt(n).appendChild(n.createElement("div"));
    function Tt(t, e) {
      return p.nodeName(t, "table") &&
        p.nodeName(11 !== e.nodeType ? e : e.firstChild, "tr")
        ? t.getElementsByTagName("tbody")[0] ||
            t.appendChild(t.ownerDocument.createElement("tbody"))
        : t;
    }
    function xt(t) {
      return (t.type = (null !== p.find.attr(t, "type")) + "/" + t.type), t;
    }
    function St(t) {
      var e = Ct.exec(t.type);
      return e ? (t.type = e[1]) : t.removeAttribute("type"), t;
    }
    function Et(t, e) {
      if (1 === e.nodeType && p.hasData(t)) {
        var i,
          n,
          s,
          o = p._data(t),
          r = p._data(e, o),
          a = o.events;
        if (a)
          for (i in (delete r.handle, (r.events = {}), a))
            for (n = 0, s = a[i].length; s > n; n++) p.event.add(e, i, a[i][n]);
        r.data && (r.data = p.extend({}, r.data));
      }
    }
    function At(t, e) {
      var i, n, s;
      if (1 === e.nodeType) {
        if (((i = e.nodeName.toLowerCase()), !h.noCloneEvent && e[p.expando])) {
          for (n in (s = p._data(e)).events) p.removeEvent(e, n, s.handle);
          e.removeAttribute(p.expando);
        }
        "script" === i && e.text !== t.text
          ? ((xt(e).text = t.text), St(e))
          : "object" === i
          ? (e.parentNode && (e.outerHTML = t.outerHTML),
            h.html5Clone &&
              t.innerHTML &&
              !p.trim(e.innerHTML) &&
              (e.innerHTML = t.innerHTML))
          : "input" === i && Q.test(t.type)
          ? ((e.defaultChecked = e.checked = t.checked),
            e.value !== t.value && (e.value = t.value))
          : "option" === i
          ? (e.defaultSelected = e.selected = t.defaultSelected)
          : ("input" !== i && "textarea" !== i) ||
            (e.defaultValue = t.defaultValue);
      }
    }
    function Mt(t, e, i, n) {
      e = o.apply([], e);
      var s,
        r,
        a,
        l,
        c,
        d,
        u = 0,
        f = t.length,
        m = f - 1,
        g = e[0],
        v = p.isFunction(g);
      if (v || (f > 1 && "string" == typeof g && !h.checkClone && bt.test(g)))
        return t.each(function (s) {
          var o = t.eq(s);
          v && (e[0] = g.call(this, s, o.html())), Mt(o, e, i, n);
        });
      if (
        f &&
        ((s = (d = at(e, t[0].ownerDocument, !1, t, n)).firstChild),
        1 === d.childNodes.length && (d = s),
        s || n)
      ) {
        for (a = (l = p.map(it(d, "script"), xt)).length; f > u; u++)
          (r = d),
            u !== m &&
              ((r = p.clone(r, !0, !0)), a && p.merge(l, it(r, "script"))),
            i.call(t[u], r, u);
        if (a)
          for (
            c = l[l.length - 1].ownerDocument, p.map(l, St), u = 0;
            a > u;
            u++
          )
            (r = l[u]),
              K.test(r.type || "") &&
                !p._data(r, "globalEval") &&
                p.contains(c, r) &&
                (r.src
                  ? p._evalUrl && p._evalUrl(r.src)
                  : p.globalEval(
                      (r.text || r.textContent || r.innerHTML || "").replace(
                        kt,
                        ""
                      )
                    ));
        d = s = null;
      }
      return t;
    }
    function $t(t, e, i) {
      for (var n, s = e ? p.filter(e, t) : t, o = 0; null != (n = s[o]); o++)
        i || 1 !== n.nodeType || p.cleanData(it(n)),
          n.parentNode &&
            (i && p.contains(n.ownerDocument, n) && nt(it(n, "script")),
            n.parentNode.removeChild(n));
      return t;
    }
    p.extend({
      htmlPrefilter: function (t) {
        return t.replace(_t, "<$1></$2>");
      },
      clone: function (t, e, i) {
        var n,
          s,
          o,
          r,
          a,
          l = p.contains(t.ownerDocument, t);
        if (
          (h.html5Clone || p.isXMLDoc(t) || !yt.test("<" + t.nodeName + ">")
            ? (o = t.cloneNode(!0))
            : ((Dt.innerHTML = t.outerHTML),
              Dt.removeChild((o = Dt.firstChild))),
          !(
            (h.noCloneEvent && h.noCloneChecked) ||
            (1 !== t.nodeType && 11 !== t.nodeType) ||
            p.isXMLDoc(t)
          ))
        )
          for (n = it(o), a = it(t), r = 0; null != (s = a[r]); ++r)
            n[r] && At(s, n[r]);
        if (e)
          if (i)
            for (a = a || it(t), n = n || it(o), r = 0; null != (s = a[r]); r++)
              Et(s, n[r]);
          else Et(t, o);
        return (
          (n = it(o, "script")).length > 0 && nt(n, !l && it(t, "script")),
          (n = a = s = null),
          o
        );
      },
      cleanData: function (t, e) {
        for (
          var n,
            s,
            o,
            r,
            a = 0,
            l = p.expando,
            c = p.cache,
            d = h.attributes,
            u = p.event.special;
          null != (n = t[a]);
          a++
        )
          if ((e || L(n)) && (r = (o = n[l]) && c[o])) {
            if (r.events)
              for (s in r.events)
                u[s] ? p.event.remove(n, s) : p.removeEvent(n, s, r.handle);
            c[o] &&
              (delete c[o],
              d || void 0 === n.removeAttribute
                ? (n[l] = void 0)
                : n.removeAttribute(l),
              i.push(o));
          }
      },
    }),
      p.fn.extend({
        domManip: Mt,
        detach: function (t) {
          return $t(this, t, !0);
        },
        remove: function (t) {
          return $t(this, t);
        },
        text: function (t) {
          return G(
            this,
            function (t) {
              return void 0 === t
                ? p.text(this)
                : this.empty().append(
                    ((this[0] && this[0].ownerDocument) || n).createTextNode(t)
                  );
            },
            null,
            t,
            arguments.length
          );
        },
        append: function () {
          return Mt(this, arguments, function (t) {
            (1 !== this.nodeType &&
              11 !== this.nodeType &&
              9 !== this.nodeType) ||
              Tt(this, t).appendChild(t);
          });
        },
        prepend: function () {
          return Mt(this, arguments, function (t) {
            if (
              1 === this.nodeType ||
              11 === this.nodeType ||
              9 === this.nodeType
            ) {
              var e = Tt(this, t);
              e.insertBefore(t, e.firstChild);
            }
          });
        },
        before: function () {
          return Mt(this, arguments, function (t) {
            this.parentNode && this.parentNode.insertBefore(t, this);
          });
        },
        after: function () {
          return Mt(this, arguments, function (t) {
            this.parentNode &&
              this.parentNode.insertBefore(t, this.nextSibling);
          });
        },
        empty: function () {
          for (var t, e = 0; null != (t = this[e]); e++) {
            for (1 === t.nodeType && p.cleanData(it(t, !1)); t.firstChild; )
              t.removeChild(t.firstChild);
            t.options && p.nodeName(t, "select") && (t.options.length = 0);
          }
          return this;
        },
        clone: function (t, e) {
          return (
            (t = null != t && t),
            (e = null == e ? t : e),
            this.map(function () {
              return p.clone(this, t, e);
            })
          );
        },
        html: function (t) {
          return G(
            this,
            function (t) {
              var e = this[0] || {},
                i = 0,
                n = this.length;
              if (void 0 === t)
                return 1 === e.nodeType ? e.innerHTML.replace(vt, "") : void 0;
              if (
                "string" == typeof t &&
                !wt.test(t) &&
                (h.htmlSerialize || !yt.test(t)) &&
                (h.leadingWhitespace || !Z.test(t)) &&
                !et[(X.exec(t) || ["", ""])[1].toLowerCase()]
              ) {
                t = p.htmlPrefilter(t);
                try {
                  for (; n > i; i++)
                    1 === (e = this[i] || {}).nodeType &&
                      (p.cleanData(it(e, !1)), (e.innerHTML = t));
                  e = 0;
                } catch (t) {}
              }
              e && this.empty().append(t);
            },
            null,
            t,
            arguments.length
          );
        },
        replaceWith: function () {
          var t = [];
          return Mt(
            this,
            arguments,
            function (e) {
              var i = this.parentNode;
              p.inArray(this, t) < 0 &&
                (p.cleanData(it(this)), i && i.replaceChild(e, this));
            },
            t
          );
        },
      }),
      p.each(
        {
          appendTo: "append",
          prependTo: "prepend",
          insertBefore: "before",
          insertAfter: "after",
          replaceAll: "replaceWith",
        },
        function (t, e) {
          p.fn[t] = function (t) {
            for (var i, n = 0, s = [], o = p(t), a = o.length - 1; a >= n; n++)
              (i = n === a ? this : this.clone(!0)),
                p(o[n])[e](i),
                r.apply(s, i.get());
            return this.pushStack(s);
          };
        }
      );
    var Ot,
      Nt = { HTML: "block", BODY: "block" };
    function It(t, e) {
      var i = p(e.createElement(t)).appendTo(e.body),
        n = p.css(i[0], "display");
      return i.detach(), n;
    }
    function Pt(t) {
      var e = n,
        i = Nt[t];
      return (
        i ||
          (("none" !== (i = It(t, e)) && i) ||
            ((e = (
              (Ot = (
                Ot || p("<iframe frameborder='0' width='0' height='0'/>")
              ).appendTo(e.documentElement))[0].contentWindow ||
              Ot[0].contentDocument
            ).document).write(),
            e.close(),
            (i = It(t, e)),
            Ot.detach()),
          (Nt[t] = i)),
        i
      );
    }
    var Lt = /^margin/,
      Yt = new RegExp("^(" + F + ")(?!px)[a-z%]+$", "i"),
      Ht = function (t, e, i, n) {
        var s,
          o,
          r = {};
        for (o in e) (r[o] = t.style[o]), (t.style[o] = e[o]);
        for (o in ((s = i.apply(t, n || [])), e)) t.style[o] = r[o];
        return s;
      },
      jt = n.documentElement;
    !(function () {
      var e,
        i,
        s,
        o,
        r,
        a,
        l = n.createElement("div"),
        c = n.createElement("div");
      if (c.style) {
        function d() {
          var d,
            h,
            u = n.documentElement;
          u.appendChild(l),
            (c.style.cssText =
              "-webkit-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%"),
            (e = s = a = !1),
            (i = r = !0),
            t.getComputedStyle &&
              ((h = t.getComputedStyle(c)),
              (e = "1%" !== (h || {}).top),
              (a = "2px" === (h || {}).marginLeft),
              (s = "4px" === (h || { width: "4px" }).width),
              (c.style.marginRight = "50%"),
              (i = "4px" === (h || { marginRight: "4px" }).marginRight),
              ((d = c.appendChild(n.createElement("div"))).style.cssText =
                c.style.cssText =
                  "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0"),
              (d.style.marginRight = d.style.width = "0"),
              (c.style.width = "1px"),
              (r = !parseFloat((t.getComputedStyle(d) || {}).marginRight)),
              c.removeChild(d)),
            (c.style.display = "none"),
            (o = 0 === c.getClientRects().length) &&
              ((c.style.display = ""),
              (c.innerHTML = "<table><tr><td></td><td>t</td></tr></table>"),
              (c.childNodes[0].style.borderCollapse = "separate"),
              ((d = c.getElementsByTagName("td"))[0].style.cssText =
                "margin:0;border:0;padding:0;display:none"),
              (o = 0 === d[0].offsetHeight) &&
                ((d[0].style.display = ""),
                (d[1].style.display = "none"),
                (o = 0 === d[0].offsetHeight))),
            u.removeChild(l);
        }
        (c.style.cssText = "float:left;opacity:.5"),
          (h.opacity = "0.5" === c.style.opacity),
          (h.cssFloat = !!c.style.cssFloat),
          (c.style.backgroundClip = "content-box"),
          (c.cloneNode(!0).style.backgroundClip = ""),
          (h.clearCloneStyle = "content-box" === c.style.backgroundClip),
          ((l = n.createElement("div")).style.cssText =
            "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute"),
          (c.innerHTML = ""),
          l.appendChild(c),
          (h.boxSizing =
            "" === c.style.boxSizing ||
            "" === c.style.MozBoxSizing ||
            "" === c.style.WebkitBoxSizing),
          p.extend(h, {
            reliableHiddenOffsets: function () {
              return null == e && d(), o;
            },
            boxSizingReliable: function () {
              return null == e && d(), s;
            },
            pixelMarginRight: function () {
              return null == e && d(), i;
            },
            pixelPosition: function () {
              return null == e && d(), e;
            },
            reliableMarginRight: function () {
              return null == e && d(), r;
            },
            reliableMarginLeft: function () {
              return null == e && d(), a;
            },
          });
      }
    })();
    var Wt,
      Rt,
      zt = /^(top|right|bottom|left)$/;
    function Ft(t, e) {
      return {
        get: function () {
          return t()
            ? void delete this.get
            : (this.get = e).apply(this, arguments);
        },
      };
    }
    t.getComputedStyle
      ? ((Wt = function (e) {
          var i = e.ownerDocument.defaultView;
          return (i && i.opener) || (i = t), i.getComputedStyle(e);
        }),
        (Rt = function (t, e, i) {
          var n,
            s,
            o,
            r,
            a = t.style;
          return (
            ("" !==
              (r = (i = i || Wt(t)) ? i.getPropertyValue(e) || i[e] : void 0) &&
              void 0 !== r) ||
              p.contains(t.ownerDocument, t) ||
              (r = p.style(t, e)),
            i &&
              !h.pixelMarginRight() &&
              Yt.test(r) &&
              Lt.test(e) &&
              ((n = a.width),
              (s = a.minWidth),
              (o = a.maxWidth),
              (a.minWidth = a.maxWidth = a.width = r),
              (r = i.width),
              (a.width = n),
              (a.minWidth = s),
              (a.maxWidth = o)),
            void 0 === r ? r : r + ""
          );
        }))
      : jt.currentStyle &&
        ((Wt = function (t) {
          return t.currentStyle;
        }),
        (Rt = function (t, e, i) {
          var n,
            s,
            o,
            r,
            a = t.style;
          return (
            null == (r = (i = i || Wt(t)) ? i[e] : void 0) &&
              a &&
              a[e] &&
              (r = a[e]),
            Yt.test(r) &&
              !zt.test(e) &&
              ((n = a.left),
              (o = (s = t.runtimeStyle) && s.left) &&
                (s.left = t.currentStyle.left),
              (a.left = "fontSize" === e ? "1em" : r),
              (r = a.pixelLeft + "px"),
              (a.left = n),
              o && (s.left = o)),
            void 0 === r ? r : r + "" || "auto"
          );
        }));
    var Ut = /alpha\([^)]*\)/i,
      Bt = /opacity\s*=\s*([^)]*)/i,
      qt = /^(none|table(?!-c[ea]).+)/,
      Vt = new RegExp("^(" + F + ")(.*)$", "i"),
      Gt = { position: "absolute", visibility: "hidden", display: "block" },
      Qt = { letterSpacing: "0", fontWeight: "400" },
      Xt = ["Webkit", "O", "Moz", "ms"],
      Kt = n.createElement("div").style;
    function Zt(t) {
      if (t in Kt) return t;
      for (var e = t.charAt(0).toUpperCase() + t.slice(1), i = Xt.length; i--; )
        if ((t = Xt[i] + e) in Kt) return t;
    }
    function Jt(t, e) {
      for (var i, n, s, o = [], r = 0, a = t.length; a > r; r++)
        (n = t[r]).style &&
          ((o[r] = p._data(n, "olddisplay")),
          (i = n.style.display),
          e
            ? (o[r] || "none" !== i || (n.style.display = ""),
              "" === n.style.display &&
                q(n) &&
                (o[r] = p._data(n, "olddisplay", Pt(n.nodeName))))
            : ((s = q(n)),
              ((i && "none" !== i) || !s) &&
                p._data(n, "olddisplay", s ? i : p.css(n, "display"))));
      for (r = 0; a > r; r++)
        (n = t[r]).style &&
          ((e && "none" !== n.style.display && "" !== n.style.display) ||
            (n.style.display = e ? o[r] || "" : "none"));
      return t;
    }
    function te(t, e, i) {
      var n = Vt.exec(e);
      return n ? Math.max(0, n[1] - (i || 0)) + (n[2] || "px") : e;
    }
    function ee(t, e, i, n, s) {
      for (
        var o = i === (n ? "border" : "content") ? 4 : "width" === e ? 1 : 0,
          r = 0;
        4 > o;
        o += 2
      )
        "margin" === i && (r += p.css(t, i + B[o], !0, s)),
          n
            ? ("content" === i && (r -= p.css(t, "padding" + B[o], !0, s)),
              "margin" !== i &&
                (r -= p.css(t, "border" + B[o] + "Width", !0, s)))
            : ((r += p.css(t, "padding" + B[o], !0, s)),
              "padding" !== i &&
                (r += p.css(t, "border" + B[o] + "Width", !0, s)));
      return r;
    }
    function ie(t, e, i) {
      var n = !0,
        s = "width" === e ? t.offsetWidth : t.offsetHeight,
        o = Wt(t),
        r = h.boxSizing && "border-box" === p.css(t, "boxSizing", !1, o);
      if (0 >= s || null == s) {
        if (
          ((0 > (s = Rt(t, e, o)) || null == s) && (s = t.style[e]), Yt.test(s))
        )
          return s;
        (n = r && (h.boxSizingReliable() || s === t.style[e])),
          (s = parseFloat(s) || 0);
      }
      return s + ee(t, e, i || (r ? "border" : "content"), n, o) + "px";
    }
    function ne(t, e, i, n, s) {
      return new ne.prototype.init(t, e, i, n, s);
    }
    p.extend({
      cssHooks: {
        opacity: {
          get: function (t, e) {
            if (e) {
              var i = Rt(t, "opacity");
              return "" === i ? "1" : i;
            }
          },
        },
      },
      cssNumber: {
        animationIterationCount: !0,
        columnCount: !0,
        fillOpacity: !0,
        flexGrow: !0,
        flexShrink: !0,
        fontWeight: !0,
        lineHeight: !0,
        opacity: !0,
        order: !0,
        orphans: !0,
        widows: !0,
        zIndex: !0,
        zoom: !0,
      },
      cssProps: { float: h.cssFloat ? "cssFloat" : "styleFloat" },
      style: function (t, e, i, n) {
        if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
          var s,
            o,
            r,
            a = p.camelCase(e),
            l = t.style;
          if (
            ((e = p.cssProps[a] || (p.cssProps[a] = Zt(a) || a)),
            (r = p.cssHooks[e] || p.cssHooks[a]),
            void 0 === i)
          )
            return r && "get" in r && void 0 !== (s = r.get(t, !1, n))
              ? s
              : l[e];
          if (
            ("string" == (o = typeof i) &&
              (s = U.exec(i)) &&
              s[1] &&
              ((i = V(t, e, s)), (o = "number")),
            null != i &&
              i == i &&
              ("number" === o &&
                (i += (s && s[3]) || (p.cssNumber[a] ? "" : "px")),
              h.clearCloneStyle ||
                "" !== i ||
                0 !== e.indexOf("background") ||
                (l[e] = "inherit"),
              !(r && "set" in r && void 0 === (i = r.set(t, i, n)))))
          )
            try {
              l[e] = i;
            } catch (t) {}
        }
      },
      css: function (t, e, i, n) {
        var s,
          o,
          r,
          a = p.camelCase(e);
        return (
          (e = p.cssProps[a] || (p.cssProps[a] = Zt(a) || a)),
          (r = p.cssHooks[e] || p.cssHooks[a]) &&
            "get" in r &&
            (o = r.get(t, !0, i)),
          void 0 === o && (o = Rt(t, e, n)),
          "normal" === o && e in Qt && (o = Qt[e]),
          "" === i || i
            ? ((s = parseFloat(o)), !0 === i || isFinite(s) ? s || 0 : o)
            : o
        );
      },
    }),
      p.each(["height", "width"], function (t, e) {
        p.cssHooks[e] = {
          get: function (t, i, n) {
            return i
              ? qt.test(p.css(t, "display")) && 0 === t.offsetWidth
                ? Ht(t, Gt, function () {
                    return ie(t, e, n);
                  })
                : ie(t, e, n)
              : void 0;
          },
          set: function (t, i, n) {
            var s = n && Wt(t);
            return te(
              0,
              i,
              n
                ? ee(
                    t,
                    e,
                    n,
                    h.boxSizing &&
                      "border-box" === p.css(t, "boxSizing", !1, s),
                    s
                  )
                : 0
            );
          },
        };
      }),
      h.opacity ||
        (p.cssHooks.opacity = {
          get: function (t, e) {
            return Bt.test(
              (e && t.currentStyle ? t.currentStyle.filter : t.style.filter) ||
                ""
            )
              ? 0.01 * parseFloat(RegExp.$1) + ""
              : e
              ? "1"
              : "";
          },
          set: function (t, e) {
            var i = t.style,
              n = t.currentStyle,
              s = p.isNumeric(e) ? "alpha(opacity=" + 100 * e + ")" : "",
              o = (n && n.filter) || i.filter || "";
            (i.zoom = 1),
              ((e >= 1 || "" === e) &&
                "" === p.trim(o.replace(Ut, "")) &&
                i.removeAttribute &&
                (i.removeAttribute("filter"), "" === e || (n && !n.filter))) ||
                (i.filter = Ut.test(o) ? o.replace(Ut, s) : o + " " + s);
          },
        }),
      (p.cssHooks.marginRight = Ft(h.reliableMarginRight, function (t, e) {
        return e
          ? Ht(t, { display: "inline-block" }, Rt, [t, "marginRight"])
          : void 0;
      })),
      (p.cssHooks.marginLeft = Ft(h.reliableMarginLeft, function (t, e) {
        return e
          ? (parseFloat(Rt(t, "marginLeft")) ||
              (p.contains(t.ownerDocument, t)
                ? t.getBoundingClientRect().left -
                  Ht(t, { marginLeft: 0 }, function () {
                    return t.getBoundingClientRect().left;
                  })
                : 0)) + "px"
          : void 0;
      })),
      p.each({ margin: "", padding: "", border: "Width" }, function (t, e) {
        (p.cssHooks[t + e] = {
          expand: function (i) {
            for (
              var n = 0, s = {}, o = "string" == typeof i ? i.split(" ") : [i];
              4 > n;
              n++
            )
              s[t + B[n] + e] = o[n] || o[n - 2] || o[0];
            return s;
          },
        }),
          Lt.test(t) || (p.cssHooks[t + e].set = te);
      }),
      p.fn.extend({
        css: function (t, e) {
          return G(
            this,
            function (t, e, i) {
              var n,
                s,
                o = {},
                r = 0;
              if (p.isArray(e)) {
                for (n = Wt(t), s = e.length; s > r; r++)
                  o[e[r]] = p.css(t, e[r], !1, n);
                return o;
              }
              return void 0 !== i ? p.style(t, e, i) : p.css(t, e);
            },
            t,
            e,
            arguments.length > 1
          );
        },
        show: function () {
          return Jt(this, !0);
        },
        hide: function () {
          return Jt(this);
        },
        toggle: function (t) {
          return "boolean" == typeof t
            ? t
              ? this.show()
              : this.hide()
            : this.each(function () {
                q(this) ? p(this).show() : p(this).hide();
              });
        },
      }),
      (p.Tween = ne),
      (ne.prototype = {
        constructor: ne,
        init: function (t, e, i, n, s, o) {
          (this.elem = t),
            (this.prop = i),
            (this.easing = s || p.easing._default),
            (this.options = e),
            (this.start = this.now = this.cur()),
            (this.end = n),
            (this.unit = o || (p.cssNumber[i] ? "" : "px"));
        },
        cur: function () {
          var t = ne.propHooks[this.prop];
          return t && t.get ? t.get(this) : ne.propHooks._default.get(this);
        },
        run: function (t) {
          var e,
            i = ne.propHooks[this.prop];
          return (
            this.options.duration
              ? (this.pos = e =
                  p.easing[this.easing](
                    t,
                    this.options.duration * t,
                    0,
                    1,
                    this.options.duration
                  ))
              : (this.pos = e = t),
            (this.now = (this.end - this.start) * e + this.start),
            this.options.step &&
              this.options.step.call(this.elem, this.now, this),
            i && i.set ? i.set(this) : ne.propHooks._default.set(this),
            this
          );
        },
      }),
      (ne.prototype.init.prototype = ne.prototype),
      (ne.propHooks = {
        _default: {
          get: function (t) {
            var e;
            return 1 !== t.elem.nodeType ||
              (null != t.elem[t.prop] && null == t.elem.style[t.prop])
              ? t.elem[t.prop]
              : (e = p.css(t.elem, t.prop, "")) && "auto" !== e
              ? e
              : 0;
          },
          set: function (t) {
            p.fx.step[t.prop]
              ? p.fx.step[t.prop](t)
              : 1 !== t.elem.nodeType ||
                (null == t.elem.style[p.cssProps[t.prop]] &&
                  !p.cssHooks[t.prop])
              ? (t.elem[t.prop] = t.now)
              : p.style(t.elem, t.prop, t.now + t.unit);
          },
        },
      }),
      (ne.propHooks.scrollTop = ne.propHooks.scrollLeft =
        {
          set: function (t) {
            t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now);
          },
        }),
      (p.easing = {
        linear: function (t) {
          return t;
        },
        swing: function (t) {
          return 0.5 - Math.cos(t * Math.PI) / 2;
        },
        _default: "swing",
      }),
      (p.fx = ne.prototype.init),
      (p.fx.step = {});
    var se,
      oe,
      re = /^(?:toggle|show|hide)$/,
      ae = /queueHooks$/;
    function le() {
      return (
        t.setTimeout(function () {
          se = void 0;
        }),
        (se = p.now())
      );
    }
    function ce(t, e) {
      var i,
        n = { height: t },
        s = 0;
      for (e = e ? 1 : 0; 4 > s; s += 2 - e)
        n["margin" + (i = B[s])] = n["padding" + i] = t;
      return e && (n.opacity = n.width = t), n;
    }
    function de(t, e, i) {
      for (
        var n,
          s = (he.tweeners[e] || []).concat(he.tweeners["*"]),
          o = 0,
          r = s.length;
        r > o;
        o++
      )
        if ((n = s[o].call(i, e, t))) return n;
    }
    function he(t, e, i) {
      var n,
        s,
        o = 0,
        r = he.prefilters.length,
        a = p.Deferred().always(function () {
          delete l.elem;
        }),
        l = function () {
          if (s) return !1;
          for (
            var e = se || le(),
              i = Math.max(0, c.startTime + c.duration - e),
              n = 1 - (i / c.duration || 0),
              o = 0,
              r = c.tweens.length;
            r > o;
            o++
          )
            c.tweens[o].run(n);
          return (
            a.notifyWith(t, [c, n, i]),
            1 > n && r ? i : (a.resolveWith(t, [c]), !1)
          );
        },
        c = a.promise({
          elem: t,
          props: p.extend({}, e),
          opts: p.extend(
            !0,
            { specialEasing: {}, easing: p.easing._default },
            i
          ),
          originalProperties: e,
          originalOptions: i,
          startTime: se || le(),
          duration: i.duration,
          tweens: [],
          createTween: function (e, i) {
            var n = p.Tween(
              t,
              c.opts,
              e,
              i,
              c.opts.specialEasing[e] || c.opts.easing
            );
            return c.tweens.push(n), n;
          },
          stop: function (e) {
            var i = 0,
              n = e ? c.tweens.length : 0;
            if (s) return this;
            for (s = !0; n > i; i++) c.tweens[i].run(1);
            return (
              e
                ? (a.notifyWith(t, [c, 1, 0]), a.resolveWith(t, [c, e]))
                : a.rejectWith(t, [c, e]),
              this
            );
          },
        }),
        d = c.props;
      for (
        (function (t, e) {
          var i, n, s, o, r;
          for (i in t)
            if (
              ((s = e[(n = p.camelCase(i))]),
              (o = t[i]),
              p.isArray(o) && ((s = o[1]), (o = t[i] = o[0])),
              i !== n && ((t[n] = o), delete t[i]),
              (r = p.cssHooks[n]) && ("expand" in r))
            )
              for (i in ((o = r.expand(o)), delete t[n], o))
                (i in t) || ((t[i] = o[i]), (e[i] = s));
            else e[n] = s;
        })(d, c.opts.specialEasing);
        r > o;
        o++
      )
        if ((n = he.prefilters[o].call(c, t, d, c.opts)))
          return (
            p.isFunction(n.stop) &&
              (p._queueHooks(c.elem, c.opts.queue).stop = p.proxy(n.stop, n)),
            n
          );
      return (
        p.map(d, de, c),
        p.isFunction(c.opts.start) && c.opts.start.call(t, c),
        p.fx.timer(p.extend(l, { elem: t, anim: c, queue: c.opts.queue })),
        c
          .progress(c.opts.progress)
          .done(c.opts.done, c.opts.complete)
          .fail(c.opts.fail)
          .always(c.opts.always)
      );
    }
    (p.Animation = p.extend(he, {
      tweeners: {
        "*": [
          function (t, e) {
            var i = this.createTween(t, e);
            return V(i.elem, t, U.exec(e), i), i;
          },
        ],
      },
      tweener: function (t, e) {
        p.isFunction(t) ? ((e = t), (t = ["*"])) : (t = t.match(N));
        for (var i, n = 0, s = t.length; s > n; n++)
          (i = t[n]),
            (he.tweeners[i] = he.tweeners[i] || []),
            he.tweeners[i].unshift(e);
      },
      prefilters: [
        function (t, e, i) {
          var n,
            s,
            o,
            r,
            a,
            l,
            c,
            d = this,
            u = {},
            f = t.style,
            m = t.nodeType && q(t),
            g = p._data(t, "fxshow");
          for (n in (i.queue ||
            (null == (a = p._queueHooks(t, "fx")).unqueued &&
              ((a.unqueued = 0),
              (l = a.empty.fire),
              (a.empty.fire = function () {
                a.unqueued || l();
              })),
            a.unqueued++,
            d.always(function () {
              d.always(function () {
                a.unqueued--, p.queue(t, "fx").length || a.empty.fire();
              });
            })),
          1 === t.nodeType &&
            ("height" in e || "width" in e) &&
            ((i.overflow = [f.overflow, f.overflowX, f.overflowY]),
            "inline" ===
              ("none" === (c = p.css(t, "display"))
                ? p._data(t, "olddisplay") || Pt(t.nodeName)
                : c) &&
              "none" === p.css(t, "float") &&
              (h.inlineBlockNeedsLayout && "inline" !== Pt(t.nodeName)
                ? (f.zoom = 1)
                : (f.display = "inline-block"))),
          i.overflow &&
            ((f.overflow = "hidden"),
            h.shrinkWrapBlocks() ||
              d.always(function () {
                (f.overflow = i.overflow[0]),
                  (f.overflowX = i.overflow[1]),
                  (f.overflowY = i.overflow[2]);
              })),
          e))
            if (((s = e[n]), re.exec(s))) {
              if (
                (delete e[n],
                (o = o || "toggle" === s),
                s === (m ? "hide" : "show"))
              ) {
                if ("show" !== s || !g || void 0 === g[n]) continue;
                m = !0;
              }
              u[n] = (g && g[n]) || p.style(t, n);
            } else c = void 0;
          if (p.isEmptyObject(u))
            "inline" === ("none" === c ? Pt(t.nodeName) : c) && (f.display = c);
          else
            for (n in (g
              ? "hidden" in g && (m = g.hidden)
              : (g = p._data(t, "fxshow", {})),
            o && (g.hidden = !m),
            m
              ? p(t).show()
              : d.done(function () {
                  p(t).hide();
                }),
            d.done(function () {
              var e;
              for (e in (p._removeData(t, "fxshow"), u)) p.style(t, e, u[e]);
            }),
            u))
              (r = de(m ? g[n] : 0, n, d)),
                n in g ||
                  ((g[n] = r.start),
                  m &&
                    ((r.end = r.start),
                    (r.start = "width" === n || "height" === n ? 1 : 0)));
        },
      ],
      prefilter: function (t, e) {
        e ? he.prefilters.unshift(t) : he.prefilters.push(t);
      },
    })),
      (p.speed = function (t, e, i) {
        var n =
          t && "object" == typeof t
            ? p.extend({}, t)
            : {
                complete: i || (!i && e) || (p.isFunction(t) && t),
                duration: t,
                easing: (i && e) || (e && !p.isFunction(e) && e),
              };
        return (
          (n.duration = p.fx.off
            ? 0
            : "number" == typeof n.duration
            ? n.duration
            : n.duration in p.fx.speeds
            ? p.fx.speeds[n.duration]
            : p.fx.speeds._default),
          (null != n.queue && !0 !== n.queue) || (n.queue = "fx"),
          (n.old = n.complete),
          (n.complete = function () {
            p.isFunction(n.old) && n.old.call(this),
              n.queue && p.dequeue(this, n.queue);
          }),
          n
        );
      }),
      p.fn.extend({
        fadeTo: function (t, e, i, n) {
          return this.filter(q)
            .css("opacity", 0)
            .show()
            .end()
            .animate({ opacity: e }, t, i, n);
        },
        animate: function (t, e, i, n) {
          var s = p.isEmptyObject(t),
            o = p.speed(e, i, n),
            r = function () {
              var e = he(this, p.extend({}, t), o);
              (s || p._data(this, "finish")) && e.stop(!0);
            };
          return (
            (r.finish = r),
            s || !1 === o.queue ? this.each(r) : this.queue(o.queue, r)
          );
        },
        stop: function (t, e, i) {
          var n = function (t) {
            var e = t.stop;
            delete t.stop, e(i);
          };
          return (
            "string" != typeof t && ((i = e), (e = t), (t = void 0)),
            e && !1 !== t && this.queue(t || "fx", []),
            this.each(function () {
              var e = !0,
                s = null != t && t + "queueHooks",
                o = p.timers,
                r = p._data(this);
              if (s) r[s] && r[s].stop && n(r[s]);
              else for (s in r) r[s] && r[s].stop && ae.test(s) && n(r[s]);
              for (s = o.length; s--; )
                o[s].elem !== this ||
                  (null != t && o[s].queue !== t) ||
                  (o[s].anim.stop(i), (e = !1), o.splice(s, 1));
              (!e && i) || p.dequeue(this, t);
            })
          );
        },
        finish: function (t) {
          return (
            !1 !== t && (t = t || "fx"),
            this.each(function () {
              var e,
                i = p._data(this),
                n = i[t + "queue"],
                s = i[t + "queueHooks"],
                o = p.timers,
                r = n ? n.length : 0;
              for (
                i.finish = !0,
                  p.queue(this, t, []),
                  s && s.stop && s.stop.call(this, !0),
                  e = o.length;
                e--;

              )
                o[e].elem === this &&
                  o[e].queue === t &&
                  (o[e].anim.stop(!0), o.splice(e, 1));
              for (e = 0; r > e; e++)
                n[e] && n[e].finish && n[e].finish.call(this);
              delete i.finish;
            })
          );
        },
      }),
      p.each(["toggle", "show", "hide"], function (t, e) {
        var i = p.fn[e];
        p.fn[e] = function (t, n, s) {
          return null == t || "boolean" == typeof t
            ? i.apply(this, arguments)
            : this.animate(ce(e, !0), t, n, s);
        };
      }),
      p.each(
        {
          slideDown: ce("show"),
          slideUp: ce("hide"),
          slideToggle: ce("toggle"),
          fadeIn: { opacity: "show" },
          fadeOut: { opacity: "hide" },
          fadeToggle: { opacity: "toggle" },
        },
        function (t, e) {
          p.fn[t] = function (t, i, n) {
            return this.animate(e, t, i, n);
          };
        }
      ),
      (p.timers = []),
      (p.fx.tick = function () {
        var t,
          e = p.timers,
          i = 0;
        for (se = p.now(); i < e.length; i++)
          (t = e[i])() || e[i] !== t || e.splice(i--, 1);
        e.length || p.fx.stop(), (se = void 0);
      }),
      (p.fx.timer = function (t) {
        p.timers.push(t), t() ? p.fx.start() : p.timers.pop();
      }),
      (p.fx.interval = 13),
      (p.fx.start = function () {
        oe || (oe = t.setInterval(p.fx.tick, p.fx.interval));
      }),
      (p.fx.stop = function () {
        t.clearInterval(oe), (oe = null);
      }),
      (p.fx.speeds = { slow: 600, fast: 200, _default: 400 }),
      (p.fn.delay = function (e, i) {
        return (
          (e = (p.fx && p.fx.speeds[e]) || e),
          (i = i || "fx"),
          this.queue(i, function (i, n) {
            var s = t.setTimeout(i, e);
            n.stop = function () {
              t.clearTimeout(s);
            };
          })
        );
      }),
      (function () {
        var t,
          e = n.createElement("input"),
          i = n.createElement("div"),
          s = n.createElement("select"),
          o = s.appendChild(n.createElement("option"));
        (i = n.createElement("div")).setAttribute("className", "t"),
          (i.innerHTML =
            "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>"),
          (t = i.getElementsByTagName("a")[0]),
          e.setAttribute("type", "checkbox"),
          i.appendChild(e),
          ((t = i.getElementsByTagName("a")[0]).style.cssText = "top:1px"),
          (h.getSetAttribute = "t" !== i.className),
          (h.style = /top/.test(t.getAttribute("style"))),
          (h.hrefNormalized = "/a" === t.getAttribute("href")),
          (h.checkOn = !!e.value),
          (h.optSelected = o.selected),
          (h.enctype = !!n.createElement("form").enctype),
          (s.disabled = !0),
          (h.optDisabled = !o.disabled),
          (e = n.createElement("input")).setAttribute("value", ""),
          (h.input = "" === e.getAttribute("value")),
          (e.value = "t"),
          e.setAttribute("type", "radio"),
          (h.radioValue = "t" === e.value);
      })();
    var ue = /\r/g,
      pe = /[\x20\t\r\n\f]+/g;
    p.fn.extend({
      val: function (t) {
        var e,
          i,
          n,
          s = this[0];
        return arguments.length
          ? ((n = p.isFunction(t)),
            this.each(function (i) {
              var s;
              1 === this.nodeType &&
                (null == (s = n ? t.call(this, i, p(this).val()) : t)
                  ? (s = "")
                  : "number" == typeof s
                  ? (s += "")
                  : p.isArray(s) &&
                    (s = p.map(s, function (t) {
                      return null == t ? "" : t + "";
                    })),
                ((e =
                  p.valHooks[this.type] ||
                  p.valHooks[this.nodeName.toLowerCase()]) &&
                  "set" in e &&
                  void 0 !== e.set(this, s, "value")) ||
                  (this.value = s));
            }))
          : s
          ? (e = p.valHooks[s.type] || p.valHooks[s.nodeName.toLowerCase()]) &&
            "get" in e &&
            void 0 !== (i = e.get(s, "value"))
            ? i
            : "string" == typeof (i = s.value)
            ? i.replace(ue, "")
            : null == i
            ? ""
            : i
          : void 0;
      },
    }),
      p.extend({
        valHooks: {
          option: {
            get: function (t) {
              var e = p.find.attr(t, "value");
              return null != e ? e : p.trim(p.text(t)).replace(pe, " ");
            },
          },
          select: {
            get: function (t) {
              for (
                var e,
                  i,
                  n = t.options,
                  s = t.selectedIndex,
                  o = "select-one" === t.type || 0 > s,
                  r = o ? null : [],
                  a = o ? s + 1 : n.length,
                  l = 0 > s ? a : o ? s : 0;
                a > l;
                l++
              )
                if (
                  ((i = n[l]).selected || l === s) &&
                  (h.optDisabled
                    ? !i.disabled
                    : null === i.getAttribute("disabled")) &&
                  (!i.parentNode.disabled ||
                    !p.nodeName(i.parentNode, "optgroup"))
                ) {
                  if (((e = p(i).val()), o)) return e;
                  r.push(e);
                }
              return r;
            },
            set: function (t, e) {
              for (
                var i, n, s = t.options, o = p.makeArray(e), r = s.length;
                r--;

              )
                if (((n = s[r]), p.inArray(p.valHooks.option.get(n), o) > -1))
                  try {
                    n.selected = i = !0;
                  } catch (t) {
                    n.scrollHeight;
                  }
                else n.selected = !1;
              return i || (t.selectedIndex = -1), s;
            },
          },
        },
      }),
      p.each(["radio", "checkbox"], function () {
        (p.valHooks[this] = {
          set: function (t, e) {
            return p.isArray(e)
              ? (t.checked = p.inArray(p(t).val(), e) > -1)
              : void 0;
          },
        }),
          h.checkOn ||
            (p.valHooks[this].get = function (t) {
              return null === t.getAttribute("value") ? "on" : t.value;
            });
      });
    var fe,
      me,
      ge = p.expr.attrHandle,
      ve = /^(?:checked|selected)$/i,
      ye = h.getSetAttribute,
      _e = h.input;
    p.fn.extend({
      attr: function (t, e) {
        return G(this, p.attr, t, e, arguments.length > 1);
      },
      removeAttr: function (t) {
        return this.each(function () {
          p.removeAttr(this, t);
        });
      },
    }),
      p.extend({
        attr: function (t, e, i) {
          var n,
            s,
            o = t.nodeType;
          if (3 !== o && 8 !== o && 2 !== o)
            return void 0 === t.getAttribute
              ? p.prop(t, e, i)
              : ((1 === o && p.isXMLDoc(t)) ||
                  ((e = e.toLowerCase()),
                  (s =
                    p.attrHooks[e] || (p.expr.match.bool.test(e) ? me : fe))),
                void 0 !== i
                  ? null === i
                    ? void p.removeAttr(t, e)
                    : s && "set" in s && void 0 !== (n = s.set(t, i, e))
                    ? n
                    : (t.setAttribute(e, i + ""), i)
                  : s && "get" in s && null !== (n = s.get(t, e))
                  ? n
                  : null == (n = p.find.attr(t, e))
                  ? void 0
                  : n);
        },
        attrHooks: {
          type: {
            set: function (t, e) {
              if (!h.radioValue && "radio" === e && p.nodeName(t, "input")) {
                var i = t.value;
                return t.setAttribute("type", e), i && (t.value = i), e;
              }
            },
          },
        },
        removeAttr: function (t, e) {
          var i,
            n,
            s = 0,
            o = e && e.match(N);
          if (o && 1 === t.nodeType)
            for (; (i = o[s++]); )
              (n = p.propFix[i] || i),
                p.expr.match.bool.test(i)
                  ? (_e && ye) || !ve.test(i)
                    ? (t[n] = !1)
                    : (t[p.camelCase("default-" + i)] = t[n] = !1)
                  : p.attr(t, i, ""),
                t.removeAttribute(ye ? i : n);
        },
      }),
      (me = {
        set: function (t, e, i) {
          return (
            !1 === e
              ? p.removeAttr(t, i)
              : (_e && ye) || !ve.test(i)
              ? t.setAttribute((!ye && p.propFix[i]) || i, i)
              : (t[p.camelCase("default-" + i)] = t[i] = !0),
            i
          );
        },
      }),
      p.each(p.expr.match.bool.source.match(/\w+/g), function (t, e) {
        var i = ge[e] || p.find.attr;
        (_e && ye) || !ve.test(e)
          ? (ge[e] = function (t, e, n) {
              var s, o;
              return (
                n ||
                  ((o = ge[e]),
                  (ge[e] = s),
                  (s = null != i(t, e, n) ? e.toLowerCase() : null),
                  (ge[e] = o)),
                s
              );
            })
          : (ge[e] = function (t, e, i) {
              return i
                ? void 0
                : t[p.camelCase("default-" + e)]
                ? e.toLowerCase()
                : null;
            });
      }),
      (_e && ye) ||
        (p.attrHooks.value = {
          set: function (t, e, i) {
            return p.nodeName(t, "input")
              ? void (t.defaultValue = e)
              : fe && fe.set(t, e, i);
          },
        }),
      ye ||
        ((fe = {
          set: function (t, e, i) {
            var n = t.getAttributeNode(i);
            return (
              n || t.setAttributeNode((n = t.ownerDocument.createAttribute(i))),
              (n.value = e += ""),
              "value" === i || e === t.getAttribute(i) ? e : void 0
            );
          },
        }),
        (ge.id =
          ge.name =
          ge.coords =
            function (t, e, i) {
              var n;
              return i
                ? void 0
                : (n = t.getAttributeNode(e)) && "" !== n.value
                ? n.value
                : null;
            }),
        (p.valHooks.button = {
          get: function (t, e) {
            var i = t.getAttributeNode(e);
            return i && i.specified ? i.value : void 0;
          },
          set: fe.set,
        }),
        (p.attrHooks.contenteditable = {
          set: function (t, e, i) {
            fe.set(t, "" !== e && e, i);
          },
        }),
        p.each(["width", "height"], function (t, e) {
          p.attrHooks[e] = {
            set: function (t, i) {
              return "" === i ? (t.setAttribute(e, "auto"), i) : void 0;
            },
          };
        })),
      h.style ||
        (p.attrHooks.style = {
          get: function (t) {
            return t.style.cssText || void 0;
          },
          set: function (t, e) {
            return (t.style.cssText = e + "");
          },
        });
    var we = /^(?:input|select|textarea|button|object)$/i,
      be = /^(?:a|area)$/i;
    p.fn.extend({
      prop: function (t, e) {
        return G(this, p.prop, t, e, arguments.length > 1);
      },
      removeProp: function (t) {
        return (
          (t = p.propFix[t] || t),
          this.each(function () {
            try {
              (this[t] = void 0), delete this[t];
            } catch (t) {}
          })
        );
      },
    }),
      p.extend({
        prop: function (t, e, i) {
          var n,
            s,
            o = t.nodeType;
          if (3 !== o && 8 !== o && 2 !== o)
            return (
              (1 === o && p.isXMLDoc(t)) ||
                ((e = p.propFix[e] || e), (s = p.propHooks[e])),
              void 0 !== i
                ? s && "set" in s && void 0 !== (n = s.set(t, i, e))
                  ? n
                  : (t[e] = i)
                : s && "get" in s && null !== (n = s.get(t, e))
                ? n
                : t[e]
            );
        },
        propHooks: {
          tabIndex: {
            get: function (t) {
              var e = p.find.attr(t, "tabindex");
              return e
                ? parseInt(e, 10)
                : we.test(t.nodeName) || (be.test(t.nodeName) && t.href)
                ? 0
                : -1;
            },
          },
        },
        propFix: { for: "htmlFor", class: "className" },
      }),
      h.hrefNormalized ||
        p.each(["href", "src"], function (t, e) {
          p.propHooks[e] = {
            get: function (t) {
              return t.getAttribute(e, 4);
            },
          };
        }),
      h.optSelected ||
        (p.propHooks.selected = {
          get: function (t) {
            var e = t.parentNode;
            return (
              e &&
                (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex),
              null
            );
          },
          set: function (t) {
            var e = t.parentNode;
            e && (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex);
          },
        }),
      p.each(
        [
          "tabIndex",
          "readOnly",
          "maxLength",
          "cellSpacing",
          "cellPadding",
          "rowSpan",
          "colSpan",
          "useMap",
          "frameBorder",
          "contentEditable",
        ],
        function () {
          p.propFix[this.toLowerCase()] = this;
        }
      ),
      h.enctype || (p.propFix.enctype = "encoding");
    var Ce = /[\t\r\n\f]/g;
    function ke(t) {
      return p.attr(t, "class") || "";
    }
    p.fn.extend({
      addClass: function (t) {
        var e,
          i,
          n,
          s,
          o,
          r,
          a,
          l = 0;
        if (p.isFunction(t))
          return this.each(function (e) {
            p(this).addClass(t.call(this, e, ke(this)));
          });
        if ("string" == typeof t && t)
          for (e = t.match(N) || []; (i = this[l++]); )
            if (
              ((s = ke(i)),
              (n = 1 === i.nodeType && (" " + s + " ").replace(Ce, " ")))
            ) {
              for (r = 0; (o = e[r++]); )
                n.indexOf(" " + o + " ") < 0 && (n += o + " ");
              s !== (a = p.trim(n)) && p.attr(i, "class", a);
            }
        return this;
      },
      removeClass: function (t) {
        var e,
          i,
          n,
          s,
          o,
          r,
          a,
          l = 0;
        if (p.isFunction(t))
          return this.each(function (e) {
            p(this).removeClass(t.call(this, e, ke(this)));
          });
        if (!arguments.length) return this.attr("class", "");
        if ("string" == typeof t && t)
          for (e = t.match(N) || []; (i = this[l++]); )
            if (
              ((s = ke(i)),
              (n = 1 === i.nodeType && (" " + s + " ").replace(Ce, " ")))
            ) {
              for (r = 0; (o = e[r++]); )
                for (; n.indexOf(" " + o + " ") > -1; )
                  n = n.replace(" " + o + " ", " ");
              s !== (a = p.trim(n)) && p.attr(i, "class", a);
            }
        return this;
      },
      toggleClass: function (t, e) {
        var i = typeof t;
        return "boolean" == typeof e && "string" === i
          ? e
            ? this.addClass(t)
            : this.removeClass(t)
          : p.isFunction(t)
          ? this.each(function (i) {
              p(this).toggleClass(t.call(this, i, ke(this), e), e);
            })
          : this.each(function () {
              var e, n, s, o;
              if ("string" === i)
                for (n = 0, s = p(this), o = t.match(N) || []; (e = o[n++]); )
                  s.hasClass(e) ? s.removeClass(e) : s.addClass(e);
              else
                (void 0 !== t && "boolean" !== i) ||
                  ((e = ke(this)) && p._data(this, "__className__", e),
                  p.attr(
                    this,
                    "class",
                    e || !1 === t ? "" : p._data(this, "__className__") || ""
                  ));
            });
      },
      hasClass: function (t) {
        var e,
          i,
          n = 0;
        for (e = " " + t + " "; (i = this[n++]); )
          if (
            1 === i.nodeType &&
            (" " + ke(i) + " ").replace(Ce, " ").indexOf(e) > -1
          )
            return !0;
        return !1;
      },
    }),
      p.each(
        "blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(
          " "
        ),
        function (t, e) {
          p.fn[e] = function (t, i) {
            return arguments.length > 0
              ? this.on(e, null, t, i)
              : this.trigger(e);
          };
        }
      ),
      p.fn.extend({
        hover: function (t, e) {
          return this.mouseenter(t).mouseleave(e || t);
        },
      });
    var De = t.location,
      Te = p.now(),
      xe = /\?/,
      Se =
        /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    (p.parseJSON = function (e) {
      if (t.JSON && t.JSON.parse) return t.JSON.parse(e + "");
      var i,
        n = null,
        s = p.trim(e + "");
      return s &&
        !p.trim(
          s.replace(Se, function (t, e, s, o) {
            return (
              i && e && (n = 0),
              0 === n ? t : ((i = s || e), (n += !o - !s), "")
            );
          })
        )
        ? Function("return " + s)()
        : p.error("Invalid JSON: " + e);
    }),
      (p.parseXML = function (e) {
        var i;
        if (!e || "string" != typeof e) return null;
        try {
          t.DOMParser
            ? (i = new t.DOMParser().parseFromString(e, "text/xml"))
            : (((i = new t.ActiveXObject("Microsoft.XMLDOM")).async = "false"),
              i.loadXML(e));
        } catch (t) {
          i = void 0;
        }
        return (
          (i &&
            i.documentElement &&
            !i.getElementsByTagName("parsererror").length) ||
            p.error("Invalid XML: " + e),
          i
        );
      });
    var Ee = /#.*$/,
      Ae = /([?&])_=[^&]*/,
      Me = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
      $e = /^(?:GET|HEAD)$/,
      Oe = /^\/\//,
      Ne = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
      Ie = {},
      Pe = {},
      Le = "*/".concat("*"),
      Ye = De.href,
      He = Ne.exec(Ye.toLowerCase()) || [];
    function je(t) {
      return function (e, i) {
        "string" != typeof e && ((i = e), (e = "*"));
        var n,
          s = 0,
          o = e.toLowerCase().match(N) || [];
        if (p.isFunction(i))
          for (; (n = o[s++]); )
            "+" === n.charAt(0)
              ? ((n = n.slice(1) || "*"), (t[n] = t[n] || []).unshift(i))
              : (t[n] = t[n] || []).push(i);
      };
    }
    function We(t, e, i, n) {
      var s = {},
        o = t === Pe;
      function r(a) {
        var l;
        return (
          (s[a] = !0),
          p.each(t[a] || [], function (t, a) {
            var c = a(e, i, n);
            return "string" != typeof c || o || s[c]
              ? o
                ? !(l = c)
                : void 0
              : (e.dataTypes.unshift(c), r(c), !1);
          }),
          l
        );
      }
      return r(e.dataTypes[0]) || (!s["*"] && r("*"));
    }
    function Re(t, e) {
      var i,
        n,
        s = p.ajaxSettings.flatOptions || {};
      for (n in e) void 0 !== e[n] && ((s[n] ? t : i || (i = {}))[n] = e[n]);
      return i && p.extend(!0, t, i), t;
    }
    function ze(t) {
      return (t.style && t.style.display) || p.css(t, "display");
    }
    p.extend({
      active: 0,
      lastModified: {},
      etag: {},
      ajaxSettings: {
        url: Ye,
        type: "GET",
        isLocal:
          /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(
            He[1]
          ),
        global: !0,
        processData: !0,
        async: !0,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        accepts: {
          "*": Le,
          text: "text/plain",
          html: "text/html",
          xml: "application/xml, text/xml",
          json: "application/json, text/javascript",
        },
        contents: { xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/ },
        responseFields: {
          xml: "responseXML",
          text: "responseText",
          json: "responseJSON",
        },
        converters: {
          "* text": String,
          "text html": !0,
          "text json": p.parseJSON,
          "text xml": p.parseXML,
        },
        flatOptions: { url: !0, context: !0 },
      },
      ajaxSetup: function (t, e) {
        return e ? Re(Re(t, p.ajaxSettings), e) : Re(p.ajaxSettings, t);
      },
      ajaxPrefilter: je(Ie),
      ajaxTransport: je(Pe),
      ajax: function (e, i) {
        "object" == typeof e && ((i = e), (e = void 0)), (i = i || {});
        var n,
          s,
          o,
          r,
          a,
          l,
          c,
          d,
          h = p.ajaxSetup({}, i),
          u = h.context || h,
          f = h.context && (u.nodeType || u.jquery) ? p(u) : p.event,
          m = p.Deferred(),
          g = p.Callbacks("once memory"),
          v = h.statusCode || {},
          y = {},
          _ = {},
          w = 0,
          b = "canceled",
          C = {
            readyState: 0,
            getResponseHeader: function (t) {
              var e;
              if (2 === w) {
                if (!d)
                  for (d = {}; (e = Me.exec(r)); ) d[e[1].toLowerCase()] = e[2];
                e = d[t.toLowerCase()];
              }
              return null == e ? null : e;
            },
            getAllResponseHeaders: function () {
              return 2 === w ? r : null;
            },
            setRequestHeader: function (t, e) {
              var i = t.toLowerCase();
              return w || ((t = _[i] = _[i] || t), (y[t] = e)), this;
            },
            overrideMimeType: function (t) {
              return w || (h.mimeType = t), this;
            },
            statusCode: function (t) {
              var e;
              if (t)
                if (2 > w) for (e in t) v[e] = [v[e], t[e]];
                else C.always(t[C.status]);
              return this;
            },
            abort: function (t) {
              var e = t || b;
              return c && c.abort(e), k(0, e), this;
            },
          };
        if (
          ((m.promise(C).complete = g.add),
          (C.success = C.done),
          (C.error = C.fail),
          (h.url = ((e || h.url || Ye) + "")
            .replace(Ee, "")
            .replace(Oe, He[1] + "//")),
          (h.type = i.method || i.type || h.method || h.type),
          (h.dataTypes = p
            .trim(h.dataType || "*")
            .toLowerCase()
            .match(N) || [""]),
          null == h.crossDomain &&
            ((n = Ne.exec(h.url.toLowerCase())),
            (h.crossDomain = !(
              !n ||
              (n[1] === He[1] &&
                n[2] === He[2] &&
                (n[3] || ("http:" === n[1] ? "80" : "443")) ===
                  (He[3] || ("http:" === He[1] ? "80" : "443")))
            ))),
          h.data &&
            h.processData &&
            "string" != typeof h.data &&
            (h.data = p.param(h.data, h.traditional)),
          We(Ie, h, i, C),
          2 === w)
        )
          return C;
        for (s in ((l = p.event && h.global) &&
          0 == p.active++ &&
          p.event.trigger("ajaxStart"),
        (h.type = h.type.toUpperCase()),
        (h.hasContent = !$e.test(h.type)),
        (o = h.url),
        h.hasContent ||
          (h.data &&
            ((o = h.url += (xe.test(o) ? "&" : "?") + h.data), delete h.data),
          !1 === h.cache &&
            (h.url = Ae.test(o)
              ? o.replace(Ae, "$1_=" + Te++)
              : o + (xe.test(o) ? "&" : "?") + "_=" + Te++)),
        h.ifModified &&
          (p.lastModified[o] &&
            C.setRequestHeader("If-Modified-Since", p.lastModified[o]),
          p.etag[o] && C.setRequestHeader("If-None-Match", p.etag[o])),
        ((h.data && h.hasContent && !1 !== h.contentType) || i.contentType) &&
          C.setRequestHeader("Content-Type", h.contentType),
        C.setRequestHeader(
          "Accept",
          h.dataTypes[0] && h.accepts[h.dataTypes[0]]
            ? h.accepts[h.dataTypes[0]] +
                ("*" !== h.dataTypes[0] ? ", " + Le + "; q=0.01" : "")
            : h.accepts["*"]
        ),
        h.headers))
          C.setRequestHeader(s, h.headers[s]);
        if (h.beforeSend && (!1 === h.beforeSend.call(u, C, h) || 2 === w))
          return C.abort();
        for (s in ((b = "abort"), { success: 1, error: 1, complete: 1 }))
          C[s](h[s]);
        if ((c = We(Pe, h, i, C))) {
          if (((C.readyState = 1), l && f.trigger("ajaxSend", [C, h]), 2 === w))
            return C;
          h.async &&
            h.timeout > 0 &&
            (a = t.setTimeout(function () {
              C.abort("timeout");
            }, h.timeout));
          try {
            (w = 1), c.send(y, k);
          } catch (t) {
            if (!(2 > w)) throw t;
            k(-1, t);
          }
        } else k(-1, "No Transport");
        function k(e, i, n, s) {
          var d,
            y,
            _,
            b,
            k,
            D = i;
          2 !== w &&
            ((w = 2),
            a && t.clearTimeout(a),
            (c = void 0),
            (r = s || ""),
            (C.readyState = e > 0 ? 4 : 0),
            (d = (e >= 200 && 300 > e) || 304 === e),
            n &&
              (b = (function (t, e, i) {
                for (
                  var n, s, o, r, a = t.contents, l = t.dataTypes;
                  "*" === l[0];

                )
                  l.shift(),
                    void 0 === s &&
                      (s = t.mimeType || e.getResponseHeader("Content-Type"));
                if (s)
                  for (r in a)
                    if (a[r] && a[r].test(s)) {
                      l.unshift(r);
                      break;
                    }
                if (l[0] in i) o = l[0];
                else {
                  for (r in i) {
                    if (!l[0] || t.converters[r + " " + l[0]]) {
                      o = r;
                      break;
                    }
                    n || (n = r);
                  }
                  o = o || n;
                }
                return o ? (o !== l[0] && l.unshift(o), i[o]) : void 0;
              })(h, C, n)),
            (b = (function (t, e, i, n) {
              var s,
                o,
                r,
                a,
                l,
                c = {},
                d = t.dataTypes.slice();
              if (d[1])
                for (r in t.converters) c[r.toLowerCase()] = t.converters[r];
              for (o = d.shift(); o; )
                if (
                  (t.responseFields[o] && (i[t.responseFields[o]] = e),
                  !l && n && t.dataFilter && (e = t.dataFilter(e, t.dataType)),
                  (l = o),
                  (o = d.shift()))
                )
                  if ("*" === o) o = l;
                  else if ("*" !== l && l !== o) {
                    if (!(r = c[l + " " + o] || c["* " + o]))
                      for (s in c)
                        if (
                          (a = s.split(" "))[1] === o &&
                          (r = c[l + " " + a[0]] || c["* " + a[0]])
                        ) {
                          !0 === r
                            ? (r = c[s])
                            : !0 !== c[s] && ((o = a[0]), d.unshift(a[1]));
                          break;
                        }
                    if (!0 !== r)
                      if (r && t.throws) e = r(e);
                      else
                        try {
                          e = r(e);
                        } catch (t) {
                          return {
                            state: "parsererror",
                            error: r
                              ? t
                              : "No conversion from " + l + " to " + o,
                          };
                        }
                  }
              return { state: "success", data: e };
            })(h, b, C, d)),
            d
              ? (h.ifModified &&
                  ((k = C.getResponseHeader("Last-Modified")) &&
                    (p.lastModified[o] = k),
                  (k = C.getResponseHeader("etag")) && (p.etag[o] = k)),
                204 === e || "HEAD" === h.type
                  ? (D = "nocontent")
                  : 304 === e
                  ? (D = "notmodified")
                  : ((D = b.state), (y = b.data), (d = !(_ = b.error))))
              : ((_ = D), (!e && D) || ((D = "error"), 0 > e && (e = 0))),
            (C.status = e),
            (C.statusText = (i || D) + ""),
            d ? m.resolveWith(u, [y, D, C]) : m.rejectWith(u, [C, D, _]),
            C.statusCode(v),
            (v = void 0),
            l && f.trigger(d ? "ajaxSuccess" : "ajaxError", [C, h, d ? y : _]),
            g.fireWith(u, [C, D]),
            l &&
              (f.trigger("ajaxComplete", [C, h]),
              --p.active || p.event.trigger("ajaxStop")));
        }
        return C;
      },
      getJSON: function (t, e, i) {
        return p.get(t, e, i, "json");
      },
      getScript: function (t, e) {
        return p.get(t, void 0, e, "script");
      },
    }),
      p.each(["get", "post"], function (t, e) {
        p[e] = function (t, i, n, s) {
          return (
            p.isFunction(i) && ((s = s || n), (n = i), (i = void 0)),
            p.ajax(
              p.extend(
                { url: t, type: e, dataType: s, data: i, success: n },
                p.isPlainObject(t) && t
              )
            )
          );
        };
      }),
      (p._evalUrl = function (t) {
        return p.ajax({
          url: t,
          type: "GET",
          dataType: "script",
          cache: !0,
          async: !1,
          global: !1,
          throws: !0,
        });
      }),
      p.fn.extend({
        wrapAll: function (t) {
          if (p.isFunction(t))
            return this.each(function (e) {
              p(this).wrapAll(t.call(this, e));
            });
          if (this[0]) {
            var e = p(t, this[0].ownerDocument).eq(0).clone(!0);
            this[0].parentNode && e.insertBefore(this[0]),
              e
                .map(function () {
                  for (
                    var t = this;
                    t.firstChild && 1 === t.firstChild.nodeType;

                  )
                    t = t.firstChild;
                  return t;
                })
                .append(this);
          }
          return this;
        },
        wrapInner: function (t) {
          return p.isFunction(t)
            ? this.each(function (e) {
                p(this).wrapInner(t.call(this, e));
              })
            : this.each(function () {
                var e = p(this),
                  i = e.contents();
                i.length ? i.wrapAll(t) : e.append(t);
              });
        },
        wrap: function (t) {
          var e = p.isFunction(t);
          return this.each(function (i) {
            p(this).wrapAll(e ? t.call(this, i) : t);
          });
        },
        unwrap: function () {
          return this.parent()
            .each(function () {
              p.nodeName(this, "body") || p(this).replaceWith(this.childNodes);
            })
            .end();
        },
      }),
      (p.expr.filters.hidden = function (t) {
        return h.reliableHiddenOffsets()
          ? t.offsetWidth <= 0 &&
              t.offsetHeight <= 0 &&
              !t.getClientRects().length
          : (function (t) {
              if (!p.contains(t.ownerDocument || n, t)) return !0;
              for (; t && 1 === t.nodeType; ) {
                if ("none" === ze(t) || "hidden" === t.type) return !0;
                t = t.parentNode;
              }
              return !1;
            })(t);
      }),
      (p.expr.filters.visible = function (t) {
        return !p.expr.filters.hidden(t);
      });
    var Fe = /%20/g,
      Ue = /\[\]$/,
      Be = /\r?\n/g,
      qe = /^(?:submit|button|image|reset|file)$/i,
      Ve = /^(?:input|select|textarea|keygen)/i;
    function Ge(t, e, i, n) {
      var s;
      if (p.isArray(e))
        p.each(e, function (e, s) {
          i || Ue.test(t)
            ? n(t, s)
            : Ge(
                t + "[" + ("object" == typeof s && null != s ? e : "") + "]",
                s,
                i,
                n
              );
        });
      else if (i || "object" !== p.type(e)) n(t, e);
      else for (s in e) Ge(t + "[" + s + "]", e[s], i, n);
    }
    (p.param = function (t, e) {
      var i,
        n = [],
        s = function (t, e) {
          (e = p.isFunction(e) ? e() : null == e ? "" : e),
            (n[n.length] = encodeURIComponent(t) + "=" + encodeURIComponent(e));
        };
      if (
        (void 0 === e && (e = p.ajaxSettings && p.ajaxSettings.traditional),
        p.isArray(t) || (t.jquery && !p.isPlainObject(t)))
      )
        p.each(t, function () {
          s(this.name, this.value);
        });
      else for (i in t) Ge(i, t[i], e, s);
      return n.join("&").replace(Fe, "+");
    }),
      p.fn.extend({
        serialize: function () {
          return p.param(this.serializeArray());
        },
        serializeArray: function () {
          return this.map(function () {
            var t = p.prop(this, "elements");
            return t ? p.makeArray(t) : this;
          })
            .filter(function () {
              var t = this.type;
              return (
                this.name &&
                !p(this).is(":disabled") &&
                Ve.test(this.nodeName) &&
                !qe.test(t) &&
                (this.checked || !Q.test(t))
              );
            })
            .map(function (t, e) {
              var i = p(this).val();
              return null == i
                ? null
                : p.isArray(i)
                ? p.map(i, function (t) {
                    return { name: e.name, value: t.replace(Be, "\r\n") };
                  })
                : { name: e.name, value: i.replace(Be, "\r\n") };
            })
            .get();
        },
      }),
      (p.ajaxSettings.xhr =
        void 0 !== t.ActiveXObject
          ? function () {
              return this.isLocal
                ? Je()
                : n.documentMode > 8
                ? Ze()
                : (/^(get|post|head|put|delete|options)$/i.test(this.type) &&
                    Ze()) ||
                  Je();
            }
          : Ze);
    var Qe = 0,
      Xe = {},
      Ke = p.ajaxSettings.xhr();
    function Ze() {
      try {
        return new t.XMLHttpRequest();
      } catch (t) {}
    }
    function Je() {
      try {
        return new t.ActiveXObject("Microsoft.XMLHTTP");
      } catch (t) {}
    }
    t.attachEvent &&
      t.attachEvent("onunload", function () {
        for (var t in Xe) Xe[t](void 0, !0);
      }),
      (h.cors = !!Ke && "withCredentials" in Ke),
      (Ke = h.ajax = !!Ke) &&
        p.ajaxTransport(function (e) {
          var i;
          if (!e.crossDomain || h.cors)
            return {
              send: function (n, s) {
                var o,
                  r = e.xhr(),
                  a = ++Qe;
                if (
                  (r.open(e.type, e.url, e.async, e.username, e.password),
                  e.xhrFields)
                )
                  for (o in e.xhrFields) r[o] = e.xhrFields[o];
                for (o in (e.mimeType &&
                  r.overrideMimeType &&
                  r.overrideMimeType(e.mimeType),
                e.crossDomain ||
                  n["X-Requested-With"] ||
                  (n["X-Requested-With"] = "XMLHttpRequest"),
                n))
                  void 0 !== n[o] && r.setRequestHeader(o, n[o] + "");
                r.send((e.hasContent && e.data) || null),
                  (i = function (t, n) {
                    var o, l, c;
                    if (i && (n || 4 === r.readyState))
                      if (
                        (delete Xe[a],
                        (i = void 0),
                        (r.onreadystatechange = p.noop),
                        n)
                      )
                        4 !== r.readyState && r.abort();
                      else {
                        (c = {}),
                          (o = r.status),
                          "string" == typeof r.responseText &&
                            (c.text = r.responseText);
                        try {
                          l = r.statusText;
                        } catch (t) {
                          l = "";
                        }
                        o || !e.isLocal || e.crossDomain
                          ? 1223 === o && (o = 204)
                          : (o = c.text ? 200 : 404);
                      }
                    c && s(o, l, c, r.getAllResponseHeaders());
                  }),
                  e.async
                    ? 4 === r.readyState
                      ? t.setTimeout(i)
                      : (r.onreadystatechange = Xe[a] = i)
                    : i();
              },
              abort: function () {
                i && i(void 0, !0);
              },
            };
        }),
      p.ajaxSetup({
        accepts: {
          script:
            "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript",
        },
        contents: { script: /\b(?:java|ecma)script\b/ },
        converters: {
          "text script": function (t) {
            return p.globalEval(t), t;
          },
        },
      }),
      p.ajaxPrefilter("script", function (t) {
        void 0 === t.cache && (t.cache = !1),
          t.crossDomain && ((t.type = "GET"), (t.global = !1));
      }),
      p.ajaxTransport("script", function (t) {
        if (t.crossDomain) {
          var e,
            i = n.head || p("head")[0] || n.documentElement;
          return {
            send: function (s, o) {
              ((e = n.createElement("script")).async = !0),
                t.scriptCharset && (e.charset = t.scriptCharset),
                (e.src = t.url),
                (e.onload = e.onreadystatechange =
                  function (t, i) {
                    (i ||
                      !e.readyState ||
                      /loaded|complete/.test(e.readyState)) &&
                      ((e.onload = e.onreadystatechange = null),
                      e.parentNode && e.parentNode.removeChild(e),
                      (e = null),
                      i || o(200, "success"));
                  }),
                i.insertBefore(e, i.firstChild);
            },
            abort: function () {
              e && e.onload(void 0, !0);
            },
          };
        }
      });
    var ti = [],
      ei = /(=)\?(?=&|$)|\?\?/;
    p.ajaxSetup({
      jsonp: "callback",
      jsonpCallback: function () {
        var t = ti.pop() || p.expando + "_" + Te++;
        return (this[t] = !0), t;
      },
    }),
      p.ajaxPrefilter("json jsonp", function (e, i, n) {
        var s,
          o,
          r,
          a =
            !1 !== e.jsonp &&
            (ei.test(e.url)
              ? "url"
              : "string" == typeof e.data &&
                0 ===
                  (e.contentType || "").indexOf(
                    "application/x-www-form-urlencoded"
                  ) &&
                ei.test(e.data) &&
                "data");
        return a || "jsonp" === e.dataTypes[0]
          ? ((s = e.jsonpCallback =
              p.isFunction(e.jsonpCallback)
                ? e.jsonpCallback()
                : e.jsonpCallback),
            a
              ? (e[a] = e[a].replace(ei, "$1" + s))
              : !1 !== e.jsonp &&
                (e.url += (xe.test(e.url) ? "&" : "?") + e.jsonp + "=" + s),
            (e.converters["script json"] = function () {
              return r || p.error(s + " was not called"), r[0];
            }),
            (e.dataTypes[0] = "json"),
            (o = t[s]),
            (t[s] = function () {
              r = arguments;
            }),
            n.always(function () {
              void 0 === o ? p(t).removeProp(s) : (t[s] = o),
                e[s] && ((e.jsonpCallback = i.jsonpCallback), ti.push(s)),
                r && p.isFunction(o) && o(r[0]),
                (r = o = void 0);
            }),
            "script")
          : void 0;
      }),
      (p.parseHTML = function (t, e, i) {
        if (!t || "string" != typeof t) return null;
        "boolean" == typeof e && ((i = e), (e = !1)), (e = e || n);
        var s = k.exec(t),
          o = !i && [];
        return s
          ? [e.createElement(s[1])]
          : ((s = at([t], e, o)),
            o && o.length && p(o).remove(),
            p.merge([], s.childNodes));
      });
    var ii = p.fn.load;
    function ni(t) {
      return p.isWindow(t)
        ? t
        : 9 === t.nodeType && (t.defaultView || t.parentWindow);
    }
    (p.fn.load = function (t, e, i) {
      if ("string" != typeof t && ii) return ii.apply(this, arguments);
      var n,
        s,
        o,
        r = this,
        a = t.indexOf(" ");
      return (
        a > -1 && ((n = p.trim(t.slice(a, t.length))), (t = t.slice(0, a))),
        p.isFunction(e)
          ? ((i = e), (e = void 0))
          : e && "object" == typeof e && (s = "POST"),
        r.length > 0 &&
          p
            .ajax({ url: t, type: s || "GET", dataType: "html", data: e })
            .done(function (t) {
              (o = arguments),
                r.html(n ? p("<div>").append(p.parseHTML(t)).find(n) : t);
            })
            .always(
              i &&
                function (t, e) {
                  r.each(function () {
                    i.apply(this, o || [t.responseText, e, t]);
                  });
                }
            ),
        this
      );
    }),
      p.each(
        [
          "ajaxStart",
          "ajaxStop",
          "ajaxComplete",
          "ajaxError",
          "ajaxSuccess",
          "ajaxSend",
        ],
        function (t, e) {
          p.fn[e] = function (t) {
            return this.on(e, t);
          };
        }
      ),
      (p.expr.filters.animated = function (t) {
        return p.grep(p.timers, function (e) {
          return t === e.elem;
        }).length;
      }),
      (p.offset = {
        setOffset: function (t, e, i) {
          var n,
            s,
            o,
            r,
            a,
            l,
            c = p.css(t, "position"),
            d = p(t),
            h = {};
          "static" === c && (t.style.position = "relative"),
            (a = d.offset()),
            (o = p.css(t, "top")),
            (l = p.css(t, "left")),
            ("absolute" === c || "fixed" === c) &&
            p.inArray("auto", [o, l]) > -1
              ? ((r = (n = d.position()).top), (s = n.left))
              : ((r = parseFloat(o) || 0), (s = parseFloat(l) || 0)),
            p.isFunction(e) && (e = e.call(t, i, p.extend({}, a))),
            null != e.top && (h.top = e.top - a.top + r),
            null != e.left && (h.left = e.left - a.left + s),
            "using" in e ? e.using.call(t, h) : d.css(h);
        },
      }),
      p.fn.extend({
        offset: function (t) {
          if (arguments.length)
            return void 0 === t
              ? this
              : this.each(function (e) {
                  p.offset.setOffset(this, t, e);
                });
          var e,
            i,
            n = { top: 0, left: 0 },
            s = this[0],
            o = s && s.ownerDocument;
          return o
            ? ((e = o.documentElement),
              p.contains(e, s)
                ? (void 0 !== s.getBoundingClientRect &&
                    (n = s.getBoundingClientRect()),
                  (i = ni(o)),
                  {
                    top:
                      n.top +
                      (i.pageYOffset || e.scrollTop) -
                      (e.clientTop || 0),
                    left:
                      n.left +
                      (i.pageXOffset || e.scrollLeft) -
                      (e.clientLeft || 0),
                  })
                : n)
            : void 0;
        },
        position: function () {
          if (this[0]) {
            var t,
              e,
              i = { top: 0, left: 0 },
              n = this[0];
            return (
              "fixed" === p.css(n, "position")
                ? (e = n.getBoundingClientRect())
                : ((t = this.offsetParent()),
                  (e = this.offset()),
                  p.nodeName(t[0], "html") || (i = t.offset()),
                  (i.top += p.css(t[0], "borderTopWidth", !0)),
                  (i.left += p.css(t[0], "borderLeftWidth", !0))),
              {
                top: e.top - i.top - p.css(n, "marginTop", !0),
                left: e.left - i.left - p.css(n, "marginLeft", !0),
              }
            );
          }
        },
        offsetParent: function () {
          return this.map(function () {
            for (
              var t = this.offsetParent;
              t && !p.nodeName(t, "html") && "static" === p.css(t, "position");

            )
              t = t.offsetParent;
            return t || jt;
          });
        },
      }),
      p.each(
        { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" },
        function (t, e) {
          var i = /Y/.test(e);
          p.fn[t] = function (n) {
            return G(
              this,
              function (t, n, s) {
                var o = ni(t);
                return void 0 === s
                  ? o
                    ? e in o
                      ? o[e]
                      : o.document.documentElement[n]
                    : t[n]
                  : void (o
                      ? o.scrollTo(
                          i ? p(o).scrollLeft() : s,
                          i ? s : p(o).scrollTop()
                        )
                      : (t[n] = s));
              },
              t,
              n,
              arguments.length,
              null
            );
          };
        }
      ),
      p.each(["top", "left"], function (t, e) {
        p.cssHooks[e] = Ft(h.pixelPosition, function (t, i) {
          return i
            ? ((i = Rt(t, e)), Yt.test(i) ? p(t).position()[e] + "px" : i)
            : void 0;
        });
      }),
      p.each({ Height: "height", Width: "width" }, function (t, e) {
        p.each(
          { padding: "inner" + t, content: e, "": "outer" + t },
          function (i, n) {
            p.fn[n] = function (n, s) {
              var o = arguments.length && (i || "boolean" != typeof n),
                r = i || (!0 === n || !0 === s ? "margin" : "border");
              return G(
                this,
                function (e, i, n) {
                  var s;
                  return p.isWindow(e)
                    ? e.document.documentElement["client" + t]
                    : 9 === e.nodeType
                    ? ((s = e.documentElement),
                      Math.max(
                        e.body["scroll" + t],
                        s["scroll" + t],
                        e.body["offset" + t],
                        s["offset" + t],
                        s["client" + t]
                      ))
                    : void 0 === n
                    ? p.css(e, i, r)
                    : p.style(e, i, n, r);
                },
                e,
                o ? n : void 0,
                o,
                null
              );
            };
          }
        );
      }),
      p.fn.extend({
        bind: function (t, e, i) {
          return this.on(t, null, e, i);
        },
        unbind: function (t, e) {
          return this.off(t, null, e);
        },
        delegate: function (t, e, i, n) {
          return this.on(e, t, i, n);
        },
        undelegate: function (t, e, i) {
          return 1 === arguments.length
            ? this.off(t, "**")
            : this.off(e, t || "**", i);
        },
      }),
      (p.fn.size = function () {
        return this.length;
      }),
      (p.fn.andSelf = p.fn.addBack),
      "function" == typeof define &&
        define.amd &&
        define("jquery", [], function () {
          return p;
        });
    var si = t.jQuery,
      oi = t.$;
    return (
      (p.noConflict = function (e) {
        return (
          t.$ === p && (t.$ = oi), e && t.jQuery === p && (t.jQuery = si), p
        );
      }),
      e || (t.jQuery = t.$ = p),
      p
    );
  }),
  (function (t, e) {
    "object" == typeof exports && "undefined" != typeof module
      ? e(exports, require("jquery"), require("popper.js"))
      : "function" == typeof define && define.amd
      ? define(["exports", "jquery", "popper.js"], e)
      : e((t.bootstrap = {}), t.jQuery, t.Popper);
  })(this, function (t, e, i) {
    "use strict";
    function n(t, e) {
      for (var i = 0; i < e.length; i++) {
        var n = e[i];
        (n.enumerable = n.enumerable || !1),
          (n.configurable = !0),
          "value" in n && (n.writable = !0),
          Object.defineProperty(t, n.key, n);
      }
    }
    function s(t, e, i) {
      return e && n(t.prototype, e), i && n(t, i), t;
    }
    function o() {
      return (o =
        Object.assign ||
        function (t) {
          for (var e = 1; e < arguments.length; e++) {
            var i = arguments[e];
            for (var n in i)
              Object.prototype.hasOwnProperty.call(i, n) && (t[n] = i[n]);
          }
          return t;
        }).apply(this, arguments);
    }
    (e = e && e.hasOwnProperty("default") ? e.default : e),
      (i = i && i.hasOwnProperty("default") ? i.default : i);
    var r,
      a,
      l,
      c,
      d,
      h,
      u,
      p,
      f,
      m,
      g,
      v,
      y,
      _,
      w,
      b,
      C,
      k,
      D = (function (t) {
        var e = !1,
          i = {
            TRANSITION_END: "bsTransitionEnd",
            getUID: function (t) {
              do {
                t += ~~(1e6 * Math.random());
              } while (document.getElementById(t));
              return t;
            },
            getSelectorFromElement: function (e) {
              var i,
                n = e.getAttribute("data-target");
              (n && "#" !== n) || (n = e.getAttribute("href") || ""),
                "#" === n.charAt(0) &&
                  ((i = n),
                  (n = i =
                    "function" == typeof t.escapeSelector
                      ? t.escapeSelector(i).substr(1)
                      : i.replace(/(:|\.|\[|\]|,|=|@)/g, "\\$1")));
              try {
                return t(document).find(n).length > 0 ? n : null;
              } catch (t) {
                return null;
              }
            },
            reflow: function (t) {
              return t.offsetHeight;
            },
            triggerTransitionEnd: function (i) {
              t(i).trigger(e.end);
            },
            supportsTransitionEnd: function () {
              return Boolean(e);
            },
            isElement: function (t) {
              return (t[0] || t).nodeType;
            },
            typeCheckConfig: function (t, e, n) {
              for (var s in n)
                if (Object.prototype.hasOwnProperty.call(n, s)) {
                  var o = n[s],
                    r = e[s],
                    a =
                      r && i.isElement(r)
                        ? "element"
                        : ((l = r),
                          {}.toString
                            .call(l)
                            .match(/\s([a-zA-Z]+)/)[1]
                            .toLowerCase());
                  if (!new RegExp(o).test(a))
                    throw new Error(
                      t.toUpperCase() +
                        ': Option "' +
                        s +
                        '" provided type "' +
                        a +
                        '" but expected type "' +
                        o +
                        '".'
                    );
                }
              var l;
            },
          };
        return (
          (e = ("undefined" == typeof window || !window.QUnit) && {
            end: "transitionend",
          }),
          (t.fn.emulateTransitionEnd = function (e) {
            var n = this,
              s = !1;
            return (
              t(this).one(i.TRANSITION_END, function () {
                s = !0;
              }),
              setTimeout(function () {
                s || i.triggerTransitionEnd(n);
              }, e),
              this
            );
          }),
          i.supportsTransitionEnd() &&
            (t.event.special[i.TRANSITION_END] = {
              bindType: e.end,
              delegateType: e.end,
              handle: function (e) {
                if (t(e.target).is(this))
                  return e.handleObj.handler.apply(this, arguments);
              },
            }),
          i
        );
      })(e),
      T =
        ((a = "alert"),
        (c = "." + (l = "bs.alert")),
        (d = (r = e).fn[a]),
        (h = {
          CLOSE: "close" + c,
          CLOSED: "closed" + c,
          CLICK_DATA_API: "click" + c + ".data-api",
        }),
        (u = (function () {
          function t(t) {
            this._element = t;
          }
          var e = t.prototype;
          return (
            (e.close = function (t) {
              t = t || this._element;
              var e = this._getRootElement(t);
              this._triggerCloseEvent(e).isDefaultPrevented() ||
                this._removeElement(e);
            }),
            (e.dispose = function () {
              r.removeData(this._element, l), (this._element = null);
            }),
            (e._getRootElement = function (t) {
              var e = D.getSelectorFromElement(t),
                i = !1;
              return (
                e && (i = r(e)[0]), i || (i = r(t).closest(".alert")[0]), i
              );
            }),
            (e._triggerCloseEvent = function (t) {
              var e = r.Event(h.CLOSE);
              return r(t).trigger(e), e;
            }),
            (e._removeElement = function (t) {
              var e = this;
              r(t).removeClass("show"),
                D.supportsTransitionEnd() && r(t).hasClass("fade")
                  ? r(t)
                      .one(D.TRANSITION_END, function (i) {
                        return e._destroyElement(t, i);
                      })
                      .emulateTransitionEnd(150)
                  : this._destroyElement(t);
            }),
            (e._destroyElement = function (t) {
              r(t).detach().trigger(h.CLOSED).remove();
            }),
            (t._jQueryInterface = function (e) {
              return this.each(function () {
                var i = r(this),
                  n = i.data(l);
                n || ((n = new t(this)), i.data(l, n)),
                  "close" === e && n[e](this);
              });
            }),
            (t._handleDismiss = function (t) {
              return function (e) {
                e && e.preventDefault(), t.close(this);
              };
            }),
            s(t, null, [
              {
                key: "VERSION",
                get: function () {
                  return "4.0.0";
                },
              },
            ]),
            t
          );
        })()),
        r(document).on(
          h.CLICK_DATA_API,
          '[data-dismiss="alert"]',
          u._handleDismiss(new u())
        ),
        (r.fn[a] = u._jQueryInterface),
        (r.fn[a].Constructor = u),
        (r.fn[a].noConflict = function () {
          return (r.fn[a] = d), u._jQueryInterface;
        }),
        u),
      x =
        ((f = "button"),
        (g = "." + (m = "bs.button")),
        (v = ".data-api"),
        (y = (p = e).fn[f]),
        (_ = "active"),
        (w = '[data-toggle^="button"]'),
        (b = ".btn"),
        (C = {
          CLICK_DATA_API: "click" + g + v,
          FOCUS_BLUR_DATA_API: "focus" + g + v + " blur" + g + v,
        }),
        (k = (function () {
          function t(t) {
            this._element = t;
          }
          var e = t.prototype;
          return (
            (e.toggle = function () {
              var t = !0,
                e = !0,
                i = p(this._element).closest('[data-toggle="buttons"]')[0];
              if (i) {
                var n = p(this._element).find("input")[0];
                if (n) {
                  if ("radio" === n.type)
                    if (n.checked && p(this._element).hasClass(_)) t = !1;
                    else {
                      var s = p(i).find(".active")[0];
                      s && p(s).removeClass(_);
                    }
                  if (t) {
                    if (
                      n.hasAttribute("disabled") ||
                      i.hasAttribute("disabled") ||
                      n.classList.contains("disabled") ||
                      i.classList.contains("disabled")
                    )
                      return;
                    (n.checked = !p(this._element).hasClass(_)),
                      p(n).trigger("change");
                  }
                  n.focus(), (e = !1);
                }
              }
              e &&
                this._element.setAttribute(
                  "aria-pressed",
                  !p(this._element).hasClass(_)
                ),
                t && p(this._element).toggleClass(_);
            }),
            (e.dispose = function () {
              p.removeData(this._element, m), (this._element = null);
            }),
            (t._jQueryInterface = function (e) {
              return this.each(function () {
                var i = p(this).data(m);
                i || ((i = new t(this)), p(this).data(m, i)),
                  "toggle" === e && i[e]();
              });
            }),
            s(t, null, [
              {
                key: "VERSION",
                get: function () {
                  return "4.0.0";
                },
              },
            ]),
            t
          );
        })()),
        p(document)
          .on(C.CLICK_DATA_API, w, function (t) {
            t.preventDefault();
            var e = t.target;
            p(e).hasClass("btn") || (e = p(e).closest(b)),
              k._jQueryInterface.call(p(e), "toggle");
          })
          .on(C.FOCUS_BLUR_DATA_API, w, function (t) {
            var e = p(t.target).closest(b)[0];
            p(e).toggleClass("focus", /^focus(in)?$/.test(t.type));
          }),
        (p.fn[f] = k._jQueryInterface),
        (p.fn[f].Constructor = k),
        (p.fn[f].noConflict = function () {
          return (p.fn[f] = y), k._jQueryInterface;
        }),
        k),
      S = (function (t) {
        var e = "carousel",
          i = "bs.carousel",
          n = "." + i,
          r = t.fn[e],
          a = {
            interval: 5e3,
            keyboard: !0,
            slide: !1,
            pause: "hover",
            wrap: !0,
          },
          l = {
            interval: "(number|boolean)",
            keyboard: "boolean",
            slide: "(boolean|string)",
            pause: "(string|boolean)",
            wrap: "boolean",
          },
          c = "next",
          d = "prev",
          h = {
            SLIDE: "slide" + n,
            SLID: "slid" + n,
            KEYDOWN: "keydown" + n,
            MOUSEENTER: "mouseenter" + n,
            MOUSELEAVE: "mouseleave" + n,
            TOUCHEND: "touchend" + n,
            LOAD_DATA_API: "load" + n + ".data-api",
            CLICK_DATA_API: "click" + n + ".data-api",
          },
          u = "active",
          p = {
            ACTIVE: ".active",
            ACTIVE_ITEM: ".active.carousel-item",
            ITEM: ".carousel-item",
            NEXT_PREV: ".carousel-item-next, .carousel-item-prev",
            INDICATORS: ".carousel-indicators",
            DATA_SLIDE: "[data-slide], [data-slide-to]",
            DATA_RIDE: '[data-ride="carousel"]',
          },
          f = (function () {
            function r(e, i) {
              (this._items = null),
                (this._interval = null),
                (this._activeElement = null),
                (this._isPaused = !1),
                (this._isSliding = !1),
                (this.touchTimeout = null),
                (this._config = this._getConfig(i)),
                (this._element = t(e)[0]),
                (this._indicatorsElement = t(this._element).find(
                  p.INDICATORS
                )[0]),
                this._addEventListeners();
            }
            var f = r.prototype;
            return (
              (f.next = function () {
                this._isSliding || this._slide(c);
              }),
              (f.nextWhenVisible = function () {
                !document.hidden &&
                  t(this._element).is(":visible") &&
                  "hidden" !== t(this._element).css("visibility") &&
                  this.next();
              }),
              (f.prev = function () {
                this._isSliding || this._slide(d);
              }),
              (f.pause = function (e) {
                e || (this._isPaused = !0),
                  t(this._element).find(p.NEXT_PREV)[0] &&
                    D.supportsTransitionEnd() &&
                    (D.triggerTransitionEnd(this._element), this.cycle(!0)),
                  clearInterval(this._interval),
                  (this._interval = null);
              }),
              (f.cycle = function (t) {
                t || (this._isPaused = !1),
                  this._interval &&
                    (clearInterval(this._interval), (this._interval = null)),
                  this._config.interval &&
                    !this._isPaused &&
                    (this._interval = setInterval(
                      (document.visibilityState
                        ? this.nextWhenVisible
                        : this.next
                      ).bind(this),
                      this._config.interval
                    ));
              }),
              (f.to = function (e) {
                var i = this;
                this._activeElement = t(this._element).find(p.ACTIVE_ITEM)[0];
                var n = this._getItemIndex(this._activeElement);
                if (!(e > this._items.length - 1 || e < 0))
                  if (this._isSliding)
                    t(this._element).one(h.SLID, function () {
                      return i.to(e);
                    });
                  else {
                    if (n === e) return this.pause(), void this.cycle();
                    var s = e > n ? c : d;
                    this._slide(s, this._items[e]);
                  }
              }),
              (f.dispose = function () {
                t(this._element).off(n),
                  t.removeData(this._element, i),
                  (this._items = null),
                  (this._config = null),
                  (this._element = null),
                  (this._interval = null),
                  (this._isPaused = null),
                  (this._isSliding = null),
                  (this._activeElement = null),
                  (this._indicatorsElement = null);
              }),
              (f._getConfig = function (t) {
                return (t = o({}, a, t)), D.typeCheckConfig(e, t, l), t;
              }),
              (f._addEventListeners = function () {
                var e = this;
                this._config.keyboard &&
                  t(this._element).on(h.KEYDOWN, function (t) {
                    return e._keydown(t);
                  }),
                  "hover" === this._config.pause &&
                    (t(this._element)
                      .on(h.MOUSEENTER, function (t) {
                        return e.pause(t);
                      })
                      .on(h.MOUSELEAVE, function (t) {
                        return e.cycle(t);
                      }),
                    "ontouchstart" in document.documentElement &&
                      t(this._element).on(h.TOUCHEND, function () {
                        e.pause(),
                          e.touchTimeout && clearTimeout(e.touchTimeout),
                          (e.touchTimeout = setTimeout(function (t) {
                            return e.cycle(t);
                          }, 500 + e._config.interval));
                      }));
              }),
              (f._keydown = function (t) {
                if (!/input|textarea/i.test(t.target.tagName))
                  switch (t.which) {
                    case 37:
                      t.preventDefault(), this.prev();
                      break;
                    case 39:
                      t.preventDefault(), this.next();
                  }
              }),
              (f._getItemIndex = function (e) {
                return (
                  (this._items = t.makeArray(t(e).parent().find(p.ITEM))),
                  this._items.indexOf(e)
                );
              }),
              (f._getItemByDirection = function (t, e) {
                var i = t === c,
                  n = t === d,
                  s = this._getItemIndex(e),
                  o = this._items.length - 1;
                if (((n && 0 === s) || (i && s === o)) && !this._config.wrap)
                  return e;
                var r = (s + (t === d ? -1 : 1)) % this._items.length;
                return -1 === r
                  ? this._items[this._items.length - 1]
                  : this._items[r];
              }),
              (f._triggerSlideEvent = function (e, i) {
                var n = this._getItemIndex(e),
                  s = this._getItemIndex(
                    t(this._element).find(p.ACTIVE_ITEM)[0]
                  ),
                  o = t.Event(h.SLIDE, {
                    relatedTarget: e,
                    direction: i,
                    from: s,
                    to: n,
                  });
                return t(this._element).trigger(o), o;
              }),
              (f._setActiveIndicatorElement = function (e) {
                if (this._indicatorsElement) {
                  t(this._indicatorsElement).find(p.ACTIVE).removeClass(u);
                  var i =
                    this._indicatorsElement.children[this._getItemIndex(e)];
                  i && t(i).addClass(u);
                }
              }),
              (f._slide = function (e, i) {
                var n,
                  s,
                  o,
                  r = this,
                  a = t(this._element).find(p.ACTIVE_ITEM)[0],
                  l = this._getItemIndex(a),
                  d = i || (a && this._getItemByDirection(e, a)),
                  f = this._getItemIndex(d),
                  m = Boolean(this._interval);
                if (
                  (e === c
                    ? ((n = "carousel-item-left"),
                      (s = "carousel-item-next"),
                      (o = "left"))
                    : ((n = "carousel-item-right"),
                      (s = "carousel-item-prev"),
                      (o = "right")),
                  d && t(d).hasClass(u))
                )
                  this._isSliding = !1;
                else if (
                  !this._triggerSlideEvent(d, o).isDefaultPrevented() &&
                  a &&
                  d
                ) {
                  (this._isSliding = !0),
                    m && this.pause(),
                    this._setActiveIndicatorElement(d);
                  var g = t.Event(h.SLID, {
                    relatedTarget: d,
                    direction: o,
                    from: l,
                    to: f,
                  });
                  D.supportsTransitionEnd() &&
                  t(this._element).hasClass("slide")
                    ? (t(d).addClass(s),
                      D.reflow(d),
                      t(a).addClass(n),
                      t(d).addClass(n),
                      t(a)
                        .one(D.TRANSITION_END, function () {
                          t(d)
                            .removeClass(n + " " + s)
                            .addClass(u),
                            t(a).removeClass(u + " " + s + " " + n),
                            (r._isSliding = !1),
                            setTimeout(function () {
                              return t(r._element).trigger(g);
                            }, 0);
                        })
                        .emulateTransitionEnd(600))
                    : (t(a).removeClass(u),
                      t(d).addClass(u),
                      (this._isSliding = !1),
                      t(this._element).trigger(g)),
                    m && this.cycle();
                }
              }),
              (r._jQueryInterface = function (e) {
                return this.each(function () {
                  var n = t(this).data(i),
                    s = o({}, a, t(this).data());
                  "object" == typeof e && (s = o({}, s, e));
                  var l = "string" == typeof e ? e : s.slide;
                  if (
                    (n || ((n = new r(this, s)), t(this).data(i, n)),
                    "number" == typeof e)
                  )
                    n.to(e);
                  else if ("string" == typeof l) {
                    if (void 0 === n[l])
                      throw new TypeError('No method named "' + l + '"');
                    n[l]();
                  } else s.interval && (n.pause(), n.cycle());
                });
              }),
              (r._dataApiClickHandler = function (e) {
                var n = D.getSelectorFromElement(this);
                if (n) {
                  var s = t(n)[0];
                  if (s && t(s).hasClass("carousel")) {
                    var a = o({}, t(s).data(), t(this).data()),
                      l = this.getAttribute("data-slide-to");
                    l && (a.interval = !1),
                      r._jQueryInterface.call(t(s), a),
                      l && t(s).data(i).to(l),
                      e.preventDefault();
                  }
                }
              }),
              s(r, null, [
                {
                  key: "VERSION",
                  get: function () {
                    return "4.0.0";
                  },
                },
                {
                  key: "Default",
                  get: function () {
                    return a;
                  },
                },
              ]),
              r
            );
          })();
        return (
          t(document).on(
            h.CLICK_DATA_API,
            p.DATA_SLIDE,
            f._dataApiClickHandler
          ),
          t(window).on(h.LOAD_DATA_API, function () {
            t(p.DATA_RIDE).each(function () {
              var e = t(this);
              f._jQueryInterface.call(e, e.data());
            });
          }),
          (t.fn[e] = f._jQueryInterface),
          (t.fn[e].Constructor = f),
          (t.fn[e].noConflict = function () {
            return (t.fn[e] = r), f._jQueryInterface;
          }),
          f
        );
      })(e),
      E = (function (t) {
        var e = "collapse",
          i = "bs.collapse",
          n = "." + i,
          r = t.fn[e],
          a = { toggle: !0, parent: "" },
          l = { toggle: "boolean", parent: "(string|element)" },
          c = {
            SHOW: "show" + n,
            SHOWN: "shown" + n,
            HIDE: "hide" + n,
            HIDDEN: "hidden" + n,
            CLICK_DATA_API: "click" + n + ".data-api",
          },
          d = "show",
          h = "collapse",
          u = "collapsing",
          p = "collapsed",
          f = "width",
          m = {
            ACTIVES: ".show, .collapsing",
            DATA_TOGGLE: '[data-toggle="collapse"]',
          },
          g = (function () {
            function n(e, i) {
              (this._isTransitioning = !1),
                (this._element = e),
                (this._config = this._getConfig(i)),
                (this._triggerArray = t.makeArray(
                  t(
                    '[data-toggle="collapse"][href="#' +
                      e.id +
                      '"],[data-toggle="collapse"][data-target="#' +
                      e.id +
                      '"]'
                  )
                ));
              for (var n = t(m.DATA_TOGGLE), s = 0; s < n.length; s++) {
                var o = n[s],
                  r = D.getSelectorFromElement(o);
                null !== r &&
                  t(r).filter(e).length > 0 &&
                  ((this._selector = r), this._triggerArray.push(o));
              }
              (this._parent = this._config.parent ? this._getParent() : null),
                this._config.parent ||
                  this._addAriaAndCollapsedClass(
                    this._element,
                    this._triggerArray
                  ),
                this._config.toggle && this.toggle();
            }
            var r = n.prototype;
            return (
              (r.toggle = function () {
                t(this._element).hasClass(d) ? this.hide() : this.show();
              }),
              (r.show = function () {
                var e,
                  s,
                  o = this;
                if (
                  !(
                    this._isTransitioning ||
                    t(this._element).hasClass(d) ||
                    (this._parent &&
                      0 ===
                        (e = t.makeArray(
                          t(this._parent)
                            .find(m.ACTIVES)
                            .filter(
                              '[data-parent="' + this._config.parent + '"]'
                            )
                        )).length &&
                      (e = null),
                    e &&
                      (s = t(e).not(this._selector).data(i)) &&
                      s._isTransitioning)
                  )
                ) {
                  var r = t.Event(c.SHOW);
                  if ((t(this._element).trigger(r), !r.isDefaultPrevented())) {
                    e &&
                      (n._jQueryInterface.call(
                        t(e).not(this._selector),
                        "hide"
                      ),
                      s || t(e).data(i, null));
                    var a = this._getDimension();
                    t(this._element).removeClass(h).addClass(u),
                      (this._element.style[a] = 0),
                      this._triggerArray.length > 0 &&
                        t(this._triggerArray)
                          .removeClass(p)
                          .attr("aria-expanded", !0),
                      this.setTransitioning(!0);
                    var l = function () {
                      t(o._element).removeClass(u).addClass(h).addClass(d),
                        (o._element.style[a] = ""),
                        o.setTransitioning(!1),
                        t(o._element).trigger(c.SHOWN);
                    };
                    if (D.supportsTransitionEnd()) {
                      var f = "scroll" + (a[0].toUpperCase() + a.slice(1));
                      t(this._element)
                        .one(D.TRANSITION_END, l)
                        .emulateTransitionEnd(600),
                        (this._element.style[a] = this._element[f] + "px");
                    } else l();
                  }
                }
              }),
              (r.hide = function () {
                var e = this;
                if (!this._isTransitioning && t(this._element).hasClass(d)) {
                  var i = t.Event(c.HIDE);
                  if ((t(this._element).trigger(i), !i.isDefaultPrevented())) {
                    var n = this._getDimension();
                    if (
                      ((this._element.style[n] =
                        this._element.getBoundingClientRect()[n] + "px"),
                      D.reflow(this._element),
                      t(this._element)
                        .addClass(u)
                        .removeClass(h)
                        .removeClass(d),
                      this._triggerArray.length > 0)
                    )
                      for (var s = 0; s < this._triggerArray.length; s++) {
                        var o = this._triggerArray[s],
                          r = D.getSelectorFromElement(o);
                        null !== r &&
                          (t(r).hasClass(d) ||
                            t(o).addClass(p).attr("aria-expanded", !1));
                      }
                    this.setTransitioning(!0);
                    var a = function () {
                      e.setTransitioning(!1),
                        t(e._element)
                          .removeClass(u)
                          .addClass(h)
                          .trigger(c.HIDDEN);
                    };
                    (this._element.style[n] = ""),
                      D.supportsTransitionEnd()
                        ? t(this._element)
                            .one(D.TRANSITION_END, a)
                            .emulateTransitionEnd(600)
                        : a();
                  }
                }
              }),
              (r.setTransitioning = function (t) {
                this._isTransitioning = t;
              }),
              (r.dispose = function () {
                t.removeData(this._element, i),
                  (this._config = null),
                  (this._parent = null),
                  (this._element = null),
                  (this._triggerArray = null),
                  (this._isTransitioning = null);
              }),
              (r._getConfig = function (t) {
                return (
                  ((t = o({}, a, t)).toggle = Boolean(t.toggle)),
                  D.typeCheckConfig(e, t, l),
                  t
                );
              }),
              (r._getDimension = function () {
                return t(this._element).hasClass(f) ? f : "height";
              }),
              (r._getParent = function () {
                var e = this,
                  i = null;
                D.isElement(this._config.parent)
                  ? ((i = this._config.parent),
                    void 0 !== this._config.parent.jquery &&
                      (i = this._config.parent[0]))
                  : (i = t(this._config.parent)[0]);
                var s =
                  '[data-toggle="collapse"][data-parent="' +
                  this._config.parent +
                  '"]';
                return (
                  t(i)
                    .find(s)
                    .each(function (t, i) {
                      e._addAriaAndCollapsedClass(n._getTargetFromElement(i), [
                        i,
                      ]);
                    }),
                  i
                );
              }),
              (r._addAriaAndCollapsedClass = function (e, i) {
                if (e) {
                  var n = t(e).hasClass(d);
                  i.length > 0 &&
                    t(i).toggleClass(p, !n).attr("aria-expanded", n);
                }
              }),
              (n._getTargetFromElement = function (e) {
                var i = D.getSelectorFromElement(e);
                return i ? t(i)[0] : null;
              }),
              (n._jQueryInterface = function (e) {
                return this.each(function () {
                  var s = t(this),
                    r = s.data(i),
                    l = o({}, a, s.data(), "object" == typeof e && e);
                  if (
                    (!r && l.toggle && /show|hide/.test(e) && (l.toggle = !1),
                    r || ((r = new n(this, l)), s.data(i, r)),
                    "string" == typeof e)
                  ) {
                    if (void 0 === r[e])
                      throw new TypeError('No method named "' + e + '"');
                    r[e]();
                  }
                });
              }),
              s(n, null, [
                {
                  key: "VERSION",
                  get: function () {
                    return "4.0.0";
                  },
                },
                {
                  key: "Default",
                  get: function () {
                    return a;
                  },
                },
              ]),
              n
            );
          })();
        return (
          t(document).on(c.CLICK_DATA_API, m.DATA_TOGGLE, function (e) {
            "A" === e.currentTarget.tagName && e.preventDefault();
            var n = t(this),
              s = D.getSelectorFromElement(this);
            t(s).each(function () {
              var e = t(this),
                s = e.data(i) ? "toggle" : n.data();
              g._jQueryInterface.call(e, s);
            });
          }),
          (t.fn[e] = g._jQueryInterface),
          (t.fn[e].Constructor = g),
          (t.fn[e].noConflict = function () {
            return (t.fn[e] = r), g._jQueryInterface;
          }),
          g
        );
      })(e),
      A = (function (t) {
        var e = "dropdown",
          n = "bs.dropdown",
          r = "." + n,
          a = ".data-api",
          l = t.fn[e],
          c = new RegExp("38|40|27"),
          d = {
            HIDE: "hide" + r,
            HIDDEN: "hidden" + r,
            SHOW: "show" + r,
            SHOWN: "shown" + r,
            CLICK: "click" + r,
            CLICK_DATA_API: "click" + r + a,
            KEYDOWN_DATA_API: "keydown" + r + a,
            KEYUP_DATA_API: "keyup" + r + a,
          },
          h = "disabled",
          u = "show",
          p = "dropup",
          f = "dropdown-menu-right",
          m = '[data-toggle="dropdown"]',
          g = ".dropdown-menu",
          v = { offset: 0, flip: !0, boundary: "scrollParent" },
          y = {
            offset: "(number|string|function)",
            flip: "boolean",
            boundary: "(string|element)",
          },
          _ = (function () {
            function a(t, e) {
              (this._element = t),
                (this._popper = null),
                (this._config = this._getConfig(e)),
                (this._menu = this._getMenuElement()),
                (this._inNavbar = this._detectNavbar()),
                this._addEventListeners();
            }
            var l = a.prototype;
            return (
              (l.toggle = function () {
                if (!this._element.disabled && !t(this._element).hasClass(h)) {
                  var e = a._getParentFromElement(this._element),
                    n = t(this._menu).hasClass(u);
                  if ((a._clearMenus(), !n)) {
                    var s = { relatedTarget: this._element },
                      o = t.Event(d.SHOW, s);
                    if ((t(e).trigger(o), !o.isDefaultPrevented())) {
                      if (!this._inNavbar) {
                        if (void 0 === i)
                          throw new TypeError(
                            "Bootstrap dropdown require Popper.js (https://popper.js.org)"
                          );
                        var r = this._element;
                        t(e).hasClass(p) &&
                          (t(this._menu).hasClass("dropdown-menu-left") ||
                            t(this._menu).hasClass(f)) &&
                          (r = e),
                          "scrollParent" !== this._config.boundary &&
                            t(e).addClass("position-static"),
                          (this._popper = new i(
                            r,
                            this._menu,
                            this._getPopperConfig()
                          ));
                      }
                      "ontouchstart" in document.documentElement &&
                        0 === t(e).closest(".navbar-nav").length &&
                        t("body").children().on("mouseover", null, t.noop),
                        this._element.focus(),
                        this._element.setAttribute("aria-expanded", !0),
                        t(this._menu).toggleClass(u),
                        t(e).toggleClass(u).trigger(t.Event(d.SHOWN, s));
                    }
                  }
                }
              }),
              (l.dispose = function () {
                t.removeData(this._element, n),
                  t(this._element).off(r),
                  (this._element = null),
                  (this._menu = null),
                  null !== this._popper &&
                    (this._popper.destroy(), (this._popper = null));
              }),
              (l.update = function () {
                (this._inNavbar = this._detectNavbar()),
                  null !== this._popper && this._popper.scheduleUpdate();
              }),
              (l._addEventListeners = function () {
                var e = this;
                t(this._element).on(d.CLICK, function (t) {
                  t.preventDefault(), t.stopPropagation(), e.toggle();
                });
              }),
              (l._getConfig = function (i) {
                return (
                  (i = o(
                    {},
                    this.constructor.Default,
                    t(this._element).data(),
                    i
                  )),
                  D.typeCheckConfig(e, i, this.constructor.DefaultType),
                  i
                );
              }),
              (l._getMenuElement = function () {
                if (!this._menu) {
                  var e = a._getParentFromElement(this._element);
                  this._menu = t(e).find(g)[0];
                }
                return this._menu;
              }),
              (l._getPlacement = function () {
                var e = t(this._element).parent(),
                  i = "bottom-start";
                return (
                  e.hasClass(p)
                    ? ((i = "top-start"),
                      t(this._menu).hasClass(f) && (i = "top-end"))
                    : e.hasClass("dropright")
                    ? (i = "right-start")
                    : e.hasClass("dropleft")
                    ? (i = "left-start")
                    : t(this._menu).hasClass(f) && (i = "bottom-end"),
                  i
                );
              }),
              (l._detectNavbar = function () {
                return t(this._element).closest(".navbar").length > 0;
              }),
              (l._getPopperConfig = function () {
                var t = this,
                  e = {};
                return (
                  "function" == typeof this._config.offset
                    ? (e.fn = function (e) {
                        return (
                          (e.offsets = o(
                            {},
                            e.offsets,
                            t._config.offset(e.offsets) || {}
                          )),
                          e
                        );
                      })
                    : (e.offset = this._config.offset),
                  {
                    placement: this._getPlacement(),
                    modifiers: {
                      offset: e,
                      flip: { enabled: this._config.flip },
                      preventOverflow: {
                        boundariesElement: this._config.boundary,
                      },
                    },
                  }
                );
              }),
              (a._jQueryInterface = function (e) {
                return this.each(function () {
                  var i = t(this).data(n);
                  if (
                    (i ||
                      ((i = new a(this, "object" == typeof e ? e : null)),
                      t(this).data(n, i)),
                    "string" == typeof e)
                  ) {
                    if (void 0 === i[e])
                      throw new TypeError('No method named "' + e + '"');
                    i[e]();
                  }
                });
              }),
              (a._clearMenus = function (e) {
                if (
                  !e ||
                  (3 !== e.which && ("keyup" !== e.type || 9 === e.which))
                )
                  for (var i = t.makeArray(t(m)), s = 0; s < i.length; s++) {
                    var o = a._getParentFromElement(i[s]),
                      r = t(i[s]).data(n),
                      l = { relatedTarget: i[s] };
                    if (r) {
                      var c = r._menu;
                      if (
                        t(o).hasClass(u) &&
                        !(
                          e &&
                          (("click" === e.type &&
                            /input|textarea/i.test(e.target.tagName)) ||
                            ("keyup" === e.type && 9 === e.which)) &&
                          t.contains(o, e.target)
                        )
                      ) {
                        var h = t.Event(d.HIDE, l);
                        t(o).trigger(h),
                          h.isDefaultPrevented() ||
                            ("ontouchstart" in document.documentElement &&
                              t("body")
                                .children()
                                .off("mouseover", null, t.noop),
                            i[s].setAttribute("aria-expanded", "false"),
                            t(c).removeClass(u),
                            t(o).removeClass(u).trigger(t.Event(d.HIDDEN, l)));
                      }
                    }
                  }
              }),
              (a._getParentFromElement = function (e) {
                var i,
                  n = D.getSelectorFromElement(e);
                return n && (i = t(n)[0]), i || e.parentNode;
              }),
              (a._dataApiKeydownHandler = function (e) {
                if (
                  (/input|textarea/i.test(e.target.tagName)
                    ? !(
                        32 === e.which ||
                        (27 !== e.which &&
                          ((40 !== e.which && 38 !== e.which) ||
                            t(e.target).closest(g).length))
                      )
                    : c.test(e.which)) &&
                  (e.preventDefault(),
                  e.stopPropagation(),
                  !this.disabled && !t(this).hasClass(h))
                ) {
                  var i = a._getParentFromElement(this),
                    n = t(i).hasClass(u);
                  if (
                    (n || (27 === e.which && 32 === e.which)) &&
                    (!n || (27 !== e.which && 32 !== e.which))
                  ) {
                    var s = t(i)
                      .find(".dropdown-menu .dropdown-item:not(.disabled)")
                      .get();
                    if (0 !== s.length) {
                      var o = s.indexOf(e.target);
                      38 === e.which && o > 0 && o--,
                        40 === e.which && o < s.length - 1 && o++,
                        o < 0 && (o = 0),
                        s[o].focus();
                    }
                  } else {
                    if (27 === e.which) {
                      var r = t(i).find(m)[0];
                      t(r).trigger("focus");
                    }
                    t(this).trigger("click");
                  }
                }
              }),
              s(a, null, [
                {
                  key: "VERSION",
                  get: function () {
                    return "4.0.0";
                  },
                },
                {
                  key: "Default",
                  get: function () {
                    return v;
                  },
                },
                {
                  key: "DefaultType",
                  get: function () {
                    return y;
                  },
                },
              ]),
              a
            );
          })();
        return (
          t(document)
            .on(d.KEYDOWN_DATA_API, m, _._dataApiKeydownHandler)
            .on(d.KEYDOWN_DATA_API, g, _._dataApiKeydownHandler)
            .on(d.CLICK_DATA_API + " " + d.KEYUP_DATA_API, _._clearMenus)
            .on(d.CLICK_DATA_API, m, function (e) {
              e.preventDefault(),
                e.stopPropagation(),
                _._jQueryInterface.call(t(this), "toggle");
            })
            .on(d.CLICK_DATA_API, ".dropdown form", function (t) {
              t.stopPropagation();
            }),
          (t.fn[e] = _._jQueryInterface),
          (t.fn[e].Constructor = _),
          (t.fn[e].noConflict = function () {
            return (t.fn[e] = l), _._jQueryInterface;
          }),
          _
        );
      })(e),
      M = (function (t) {
        var e = "bs.modal",
          i = "." + e,
          n = t.fn.modal,
          r = { backdrop: !0, keyboard: !0, focus: !0, show: !0 },
          a = {
            backdrop: "(boolean|string)",
            keyboard: "boolean",
            focus: "boolean",
            show: "boolean",
          },
          l = {
            HIDE: "hide" + i,
            HIDDEN: "hidden" + i,
            SHOW: "show" + i,
            SHOWN: "shown" + i,
            FOCUSIN: "focusin" + i,
            RESIZE: "resize" + i,
            CLICK_DISMISS: "click.dismiss" + i,
            KEYDOWN_DISMISS: "keydown.dismiss" + i,
            MOUSEUP_DISMISS: "mouseup.dismiss" + i,
            MOUSEDOWN_DISMISS: "mousedown.dismiss" + i,
            CLICK_DATA_API: "click" + i + ".data-api",
          },
          c = "modal-open",
          d = "fade",
          h = "show",
          u = {
            DIALOG: ".modal-dialog",
            DATA_TOGGLE: '[data-toggle="modal"]',
            DATA_DISMISS: '[data-dismiss="modal"]',
            FIXED_CONTENT: ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",
            STICKY_CONTENT: ".sticky-top",
            NAVBAR_TOGGLER: ".navbar-toggler",
          },
          p = (function () {
            function n(e, i) {
              (this._config = this._getConfig(i)),
                (this._element = e),
                (this._dialog = t(e).find(u.DIALOG)[0]),
                (this._backdrop = null),
                (this._isShown = !1),
                (this._isBodyOverflowing = !1),
                (this._ignoreBackdropClick = !1),
                (this._originalBodyPadding = 0),
                (this._scrollbarWidth = 0);
            }
            var p = n.prototype;
            return (
              (p.toggle = function (t) {
                return this._isShown ? this.hide() : this.show(t);
              }),
              (p.show = function (e) {
                var i = this;
                if (!this._isTransitioning && !this._isShown) {
                  D.supportsTransitionEnd() &&
                    t(this._element).hasClass(d) &&
                    (this._isTransitioning = !0);
                  var n = t.Event(l.SHOW, { relatedTarget: e });
                  t(this._element).trigger(n),
                    this._isShown ||
                      n.isDefaultPrevented() ||
                      ((this._isShown = !0),
                      this._checkScrollbar(),
                      this._setScrollbar(),
                      this._adjustDialog(),
                      t(document.body).addClass(c),
                      this._setEscapeEvent(),
                      this._setResizeEvent(),
                      t(this._element).on(
                        l.CLICK_DISMISS,
                        u.DATA_DISMISS,
                        function (t) {
                          return i.hide(t);
                        }
                      ),
                      t(this._dialog).on(l.MOUSEDOWN_DISMISS, function () {
                        t(i._element).one(l.MOUSEUP_DISMISS, function (e) {
                          t(e.target).is(i._element) &&
                            (i._ignoreBackdropClick = !0);
                        });
                      }),
                      this._showBackdrop(function () {
                        return i._showElement(e);
                      }));
                }
              }),
              (p.hide = function (e) {
                var i = this;
                if (
                  (e && e.preventDefault(),
                  !this._isTransitioning && this._isShown)
                ) {
                  var n = t.Event(l.HIDE);
                  if (
                    (t(this._element).trigger(n),
                    this._isShown && !n.isDefaultPrevented())
                  ) {
                    this._isShown = !1;
                    var s =
                      D.supportsTransitionEnd() && t(this._element).hasClass(d);
                    s && (this._isTransitioning = !0),
                      this._setEscapeEvent(),
                      this._setResizeEvent(),
                      t(document).off(l.FOCUSIN),
                      t(this._element).removeClass(h),
                      t(this._element).off(l.CLICK_DISMISS),
                      t(this._dialog).off(l.MOUSEDOWN_DISMISS),
                      s
                        ? t(this._element)
                            .one(D.TRANSITION_END, function (t) {
                              return i._hideModal(t);
                            })
                            .emulateTransitionEnd(300)
                        : this._hideModal();
                  }
                }
              }),
              (p.dispose = function () {
                t.removeData(this._element, e),
                  t(window, document, this._element, this._backdrop).off(i),
                  (this._config = null),
                  (this._element = null),
                  (this._dialog = null),
                  (this._backdrop = null),
                  (this._isShown = null),
                  (this._isBodyOverflowing = null),
                  (this._ignoreBackdropClick = null),
                  (this._scrollbarWidth = null);
              }),
              (p.handleUpdate = function () {
                this._adjustDialog();
              }),
              (p._getConfig = function (t) {
                return (t = o({}, r, t)), D.typeCheckConfig("modal", t, a), t;
              }),
              (p._showElement = function (e) {
                var i = this,
                  n = D.supportsTransitionEnd() && t(this._element).hasClass(d);
                (this._element.parentNode &&
                  this._element.parentNode.nodeType === Node.ELEMENT_NODE) ||
                  document.body.appendChild(this._element),
                  (this._element.style.display = "block"),
                  this._element.removeAttribute("aria-hidden"),
                  (this._element.scrollTop = 0),
                  n && D.reflow(this._element),
                  t(this._element).addClass(h),
                  this._config.focus && this._enforceFocus();
                var s = t.Event(l.SHOWN, { relatedTarget: e }),
                  o = function () {
                    i._config.focus && i._element.focus(),
                      (i._isTransitioning = !1),
                      t(i._element).trigger(s);
                  };
                n
                  ? t(this._dialog)
                      .one(D.TRANSITION_END, o)
                      .emulateTransitionEnd(300)
                  : o();
              }),
              (p._enforceFocus = function () {
                var e = this;
                t(document)
                  .off(l.FOCUSIN)
                  .on(l.FOCUSIN, function (i) {
                    document !== i.target &&
                      e._element !== i.target &&
                      0 === t(e._element).has(i.target).length &&
                      e._element.focus();
                  });
              }),
              (p._setEscapeEvent = function () {
                var e = this;
                this._isShown && this._config.keyboard
                  ? t(this._element).on(l.KEYDOWN_DISMISS, function (t) {
                      27 === t.which && (t.preventDefault(), e.hide());
                    })
                  : this._isShown || t(this._element).off(l.KEYDOWN_DISMISS);
              }),
              (p._setResizeEvent = function () {
                var e = this;
                this._isShown
                  ? t(window).on(l.RESIZE, function (t) {
                      return e.handleUpdate(t);
                    })
                  : t(window).off(l.RESIZE);
              }),
              (p._hideModal = function () {
                var e = this;
                (this._element.style.display = "none"),
                  this._element.setAttribute("aria-hidden", !0),
                  (this._isTransitioning = !1),
                  this._showBackdrop(function () {
                    t(document.body).removeClass(c),
                      e._resetAdjustments(),
                      e._resetScrollbar(),
                      t(e._element).trigger(l.HIDDEN);
                  });
              }),
              (p._removeBackdrop = function () {
                this._backdrop &&
                  (t(this._backdrop).remove(), (this._backdrop = null));
              }),
              (p._showBackdrop = function (e) {
                var i = this,
                  n = t(this._element).hasClass(d) ? d : "";
                if (this._isShown && this._config.backdrop) {
                  var s = D.supportsTransitionEnd() && n;
                  if (
                    ((this._backdrop = document.createElement("div")),
                    (this._backdrop.className = "modal-backdrop"),
                    n && t(this._backdrop).addClass(n),
                    t(this._backdrop).appendTo(document.body),
                    t(this._element).on(l.CLICK_DISMISS, function (t) {
                      i._ignoreBackdropClick
                        ? (i._ignoreBackdropClick = !1)
                        : t.target === t.currentTarget &&
                          ("static" === i._config.backdrop
                            ? i._element.focus()
                            : i.hide());
                    }),
                    s && D.reflow(this._backdrop),
                    t(this._backdrop).addClass(h),
                    !e)
                  )
                    return;
                  if (!s) return void e();
                  t(this._backdrop)
                    .one(D.TRANSITION_END, e)
                    .emulateTransitionEnd(150);
                } else if (!this._isShown && this._backdrop) {
                  t(this._backdrop).removeClass(h);
                  var o = function () {
                    i._removeBackdrop(), e && e();
                  };
                  D.supportsTransitionEnd() && t(this._element).hasClass(d)
                    ? t(this._backdrop)
                        .one(D.TRANSITION_END, o)
                        .emulateTransitionEnd(150)
                    : o();
                } else e && e();
              }),
              (p._adjustDialog = function () {
                var t =
                  this._element.scrollHeight >
                  document.documentElement.clientHeight;
                !this._isBodyOverflowing &&
                  t &&
                  (this._element.style.paddingLeft =
                    this._scrollbarWidth + "px"),
                  this._isBodyOverflowing &&
                    !t &&
                    (this._element.style.paddingRight =
                      this._scrollbarWidth + "px");
              }),
              (p._resetAdjustments = function () {
                (this._element.style.paddingLeft = ""),
                  (this._element.style.paddingRight = "");
              }),
              (p._checkScrollbar = function () {
                var t = document.body.getBoundingClientRect();
                (this._isBodyOverflowing =
                  t.left + t.right < window.innerWidth),
                  (this._scrollbarWidth = this._getScrollbarWidth());
              }),
              (p._setScrollbar = function () {
                var e = this;
                if (this._isBodyOverflowing) {
                  t(u.FIXED_CONTENT).each(function (i, n) {
                    var s = t(n)[0].style.paddingRight,
                      o = t(n).css("padding-right");
                    t(n)
                      .data("padding-right", s)
                      .css(
                        "padding-right",
                        parseFloat(o) + e._scrollbarWidth + "px"
                      );
                  }),
                    t(u.STICKY_CONTENT).each(function (i, n) {
                      var s = t(n)[0].style.marginRight,
                        o = t(n).css("margin-right");
                      t(n)
                        .data("margin-right", s)
                        .css(
                          "margin-right",
                          parseFloat(o) - e._scrollbarWidth + "px"
                        );
                    }),
                    t(u.NAVBAR_TOGGLER).each(function (i, n) {
                      var s = t(n)[0].style.marginRight,
                        o = t(n).css("margin-right");
                      t(n)
                        .data("margin-right", s)
                        .css(
                          "margin-right",
                          parseFloat(o) + e._scrollbarWidth + "px"
                        );
                    });
                  var i = document.body.style.paddingRight,
                    n = t("body").css("padding-right");
                  t("body")
                    .data("padding-right", i)
                    .css(
                      "padding-right",
                      parseFloat(n) + this._scrollbarWidth + "px"
                    );
                }
              }),
              (p._resetScrollbar = function () {
                t(u.FIXED_CONTENT).each(function (e, i) {
                  var n = t(i).data("padding-right");
                  void 0 !== n &&
                    t(i).css("padding-right", n).removeData("padding-right");
                }),
                  t(u.STICKY_CONTENT + ", " + u.NAVBAR_TOGGLER).each(function (
                    e,
                    i
                  ) {
                    var n = t(i).data("margin-right");
                    void 0 !== n &&
                      t(i).css("margin-right", n).removeData("margin-right");
                  });
                var e = t("body").data("padding-right");
                void 0 !== e &&
                  t("body").css("padding-right", e).removeData("padding-right");
              }),
              (p._getScrollbarWidth = function () {
                var t = document.createElement("div");
                (t.className = "modal-scrollbar-measure"),
                  document.body.appendChild(t);
                var e = t.getBoundingClientRect().width - t.clientWidth;
                return document.body.removeChild(t), e;
              }),
              (n._jQueryInterface = function (i, s) {
                return this.each(function () {
                  var r = t(this).data(e),
                    a = o(
                      {},
                      n.Default,
                      t(this).data(),
                      "object" == typeof i && i
                    );
                  if (
                    (r || ((r = new n(this, a)), t(this).data(e, r)),
                    "string" == typeof i)
                  ) {
                    if (void 0 === r[i])
                      throw new TypeError('No method named "' + i + '"');
                    r[i](s);
                  } else a.show && r.show(s);
                });
              }),
              s(n, null, [
                {
                  key: "VERSION",
                  get: function () {
                    return "4.0.0";
                  },
                },
                {
                  key: "Default",
                  get: function () {
                    return r;
                  },
                },
              ]),
              n
            );
          })();
        return (
          t(document).on(l.CLICK_DATA_API, u.DATA_TOGGLE, function (i) {
            var n,
              s = this,
              r = D.getSelectorFromElement(this);
            r && (n = t(r)[0]);
            var a = t(n).data(e)
              ? "toggle"
              : o({}, t(n).data(), t(this).data());
            ("A" !== this.tagName && "AREA" !== this.tagName) ||
              i.preventDefault();
            var c = t(n).one(l.SHOW, function (e) {
              e.isDefaultPrevented() ||
                c.one(l.HIDDEN, function () {
                  t(s).is(":visible") && s.focus();
                });
            });
            p._jQueryInterface.call(t(n), a, this);
          }),
          (t.fn.modal = p._jQueryInterface),
          (t.fn.modal.Constructor = p),
          (t.fn.modal.noConflict = function () {
            return (t.fn.modal = n), p._jQueryInterface;
          }),
          p
        );
      })(e),
      $ = (function (t) {
        var e = "tooltip",
          n = "bs.tooltip",
          r = "." + n,
          a = t.fn[e],
          l = new RegExp("(^|\\s)bs-tooltip\\S+", "g"),
          c = {
            animation: "boolean",
            template: "string",
            title: "(string|element|function)",
            trigger: "string",
            delay: "(number|object)",
            html: "boolean",
            selector: "(string|boolean)",
            placement: "(string|function)",
            offset: "(number|string)",
            container: "(string|element|boolean)",
            fallbackPlacement: "(string|array)",
            boundary: "(string|element)",
          },
          d = {
            AUTO: "auto",
            TOP: "top",
            RIGHT: "right",
            BOTTOM: "bottom",
            LEFT: "left",
          },
          h = {
            animation: !0,
            template:
              '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
            trigger: "hover focus",
            title: "",
            delay: 0,
            html: !1,
            selector: !1,
            placement: "top",
            offset: 0,
            container: !1,
            fallbackPlacement: "flip",
            boundary: "scrollParent",
          },
          u = "show",
          p = "out",
          f = {
            HIDE: "hide" + r,
            HIDDEN: "hidden" + r,
            SHOW: "show" + r,
            SHOWN: "shown" + r,
            INSERTED: "inserted" + r,
            CLICK: "click" + r,
            FOCUSIN: "focusin" + r,
            FOCUSOUT: "focusout" + r,
            MOUSEENTER: "mouseenter" + r,
            MOUSELEAVE: "mouseleave" + r,
          },
          m = "fade",
          g = "show",
          v = "hover",
          y = "focus",
          _ = (function () {
            function a(t, e) {
              if (void 0 === i)
                throw new TypeError(
                  "Bootstrap tooltips require Popper.js (https://popper.js.org)"
                );
              (this._isEnabled = !0),
                (this._timeout = 0),
                (this._hoverState = ""),
                (this._activeTrigger = {}),
                (this._popper = null),
                (this.element = t),
                (this.config = this._getConfig(e)),
                (this.tip = null),
                this._setListeners();
            }
            var _ = a.prototype;
            return (
              (_.enable = function () {
                this._isEnabled = !0;
              }),
              (_.disable = function () {
                this._isEnabled = !1;
              }),
              (_.toggleEnabled = function () {
                this._isEnabled = !this._isEnabled;
              }),
              (_.toggle = function (e) {
                if (this._isEnabled)
                  if (e) {
                    var i = this.constructor.DATA_KEY,
                      n = t(e.currentTarget).data(i);
                    n ||
                      ((n = new this.constructor(
                        e.currentTarget,
                        this._getDelegateConfig()
                      )),
                      t(e.currentTarget).data(i, n)),
                      (n._activeTrigger.click = !n._activeTrigger.click),
                      n._isWithActiveTrigger()
                        ? n._enter(null, n)
                        : n._leave(null, n);
                  } else {
                    if (t(this.getTipElement()).hasClass(g))
                      return void this._leave(null, this);
                    this._enter(null, this);
                  }
              }),
              (_.dispose = function () {
                clearTimeout(this._timeout),
                  t.removeData(this.element, this.constructor.DATA_KEY),
                  t(this.element).off(this.constructor.EVENT_KEY),
                  t(this.element).closest(".modal").off("hide.bs.modal"),
                  this.tip && t(this.tip).remove(),
                  (this._isEnabled = null),
                  (this._timeout = null),
                  (this._hoverState = null),
                  (this._activeTrigger = null),
                  null !== this._popper && this._popper.destroy(),
                  (this._popper = null),
                  (this.element = null),
                  (this.config = null),
                  (this.tip = null);
              }),
              (_.show = function () {
                var e = this;
                if ("none" === t(this.element).css("display"))
                  throw new Error("Please use show on visible elements");
                var n = t.Event(this.constructor.Event.SHOW);
                if (this.isWithContent() && this._isEnabled) {
                  t(this.element).trigger(n);
                  var s = t.contains(
                    this.element.ownerDocument.documentElement,
                    this.element
                  );
                  if (n.isDefaultPrevented() || !s) return;
                  var o = this.getTipElement(),
                    r = D.getUID(this.constructor.NAME);
                  o.setAttribute("id", r),
                    this.element.setAttribute("aria-describedby", r),
                    this.setContent(),
                    this.config.animation && t(o).addClass(m);
                  var l =
                      "function" == typeof this.config.placement
                        ? this.config.placement.call(this, o, this.element)
                        : this.config.placement,
                    c = this._getAttachment(l);
                  this.addAttachmentClass(c);
                  var d =
                    !1 === this.config.container
                      ? document.body
                      : t(this.config.container);
                  t(o).data(this.constructor.DATA_KEY, this),
                    t.contains(
                      this.element.ownerDocument.documentElement,
                      this.tip
                    ) || t(o).appendTo(d),
                    t(this.element).trigger(this.constructor.Event.INSERTED),
                    (this._popper = new i(this.element, o, {
                      placement: c,
                      modifiers: {
                        offset: { offset: this.config.offset },
                        flip: { behavior: this.config.fallbackPlacement },
                        arrow: { element: ".arrow" },
                        preventOverflow: {
                          boundariesElement: this.config.boundary,
                        },
                      },
                      onCreate: function (t) {
                        t.originalPlacement !== t.placement &&
                          e._handlePopperPlacementChange(t);
                      },
                      onUpdate: function (t) {
                        e._handlePopperPlacementChange(t);
                      },
                    })),
                    t(o).addClass(g),
                    "ontouchstart" in document.documentElement &&
                      t("body").children().on("mouseover", null, t.noop);
                  var h = function () {
                    e.config.animation && e._fixTransition();
                    var i = e._hoverState;
                    (e._hoverState = null),
                      t(e.element).trigger(e.constructor.Event.SHOWN),
                      i === p && e._leave(null, e);
                  };
                  D.supportsTransitionEnd() && t(this.tip).hasClass(m)
                    ? t(this.tip)
                        .one(D.TRANSITION_END, h)
                        .emulateTransitionEnd(a._TRANSITION_DURATION)
                    : h();
                }
              }),
              (_.hide = function (e) {
                var i = this,
                  n = this.getTipElement(),
                  s = t.Event(this.constructor.Event.HIDE),
                  o = function () {
                    i._hoverState !== u &&
                      n.parentNode &&
                      n.parentNode.removeChild(n),
                      i._cleanTipClass(),
                      i.element.removeAttribute("aria-describedby"),
                      t(i.element).trigger(i.constructor.Event.HIDDEN),
                      null !== i._popper && i._popper.destroy(),
                      e && e();
                  };
                t(this.element).trigger(s),
                  s.isDefaultPrevented() ||
                    (t(n).removeClass(g),
                    "ontouchstart" in document.documentElement &&
                      t("body").children().off("mouseover", null, t.noop),
                    (this._activeTrigger.click = !1),
                    (this._activeTrigger[y] = !1),
                    (this._activeTrigger[v] = !1),
                    D.supportsTransitionEnd() && t(this.tip).hasClass(m)
                      ? t(n).one(D.TRANSITION_END, o).emulateTransitionEnd(150)
                      : o(),
                    (this._hoverState = ""));
              }),
              (_.update = function () {
                null !== this._popper && this._popper.scheduleUpdate();
              }),
              (_.isWithContent = function () {
                return Boolean(this.getTitle());
              }),
              (_.addAttachmentClass = function (e) {
                t(this.getTipElement()).addClass("bs-tooltip-" + e);
              }),
              (_.getTipElement = function () {
                return (
                  (this.tip = this.tip || t(this.config.template)[0]), this.tip
                );
              }),
              (_.setContent = function () {
                var e = t(this.getTipElement());
                this.setElementContent(
                  e.find(".tooltip-inner"),
                  this.getTitle()
                ),
                  e.removeClass(m + " " + g);
              }),
              (_.setElementContent = function (e, i) {
                var n = this.config.html;
                "object" == typeof i && (i.nodeType || i.jquery)
                  ? n
                    ? t(i).parent().is(e) || e.empty().append(i)
                    : e.text(t(i).text())
                  : e[n ? "html" : "text"](i);
              }),
              (_.getTitle = function () {
                var t = this.element.getAttribute("data-original-title");
                return (
                  t ||
                    (t =
                      "function" == typeof this.config.title
                        ? this.config.title.call(this.element)
                        : this.config.title),
                  t
                );
              }),
              (_._getAttachment = function (t) {
                return d[t.toUpperCase()];
              }),
              (_._setListeners = function () {
                var e = this;
                this.config.trigger.split(" ").forEach(function (i) {
                  if ("click" === i)
                    t(e.element).on(
                      e.constructor.Event.CLICK,
                      e.config.selector,
                      function (t) {
                        return e.toggle(t);
                      }
                    );
                  else if ("manual" !== i) {
                    var n =
                        i === v
                          ? e.constructor.Event.MOUSEENTER
                          : e.constructor.Event.FOCUSIN,
                      s =
                        i === v
                          ? e.constructor.Event.MOUSELEAVE
                          : e.constructor.Event.FOCUSOUT;
                    t(e.element)
                      .on(n, e.config.selector, function (t) {
                        return e._enter(t);
                      })
                      .on(s, e.config.selector, function (t) {
                        return e._leave(t);
                      });
                  }
                  t(e.element)
                    .closest(".modal")
                    .on("hide.bs.modal", function () {
                      return e.hide();
                    });
                }),
                  this.config.selector
                    ? (this.config = o({}, this.config, {
                        trigger: "manual",
                        selector: "",
                      }))
                    : this._fixTitle();
              }),
              (_._fixTitle = function () {
                var t = typeof this.element.getAttribute("data-original-title");
                (this.element.getAttribute("title") || "string" !== t) &&
                  (this.element.setAttribute(
                    "data-original-title",
                    this.element.getAttribute("title") || ""
                  ),
                  this.element.setAttribute("title", ""));
              }),
              (_._enter = function (e, i) {
                var n = this.constructor.DATA_KEY;
                (i = i || t(e.currentTarget).data(n)) ||
                  ((i = new this.constructor(
                    e.currentTarget,
                    this._getDelegateConfig()
                  )),
                  t(e.currentTarget).data(n, i)),
                  e && (i._activeTrigger["focusin" === e.type ? y : v] = !0),
                  t(i.getTipElement()).hasClass(g) || i._hoverState === u
                    ? (i._hoverState = u)
                    : (clearTimeout(i._timeout),
                      (i._hoverState = u),
                      i.config.delay && i.config.delay.show
                        ? (i._timeout = setTimeout(function () {
                            i._hoverState === u && i.show();
                          }, i.config.delay.show))
                        : i.show());
              }),
              (_._leave = function (e, i) {
                var n = this.constructor.DATA_KEY;
                (i = i || t(e.currentTarget).data(n)) ||
                  ((i = new this.constructor(
                    e.currentTarget,
                    this._getDelegateConfig()
                  )),
                  t(e.currentTarget).data(n, i)),
                  e && (i._activeTrigger["focusout" === e.type ? y : v] = !1),
                  i._isWithActiveTrigger() ||
                    (clearTimeout(i._timeout),
                    (i._hoverState = p),
                    i.config.delay && i.config.delay.hide
                      ? (i._timeout = setTimeout(function () {
                          i._hoverState === p && i.hide();
                        }, i.config.delay.hide))
                      : i.hide());
              }),
              (_._isWithActiveTrigger = function () {
                for (var t in this._activeTrigger)
                  if (this._activeTrigger[t]) return !0;
                return !1;
              }),
              (_._getConfig = function (i) {
                return (
                  "number" ==
                    typeof (i = o(
                      {},
                      this.constructor.Default,
                      t(this.element).data(),
                      i
                    )).delay && (i.delay = { show: i.delay, hide: i.delay }),
                  "number" == typeof i.title && (i.title = i.title.toString()),
                  "number" == typeof i.content &&
                    (i.content = i.content.toString()),
                  D.typeCheckConfig(e, i, this.constructor.DefaultType),
                  i
                );
              }),
              (_._getDelegateConfig = function () {
                var t = {};
                if (this.config)
                  for (var e in this.config)
                    this.constructor.Default[e] !== this.config[e] &&
                      (t[e] = this.config[e]);
                return t;
              }),
              (_._cleanTipClass = function () {
                var e = t(this.getTipElement()),
                  i = e.attr("class").match(l);
                null !== i && i.length > 0 && e.removeClass(i.join(""));
              }),
              (_._handlePopperPlacementChange = function (t) {
                this._cleanTipClass(),
                  this.addAttachmentClass(this._getAttachment(t.placement));
              }),
              (_._fixTransition = function () {
                var e = this.getTipElement(),
                  i = this.config.animation;
                null === e.getAttribute("x-placement") &&
                  (t(e).removeClass(m),
                  (this.config.animation = !1),
                  this.hide(),
                  this.show(),
                  (this.config.animation = i));
              }),
              (a._jQueryInterface = function (e) {
                return this.each(function () {
                  var i = t(this).data(n),
                    s = "object" == typeof e && e;
                  if (
                    (i || !/dispose|hide/.test(e)) &&
                    (i || ((i = new a(this, s)), t(this).data(n, i)),
                    "string" == typeof e)
                  ) {
                    if (void 0 === i[e])
                      throw new TypeError('No method named "' + e + '"');
                    i[e]();
                  }
                });
              }),
              s(a, null, [
                {
                  key: "VERSION",
                  get: function () {
                    return "4.0.0";
                  },
                },
                {
                  key: "Default",
                  get: function () {
                    return h;
                  },
                },
                {
                  key: "NAME",
                  get: function () {
                    return e;
                  },
                },
                {
                  key: "DATA_KEY",
                  get: function () {
                    return n;
                  },
                },
                {
                  key: "Event",
                  get: function () {
                    return f;
                  },
                },
                {
                  key: "EVENT_KEY",
                  get: function () {
                    return r;
                  },
                },
                {
                  key: "DefaultType",
                  get: function () {
                    return c;
                  },
                },
              ]),
              a
            );
          })();
        return (
          (t.fn[e] = _._jQueryInterface),
          (t.fn[e].Constructor = _),
          (t.fn[e].noConflict = function () {
            return (t.fn[e] = a), _._jQueryInterface;
          }),
          _
        );
      })(e),
      O = (function (t) {
        var e = "popover",
          i = "bs.popover",
          n = "." + i,
          r = t.fn[e],
          a = new RegExp("(^|\\s)bs-popover\\S+", "g"),
          l = o({}, $.Default, {
            placement: "right",
            trigger: "click",
            content: "",
            template:
              '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>',
          }),
          c = o({}, $.DefaultType, { content: "(string|element|function)" }),
          d = {
            HIDE: "hide" + n,
            HIDDEN: "hidden" + n,
            SHOW: "show" + n,
            SHOWN: "shown" + n,
            INSERTED: "inserted" + n,
            CLICK: "click" + n,
            FOCUSIN: "focusin" + n,
            FOCUSOUT: "focusout" + n,
            MOUSEENTER: "mouseenter" + n,
            MOUSELEAVE: "mouseleave" + n,
          },
          h = (function (o) {
            var r, h;
            function u() {
              return o.apply(this, arguments) || this;
            }
            (h = o),
              ((r = u).prototype = Object.create(h.prototype)),
              (r.prototype.constructor = r),
              (r.__proto__ = h);
            var p = u.prototype;
            return (
              (p.isWithContent = function () {
                return this.getTitle() || this._getContent();
              }),
              (p.addAttachmentClass = function (e) {
                t(this.getTipElement()).addClass("bs-popover-" + e);
              }),
              (p.getTipElement = function () {
                return (
                  (this.tip = this.tip || t(this.config.template)[0]), this.tip
                );
              }),
              (p.setContent = function () {
                var e = t(this.getTipElement());
                this.setElementContent(
                  e.find(".popover-header"),
                  this.getTitle()
                );
                var i = this._getContent();
                "function" == typeof i && (i = i.call(this.element)),
                  this.setElementContent(e.find(".popover-body"), i),
                  e.removeClass("fade show");
              }),
              (p._getContent = function () {
                return (
                  this.element.getAttribute("data-content") ||
                  this.config.content
                );
              }),
              (p._cleanTipClass = function () {
                var e = t(this.getTipElement()),
                  i = e.attr("class").match(a);
                null !== i && i.length > 0 && e.removeClass(i.join(""));
              }),
              (u._jQueryInterface = function (e) {
                return this.each(function () {
                  var n = t(this).data(i),
                    s = "object" == typeof e ? e : null;
                  if (
                    (n || !/destroy|hide/.test(e)) &&
                    (n || ((n = new u(this, s)), t(this).data(i, n)),
                    "string" == typeof e)
                  ) {
                    if (void 0 === n[e])
                      throw new TypeError('No method named "' + e + '"');
                    n[e]();
                  }
                });
              }),
              s(u, null, [
                {
                  key: "VERSION",
                  get: function () {
                    return "4.0.0";
                  },
                },
                {
                  key: "Default",
                  get: function () {
                    return l;
                  },
                },
                {
                  key: "NAME",
                  get: function () {
                    return e;
                  },
                },
                {
                  key: "DATA_KEY",
                  get: function () {
                    return i;
                  },
                },
                {
                  key: "Event",
                  get: function () {
                    return d;
                  },
                },
                {
                  key: "EVENT_KEY",
                  get: function () {
                    return n;
                  },
                },
                {
                  key: "DefaultType",
                  get: function () {
                    return c;
                  },
                },
              ]),
              u
            );
          })($);
        return (
          (t.fn[e] = h._jQueryInterface),
          (t.fn[e].Constructor = h),
          (t.fn[e].noConflict = function () {
            return (t.fn[e] = r), h._jQueryInterface;
          }),
          h
        );
      })(e),
      N = (function (t) {
        var e = "scrollspy",
          i = "bs.scrollspy",
          n = "." + i,
          r = t.fn[e],
          a = { offset: 10, method: "auto", target: "" },
          l = {
            offset: "number",
            method: "string",
            target: "(string|element)",
          },
          c = {
            ACTIVATE: "activate" + n,
            SCROLL: "scroll" + n,
            LOAD_DATA_API: "load" + n + ".data-api",
          },
          d = "active",
          h = {
            DATA_SPY: '[data-spy="scroll"]',
            ACTIVE: ".active",
            NAV_LIST_GROUP: ".nav, .list-group",
            NAV_LINKS: ".nav-link",
            NAV_ITEMS: ".nav-item",
            LIST_ITEMS: ".list-group-item",
            DROPDOWN: ".dropdown",
            DROPDOWN_ITEMS: ".dropdown-item",
            DROPDOWN_TOGGLE: ".dropdown-toggle",
          },
          u = "position",
          p = (function () {
            function r(e, i) {
              var n = this;
              (this._element = e),
                (this._scrollElement = "BODY" === e.tagName ? window : e),
                (this._config = this._getConfig(i)),
                (this._selector =
                  this._config.target +
                  " " +
                  h.NAV_LINKS +
                  "," +
                  this._config.target +
                  " " +
                  h.LIST_ITEMS +
                  "," +
                  this._config.target +
                  " " +
                  h.DROPDOWN_ITEMS),
                (this._offsets = []),
                (this._targets = []),
                (this._activeTarget = null),
                (this._scrollHeight = 0),
                t(this._scrollElement).on(c.SCROLL, function (t) {
                  return n._process(t);
                }),
                this.refresh(),
                this._process();
            }
            var p = r.prototype;
            return (
              (p.refresh = function () {
                var e = this,
                  i =
                    this._scrollElement === this._scrollElement.window
                      ? "offset"
                      : u,
                  n = "auto" === this._config.method ? i : this._config.method,
                  s = n === u ? this._getScrollTop() : 0;
                (this._offsets = []),
                  (this._targets = []),
                  (this._scrollHeight = this._getScrollHeight()),
                  t
                    .makeArray(t(this._selector))
                    .map(function (e) {
                      var i,
                        o = D.getSelectorFromElement(e);
                      if ((o && (i = t(o)[0]), i)) {
                        var r = i.getBoundingClientRect();
                        if (r.width || r.height) return [t(i)[n]().top + s, o];
                      }
                      return null;
                    })
                    .filter(function (t) {
                      return t;
                    })
                    .sort(function (t, e) {
                      return t[0] - e[0];
                    })
                    .forEach(function (t) {
                      e._offsets.push(t[0]), e._targets.push(t[1]);
                    });
              }),
              (p.dispose = function () {
                t.removeData(this._element, i),
                  t(this._scrollElement).off(n),
                  (this._element = null),
                  (this._scrollElement = null),
                  (this._config = null),
                  (this._selector = null),
                  (this._offsets = null),
                  (this._targets = null),
                  (this._activeTarget = null),
                  (this._scrollHeight = null);
              }),
              (p._getConfig = function (i) {
                if ("string" != typeof (i = o({}, a, i)).target) {
                  var n = t(i.target).attr("id");
                  n || ((n = D.getUID(e)), t(i.target).attr("id", n)),
                    (i.target = "#" + n);
                }
                return D.typeCheckConfig(e, i, l), i;
              }),
              (p._getScrollTop = function () {
                return this._scrollElement === window
                  ? this._scrollElement.pageYOffset
                  : this._scrollElement.scrollTop;
              }),
              (p._getScrollHeight = function () {
                return (
                  this._scrollElement.scrollHeight ||
                  Math.max(
                    document.body.scrollHeight,
                    document.documentElement.scrollHeight
                  )
                );
              }),
              (p._getOffsetHeight = function () {
                return this._scrollElement === window
                  ? window.innerHeight
                  : this._scrollElement.getBoundingClientRect().height;
              }),
              (p._process = function () {
                var t = this._getScrollTop() + this._config.offset,
                  e = this._getScrollHeight(),
                  i = this._config.offset + e - this._getOffsetHeight();
                if ((this._scrollHeight !== e && this.refresh(), t >= i)) {
                  var n = this._targets[this._targets.length - 1];
                  this._activeTarget !== n && this._activate(n);
                } else {
                  if (
                    this._activeTarget &&
                    t < this._offsets[0] &&
                    this._offsets[0] > 0
                  )
                    return (this._activeTarget = null), void this._clear();
                  for (var s = this._offsets.length; s--; )
                    this._activeTarget !== this._targets[s] &&
                      t >= this._offsets[s] &&
                      (void 0 === this._offsets[s + 1] ||
                        t < this._offsets[s + 1]) &&
                      this._activate(this._targets[s]);
                }
              }),
              (p._activate = function (e) {
                (this._activeTarget = e), this._clear();
                var i = this._selector.split(",");
                i = i.map(function (t) {
                  return (
                    t + '[data-target="' + e + '"],' + t + '[href="' + e + '"]'
                  );
                });
                var n = t(i.join(","));
                n.hasClass("dropdown-item")
                  ? (n.closest(h.DROPDOWN).find(h.DROPDOWN_TOGGLE).addClass(d),
                    n.addClass(d))
                  : (n.addClass(d),
                    n
                      .parents(h.NAV_LIST_GROUP)
                      .prev(h.NAV_LINKS + ", " + h.LIST_ITEMS)
                      .addClass(d),
                    n
                      .parents(h.NAV_LIST_GROUP)
                      .prev(h.NAV_ITEMS)
                      .children(h.NAV_LINKS)
                      .addClass(d)),
                  t(this._scrollElement).trigger(c.ACTIVATE, {
                    relatedTarget: e,
                  });
              }),
              (p._clear = function () {
                t(this._selector).filter(h.ACTIVE).removeClass(d);
              }),
              (r._jQueryInterface = function (e) {
                return this.each(function () {
                  var n = t(this).data(i);
                  if (
                    (n ||
                      ((n = new r(this, "object" == typeof e && e)),
                      t(this).data(i, n)),
                    "string" == typeof e)
                  ) {
                    if (void 0 === n[e])
                      throw new TypeError('No method named "' + e + '"');
                    n[e]();
                  }
                });
              }),
              s(r, null, [
                {
                  key: "VERSION",
                  get: function () {
                    return "4.0.0";
                  },
                },
                {
                  key: "Default",
                  get: function () {
                    return a;
                  },
                },
              ]),
              r
            );
          })();
        return (
          t(window).on(c.LOAD_DATA_API, function () {
            for (var e = t.makeArray(t(h.DATA_SPY)), i = e.length; i--; ) {
              var n = t(e[i]);
              p._jQueryInterface.call(n, n.data());
            }
          }),
          (t.fn[e] = p._jQueryInterface),
          (t.fn[e].Constructor = p),
          (t.fn[e].noConflict = function () {
            return (t.fn[e] = r), p._jQueryInterface;
          }),
          p
        );
      })(e),
      I = (function (t) {
        var e = "bs.tab",
          i = "." + e,
          n = t.fn.tab,
          o = {
            HIDE: "hide" + i,
            HIDDEN: "hidden" + i,
            SHOW: "show" + i,
            SHOWN: "shown" + i,
            CLICK_DATA_API: "click.bs.tab.data-api",
          },
          r = "active",
          a = "show",
          l = ".active",
          c = "> li > .active",
          d = (function () {
            function i(t) {
              this._element = t;
            }
            var n = i.prototype;
            return (
              (n.show = function () {
                var e = this;
                if (
                  !(
                    (this._element.parentNode &&
                      this._element.parentNode.nodeType === Node.ELEMENT_NODE &&
                      t(this._element).hasClass(r)) ||
                    t(this._element).hasClass("disabled")
                  )
                ) {
                  var i,
                    n,
                    s = t(this._element).closest(".nav, .list-group")[0],
                    a = D.getSelectorFromElement(this._element);
                  if (s) {
                    var d = "UL" === s.nodeName ? c : l;
                    n = (n = t.makeArray(t(s).find(d)))[n.length - 1];
                  }
                  var h = t.Event(o.HIDE, { relatedTarget: this._element }),
                    u = t.Event(o.SHOW, { relatedTarget: n });
                  if (
                    (n && t(n).trigger(h),
                    t(this._element).trigger(u),
                    !u.isDefaultPrevented() && !h.isDefaultPrevented())
                  ) {
                    a && (i = t(a)[0]), this._activate(this._element, s);
                    var p = function () {
                      var i = t.Event(o.HIDDEN, { relatedTarget: e._element }),
                        s = t.Event(o.SHOWN, { relatedTarget: n });
                      t(n).trigger(i), t(e._element).trigger(s);
                    };
                    i ? this._activate(i, i.parentNode, p) : p();
                  }
                }
              }),
              (n.dispose = function () {
                t.removeData(this._element, e), (this._element = null);
              }),
              (n._activate = function (e, i, n) {
                var s = this,
                  o = (
                    "UL" === i.nodeName ? t(i).find(c) : t(i).children(l)
                  )[0],
                  r =
                    n &&
                    D.supportsTransitionEnd() &&
                    o &&
                    t(o).hasClass("fade"),
                  a = function () {
                    return s._transitionComplete(e, o, n);
                  };
                o && r
                  ? t(o).one(D.TRANSITION_END, a).emulateTransitionEnd(150)
                  : a();
              }),
              (n._transitionComplete = function (e, i, n) {
                if (i) {
                  t(i).removeClass(a + " " + r);
                  var s = t(i.parentNode).find("> .dropdown-menu .active")[0];
                  s && t(s).removeClass(r),
                    "tab" === i.getAttribute("role") &&
                      i.setAttribute("aria-selected", !1);
                }
                if (
                  (t(e).addClass(r),
                  "tab" === e.getAttribute("role") &&
                    e.setAttribute("aria-selected", !0),
                  D.reflow(e),
                  t(e).addClass(a),
                  e.parentNode && t(e.parentNode).hasClass("dropdown-menu"))
                ) {
                  var o = t(e).closest(".dropdown")[0];
                  o && t(o).find(".dropdown-toggle").addClass(r),
                    e.setAttribute("aria-expanded", !0);
                }
                n && n();
              }),
              (i._jQueryInterface = function (n) {
                return this.each(function () {
                  var s = t(this),
                    o = s.data(e);
                  if (
                    (o || ((o = new i(this)), s.data(e, o)),
                    "string" == typeof n)
                  ) {
                    if (void 0 === o[n])
                      throw new TypeError('No method named "' + n + '"');
                    o[n]();
                  }
                });
              }),
              s(i, null, [
                {
                  key: "VERSION",
                  get: function () {
                    return "4.0.0";
                  },
                },
              ]),
              i
            );
          })();
        return (
          t(document).on(
            o.CLICK_DATA_API,
            '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',
            function (e) {
              e.preventDefault(), d._jQueryInterface.call(t(this), "show");
            }
          ),
          (t.fn.tab = d._jQueryInterface),
          (t.fn.tab.Constructor = d),
          (t.fn.tab.noConflict = function () {
            return (t.fn.tab = n), d._jQueryInterface;
          }),
          d
        );
      })(e);
    !(function (t) {
      if (void 0 === t)
        throw new TypeError(
          "Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript."
        );
      var e = t.fn.jquery.split(" ")[0].split(".");
      if (
        (e[0] < 2 && e[1] < 9) ||
        (1 === e[0] && 9 === e[1] && e[2] < 1) ||
        e[0] >= 4
      )
        throw new Error(
          "Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0"
        );
    })(e),
      (t.Util = D),
      (t.Alert = T),
      (t.Button = x),
      (t.Carousel = S),
      (t.Collapse = E),
      (t.Dropdown = A),
      (t.Modal = M),
      (t.Popover = O),
      (t.Scrollspy = N),
      (t.Tab = I),
      (t.Tooltip = $),
      Object.defineProperty(t, "__esModule", { value: !0 });
  }),
  (function (t, e, i, n) {
    function s(e, i) {
      (this.settings = null),
        (this.options = t.extend({}, s.Defaults, i)),
        (this.$element = t(e)),
        (this._handlers = {}),
        (this._plugins = {}),
        (this._supress = {}),
        (this._current = null),
        (this._speed = null),
        (this._coordinates = []),
        (this._breakpoint = null),
        (this._width = null),
        (this._items = []),
        (this._clones = []),
        (this._mergers = []),
        (this._widths = []),
        (this._invalidated = {}),
        (this._pipe = []),
        (this._drag = {
          time: null,
          target: null,
          pointer: null,
          stage: { start: null, current: null },
          direction: null,
        }),
        (this._states = {
          current: {},
          tags: {
            initializing: ["busy"],
            animating: ["busy"],
            dragging: ["interacting"],
          },
        }),
        t.each(
          ["onResize", "onThrottledResize"],
          t.proxy(function (e, i) {
            this._handlers[i] = t.proxy(this[i], this);
          }, this)
        ),
        t.each(
          s.Plugins,
          t.proxy(function (t, e) {
            this._plugins[t.charAt(0).toLowerCase() + t.slice(1)] = new e(this);
          }, this)
        ),
        t.each(
          s.Workers,
          t.proxy(function (e, i) {
            this._pipe.push({ filter: i.filter, run: t.proxy(i.run, this) });
          }, this)
        ),
        this.setup(),
        this.initialize();
    }
    (s.Defaults = {
      items: 3,
      loop: !1,
      center: !1,
      rewind: !1,
      checkVisibility: !0,
      mouseDrag: !0,
      touchDrag: !0,
      pullDrag: !0,
      freeDrag: !1,
      margin: 0,
      stagePadding: 0,
      merge: !1,
      mergeFit: !0,
      autoWidth: !1,
      startPosition: 0,
      rtl: !1,
      smartSpeed: 250,
      fluidSpeed: !1,
      dragEndSpeed: !1,
      responsive: {},
      responsiveRefreshRate: 200,
      responsiveBaseElement: e,
      fallbackEasing: "swing",
      slideTransition: "",
      info: !1,
      nestedItemSelector: !1,
      itemElement: "div",
      stageElement: "div",
      refreshClass: "owl-refresh",
      loadedClass: "owl-loaded",
      loadingClass: "owl-loading",
      rtlClass: "owl-rtl",
      responsiveClass: "owl-responsive",
      dragClass: "owl-drag",
      itemClass: "owl-item",
      stageClass: "owl-stage",
      stageOuterClass: "owl-stage-outer",
      grabClass: "owl-grab",
    }),
      (s.Width = { Default: "default", Inner: "inner", Outer: "outer" }),
      (s.Type = { Event: "event", State: "state" }),
      (s.Plugins = {}),
      (s.Workers = [
        {
          filter: ["width", "settings"],
          run: function () {
            this._width = this.$element.width();
          },
        },
        {
          filter: ["width", "items", "settings"],
          run: function (t) {
            t.current =
              this._items && this._items[this.relative(this._current)];
          },
        },
        {
          filter: ["items", "settings"],
          run: function () {
            this.$stage.children(".cloned").remove();
          },
        },
        {
          filter: ["width", "items", "settings"],
          run: function (t) {
            var e = this.settings.margin || "",
              i = !this.settings.autoWidth,
              n = this.settings.rtl,
              s = {
                width: "auto",
                "margin-left": n ? e : "",
                "margin-right": n ? "" : e,
              };
            !i && this.$stage.children().css(s), (t.css = s);
          },
        },
        {
          filter: ["width", "items", "settings"],
          run: function (t) {
            var e =
                (this.width() / this.settings.items).toFixed(3) -
                this.settings.margin,
              i = null,
              n = this._items.length,
              s = !this.settings.autoWidth,
              o = [];
            for (t.items = { merge: !1, width: e }; n--; )
              (i = this._mergers[n]),
                (i =
                  (this.settings.mergeFit &&
                    Math.min(i, this.settings.items)) ||
                  i),
                (t.items.merge = i > 1 || t.items.merge),
                (o[n] = s ? e * i : this._items[n].width());
            this._widths = o;
          },
        },
        {
          filter: ["items", "settings"],
          run: function () {
            var e = [],
              i = this._items,
              n = this.settings,
              s = Math.max(2 * n.items, 4),
              o = 2 * Math.ceil(i.length / 2),
              r = n.loop && i.length ? (n.rewind ? s : Math.max(s, o)) : 0,
              a = "",
              l = "";
            for (r /= 2; r > 0; )
              e.push(this.normalize(e.length / 2, !0)),
                (a += i[e[e.length - 1]][0].outerHTML),
                e.push(this.normalize(i.length - 1 - (e.length - 1) / 2, !0)),
                (l = i[e[e.length - 1]][0].outerHTML + l),
                (r -= 1);
            (this._clones = e),
              t(a).addClass("cloned").appendTo(this.$stage),
              t(l).addClass("cloned").prependTo(this.$stage);
          },
        },
        {
          filter: ["width", "items", "settings"],
          run: function () {
            for (
              var t = this.settings.rtl ? 1 : -1,
                e = this._clones.length + this._items.length,
                i = -1,
                n = 0,
                s = 0,
                o = [];
              ++i < e;

            )
              (n = o[i - 1] || 0),
                (s = this._widths[this.relative(i)] + this.settings.margin),
                o.push(n + s * t);
            this._coordinates = o;
          },
        },
        {
          filter: ["width", "items", "settings"],
          run: function () {
            var t = this.settings.stagePadding,
              e = this._coordinates,
              i = {
                width: Math.ceil(Math.abs(e[e.length - 1])) + 2 * t,
                "padding-left": t || "",
                "padding-right": t || "",
              };
            this.$stage.css(i);
          },
        },
        {
          filter: ["width", "items", "settings"],
          run: function (t) {
            var e = this._coordinates.length,
              i = !this.settings.autoWidth,
              n = this.$stage.children();
            if (i && t.items.merge)
              for (; e--; )
                (t.css.width = this._widths[this.relative(e)]),
                  n.eq(e).css(t.css);
            else i && ((t.css.width = t.items.width), n.css(t.css));
          },
        },
        {
          filter: ["items"],
          run: function () {
            this._coordinates.length < 1 && this.$stage.removeAttr("style");
          },
        },
        {
          filter: ["width", "items", "settings"],
          run: function (t) {
            (t.current = t.current
              ? this.$stage.children().index(t.current)
              : 0),
              (t.current = Math.max(
                this.minimum(),
                Math.min(this.maximum(), t.current)
              )),
              this.reset(t.current);
          },
        },
        {
          filter: ["position"],
          run: function () {
            this.animate(this.coordinates(this._current));
          },
        },
        {
          filter: ["width", "position", "items", "settings"],
          run: function () {
            var t,
              e,
              i,
              n,
              s = this.settings.rtl ? 1 : -1,
              o = 2 * this.settings.stagePadding,
              r = this.coordinates(this.current()) + o,
              a = r + this.width() * s,
              l = [];
            for (i = 0, n = this._coordinates.length; i < n; i++)
              (t = this._coordinates[i - 1] || 0),
                (e = Math.abs(this._coordinates[i]) + o * s),
                ((this.op(t, "<=", r) && this.op(t, ">", a)) ||
                  (this.op(e, "<", r) && this.op(e, ">", a))) &&
                  l.push(i);
            this.$stage.children(".active").removeClass("active"),
              this.$stage
                .children(":eq(" + l.join("), :eq(") + ")")
                .addClass("active"),
              this.$stage.children(".center").removeClass("center"),
              this.settings.center &&
                this.$stage.children().eq(this.current()).addClass("center");
          },
        },
      ]),
      (s.prototype.initializeStage = function () {
        (this.$stage = this.$element.find("." + this.settings.stageClass)),
          this.$stage.length ||
            (this.$element.addClass(this.options.loadingClass),
            (this.$stage = t("<" + this.settings.stageElement + ">", {
              class: this.settings.stageClass,
            }).wrap(t("<div/>", { class: this.settings.stageOuterClass }))),
            this.$element.append(this.$stage.parent()));
      }),
      (s.prototype.initializeItems = function () {
        var e = this.$element.find(".owl-item");
        if (e.length)
          return (
            (this._items = e.get().map(function (e) {
              return t(e);
            })),
            (this._mergers = this._items.map(function () {
              return 1;
            })),
            void this.refresh()
          );
        this.replace(this.$element.children().not(this.$stage.parent())),
          this.isVisible() ? this.refresh() : this.invalidate("width"),
          this.$element
            .removeClass(this.options.loadingClass)
            .addClass(this.options.loadedClass);
      }),
      (s.prototype.initialize = function () {
        var t, e, i;
        this.enter("initializing"),
          this.trigger("initialize"),
          this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl),
          this.settings.autoWidth &&
            !this.is("pre-loading") &&
            ((t = this.$element.find("img")),
            (e = this.settings.nestedItemSelector
              ? "." + this.settings.nestedItemSelector
              : n),
            (i = this.$element.children(e).width()),
            t.length && i <= 0 && this.preloadAutoWidthImages(t)),
          this.initializeStage(),
          this.initializeItems(),
          this.registerEventHandlers(),
          this.leave("initializing"),
          this.trigger("initialized");
      }),
      (s.prototype.isVisible = function () {
        return !this.settings.checkVisibility || this.$element.is(":visible");
      }),
      (s.prototype.setup = function () {
        var e = this.viewport(),
          i = this.options.responsive,
          n = -1,
          s = null;
        i
          ? (t.each(i, function (t) {
              t <= e && t > n && (n = Number(t));
            }),
            "function" ==
              typeof (s = t.extend({}, this.options, i[n])).stagePadding &&
              (s.stagePadding = s.stagePadding()),
            delete s.responsive,
            s.responsiveClass &&
              this.$element.attr(
                "class",
                this.$element
                  .attr("class")
                  .replace(
                    new RegExp(
                      "(" + this.options.responsiveClass + "-)\\S+\\s",
                      "g"
                    ),
                    "$1" + n
                  )
              ))
          : (s = t.extend({}, this.options)),
          this.trigger("change", { property: { name: "settings", value: s } }),
          (this._breakpoint = n),
          (this.settings = s),
          this.invalidate("settings"),
          this.trigger("changed", {
            property: { name: "settings", value: this.settings },
          });
      }),
      (s.prototype.optionsLogic = function () {
        this.settings.autoWidth &&
          ((this.settings.stagePadding = !1), (this.settings.merge = !1));
      }),
      (s.prototype.prepare = function (e) {
        var i = this.trigger("prepare", { content: e });
        return (
          i.data ||
            (i.data = t("<" + this.settings.itemElement + "/>")
              .addClass(this.options.itemClass)
              .append(e)),
          this.trigger("prepared", { content: i.data }),
          i.data
        );
      }),
      (s.prototype.update = function () {
        for (
          var e = 0,
            i = this._pipe.length,
            n = t.proxy(function (t) {
              return this[t];
            }, this._invalidated),
            s = {};
          e < i;

        )
          (this._invalidated.all ||
            t.grep(this._pipe[e].filter, n).length > 0) &&
            this._pipe[e].run(s),
            e++;
        (this._invalidated = {}), !this.is("valid") && this.enter("valid");
      }),
      (s.prototype.width = function (t) {
        switch ((t = t || s.Width.Default)) {
          case s.Width.Inner:
          case s.Width.Outer:
            return this._width;
          default:
            return (
              this._width -
              2 * this.settings.stagePadding +
              this.settings.margin
            );
        }
      }),
      (s.prototype.refresh = function () {
        this.enter("refreshing"),
          this.trigger("refresh"),
          this.setup(),
          this.optionsLogic(),
          this.$element.addClass(this.options.refreshClass),
          this.update(),
          this.$element.removeClass(this.options.refreshClass),
          this.leave("refreshing"),
          this.trigger("refreshed");
      }),
      (s.prototype.onThrottledResize = function () {
        e.clearTimeout(this.resizeTimer),
          (this.resizeTimer = e.setTimeout(
            this._handlers.onResize,
            this.settings.responsiveRefreshRate
          ));
      }),
      (s.prototype.onResize = function () {
        return (
          !!this._items.length &&
          this._width !== this.$element.width() &&
          !!this.isVisible() &&
          (this.enter("resizing"),
          this.trigger("resize").isDefaultPrevented()
            ? (this.leave("resizing"), !1)
            : (this.invalidate("width"),
              this.refresh(),
              this.leave("resizing"),
              void this.trigger("resized")))
        );
      }),
      (s.prototype.registerEventHandlers = function () {
        t.support.transition &&
          this.$stage.on(
            t.support.transition.end + ".owl.core",
            t.proxy(this.onTransitionEnd, this)
          ),
          !1 !== this.settings.responsive &&
            this.on(e, "resize", this._handlers.onThrottledResize),
          this.settings.mouseDrag &&
            (this.$element.addClass(this.options.dragClass),
            this.$stage.on(
              "mousedown.owl.core",
              t.proxy(this.onDragStart, this)
            ),
            this.$stage.on(
              "dragstart.owl.core selectstart.owl.core",
              function () {
                return !1;
              }
            )),
          this.settings.touchDrag &&
            (this.$stage.on(
              "touchstart.owl.core",
              t.proxy(this.onDragStart, this)
            ),
            this.$stage.on(
              "touchcancel.owl.core",
              t.proxy(this.onDragEnd, this)
            ));
      }),
      (s.prototype.onDragStart = function (e) {
        var n = null;
        3 !== e.which &&
          (t.support.transform
            ? (n = {
                x: (n = this.$stage
                  .css("transform")
                  .replace(/.*\(|\)| /g, "")
                  .split(","))[16 === n.length ? 12 : 4],
                y: n[16 === n.length ? 13 : 5],
              })
            : ((n = this.$stage.position()),
              (n = {
                x: this.settings.rtl
                  ? n.left +
                    this.$stage.width() -
                    this.width() +
                    this.settings.margin
                  : n.left,
                y: n.top,
              })),
          this.is("animating") &&
            (t.support.transform ? this.animate(n.x) : this.$stage.stop(),
            this.invalidate("position")),
          this.$element.toggleClass(
            this.options.grabClass,
            "mousedown" === e.type
          ),
          this.speed(0),
          (this._drag.time = new Date().getTime()),
          (this._drag.target = t(e.target)),
          (this._drag.stage.start = n),
          (this._drag.stage.current = n),
          (this._drag.pointer = this.pointer(e)),
          t(i).on(
            "mouseup.owl.core touchend.owl.core",
            t.proxy(this.onDragEnd, this)
          ),
          t(i).one(
            "mousemove.owl.core touchmove.owl.core",
            t.proxy(function (e) {
              var n = this.difference(this._drag.pointer, this.pointer(e));
              t(i).on(
                "mousemove.owl.core touchmove.owl.core",
                t.proxy(this.onDragMove, this)
              ),
                (Math.abs(n.x) < Math.abs(n.y) && this.is("valid")) ||
                  (e.preventDefault(),
                  this.enter("dragging"),
                  this.trigger("drag"));
            }, this)
          ));
      }),
      (s.prototype.onDragMove = function (t) {
        var e = null,
          i = null,
          n = null,
          s = this.difference(this._drag.pointer, this.pointer(t)),
          o = this.difference(this._drag.stage.start, s);
        this.is("dragging") &&
          (t.preventDefault(),
          this.settings.loop
            ? ((e = this.coordinates(this.minimum())),
              (i = this.coordinates(this.maximum() + 1) - e),
              (o.x = ((((o.x - e) % i) + i) % i) + e))
            : ((e = this.settings.rtl
                ? this.coordinates(this.maximum())
                : this.coordinates(this.minimum())),
              (i = this.settings.rtl
                ? this.coordinates(this.minimum())
                : this.coordinates(this.maximum())),
              (n = this.settings.pullDrag ? (-1 * s.x) / 5 : 0),
              (o.x = Math.max(Math.min(o.x, e + n), i + n))),
          (this._drag.stage.current = o),
          this.animate(o.x));
      }),
      (s.prototype.onDragEnd = function (e) {
        var n = this.difference(this._drag.pointer, this.pointer(e)),
          s = this._drag.stage.current,
          o = (n.x > 0) ^ this.settings.rtl ? "left" : "right";
        t(i).off(".owl.core"),
          this.$element.removeClass(this.options.grabClass),
          ((0 !== n.x && this.is("dragging")) || !this.is("valid")) &&
            (this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed),
            this.current(
              this.closest(s.x, 0 !== n.x ? o : this._drag.direction)
            ),
            this.invalidate("position"),
            this.update(),
            (this._drag.direction = o),
            (Math.abs(n.x) > 3 ||
              new Date().getTime() - this._drag.time > 300) &&
              this._drag.target.one("click.owl.core", function () {
                return !1;
              })),
          this.is("dragging") &&
            (this.leave("dragging"), this.trigger("dragged"));
      }),
      (s.prototype.closest = function (e, i) {
        var s = -1,
          o = this.width(),
          r = this.coordinates();
        return (
          this.settings.freeDrag ||
            t.each(
              r,
              t.proxy(function (t, a) {
                return (
                  "left" === i && e > a - 30 && e < a + 30
                    ? (s = t)
                    : "right" === i && e > a - o - 30 && e < a - o + 30
                    ? (s = t + 1)
                    : this.op(e, "<", a) &&
                      this.op(e, ">", r[t + 1] !== n ? r[t + 1] : a - o) &&
                      (s = "left" === i ? t + 1 : t),
                  -1 === s
                );
              }, this)
            ),
          this.settings.loop ||
            (this.op(e, ">", r[this.minimum()])
              ? (s = e = this.minimum())
              : this.op(e, "<", r[this.maximum()]) && (s = e = this.maximum())),
          s
        );
      }),
      (s.prototype.animate = function (e) {
        var i = this.speed() > 0;
        this.is("animating") && this.onTransitionEnd(),
          i && (this.enter("animating"), this.trigger("translate")),
          t.support.transform3d && t.support.transition
            ? this.$stage.css({
                transform: "translate3d(" + e + "px,0px,0px)",
                transition:
                  this.speed() / 1e3 +
                  "s" +
                  (this.settings.slideTransition
                    ? " " + this.settings.slideTransition
                    : ""),
              })
            : i
            ? this.$stage.animate(
                { left: e + "px" },
                this.speed(),
                this.settings.fallbackEasing,
                t.proxy(this.onTransitionEnd, this)
              )
            : this.$stage.css({ left: e + "px" });
      }),
      (s.prototype.is = function (t) {
        return this._states.current[t] && this._states.current[t] > 0;
      }),
      (s.prototype.current = function (t) {
        if (t === n) return this._current;
        if (0 === this._items.length) return n;
        if (((t = this.normalize(t)), this._current !== t)) {
          var e = this.trigger("change", {
            property: { name: "position", value: t },
          });
          e.data !== n && (t = this.normalize(e.data)),
            (this._current = t),
            this.invalidate("position"),
            this.trigger("changed", {
              property: { name: "position", value: this._current },
            });
        }
        return this._current;
      }),
      (s.prototype.invalidate = function (e) {
        return (
          "string" === t.type(e) &&
            ((this._invalidated[e] = !0),
            this.is("valid") && this.leave("valid")),
          t.map(this._invalidated, function (t, e) {
            return e;
          })
        );
      }),
      (s.prototype.reset = function (t) {
        (t = this.normalize(t)) !== n &&
          ((this._speed = 0),
          (this._current = t),
          this.suppress(["translate", "translated"]),
          this.animate(this.coordinates(t)),
          this.release(["translate", "translated"]));
      }),
      (s.prototype.normalize = function (t, e) {
        var i = this._items.length,
          s = e ? 0 : this._clones.length;
        return (
          !this.isNumeric(t) || i < 1
            ? (t = n)
            : (t < 0 || t >= i + s) &&
              (t = ((((t - s / 2) % i) + i) % i) + s / 2),
          t
        );
      }),
      (s.prototype.relative = function (t) {
        return (t -= this._clones.length / 2), this.normalize(t, !0);
      }),
      (s.prototype.maximum = function (t) {
        var e,
          i,
          n,
          s = this.settings,
          o = this._coordinates.length;
        if (s.loop) o = this._clones.length / 2 + this._items.length - 1;
        else if (s.autoWidth || s.merge) {
          if ((e = this._items.length))
            for (
              i = this._items[--e].width(), n = this.$element.width();
              e-- &&
              !((i += this._items[e].width() + this.settings.margin) > n);

            );
          o = e + 1;
        } else
          o = s.center ? this._items.length - 1 : this._items.length - s.items;
        return t && (o -= this._clones.length / 2), Math.max(o, 0);
      }),
      (s.prototype.minimum = function (t) {
        return t ? 0 : this._clones.length / 2;
      }),
      (s.prototype.items = function (t) {
        return t === n
          ? this._items.slice()
          : ((t = this.normalize(t, !0)), this._items[t]);
      }),
      (s.prototype.mergers = function (t) {
        return t === n
          ? this._mergers.slice()
          : ((t = this.normalize(t, !0)), this._mergers[t]);
      }),
      (s.prototype.clones = function (e) {
        var i = this._clones.length / 2,
          s = i + this._items.length,
          o = function (t) {
            return t % 2 == 0 ? s + t / 2 : i - (t + 1) / 2;
          };
        return e === n
          ? t.map(this._clones, function (t, e) {
              return o(e);
            })
          : t.map(this._clones, function (t, i) {
              return t === e ? o(i) : null;
            });
      }),
      (s.prototype.speed = function (t) {
        return t !== n && (this._speed = t), this._speed;
      }),
      (s.prototype.coordinates = function (e) {
        var i,
          s = 1,
          o = e - 1;
        return e === n
          ? t.map(
              this._coordinates,
              t.proxy(function (t, e) {
                return this.coordinates(e);
              }, this)
            )
          : (this.settings.center
              ? (this.settings.rtl && ((s = -1), (o = e + 1)),
                (i = this._coordinates[e]),
                (i +=
                  ((this.width() - i + (this._coordinates[o] || 0)) / 2) * s))
              : (i = this._coordinates[o] || 0),
            (i = Math.ceil(i)));
      }),
      (s.prototype.duration = function (t, e, i) {
        return 0 === i
          ? 0
          : Math.min(Math.max(Math.abs(e - t), 1), 6) *
              Math.abs(i || this.settings.smartSpeed);
      }),
      (s.prototype.to = function (t, e) {
        var i = this.current(),
          n = null,
          s = t - this.relative(i),
          o = (s > 0) - (s < 0),
          r = this._items.length,
          a = this.minimum(),
          l = this.maximum();
        this.settings.loop
          ? (!this.settings.rewind && Math.abs(s) > r / 2 && (s += -1 * o * r),
            (n = (((((t = i + s) - a) % r) + r) % r) + a) !== t &&
              n - s <= l &&
              n - s > 0 &&
              ((i = n - s), (t = n), this.reset(i)))
          : (t = this.settings.rewind
              ? ((t % (l += 1)) + l) % l
              : Math.max(a, Math.min(l, t))),
          this.speed(this.duration(i, t, e)),
          this.current(t),
          this.isVisible() && this.update();
      }),
      (s.prototype.next = function (t) {
        (t = t || !1), this.to(this.relative(this.current()) + 1, t);
      }),
      (s.prototype.prev = function (t) {
        (t = t || !1), this.to(this.relative(this.current()) - 1, t);
      }),
      (s.prototype.onTransitionEnd = function (t) {
        if (
          t !== n &&
          (t.stopPropagation(),
          (t.target || t.srcElement || t.originalTarget) !== this.$stage.get(0))
        )
          return !1;
        this.leave("animating"), this.trigger("translated");
      }),
      (s.prototype.viewport = function () {
        var n;
        return (
          this.options.responsiveBaseElement !== e
            ? (n = t(this.options.responsiveBaseElement).width())
            : e.innerWidth
            ? (n = e.innerWidth)
            : i.documentElement && i.documentElement.clientWidth
            ? (n = i.documentElement.clientWidth)
            : console.warn("Can not detect viewport width."),
          n
        );
      }),
      (s.prototype.replace = function (e) {
        this.$stage.empty(),
          (this._items = []),
          e && (e = e instanceof jQuery ? e : t(e)),
          this.settings.nestedItemSelector &&
            (e = e.find("." + this.settings.nestedItemSelector)),
          e
            .filter(function () {
              return 1 === this.nodeType;
            })
            .each(
              t.proxy(function (t, e) {
                (e = this.prepare(e)),
                  this.$stage.append(e),
                  this._items.push(e),
                  this._mergers.push(
                    1 *
                      e
                        .find("[data-merge]")
                        .addBack("[data-merge]")
                        .attr("data-merge") || 1
                  );
              }, this)
            ),
          this.reset(
            this.isNumeric(this.settings.startPosition)
              ? this.settings.startPosition
              : 0
          ),
          this.invalidate("items");
      }),
      (s.prototype.add = function (e, i) {
        var s = this.relative(this._current);
        (i = i === n ? this._items.length : this.normalize(i, !0)),
          (e = e instanceof jQuery ? e : t(e)),
          this.trigger("add", { content: e, position: i }),
          (e = this.prepare(e)),
          0 === this._items.length || i === this._items.length
            ? (0 === this._items.length && this.$stage.append(e),
              0 !== this._items.length && this._items[i - 1].after(e),
              this._items.push(e),
              this._mergers.push(
                1 *
                  e
                    .find("[data-merge]")
                    .addBack("[data-merge]")
                    .attr("data-merge") || 1
              ))
            : (this._items[i].before(e),
              this._items.splice(i, 0, e),
              this._mergers.splice(
                i,
                0,
                1 *
                  e
                    .find("[data-merge]")
                    .addBack("[data-merge]")
                    .attr("data-merge") || 1
              )),
          this._items[s] && this.reset(this._items[s].index()),
          this.invalidate("items"),
          this.trigger("added", { content: e, position: i });
      }),
      (s.prototype.remove = function (t) {
        (t = this.normalize(t, !0)) !== n &&
          (this.trigger("remove", { content: this._items[t], position: t }),
          this._items[t].remove(),
          this._items.splice(t, 1),
          this._mergers.splice(t, 1),
          this.invalidate("items"),
          this.trigger("removed", { content: null, position: t }));
      }),
      (s.prototype.preloadAutoWidthImages = function (e) {
        e.each(
          t.proxy(function (e, i) {
            this.enter("pre-loading"),
              (i = t(i)),
              t(new Image())
                .one(
                  "load",
                  t.proxy(function (t) {
                    i.attr("src", t.target.src),
                      i.css("opacity", 1),
                      this.leave("pre-loading"),
                      !this.is("pre-loading") &&
                        !this.is("initializing") &&
                        this.refresh();
                  }, this)
                )
                .attr(
                  "src",
                  i.attr("src") ||
                    i.attr("data-src") ||
                    i.attr("data-src-retina")
                );
          }, this)
        );
      }),
      (s.prototype.destroy = function () {
        for (var n in (this.$element.off(".owl.core"),
        this.$stage.off(".owl.core"),
        t(i).off(".owl.core"),
        !1 !== this.settings.responsive &&
          (e.clearTimeout(this.resizeTimer),
          this.off(e, "resize", this._handlers.onThrottledResize)),
        this._plugins))
          this._plugins[n].destroy();
        this.$stage.children(".cloned").remove(),
          this.$stage.unwrap(),
          this.$stage.children().contents().unwrap(),
          this.$stage.children().unwrap(),
          this.$stage.remove(),
          this.$element
            .removeClass(this.options.refreshClass)
            .removeClass(this.options.loadingClass)
            .removeClass(this.options.loadedClass)
            .removeClass(this.options.rtlClass)
            .removeClass(this.options.dragClass)
            .removeClass(this.options.grabClass)
            .attr(
              "class",
              this.$element
                .attr("class")
                .replace(
                  new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"),
                  ""
                )
            )
            .removeData("owl.carousel");
      }),
      (s.prototype.op = function (t, e, i) {
        var n = this.settings.rtl;
        switch (e) {
          case "<":
            return n ? t > i : t < i;
          case ">":
            return n ? t < i : t > i;
          case ">=":
            return n ? t <= i : t >= i;
          case "<=":
            return n ? t >= i : t <= i;
        }
      }),
      (s.prototype.on = function (t, e, i, n) {
        t.addEventListener
          ? t.addEventListener(e, i, n)
          : t.attachEvent && t.attachEvent("on" + e, i);
      }),
      (s.prototype.off = function (t, e, i, n) {
        t.removeEventListener
          ? t.removeEventListener(e, i, n)
          : t.detachEvent && t.detachEvent("on" + e, i);
      }),
      (s.prototype.trigger = function (e, i, n, o, r) {
        var a = { item: { count: this._items.length, index: this.current() } },
          l = t.camelCase(
            t
              .grep(["on", e, n], function (t) {
                return t;
              })
              .join("-")
              .toLowerCase()
          ),
          c = t.Event(
            [e, "owl", n || "carousel"].join(".").toLowerCase(),
            t.extend({ relatedTarget: this }, a, i)
          );
        return (
          this._supress[e] ||
            (t.each(this._plugins, function (t, e) {
              e.onTrigger && e.onTrigger(c);
            }),
            this.register({ type: s.Type.Event, name: e }),
            this.$element.trigger(c),
            this.settings &&
              "function" == typeof this.settings[l] &&
              this.settings[l].call(this, c)),
          c
        );
      }),
      (s.prototype.enter = function (e) {
        t.each(
          [e].concat(this._states.tags[e] || []),
          t.proxy(function (t, e) {
            this._states.current[e] === n && (this._states.current[e] = 0),
              this._states.current[e]++;
          }, this)
        );
      }),
      (s.prototype.leave = function (e) {
        t.each(
          [e].concat(this._states.tags[e] || []),
          t.proxy(function (t, e) {
            this._states.current[e]--;
          }, this)
        );
      }),
      (s.prototype.register = function (e) {
        if (e.type === s.Type.Event) {
          if (
            (t.event.special[e.name] || (t.event.special[e.name] = {}),
            !t.event.special[e.name].owl)
          ) {
            var i = t.event.special[e.name]._default;
            (t.event.special[e.name]._default = function (t) {
              return !i ||
                !i.apply ||
                (t.namespace && -1 !== t.namespace.indexOf("owl"))
                ? t.namespace && t.namespace.indexOf("owl") > -1
                : i.apply(this, arguments);
            }),
              (t.event.special[e.name].owl = !0);
          }
        } else
          e.type === s.Type.State &&
            (this._states.tags[e.name]
              ? (this._states.tags[e.name] = this._states.tags[e.name].concat(
                  e.tags
                ))
              : (this._states.tags[e.name] = e.tags),
            (this._states.tags[e.name] = t.grep(
              this._states.tags[e.name],
              t.proxy(function (i, n) {
                return t.inArray(i, this._states.tags[e.name]) === n;
              }, this)
            )));
      }),
      (s.prototype.suppress = function (e) {
        t.each(
          e,
          t.proxy(function (t, e) {
            this._supress[e] = !0;
          }, this)
        );
      }),
      (s.prototype.release = function (e) {
        t.each(
          e,
          t.proxy(function (t, e) {
            delete this._supress[e];
          }, this)
        );
      }),
      (s.prototype.pointer = function (t) {
        var i = { x: null, y: null };
        return (
          (t =
            (t = t.originalEvent || t || e.event).touches && t.touches.length
              ? t.touches[0]
              : t.changedTouches && t.changedTouches.length
              ? t.changedTouches[0]
              : t).pageX
            ? ((i.x = t.pageX), (i.y = t.pageY))
            : ((i.x = t.clientX), (i.y = t.clientY)),
          i
        );
      }),
      (s.prototype.isNumeric = function (t) {
        return !isNaN(parseFloat(t));
      }),
      (s.prototype.difference = function (t, e) {
        return { x: t.x - e.x, y: t.y - e.y };
      }),
      (t.fn.owlCarousel = function (e) {
        var i = Array.prototype.slice.call(arguments, 1);
        return this.each(function () {
          var n = t(this),
            o = n.data("owl.carousel");
          o ||
            ((o = new s(this, "object" == typeof e && e)),
            n.data("owl.carousel", o),
            t.each(
              [
                "next",
                "prev",
                "to",
                "destroy",
                "refresh",
                "replace",
                "add",
                "remove",
              ],
              function (e, i) {
                o.register({ type: s.Type.Event, name: i }),
                  o.$element.on(
                    i + ".owl.carousel.core",
                    t.proxy(function (t) {
                      t.namespace &&
                        t.relatedTarget !== this &&
                        (this.suppress([i]),
                        o[i].apply(this, [].slice.call(arguments, 1)),
                        this.release([i]));
                    }, o)
                  );
              }
            )),
            "string" == typeof e && "_" !== e.charAt(0) && o[e].apply(o, i);
        });
      }),
      (t.fn.owlCarousel.Constructor = s);
  })(window.Zepto || window.jQuery, window, document),
  (function (t, e, i, n) {
    var s = function (e) {
      (this._core = e),
        (this._interval = null),
        (this._visible = null),
        (this._handlers = {
          "initialized.owl.carousel": t.proxy(function (t) {
            t.namespace && this._core.settings.autoRefresh && this.watch();
          }, this),
        }),
        (this._core.options = t.extend({}, s.Defaults, this._core.options)),
        this._core.$element.on(this._handlers);
    };
    (s.Defaults = { autoRefresh: !0, autoRefreshInterval: 500 }),
      (s.prototype.watch = function () {
        this._interval ||
          ((this._visible = this._core.isVisible()),
          (this._interval = e.setInterval(
            t.proxy(this.refresh, this),
            this._core.settings.autoRefreshInterval
          )));
      }),
      (s.prototype.refresh = function () {
        this._core.isVisible() !== this._visible &&
          ((this._visible = !this._visible),
          this._core.$element.toggleClass("owl-hidden", !this._visible),
          this._visible &&
            this._core.invalidate("width") &&
            this._core.refresh());
      }),
      (s.prototype.destroy = function () {
        var t, i;
        for (t in (e.clearInterval(this._interval), this._handlers))
          this._core.$element.off(t, this._handlers[t]);
        for (i in Object.getOwnPropertyNames(this))
          "function" != typeof this[i] && (this[i] = null);
      }),
      (t.fn.owlCarousel.Constructor.Plugins.AutoRefresh = s);
  })(window.Zepto || window.jQuery, window, document),
  (function (t, e, i, n) {
    var s = function (e) {
      (this._core = e),
        (this._loaded = []),
        (this._handlers = {
          "initialized.owl.carousel change.owl.carousel resized.owl.carousel":
            t.proxy(function (e) {
              if (
                e.namespace &&
                this._core.settings &&
                this._core.settings.lazyLoad &&
                ((e.property && "position" == e.property.name) ||
                  "initialized" == e.type)
              ) {
                var i = this._core.settings,
                  n = (i.center && Math.ceil(i.items / 2)) || i.items,
                  s = (i.center && -1 * n) || 0,
                  o =
                    (e.property && void 0 !== e.property.value
                      ? e.property.value
                      : this._core.current()) + s,
                  r = this._core.clones().length,
                  a = t.proxy(function (t, e) {
                    this.load(e);
                  }, this);
                for (
                  i.lazyLoadEager > 0 &&
                  ((n += i.lazyLoadEager),
                  i.loop && ((o -= i.lazyLoadEager), n++));
                  s++ < n;

                )
                  this.load(r / 2 + this._core.relative(o)),
                    r && t.each(this._core.clones(this._core.relative(o)), a),
                    o++;
              }
            }, this),
        }),
        (this._core.options = t.extend({}, s.Defaults, this._core.options)),
        this._core.$element.on(this._handlers);
    };
    (s.Defaults = { lazyLoad: !1, lazyLoadEager: 0 }),
      (s.prototype.load = function (i) {
        var n = this._core.$stage.children().eq(i),
          s = n && n.find(".owl-lazy");
        !s ||
          t.inArray(n.get(0), this._loaded) > -1 ||
          (s.each(
            t.proxy(function (i, n) {
              var s,
                o = t(n),
                r =
                  (e.devicePixelRatio > 1 && o.attr("data-src-retina")) ||
                  o.attr("data-src") ||
                  o.attr("data-srcset");
              this._core.trigger("load", { element: o, url: r }, "lazy"),
                o.is("img")
                  ? o
                      .one(
                        "load.owl.lazy",
                        t.proxy(function () {
                          o.css("opacity", 1),
                            this._core.trigger(
                              "loaded",
                              { element: o, url: r },
                              "lazy"
                            );
                        }, this)
                      )
                      .attr("src", r)
                  : o.is("source")
                  ? o
                      .one(
                        "load.owl.lazy",
                        t.proxy(function () {
                          this._core.trigger(
                            "loaded",
                            { element: o, url: r },
                            "lazy"
                          );
                        }, this)
                      )
                      .attr("srcset", r)
                  : (((s = new Image()).onload = t.proxy(function () {
                      o.css({
                        "background-image": 'url("' + r + '")',
                        opacity: "1",
                      }),
                        this._core.trigger(
                          "loaded",
                          { element: o, url: r },
                          "lazy"
                        );
                    }, this)),
                    (s.src = r));
            }, this)
          ),
          this._loaded.push(n.get(0)));
      }),
      (s.prototype.destroy = function () {
        var t, e;
        for (t in this.handlers) this._core.$element.off(t, this.handlers[t]);
        for (e in Object.getOwnPropertyNames(this))
          "function" != typeof this[e] && (this[e] = null);
      }),
      (t.fn.owlCarousel.Constructor.Plugins.Lazy = s);
  })(window.Zepto || window.jQuery, window, document),
  (function (t, e, i, n) {
    var s = function (i) {
      (this._core = i),
        (this._previousHeight = null),
        (this._handlers = {
          "initialized.owl.carousel refreshed.owl.carousel": t.proxy(function (
            t
          ) {
            t.namespace && this._core.settings.autoHeight && this.update();
          },
          this),
          "changed.owl.carousel": t.proxy(function (t) {
            t.namespace &&
              this._core.settings.autoHeight &&
              "position" === t.property.name &&
              this.update();
          }, this),
          "loaded.owl.lazy": t.proxy(function (t) {
            t.namespace &&
              this._core.settings.autoHeight &&
              t.element.closest("." + this._core.settings.itemClass).index() ===
                this._core.current() &&
              this.update();
          }, this),
        }),
        (this._core.options = t.extend({}, s.Defaults, this._core.options)),
        this._core.$element.on(this._handlers),
        (this._intervalId = null);
      var n = this;
      t(e).on("load", function () {
        n._core.settings.autoHeight && n.update();
      }),
        t(e).resize(function () {
          n._core.settings.autoHeight &&
            (null != n._intervalId && clearTimeout(n._intervalId),
            (n._intervalId = setTimeout(function () {
              n.update();
            }, 250)));
        });
    };
    (s.Defaults = { autoHeight: !1, autoHeightClass: "owl-height" }),
      (s.prototype.update = function () {
        var e = this._core._current,
          i = e + this._core.settings.items,
          n = this._core.settings.lazyLoad,
          s = this._core.$stage.children().toArray().slice(e, i),
          o = [],
          r = 0;
        t.each(s, function (e, i) {
          o.push(t(i).height());
        }),
          (r = Math.max.apply(null, o)) <= 1 &&
            n &&
            this._previousHeight &&
            (r = this._previousHeight),
          (this._previousHeight = r),
          this._core.$stage
            .parent()
            .height(r)
            .addClass(this._core.settings.autoHeightClass);
      }),
      (s.prototype.destroy = function () {
        var t, e;
        for (t in this._handlers) this._core.$element.off(t, this._handlers[t]);
        for (e in Object.getOwnPropertyNames(this))
          "function" != typeof this[e] && (this[e] = null);
      }),
      (t.fn.owlCarousel.Constructor.Plugins.AutoHeight = s);
  })(window.Zepto || window.jQuery, window, document),
  (function (t, e, i, n) {
    var s = function (e) {
      (this._core = e),
        (this._videos = {}),
        (this._playing = null),
        (this._handlers = {
          "initialized.owl.carousel": t.proxy(function (t) {
            t.namespace &&
              this._core.register({
                type: "state",
                name: "playing",
                tags: ["interacting"],
              });
          }, this),
          "resize.owl.carousel": t.proxy(function (t) {
            t.namespace &&
              this._core.settings.video &&
              this.isInFullScreen() &&
              t.preventDefault();
          }, this),
          "refreshed.owl.carousel": t.proxy(function (t) {
            t.namespace &&
              this._core.is("resizing") &&
              this._core.$stage.find(".cloned .owl-video-frame").remove();
          }, this),
          "changed.owl.carousel": t.proxy(function (t) {
            t.namespace &&
              "position" === t.property.name &&
              this._playing &&
              this.stop();
          }, this),
          "prepared.owl.carousel": t.proxy(function (e) {
            if (e.namespace) {
              var i = t(e.content).find(".owl-video");
              i.length &&
                (i.css("display", "none"), this.fetch(i, t(e.content)));
            }
          }, this),
        }),
        (this._core.options = t.extend({}, s.Defaults, this._core.options)),
        this._core.$element.on(this._handlers),
        this._core.$element.on(
          "click.owl.video",
          ".owl-video-play-icon",
          t.proxy(function (t) {
            this.play(t);
          }, this)
        );
    };
    (s.Defaults = { video: !1, videoHeight: !1, videoWidth: !1 }),
      (s.prototype.fetch = function (t, e) {
        var i = t.attr("data-vimeo-id")
            ? "vimeo"
            : t.attr("data-vzaar-id")
            ? "vzaar"
            : "youtube",
          n =
            t.attr("data-vimeo-id") ||
            t.attr("data-youtube-id") ||
            t.attr("data-vzaar-id"),
          s = t.attr("data-width") || this._core.settings.videoWidth,
          o = t.attr("data-height") || this._core.settings.videoHeight,
          r = t.attr("href");
        if (!r) throw new Error("Missing video URL.");
        if (
          (n = r.match(
            /(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com|be\-nocookie\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/
          ))[3].indexOf("youtu") > -1
        )
          i = "youtube";
        else if (n[3].indexOf("vimeo") > -1) i = "vimeo";
        else {
          if (!(n[3].indexOf("vzaar") > -1))
            throw new Error("Video URL not supported.");
          i = "vzaar";
        }
        (n = n[6]),
          (this._videos[r] = { type: i, id: n, width: s, height: o }),
          e.attr("data-video", r),
          this.thumbnail(t, this._videos[r]);
      }),
      (s.prototype.thumbnail = function (e, i) {
        var n,
          s,
          o =
            i.width && i.height
              ? "width:" + i.width + "px;height:" + i.height + "px;"
              : "",
          r = e.find("img"),
          a = "src",
          l = "",
          c = this._core.settings,
          d = function (i) {
            (n = c.lazyLoad
              ? t("<div/>", { class: "owl-video-tn " + l, srcType: i })
              : t("<div/>", {
                  class: "owl-video-tn",
                  style: "opacity:1;background-image:url(" + i + ")",
                })),
              e.after(n),
              e.after('<div class="owl-video-play-icon"></div>');
          };
        if (
          (e.wrap(t("<div/>", { class: "owl-video-wrapper", style: o })),
          this._core.settings.lazyLoad && ((a = "data-src"), (l = "owl-lazy")),
          r.length)
        )
          return d(r.attr(a)), r.remove(), !1;
        "youtube" === i.type
          ? ((s = "//img.youtube.com/vi/" + i.id + "/hqdefault.jpg"), d(s))
          : "vimeo" === i.type
          ? t.ajax({
              type: "GET",
              url: "//vimeo.com/api/v2/video/" + i.id + ".json",
              jsonp: "callback",
              dataType: "jsonp",
              success: function (t) {
                (s = t[0].thumbnail_large), d(s);
              },
            })
          : "vzaar" === i.type &&
            t.ajax({
              type: "GET",
              url: "//vzaar.com/api/videos/" + i.id + ".json",
              jsonp: "callback",
              dataType: "jsonp",
              success: function (t) {
                (s = t.framegrab_url), d(s);
              },
            });
      }),
      (s.prototype.stop = function () {
        this._core.trigger("stop", null, "video"),
          this._playing.find(".owl-video-frame").remove(),
          this._playing.removeClass("owl-video-playing"),
          (this._playing = null),
          this._core.leave("playing"),
          this._core.trigger("stopped", null, "video");
      }),
      (s.prototype.play = function (e) {
        var i,
          n = t(e.target).closest("." + this._core.settings.itemClass),
          s = this._videos[n.attr("data-video")],
          o = s.width || "100%",
          r = s.height || this._core.$stage.height();
        this._playing ||
          (this._core.enter("playing"),
          this._core.trigger("play", null, "video"),
          (n = this._core.items(this._core.relative(n.index()))),
          this._core.reset(n.index()),
          (i = t(
            '<iframe frameborder="0" allowfullscreen mozallowfullscreen webkitAllowFullScreen ></iframe>'
          )).attr("height", r),
          i.attr("width", o),
          "youtube" === s.type
            ? i.attr(
                "src",
                "//www.youtube.com/embed/" +
                  s.id +
                  "?autoplay=1&rel=0&v=" +
                  s.id
              )
            : "vimeo" === s.type
            ? i.attr("src", "//player.vimeo.com/video/" + s.id + "?autoplay=1")
            : "vzaar" === s.type &&
              i.attr(
                "src",
                "//view.vzaar.com/" + s.id + "/player?autoplay=true"
              ),
          t(i)
            .wrap('<div class="owl-video-frame" />')
            .insertAfter(n.find(".owl-video")),
          (this._playing = n.addClass("owl-video-playing")));
      }),
      (s.prototype.isInFullScreen = function () {
        var e =
          i.fullscreenElement ||
          i.mozFullScreenElement ||
          i.webkitFullscreenElement;
        return e && t(e).parent().hasClass("owl-video-frame");
      }),
      (s.prototype.destroy = function () {
        var t, e;
        for (t in (this._core.$element.off("click.owl.video"), this._handlers))
          this._core.$element.off(t, this._handlers[t]);
        for (e in Object.getOwnPropertyNames(this))
          "function" != typeof this[e] && (this[e] = null);
      }),
      (t.fn.owlCarousel.Constructor.Plugins.Video = s);
  })(window.Zepto || window.jQuery, window, document),
  (function (t, e, i, n) {
    var s = function (e) {
      (this.core = e),
        (this.core.options = t.extend({}, s.Defaults, this.core.options)),
        (this.swapping = !0),
        (this.previous = n),
        (this.next = n),
        (this.handlers = {
          "change.owl.carousel": t.proxy(function (t) {
            t.namespace &&
              "position" == t.property.name &&
              ((this.previous = this.core.current()),
              (this.next = t.property.value));
          }, this),
          "drag.owl.carousel dragged.owl.carousel translated.owl.carousel":
            t.proxy(function (t) {
              t.namespace && (this.swapping = "translated" == t.type);
            }, this),
          "translate.owl.carousel": t.proxy(function (t) {
            t.namespace &&
              this.swapping &&
              (this.core.options.animateOut || this.core.options.animateIn) &&
              this.swap();
          }, this),
        }),
        this.core.$element.on(this.handlers);
    };
    (s.Defaults = { animateOut: !1, animateIn: !1 }),
      (s.prototype.swap = function () {
        if (
          1 === this.core.settings.items &&
          t.support.animation &&
          t.support.transition
        ) {
          this.core.speed(0);
          var e,
            i = t.proxy(this.clear, this),
            n = this.core.$stage.children().eq(this.previous),
            s = this.core.$stage.children().eq(this.next),
            o = this.core.settings.animateIn,
            r = this.core.settings.animateOut;
          this.core.current() !== this.previous &&
            (r &&
              ((e =
                this.core.coordinates(this.previous) -
                this.core.coordinates(this.next)),
              n
                .one(t.support.animation.end, i)
                .css({ left: e + "px" })
                .addClass("animated owl-animated-out")
                .addClass(r)),
            o &&
              s
                .one(t.support.animation.end, i)
                .addClass("animated owl-animated-in")
                .addClass(o));
        }
      }),
      (s.prototype.clear = function (e) {
        t(e.target)
          .css({ left: "" })
          .removeClass("animated owl-animated-out owl-animated-in")
          .removeClass(this.core.settings.animateIn)
          .removeClass(this.core.settings.animateOut),
          this.core.onTransitionEnd();
      }),
      (s.prototype.destroy = function () {
        var t, e;
        for (t in this.handlers) this.core.$element.off(t, this.handlers[t]);
        for (e in Object.getOwnPropertyNames(this))
          "function" != typeof this[e] && (this[e] = null);
      }),
      (t.fn.owlCarousel.Constructor.Plugins.Animate = s);
  })(window.Zepto || window.jQuery, window, document),
  (function (t, e, i, n) {
    var s = function (e) {
      (this._core = e),
        (this._call = null),
        (this._time = 0),
        (this._timeout = 0),
        (this._paused = !0),
        (this._handlers = {
          "changed.owl.carousel": t.proxy(function (t) {
            t.namespace && "settings" === t.property.name
              ? this._core.settings.autoplay
                ? this.play()
                : this.stop()
              : t.namespace &&
                "position" === t.property.name &&
                this._paused &&
                (this._time = 0);
          }, this),
          "initialized.owl.carousel": t.proxy(function (t) {
            t.namespace && this._core.settings.autoplay && this.play();
          }, this),
          "play.owl.autoplay": t.proxy(function (t, e, i) {
            t.namespace && this.play(e, i);
          }, this),
          "stop.owl.autoplay": t.proxy(function (t) {
            t.namespace && this.stop();
          }, this),
          "mouseover.owl.autoplay": t.proxy(function () {
            this._core.settings.autoplayHoverPause &&
              this._core.is("rotating") &&
              this.pause();
          }, this),
          "mouseleave.owl.autoplay": t.proxy(function () {
            this._core.settings.autoplayHoverPause &&
              this._core.is("rotating") &&
              this.play();
          }, this),
          "touchstart.owl.core": t.proxy(function () {
            this._core.settings.autoplayHoverPause &&
              this._core.is("rotating") &&
              this.pause();
          }, this),
          "touchend.owl.core": t.proxy(function () {
            this._core.settings.autoplayHoverPause && this.play();
          }, this),
        }),
        this._core.$element.on(this._handlers),
        (this._core.options = t.extend({}, s.Defaults, this._core.options));
    };
    (s.Defaults = {
      autoplay: !1,
      autoplayTimeout: 5e3,
      autoplayHoverPause: !1,
      autoplaySpeed: !1,
    }),
      (s.prototype._next = function (n) {
        (this._call = e.setTimeout(
          t.proxy(this._next, this, n),
          this._timeout * (Math.round(this.read() / this._timeout) + 1) -
            this.read()
        )),
          this._core.is("interacting") ||
            i.hidden ||
            this._core.next(n || this._core.settings.autoplaySpeed);
      }),
      (s.prototype.read = function () {
        return new Date().getTime() - this._time;
      }),
      (s.prototype.play = function (i, n) {
        var s;
        this._core.is("rotating") || this._core.enter("rotating"),
          (i = i || this._core.settings.autoplayTimeout),
          (s = Math.min(this._time % (this._timeout || i), i)),
          this._paused
            ? ((this._time = this.read()), (this._paused = !1))
            : e.clearTimeout(this._call),
          (this._time += (this.read() % i) - s),
          (this._timeout = i),
          (this._call = e.setTimeout(t.proxy(this._next, this, n), i - s));
      }),
      (s.prototype.stop = function () {
        this._core.is("rotating") &&
          ((this._time = 0),
          (this._paused = !0),
          e.clearTimeout(this._call),
          this._core.leave("rotating"));
      }),
      (s.prototype.pause = function () {
        this._core.is("rotating") &&
          !this._paused &&
          ((this._time = this.read()),
          (this._paused = !0),
          e.clearTimeout(this._call));
      }),
      (s.prototype.destroy = function () {
        var t, e;
        for (t in (this.stop(), this._handlers))
          this._core.$element.off(t, this._handlers[t]);
        for (e in Object.getOwnPropertyNames(this))
          "function" != typeof this[e] && (this[e] = null);
      }),
      (t.fn.owlCarousel.Constructor.Plugins.autoplay = s);
  })(window.Zepto || window.jQuery, window, document),
  (function (t, e, i, n) {
    "use strict";
    var s = function (e) {
      (this._core = e),
        (this._initialized = !1),
        (this._pages = []),
        (this._controls = {}),
        (this._templates = []),
        (this.$element = this._core.$element),
        (this._overrides = {
          next: this._core.next,
          prev: this._core.prev,
          to: this._core.to,
        }),
        (this._handlers = {
          "prepared.owl.carousel": t.proxy(function (e) {
            e.namespace &&
              this._core.settings.dotsData &&
              this._templates.push(
                '<div class="' +
                  this._core.settings.dotClass +
                  '">' +
                  t(e.content)
                    .find("[data-dot]")
                    .addBack("[data-dot]")
                    .attr("data-dot") +
                  "</div>"
              );
          }, this),
          "added.owl.carousel": t.proxy(function (t) {
            t.namespace &&
              this._core.settings.dotsData &&
              this._templates.splice(t.position, 0, this._templates.pop());
          }, this),
          "remove.owl.carousel": t.proxy(function (t) {
            t.namespace &&
              this._core.settings.dotsData &&
              this._templates.splice(t.position, 1);
          }, this),
          "changed.owl.carousel": t.proxy(function (t) {
            t.namespace && "position" == t.property.name && this.draw();
          }, this),
          "initialized.owl.carousel": t.proxy(function (t) {
            t.namespace &&
              !this._initialized &&
              (this._core.trigger("initialize", null, "navigation"),
              this.initialize(),
              this.update(),
              this.draw(),
              (this._initialized = !0),
              this._core.trigger("initialized", null, "navigation"));
          }, this),
          "refreshed.owl.carousel": t.proxy(function (t) {
            t.namespace &&
              this._initialized &&
              (this._core.trigger("refresh", null, "navigation"),
              this.update(),
              this.draw(),
              this._core.trigger("refreshed", null, "navigation"));
          }, this),
        }),
        (this._core.options = t.extend({}, s.Defaults, this._core.options)),
        this.$element.on(this._handlers);
    };
    (s.Defaults = {
      nav: !1,
      navText: [
        '<span aria-label="Previous">&#x2039;</span>',
        '<span aria-label="Next">&#x203a;</span>',
      ],
      navSpeed: !1,
      navElement: 'button type="button" role="presentation"',
      navContainer: !1,
      navContainerClass: "owl-nav",
      navClass: ["owl-prev", "owl-next"],
      slideBy: 1,
      dotClass: "owl-dot",
      dotsClass: "owl-dots",
      dots: !0,
      dotsEach: !1,
      dotsData: !1,
      dotsSpeed: !1,
      dotsContainer: !1,
    }),
      (s.prototype.initialize = function () {
        var e,
          i = this._core.settings;
        for (e in ((this._controls.$relative = (
          i.navContainer
            ? t(i.navContainer)
            : t("<div>").addClass(i.navContainerClass).appendTo(this.$element)
        ).addClass("disabled")),
        (this._controls.$previous = t("<" + i.navElement + ">")
          .addClass(i.navClass[0])
          .html(i.navText[0])
          .prependTo(this._controls.$relative)
          .on(
            "click",
            t.proxy(function (t) {
              this.prev(i.navSpeed);
            }, this)
          )),
        (this._controls.$next = t("<" + i.navElement + ">")
          .addClass(i.navClass[1])
          .html(i.navText[1])
          .appendTo(this._controls.$relative)
          .on(
            "click",
            t.proxy(function (t) {
              this.next(i.navSpeed);
            }, this)
          )),
        i.dotsData ||
          (this._templates = [
            t('<button role="button">')
              .addClass(i.dotClass)
              .append(t("<span>"))
              .prop("outerHTML"),
          ]),
        (this._controls.$absolute = (
          i.dotsContainer
            ? t(i.dotsContainer)
            : t("<div>").addClass(i.dotsClass).appendTo(this.$element)
        ).addClass("disabled")),
        this._controls.$absolute.on(
          "click",
          "button",
          t.proxy(function (e) {
            var n = t(e.target).parent().is(this._controls.$absolute)
              ? t(e.target).index()
              : t(e.target).parent().index();
            e.preventDefault(), this.to(n, i.dotsSpeed);
          }, this)
        ),
        this._overrides))
          this._core[e] = t.proxy(this[e], this);
      }),
      (s.prototype.destroy = function () {
        var t, e, i, n, s;
        for (t in ((s = this._core.settings), this._handlers))
          this.$element.off(t, this._handlers[t]);
        for (e in this._controls)
          "$relative" === e && s.navContainer
            ? this._controls[e].html("")
            : this._controls[e].remove();
        for (n in this.overides) this._core[n] = this._overrides[n];
        for (i in Object.getOwnPropertyNames(this))
          "function" != typeof this[i] && (this[i] = null);
      }),
      (s.prototype.update = function () {
        var t,
          e,
          i = this._core.clones().length / 2,
          n = i + this._core.items().length,
          s = this._core.maximum(!0),
          o = this._core.settings,
          r = o.center || o.autoWidth || o.dotsData ? 1 : o.dotsEach || o.items;
        if (
          ("page" !== o.slideBy && (o.slideBy = Math.min(o.slideBy, o.items)),
          o.dots || "page" == o.slideBy)
        )
          for (this._pages = [], t = i, e = 0; t < n; t++) {
            if (e >= r || 0 === e) {
              if (
                (this._pages.push({
                  start: Math.min(s, t - i),
                  end: t - i + r - 1,
                }),
                Math.min(s, t - i) === s)
              )
                break;
              e = 0;
            }
            e += this._core.mergers(this._core.relative(t));
          }
      }),
      (s.prototype.draw = function () {
        var e,
          i = this._core.settings,
          n = this._core.items().length <= i.items,
          s = this._core.relative(this._core.current()),
          o = i.loop || i.rewind;
        this._controls.$relative.toggleClass("disabled", !i.nav || n),
          i.nav &&
            (this._controls.$previous.toggleClass(
              "disabled",
              !o && s <= this._core.minimum(!0)
            ),
            this._controls.$next.toggleClass(
              "disabled",
              !o && s >= this._core.maximum(!0)
            )),
          this._controls.$absolute.toggleClass("disabled", !i.dots || n),
          i.dots &&
            ((e =
              this._pages.length - this._controls.$absolute.children().length),
            i.dotsData && 0 !== e
              ? this._controls.$absolute.html(this._templates.join(""))
              : e > 0
              ? this._controls.$absolute.append(
                  new Array(e + 1).join(this._templates[0])
                )
              : e < 0 && this._controls.$absolute.children().slice(e).remove(),
            this._controls.$absolute.find(".active").removeClass("active"),
            this._controls.$absolute
              .children()
              .eq(t.inArray(this.current(), this._pages))
              .addClass("active"));
      }),
      (s.prototype.onTrigger = function (e) {
        var i = this._core.settings;
        e.page = {
          index: t.inArray(this.current(), this._pages),
          count: this._pages.length,
          size:
            i &&
            (i.center || i.autoWidth || i.dotsData ? 1 : i.dotsEach || i.items),
        };
      }),
      (s.prototype.current = function () {
        var e = this._core.relative(this._core.current());
        return t
          .grep(
            this._pages,
            t.proxy(function (t, i) {
              return t.start <= e && t.end >= e;
            }, this)
          )
          .pop();
      }),
      (s.prototype.getPosition = function (e) {
        var i,
          n,
          s = this._core.settings;
        return (
          "page" == s.slideBy
            ? ((i = t.inArray(this.current(), this._pages)),
              (n = this._pages.length),
              e ? ++i : --i,
              (i = this._pages[((i % n) + n) % n].start))
            : ((i = this._core.relative(this._core.current())),
              (n = this._core.items().length),
              e ? (i += s.slideBy) : (i -= s.slideBy)),
          i
        );
      }),
      (s.prototype.next = function (e) {
        t.proxy(this._overrides.to, this._core)(this.getPosition(!0), e);
      }),
      (s.prototype.prev = function (e) {
        t.proxy(this._overrides.to, this._core)(this.getPosition(!1), e);
      }),
      (s.prototype.to = function (e, i, n) {
        var s;
        !n && this._pages.length
          ? ((s = this._pages.length),
            t.proxy(this._overrides.to, this._core)(
              this._pages[((e % s) + s) % s].start,
              i
            ))
          : t.proxy(this._overrides.to, this._core)(e, i);
      }),
      (t.fn.owlCarousel.Constructor.Plugins.Navigation = s);
  })(window.Zepto || window.jQuery, window, document),
  (function (t, e, i, n) {
    "use strict";
    var s = function (i) {
      (this._core = i),
        (this._hashes = {}),
        (this.$element = this._core.$element),
        (this._handlers = {
          "initialized.owl.carousel": t.proxy(function (i) {
            i.namespace &&
              "URLHash" === this._core.settings.startPosition &&
              t(e).trigger("hashchange.owl.navigation");
          }, this),
          "prepared.owl.carousel": t.proxy(function (e) {
            if (e.namespace) {
              var i = t(e.content)
                .find("[data-hash]")
                .addBack("[data-hash]")
                .attr("data-hash");
              if (!i) return;
              this._hashes[i] = e.content;
            }
          }, this),
          "changed.owl.carousel": t.proxy(function (i) {
            if (i.namespace && "position" === i.property.name) {
              var n = this._core.items(
                  this._core.relative(this._core.current())
                ),
                s = t
                  .map(this._hashes, function (t, e) {
                    return t === n ? e : null;
                  })
                  .join();
              if (!s || e.location.hash.slice(1) === s) return;
              e.location.hash = s;
            }
          }, this),
        }),
        (this._core.options = t.extend({}, s.Defaults, this._core.options)),
        this.$element.on(this._handlers),
        t(e).on(
          "hashchange.owl.navigation",
          t.proxy(function (t) {
            var i = e.location.hash.substring(1),
              n = this._core.$stage.children(),
              s = this._hashes[i] && n.index(this._hashes[i]);
            void 0 !== s &&
              s !== this._core.current() &&
              this._core.to(this._core.relative(s), !1, !0);
          }, this)
        );
    };
    (s.Defaults = { URLhashListener: !1 }),
      (s.prototype.destroy = function () {
        var i, n;
        for (i in (t(e).off("hashchange.owl.navigation"), this._handlers))
          this._core.$element.off(i, this._handlers[i]);
        for (n in Object.getOwnPropertyNames(this))
          "function" != typeof this[n] && (this[n] = null);
      }),
      (t.fn.owlCarousel.Constructor.Plugins.Hash = s);
  })(window.Zepto || window.jQuery, window, document),
  (function (t, e, i, n) {
    function s(e, i) {
      var s = !1,
        o = e.charAt(0).toUpperCase() + e.slice(1);
      return (
        t.each((e + " " + a.join(o + " ") + o).split(" "), function (t, e) {
          if (r[e] !== n) return (s = !i || e), !1;
        }),
        s
      );
    }
    function o(t) {
      return s(t, !0);
    }
    var r = t("<support>").get(0).style,
      a = "Webkit Moz O ms".split(" "),
      l = {
        transition: {
          end: {
            WebkitTransition: "webkitTransitionEnd",
            MozTransition: "transitionend",
            OTransition: "oTransitionEnd",
            transition: "transitionend",
          },
        },
        animation: {
          end: {
            WebkitAnimation: "webkitAnimationEnd",
            MozAnimation: "animationend",
            OAnimation: "oAnimationEnd",
            animation: "animationend",
          },
        },
      };
    !!s("transition") &&
      ((t.support.transition = new String(o("transition"))),
      (t.support.transition.end = l.transition.end[t.support.transition])),
      !!s("animation") &&
        ((t.support.animation = new String(o("animation"))),
        (t.support.animation.end = l.animation.end[t.support.animation])),
      s("transform") &&
        ((t.support.transform = new String(o("transform"))),
        (t.support.transform3d = !!s("perspective")));
  })(window.Zepto || window.jQuery, window, document),
  function () {
    var t,
      e,
      i,
      n,
      s,
      o = function (t, e) {
        return function () {
          return t.apply(e, arguments);
        };
      },
      r =
        [].indexOf ||
        function (t) {
          for (var e = 0, i = this.length; i > e; e++)
            if (e in this && this[e] === t) return e;
          return -1;
        };
    (e = (function () {
      function t() {}
      return (
        (t.prototype.extend = function (t, e) {
          var i, n;
          for (i in e) (n = e[i]), null == t[i] && (t[i] = n);
          return t;
        }),
        (t.prototype.isMobile = function (t) {
          return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
            t
          );
        }),
        (t.prototype.createEvent = function (t, e, i, n) {
          var s;
          return (
            null == e && (e = !1),
            null == i && (i = !1),
            null == n && (n = null),
            null != document.createEvent
              ? (s = document.createEvent("CustomEvent")).initCustomEvent(
                  t,
                  e,
                  i,
                  n
                )
              : null != document.createEventObject
              ? ((s = document.createEventObject()).eventType = t)
              : (s.eventName = t),
            s
          );
        }),
        (t.prototype.emitEvent = function (t, e) {
          return null != t.dispatchEvent
            ? t.dispatchEvent(e)
            : e in (null != t)
            ? t[e]()
            : "on" + e in (null != t)
            ? t["on" + e]()
            : void 0;
        }),
        (t.prototype.addEvent = function (t, e, i) {
          return null != t.addEventListener
            ? t.addEventListener(e, i, !1)
            : null != t.attachEvent
            ? t.attachEvent("on" + e, i)
            : (t[e] = i);
        }),
        (t.prototype.removeEvent = function (t, e, i) {
          return null != t.removeEventListener
            ? t.removeEventListener(e, i, !1)
            : null != t.detachEvent
            ? t.detachEvent("on" + e, i)
            : delete t[e];
        }),
        (t.prototype.innerHeight = function () {
          return "innerHeight" in window
            ? window.innerHeight
            : document.documentElement.clientHeight;
        }),
        t
      );
    })()),
      (i =
        this.WeakMap ||
        this.MozWeakMap ||
        (i = (function () {
          function t() {
            (this.keys = []), (this.values = []);
          }
          return (
            (t.prototype.get = function (t) {
              var e, i, n, s;
              for (e = i = 0, n = (s = this.keys).length; n > i; e = ++i)
                if (s[e] === t) return this.values[e];
            }),
            (t.prototype.set = function (t, e) {
              var i, n, s, o;
              for (i = n = 0, s = (o = this.keys).length; s > n; i = ++n)
                if (o[i] === t) return void (this.values[i] = e);
              return this.keys.push(t), this.values.push(e);
            }),
            t
          );
        })())),
      (t =
        this.MutationObserver ||
        this.WebkitMutationObserver ||
        this.MozMutationObserver ||
        (t = (function () {
          function t() {
            "undefined" != typeof console &&
              null !== console &&
              console.warn(
                "MutationObserver is not supported by your browser."
              ),
              "undefined" != typeof console &&
                null !== console &&
                console.warn(
                  "WOW.js cannot detect dom mutations, please call .sync() after loading new content."
                );
          }
          return (
            (t.notSupported = !0), (t.prototype.observe = function () {}), t
          );
        })())),
      (n =
        this.getComputedStyle ||
        function (t, e) {
          return (
            (this.getPropertyValue = function (e) {
              var i;
              return (
                "float" === e && (e = "styleFloat"),
                s.test(e) &&
                  e.replace(s, function (t, e) {
                    return e.toUpperCase();
                  }),
                (null != (i = t.currentStyle) ? i[e] : void 0) || null
              );
            }),
            this
          );
        }),
      (s = /(\-([a-z]){1})/g),
      (this.WOW = (function () {
        function s(t) {
          null == t && (t = {}),
            (this.scrollCallback = o(this.scrollCallback, this)),
            (this.scrollHandler = o(this.scrollHandler, this)),
            (this.resetAnimation = o(this.resetAnimation, this)),
            (this.start = o(this.start, this)),
            (this.scrolled = !0),
            (this.config = this.util().extend(t, this.defaults)),
            null != t.scrollContainer &&
              (this.config.scrollContainer = document.querySelector(
                t.scrollContainer
              )),
            (this.animationNameCache = new i()),
            (this.wowEvent = this.util().createEvent(this.config.boxClass));
        }
        return (
          (s.prototype.defaults = {
            boxClass: "wow",
            animateClass: "animated",
            offset: 0,
            mobile: !0,
            live: !0,
            callback: null,
            scrollContainer: null,
          }),
          (s.prototype.init = function () {
            var t;
            return (
              (this.element = window.document.documentElement),
              "interactive" === (t = document.readyState) || "complete" === t
                ? this.start()
                : this.util().addEvent(
                    document,
                    "DOMContentLoaded",
                    this.start
                  ),
              (this.finished = [])
            );
          }),
          (s.prototype.start = function () {
            var e, i, n, s;
            if (
              ((this.stopped = !1),
              (this.boxes = function () {
                var t, i, n, s;
                for (
                  s = [],
                    t = 0,
                    i = (n = this.element.querySelectorAll(
                      "." + this.config.boxClass
                    )).length;
                  i > t;
                  t++
                )
                  (e = n[t]), s.push(e);
                return s;
              }.call(this)),
              (this.all = function () {
                var t, i, n, s;
                for (s = [], t = 0, i = (n = this.boxes).length; i > t; t++)
                  (e = n[t]), s.push(e);
                return s;
              }.call(this)),
              this.boxes.length)
            )
              if (this.disabled()) this.resetStyle();
              else
                for (i = 0, n = (s = this.boxes).length; n > i; i++)
                  (e = s[i]), this.applyStyle(e, !0);
            return (
              this.disabled() ||
                (this.util().addEvent(
                  this.config.scrollContainer || window,
                  "scroll",
                  this.scrollHandler
                ),
                this.util().addEvent(window, "resize", this.scrollHandler),
                (this.interval = setInterval(this.scrollCallback, 50))),
              this.config.live
                ? new t(
                    (function (t) {
                      return function (e) {
                        var i, n, s, o, r;
                        for (r = [], i = 0, n = e.length; n > i; i++)
                          (o = e[i]),
                            r.push(
                              function () {
                                var t, e, i, n;
                                for (
                                  n = [],
                                    t = 0,
                                    e = (i = o.addedNodes || []).length;
                                  e > t;
                                  t++
                                )
                                  (s = i[t]), n.push(this.doSync(s));
                                return n;
                              }.call(t)
                            );
                        return r;
                      };
                    })(this)
                  ).observe(document.body, { childList: !0, subtree: !0 })
                : void 0
            );
          }),
          (s.prototype.stop = function () {
            return (
              (this.stopped = !0),
              this.util().removeEvent(
                this.config.scrollContainer || window,
                "scroll",
                this.scrollHandler
              ),
              this.util().removeEvent(window, "resize", this.scrollHandler),
              null != this.interval ? clearInterval(this.interval) : void 0
            );
          }),
          (s.prototype.sync = function (e) {
            return t.notSupported ? this.doSync(this.element) : void 0;
          }),
          (s.prototype.doSync = function (t) {
            var e, i, n, s, o;
            if ((null == t && (t = this.element), 1 === t.nodeType)) {
              for (
                o = [],
                  i = 0,
                  n = (s = (t = t.parentNode || t).querySelectorAll(
                    "." + this.config.boxClass
                  )).length;
                n > i;
                i++
              )
                (e = s[i]),
                  r.call(this.all, e) < 0
                    ? (this.boxes.push(e),
                      this.all.push(e),
                      this.stopped || this.disabled()
                        ? this.resetStyle()
                        : this.applyStyle(e, !0),
                      o.push((this.scrolled = !0)))
                    : o.push(void 0);
              return o;
            }
          }),
          (s.prototype.show = function (t) {
            return (
              this.applyStyle(t),
              (t.className = t.className + " " + this.config.animateClass),
              null != this.config.callback && this.config.callback(t),
              this.util().emitEvent(t, this.wowEvent),
              this.util().addEvent(t, "animationend", this.resetAnimation),
              this.util().addEvent(t, "oanimationend", this.resetAnimation),
              this.util().addEvent(
                t,
                "webkitAnimationEnd",
                this.resetAnimation
              ),
              this.util().addEvent(t, "MSAnimationEnd", this.resetAnimation),
              t
            );
          }),
          (s.prototype.applyStyle = function (t, e) {
            var i, n, s, o;
            return (
              (n = t.getAttribute("data-wow-duration")),
              (i = t.getAttribute("data-wow-delay")),
              (s = t.getAttribute("data-wow-iteration")),
              this.animate(
                ((o = this),
                function () {
                  return o.customStyle(t, e, n, i, s);
                })
              )
            );
          }),
          (s.prototype.animate =
            "requestAnimationFrame" in window
              ? function (t) {
                  return window.requestAnimationFrame(t);
                }
              : function (t) {
                  return t();
                }),
          (s.prototype.resetStyle = function () {
            var t, e, i, n, s;
            for (s = [], e = 0, i = (n = this.boxes).length; i > e; e++)
              (t = n[e]), s.push((t.style.visibility = "visible"));
            return s;
          }),
          (s.prototype.resetAnimation = function (t) {
            var e;
            return t.type.toLowerCase().indexOf("animationend") >= 0
              ? ((e = t.target || t.srcElement).className = e.className
                  .replace(this.config.animateClass, "")
                  .trim())
              : void 0;
          }),
          (s.prototype.customStyle = function (t, e, i, n, s) {
            return (
              e && this.cacheAnimationName(t),
              (t.style.visibility = e ? "hidden" : "visible"),
              i && this.vendorSet(t.style, { animationDuration: i }),
              n && this.vendorSet(t.style, { animationDelay: n }),
              s && this.vendorSet(t.style, { animationIterationCount: s }),
              this.vendorSet(t.style, {
                animationName: e ? "none" : this.cachedAnimationName(t),
              }),
              t
            );
          }),
          (s.prototype.vendors = ["moz", "webkit"]),
          (s.prototype.vendorSet = function (t, e) {
            var i, n, s, o;
            for (i in ((n = []), e))
              (s = e[i]),
                (t["" + i] = s),
                n.push(
                  function () {
                    var e, n, r, a;
                    for (
                      a = [], e = 0, n = (r = this.vendors).length;
                      n > e;
                      e++
                    )
                      (o = r[e]),
                        a.push(
                          (t["" + o + i.charAt(0).toUpperCase() + i.substr(1)] =
                            s)
                        );
                    return a;
                  }.call(this)
                );
            return n;
          }),
          (s.prototype.vendorCSS = function (t, e) {
            var i, s, o, r, a, l;
            for (
              r = (a = n(t)).getPropertyCSSValue(e),
                i = 0,
                s = (o = this.vendors).length;
              s > i;
              i++
            )
              (l = o[i]), (r = r || a.getPropertyCSSValue("-" + l + "-" + e));
            return r;
          }),
          (s.prototype.animationName = function (t) {
            var e;
            try {
              e = this.vendorCSS(t, "animation-name").cssText;
            } catch (i) {
              e = n(t).getPropertyValue("animation-name");
            }
            return "none" === e ? "" : e;
          }),
          (s.prototype.cacheAnimationName = function (t) {
            return this.animationNameCache.set(t, this.animationName(t));
          }),
          (s.prototype.cachedAnimationName = function (t) {
            return this.animationNameCache.get(t);
          }),
          (s.prototype.scrollHandler = function () {
            return (this.scrolled = !0);
          }),
          (s.prototype.scrollCallback = function () {
            var t;
            return !this.scrolled ||
              ((this.scrolled = !1),
              (this.boxes = function () {
                var e, i, n, s;
                for (s = [], e = 0, i = (n = this.boxes).length; i > e; e++)
                  (t = n[e]) && (this.isVisible(t) ? this.show(t) : s.push(t));
                return s;
              }.call(this)),
              this.boxes.length || this.config.live)
              ? void 0
              : this.stop();
          }),
          (s.prototype.offsetTop = function (t) {
            for (var e; void 0 === t.offsetTop; ) t = t.parentNode;
            for (e = t.offsetTop; (t = t.offsetParent); ) e += t.offsetTop;
            return e;
          }),
          (s.prototype.isVisible = function (t) {
            var e, i, n, s, o;
            return (
              (i = t.getAttribute("data-wow-offset") || this.config.offset),
              (s =
                (o =
                  (this.config.scrollContainer &&
                    this.config.scrollContainer.scrollTop) ||
                  window.pageYOffset) +
                Math.min(this.element.clientHeight, this.util().innerHeight()) -
                i),
              (e = (n = this.offsetTop(t)) + t.clientHeight),
              s >= n && e >= o
            );
          }),
          (s.prototype.util = function () {
            return null != this._util ? this._util : (this._util = new e());
          }),
          (s.prototype.disabled = function () {
            return (
              !this.config.mobile && this.util().isMobile(navigator.userAgent)
            );
          }),
          s
        );
      })());
  }.call(this),
  (function (t) {
    t.fn.niceSelect = function (e) {
      function i(e) {
        e.after(
          t("<div></div>")
            .addClass("nice-select")
            .addClass(e.attr("class") || "")
            .addClass(e.attr("disabled") ? "disabled" : "")
            .attr("tabindex", e.attr("disabled") ? null : "0")
            .html('<span class="current"></span><ul class="list"></ul>')
        );
        var i = e.next(),
          n = e.find("option"),
          s = e.find("option:selected");
        i.find(".current").html(s.data("display") || s.text()),
          n.each(function (e) {
            var n = t(this),
              s = n.data("display");
            i.find("ul").append(
              t("<li></li>")
                .attr("data-value", n.val())
                .attr("data-display", s || null)
                .addClass(
                  "option" +
                    (n.is(":selected") ? " selected" : "") +
                    (n.is(":disabled") ? " disabled" : "")
                )
                .html(n.text())
            );
          });
      }
      if ("string" == typeof e)
        return (
          "update" == e
            ? this.each(function () {
                var e = t(this),
                  n = t(this).next(".nice-select"),
                  s = n.hasClass("open");
                n.length && (n.remove(), i(e), s && e.next().trigger("click"));
              })
            : "destroy" == e
            ? (this.each(function () {
                var e = t(this),
                  i = t(this).next(".nice-select");
                i.length && (i.remove(), e.css("display", ""));
              }),
              0 == t(".nice-select").length && t(document).off(".nice_select"))
            : console.log('Method "' + e + '" does not exist.'),
          this
        );
      this.hide(),
        this.each(function () {
          var e = t(this);
          e.next().hasClass("nice-select") || i(e);
        }),
        t(document).off(".nice_select"),
        t(document).on("click.nice_select", ".nice-select", function (e) {
          var i = t(this);
          t(".nice-select").not(i).removeClass("open"),
            i.toggleClass("open"),
            i.hasClass("open")
              ? (i.find(".option"),
                i.find(".focus").removeClass("focus"),
                i.find(".selected").addClass("focus"))
              : i.focus();
        }),
        t(document).on("click.nice_select", function (e) {
          0 === t(e.target).closest(".nice-select").length &&
            t(".nice-select").removeClass("open").find(".option");
        }),
        t(document).on(
          "click.nice_select",
          ".nice-select .option:not(.disabled)",
          function (e) {
            var i = t(this),
              n = i.closest(".nice-select");
            n.find(".selected").removeClass("selected"), i.addClass("selected");
            var s = i.data("display") || i.text();
            n.find(".current").text(s),
              n.prev("select").val(i.data("value")).trigger("change");
          }
        ),
        t(document).on("keydown.nice_select", ".nice-select", function (e) {
          var i = t(this),
            n = t(i.find(".focus") || i.find(".list .option.selected"));
          if (32 == e.keyCode || 13 == e.keyCode)
            return (
              i.hasClass("open") ? n.trigger("click") : i.trigger("click"), !1
            );
          if (40 == e.keyCode) {
            if (i.hasClass("open")) {
              var s = n.nextAll(".option:not(.disabled)").first();
              s.length > 0 &&
                (i.find(".focus").removeClass("focus"), s.addClass("focus"));
            } else i.trigger("click");
            return !1;
          }
          if (38 == e.keyCode) {
            if (i.hasClass("open")) {
              var o = n.prevAll(".option:not(.disabled)").first();
              o.length > 0 &&
                (i.find(".focus").removeClass("focus"), o.addClass("focus"));
            } else i.trigger("click");
            return !1;
          }
          if (27 == e.keyCode) i.hasClass("open") && i.trigger("click");
          else if (9 == e.keyCode && i.hasClass("open")) return !1;
        });
      var n = document.createElement("a").style;
      return (
        (n.cssText = "pointer-events:auto"),
        "auto" !== n.pointerEvents && t("html").addClass("no-csspointerevents"),
        this
      );
    };
  })(jQuery),
  (function (t) {
    "function" == typeof define && define.amd
      ? define(["jquery"], t)
      : t(jQuery);
  })(function (t) {
    t.extend(t.fn, {
      validate: function (e) {
        if (this.length) {
          var i = t.data(this[0], "validator");
          return (
            i ||
            (this.attr("novalidate", "novalidate"),
            (i = new t.validator(e, this[0])),
            t.data(this[0], "validator", i),
            i.settings.onsubmit &&
              (this.on("click.validate", ":submit", function (e) {
                i.settings.submitHandler && (i.submitButton = e.target),
                  t(this).hasClass("cancel") && (i.cancelSubmit = !0),
                  void 0 !== t(this).attr("formnovalidate") &&
                    (i.cancelSubmit = !0);
              }),
              this.on("submit.validate", function (e) {
                function n() {
                  var n, s;
                  return (
                    !i.settings.submitHandler ||
                    (i.submitButton &&
                      (n = t("<input type='hidden'/>")
                        .attr("name", i.submitButton.name)
                        .val(t(i.submitButton).val())
                        .appendTo(i.currentForm)),
                    (s = i.settings.submitHandler.call(i, i.currentForm, e)),
                    i.submitButton && n.remove(),
                    void 0 !== s && s)
                  );
                }
                return (
                  i.settings.debug && e.preventDefault(),
                  i.cancelSubmit
                    ? ((i.cancelSubmit = !1), n())
                    : i.form()
                    ? i.pendingRequest
                      ? ((i.formSubmitted = !0), !1)
                      : n()
                    : (i.focusInvalid(), !1)
                );
              })),
            i)
          );
        }
        e &&
          e.debug &&
          window.console &&
          console.warn("Nothing selected, can't validate, returning nothing.");
      },
      valid: function () {
        var e, i, n;
        return (
          t(this[0]).is("form")
            ? (e = this.validate().form())
            : ((n = []),
              (e = !0),
              (i = t(this[0].form).validate()),
              this.each(function () {
                (e = i.element(this) && e), (n = n.concat(i.errorList));
              }),
              (i.errorList = n)),
          e
        );
      },
      rules: function (e, i) {
        var n,
          s,
          o,
          r,
          a,
          l,
          c = this[0];
        if (e)
          switch (
            ((n = t.data(c.form, "validator").settings),
            (s = n.rules),
            (o = t.validator.staticRules(c)),
            e)
          ) {
            case "add":
              t.extend(o, t.validator.normalizeRule(i)),
                delete o.messages,
                (s[c.name] = o),
                i.messages &&
                  (n.messages[c.name] = t.extend(
                    n.messages[c.name],
                    i.messages
                  ));
              break;
            case "remove":
              return i
                ? ((l = {}),
                  t.each(i.split(/\s/), function (e, i) {
                    (l[i] = o[i]),
                      delete o[i],
                      "required" === i && t(c).removeAttr("aria-required");
                  }),
                  l)
                : (delete s[c.name], o);
          }
        return (
          (r = t.validator.normalizeRules(
            t.extend(
              {},
              t.validator.classRules(c),
              t.validator.attributeRules(c),
              t.validator.dataRules(c),
              t.validator.staticRules(c)
            ),
            c
          )).required &&
            ((a = r.required),
            delete r.required,
            (r = t.extend({ required: a }, r)),
            t(c).attr("aria-required", "true")),
          r.remote &&
            ((a = r.remote), delete r.remote, (r = t.extend(r, { remote: a }))),
          r
        );
      },
    }),
      t.extend(t.expr[":"], {
        blank: function (e) {
          return !t.trim("" + t(e).val());
        },
        filled: function (e) {
          return !!t.trim("" + t(e).val());
        },
        unchecked: function (e) {
          return !t(e).prop("checked");
        },
      }),
      (t.validator = function (e, i) {
        (this.settings = t.extend(!0, {}, t.validator.defaults, e)),
          (this.currentForm = i),
          this.init();
      }),
      (t.validator.format = function (e, i) {
        return 1 === arguments.length
          ? function () {
              var i = t.makeArray(arguments);
              return i.unshift(e), t.validator.format.apply(this, i);
            }
          : (arguments.length > 2 &&
              i.constructor !== Array &&
              (i = t.makeArray(arguments).slice(1)),
            i.constructor !== Array && (i = [i]),
            t.each(i, function (t, i) {
              e = e.replace(new RegExp("\\{" + t + "\\}", "g"), function () {
                return i;
              });
            }),
            e);
      }),
      t.extend(t.validator, {
        defaults: {
          messages: {},
          groups: {},
          rules: {},
          errorClass: "error",
          validClass: "valid",
          errorElement: "label",
          focusCleanup: !1,
          focusInvalid: !0,
          errorContainer: t([]),
          errorLabelContainer: t([]),
          onsubmit: !0,
          ignore: ":hidden",
          ignoreTitle: !1,
          onfocusin: function (t) {
            (this.lastActive = t),
              this.settings.focusCleanup &&
                (this.settings.unhighlight &&
                  this.settings.unhighlight.call(
                    this,
                    t,
                    this.settings.errorClass,
                    this.settings.validClass
                  ),
                this.hideThese(this.errorsFor(t)));
          },
          onfocusout: function (t) {
            this.checkable(t) ||
              (!(t.name in this.submitted) && this.optional(t)) ||
              this.element(t);
          },
          onkeyup: function (e, i) {
            (9 === i.which && "" === this.elementValue(e)) ||
              -1 !==
                t.inArray(
                  i.keyCode,
                  [16, 17, 18, 20, 35, 36, 37, 38, 39, 40, 45, 144, 225]
                ) ||
              ((e.name in this.submitted || e === this.lastElement) &&
                this.element(e));
          },
          onclick: function (t) {
            t.name in this.submitted
              ? this.element(t)
              : t.parentNode.name in this.submitted &&
                this.element(t.parentNode);
          },
          highlight: function (e, i, n) {
            "radio" === e.type
              ? this.findByName(e.name).addClass(i).removeClass(n)
              : t(e).addClass(i).removeClass(n);
          },
          unhighlight: function (e, i, n) {
            "radio" === e.type
              ? this.findByName(e.name).removeClass(i).addClass(n)
              : t(e).removeClass(i).addClass(n);
          },
        },
        setDefaults: function (e) {
          t.extend(t.validator.defaults, e);
        },
        messages: {
          required: "This field is required.",
          remote: "Please fix this field.",
          email: "Please enter a valid email address.",
          url: "Please enter a valid URL.",
          date: "Please enter a valid date.",
          dateISO: "Please enter a valid date ( ISO ).",
          number: "Please enter a valid number.",
          digits: "Please enter only digits.",
          creditcard: "Please enter a valid credit card number.",
          equalTo: "Please enter the same value again.",
          maxlength: t.validator.format(
            "Please enter no more than {0} characters."
          ),
          minlength: t.validator.format(
            "Please enter at least {0} characters."
          ),
          rangelength: t.validator.format(
            "Please enter a value between {0} and {1} characters long."
          ),
          range: t.validator.format(
            "Please enter a value between {0} and {1}."
          ),
          max: t.validator.format(
            "Please enter a value less than or equal to {0}."
          ),
          min: t.validator.format(
            "Please enter a value greater than or equal to {0}."
          ),
        },
        autoCreateRanges: !1,
        prototype: {
          init: function () {
            function e(e) {
              var i = t.data(this.form, "validator"),
                n = "on" + e.type.replace(/^validate/, ""),
                s = i.settings;
              s[n] && !t(this).is(s.ignore) && s[n].call(i, this, e);
            }
            (this.labelContainer = t(this.settings.errorLabelContainer)),
              (this.errorContext =
                (this.labelContainer.length && this.labelContainer) ||
                t(this.currentForm)),
              (this.containers = t(this.settings.errorContainer).add(
                this.settings.errorLabelContainer
              )),
              (this.submitted = {}),
              (this.valueCache = {}),
              (this.pendingRequest = 0),
              (this.pending = {}),
              (this.invalid = {}),
              this.reset();
            var i,
              n = (this.groups = {});
            t.each(this.settings.groups, function (e, i) {
              "string" == typeof i && (i = i.split(/\s/)),
                t.each(i, function (t, i) {
                  n[i] = e;
                });
            }),
              (i = this.settings.rules),
              t.each(i, function (e, n) {
                i[e] = t.validator.normalizeRule(n);
              }),
              t(this.currentForm)
                .on(
                  "focusin.validate focusout.validate keyup.validate",
                  ":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'], [type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], [type='radio'], [type='checkbox']",
                  e
                )
                .on(
                  "click.validate",
                  "select, option, [type='radio'], [type='checkbox']",
                  e
                ),
              this.settings.invalidHandler &&
                t(this.currentForm).on(
                  "invalid-form.validate",
                  this.settings.invalidHandler
                ),
              t(this.currentForm)
                .find("[required], [data-rule-required], .required")
                .attr("aria-required", "true");
          },
          form: function () {
            return (
              this.checkForm(),
              t.extend(this.submitted, this.errorMap),
              (this.invalid = t.extend({}, this.errorMap)),
              this.valid() ||
                t(this.currentForm).triggerHandler("invalid-form", [this]),
              this.showErrors(),
              this.valid()
            );
          },
          checkForm: function () {
            this.prepareForm();
            for (
              var t = 0, e = (this.currentElements = this.elements());
              e[t];
              t++
            )
              this.check(e[t]);
            return this.valid();
          },
          element: function (e) {
            var i = this.clean(e),
              n = this.validationTargetFor(i),
              s = !0;
            return (
              (this.lastElement = n),
              void 0 === n
                ? delete this.invalid[i.name]
                : (this.prepareElement(n),
                  (this.currentElements = t(n)),
                  (s = !1 !== this.check(n))
                    ? delete this.invalid[n.name]
                    : (this.invalid[n.name] = !0)),
              t(e).attr("aria-invalid", !s),
              this.numberOfInvalids() ||
                (this.toHide = this.toHide.add(this.containers)),
              this.showErrors(),
              s
            );
          },
          showErrors: function (e) {
            if (e) {
              for (var i in (t.extend(this.errorMap, e),
              (this.errorList = []),
              e))
                this.errorList.push({
                  message: e[i],
                  element: this.findByName(i)[0],
                });
              this.successList = t.grep(this.successList, function (t) {
                return !(t.name in e);
              });
            }
            this.settings.showErrors
              ? this.settings.showErrors.call(
                  this,
                  this.errorMap,
                  this.errorList
                )
              : this.defaultShowErrors();
          },
          resetForm: function () {
            t.fn.resetForm && t(this.currentForm).resetForm(),
              (this.submitted = {}),
              (this.lastElement = null),
              this.prepareForm(),
              this.hideErrors();
            var e,
              i = this.elements()
                .removeData("previousValue")
                .removeAttr("aria-invalid");
            if (this.settings.unhighlight)
              for (e = 0; i[e]; e++)
                this.settings.unhighlight.call(
                  this,
                  i[e],
                  this.settings.errorClass,
                  ""
                );
            else i.removeClass(this.settings.errorClass);
          },
          numberOfInvalids: function () {
            return this.objectLength(this.invalid);
          },
          objectLength: function (t) {
            var e,
              i = 0;
            for (e in t) i++;
            return i;
          },
          hideErrors: function () {
            this.hideThese(this.toHide);
          },
          hideThese: function (t) {
            t.not(this.containers).text(""), this.addWrapper(t).hide();
          },
          valid: function () {
            return 0 === this.size();
          },
          size: function () {
            return this.errorList.length;
          },
          focusInvalid: function () {
            if (this.settings.focusInvalid)
              try {
                t(
                  this.findLastActive() ||
                    (this.errorList.length && this.errorList[0].element) ||
                    []
                )
                  .filter(":visible")
                  .focus()
                  .trigger("focusin");
              } catch (t) {}
          },
          findLastActive: function () {
            var e = this.lastActive;
            return (
              e &&
              1 ===
                t.grep(this.errorList, function (t) {
                  return t.element.name === e.name;
                }).length &&
              e
            );
          },
          elements: function () {
            var e = this,
              i = {};
            return t(this.currentForm)
              .find("input, select, textarea")
              .not(":submit, :reset, :image, :disabled")
              .not(this.settings.ignore)
              .filter(function () {
                return (
                  !this.name &&
                    e.settings.debug &&
                    window.console &&
                    console.error("%o has no name assigned", this),
                  !(
                    this.name in i ||
                    !e.objectLength(t(this).rules()) ||
                    ((i[this.name] = !0), 0)
                  )
                );
              });
          },
          clean: function (e) {
            return t(e)[0];
          },
          errors: function () {
            var e = this.settings.errorClass.split(" ").join(".");
            return t(this.settings.errorElement + "." + e, this.errorContext);
          },
          reset: function () {
            (this.successList = []),
              (this.errorList = []),
              (this.errorMap = {}),
              (this.toShow = t([])),
              (this.toHide = t([])),
              (this.currentElements = t([]));
          },
          prepareForm: function () {
            this.reset(), (this.toHide = this.errors().add(this.containers));
          },
          prepareElement: function (t) {
            this.reset(), (this.toHide = this.errorsFor(t));
          },
          elementValue: function (e) {
            var i,
              n = t(e),
              s = e.type;
            return "radio" === s || "checkbox" === s
              ? this.findByName(e.name).filter(":checked").val()
              : "number" === s && void 0 !== e.validity
              ? !e.validity.badInput && n.val()
              : "string" == typeof (i = n.val())
              ? i.replace(/\r/g, "")
              : i;
          },
          check: function (e) {
            e = this.validationTargetFor(this.clean(e));
            var i,
              n,
              s,
              o = t(e).rules(),
              r = t.map(o, function (t, e) {
                return e;
              }).length,
              a = !1,
              l = this.elementValue(e);
            for (n in o) {
              s = { method: n, parameters: o[n] };
              try {
                if (
                  "dependency-mismatch" ===
                    (i = t.validator.methods[n].call(
                      this,
                      l,
                      e,
                      s.parameters
                    )) &&
                  1 === r
                ) {
                  a = !0;
                  continue;
                }
                if (((a = !1), "pending" === i))
                  return void (this.toHide = this.toHide.not(
                    this.errorsFor(e)
                  ));
                if (!i) return this.formatAndAdd(e, s), !1;
              } catch (t) {
                throw (
                  (this.settings.debug &&
                    window.console &&
                    console.log(
                      "Exception occurred when checking element " +
                        e.id +
                        ", check the '" +
                        s.method +
                        "' method.",
                      t
                    ),
                  t instanceof TypeError &&
                    (t.message +=
                      ".  Exception occurred when checking element " +
                      e.id +
                      ", check the '" +
                      s.method +
                      "' method."),
                  t)
                );
              }
            }
            if (!a) return this.objectLength(o) && this.successList.push(e), !0;
          },
          customDataMessage: function (e, i) {
            return (
              t(e).data(
                "msg" + i.charAt(0).toUpperCase() + i.substring(1).toLowerCase()
              ) || t(e).data("msg")
            );
          },
          customMessage: function (t, e) {
            var i = this.settings.messages[t];
            return i && (i.constructor === String ? i : i[e]);
          },
          findDefined: function () {
            for (var t = 0; t < arguments.length; t++)
              if (void 0 !== arguments[t]) return arguments[t];
          },
          defaultMessage: function (e, i) {
            return this.findDefined(
              this.customMessage(e.name, i),
              this.customDataMessage(e, i),
              (!this.settings.ignoreTitle && e.title) || void 0,
              t.validator.messages[i],
              "<strong>Warning: No message defined for " + e.name + "</strong>"
            );
          },
          formatAndAdd: function (e, i) {
            var n = this.defaultMessage(e, i.method),
              s = /\$?\{(\d+)\}/g;
            "function" == typeof n
              ? (n = n.call(this, i.parameters, e))
              : s.test(n) &&
                (n = t.validator.format(n.replace(s, "{$1}"), i.parameters)),
              this.errorList.push({ message: n, element: e, method: i.method }),
              (this.errorMap[e.name] = n),
              (this.submitted[e.name] = n);
          },
          addWrapper: function (t) {
            return (
              this.settings.wrapper &&
                (t = t.add(t.parent(this.settings.wrapper))),
              t
            );
          },
          defaultShowErrors: function () {
            var t, e, i;
            for (t = 0; this.errorList[t]; t++)
              (i = this.errorList[t]),
                this.settings.highlight &&
                  this.settings.highlight.call(
                    this,
                    i.element,
                    this.settings.errorClass,
                    this.settings.validClass
                  ),
                this.showLabel(i.element, i.message);
            if (
              (this.errorList.length &&
                (this.toShow = this.toShow.add(this.containers)),
              this.settings.success)
            )
              for (t = 0; this.successList[t]; t++)
                this.showLabel(this.successList[t]);
            if (this.settings.unhighlight)
              for (t = 0, e = this.validElements(); e[t]; t++)
                this.settings.unhighlight.call(
                  this,
                  e[t],
                  this.settings.errorClass,
                  this.settings.validClass
                );
            (this.toHide = this.toHide.not(this.toShow)),
              this.hideErrors(),
              this.addWrapper(this.toShow).show();
          },
          validElements: function () {
            return this.currentElements.not(this.invalidElements());
          },
          invalidElements: function () {
            return t(this.errorList).map(function () {
              return this.element;
            });
          },
          showLabel: function (e, i) {
            var n,
              s,
              o,
              r = this.errorsFor(e),
              a = this.idOrName(e),
              l = t(e).attr("aria-describedby");
            r.length
              ? (r
                  .removeClass(this.settings.validClass)
                  .addClass(this.settings.errorClass),
                r.html(i))
              : ((n = r =
                  t("<" + this.settings.errorElement + ">")
                    .attr("id", a + "-error")
                    .addClass(this.settings.errorClass)
                    .html(i || "")),
                this.settings.wrapper &&
                  (n = r
                    .hide()
                    .show()
                    .wrap("<" + this.settings.wrapper + "/>")
                    .parent()),
                this.labelContainer.length
                  ? this.labelContainer.append(n)
                  : this.settings.errorPlacement
                  ? this.settings.errorPlacement(n, t(e))
                  : n.insertAfter(e),
                r.is("label")
                  ? r.attr("for", a)
                  : 0 === r.parents("label[for='" + a + "']").length &&
                    ((o = r.attr("id").replace(/(:|\.|\[|\]|\$)/g, "\\$1")),
                    l
                      ? l.match(new RegExp("\\b" + o + "\\b")) || (l += " " + o)
                      : (l = o),
                    t(e).attr("aria-describedby", l),
                    (s = this.groups[e.name]) &&
                      t.each(this.groups, function (e, i) {
                        i === s &&
                          t("[name='" + e + "']", this.currentForm).attr(
                            "aria-describedby",
                            r.attr("id")
                          );
                      }))),
              !i &&
                this.settings.success &&
                (r.text(""),
                "string" == typeof this.settings.success
                  ? r.addClass(this.settings.success)
                  : this.settings.success(r, e)),
              (this.toShow = this.toShow.add(r));
          },
          errorsFor: function (e) {
            var i = this.idOrName(e),
              n = t(e).attr("aria-describedby"),
              s = "label[for='" + i + "'], label[for='" + i + "'] *";
            return (
              n && (s = s + ", #" + n.replace(/\s+/g, ", #")),
              this.errors().filter(s)
            );
          },
          idOrName: function (t) {
            return (
              this.groups[t.name] ||
              (this.checkable(t) ? t.name : t.id || t.name)
            );
          },
          validationTargetFor: function (e) {
            return (
              this.checkable(e) && (e = this.findByName(e.name)),
              t(e).not(this.settings.ignore)[0]
            );
          },
          checkable: function (t) {
            return /radio|checkbox/i.test(t.type);
          },
          findByName: function (e) {
            return t(this.currentForm).find("[name='" + e + "']");
          },
          getLength: function (e, i) {
            switch (i.nodeName.toLowerCase()) {
              case "select":
                return t("option:selected", i).length;
              case "input":
                if (this.checkable(i))
                  return this.findByName(i.name).filter(":checked").length;
            }
            return e.length;
          },
          depend: function (t, e) {
            return (
              !this.dependTypes[typeof t] || this.dependTypes[typeof t](t, e)
            );
          },
          dependTypes: {
            boolean: function (t) {
              return t;
            },
            string: function (e, i) {
              return !!t(e, i.form).length;
            },
            function: function (t, e) {
              return t(e);
            },
          },
          optional: function (e) {
            var i = this.elementValue(e);
            return (
              !t.validator.methods.required.call(this, i, e) &&
              "dependency-mismatch"
            );
          },
          startRequest: function (t) {
            this.pending[t.name] ||
              (this.pendingRequest++, (this.pending[t.name] = !0));
          },
          stopRequest: function (e, i) {
            this.pendingRequest--,
              this.pendingRequest < 0 && (this.pendingRequest = 0),
              delete this.pending[e.name],
              i &&
              0 === this.pendingRequest &&
              this.formSubmitted &&
              this.form()
                ? (t(this.currentForm).submit(), (this.formSubmitted = !1))
                : !i &&
                  0 === this.pendingRequest &&
                  this.formSubmitted &&
                  (t(this.currentForm).triggerHandler("invalid-form", [this]),
                  (this.formSubmitted = !1));
          },
          previousValue: function (e) {
            return (
              t.data(e, "previousValue") ||
              t.data(e, "previousValue", {
                old: null,
                valid: !0,
                message: this.defaultMessage(e, "remote"),
              })
            );
          },
          destroy: function () {
            this.resetForm(),
              t(this.currentForm).off(".validate").removeData("validator");
          },
        },
        classRuleSettings: {
          required: { required: !0 },
          email: { email: !0 },
          url: { url: !0 },
          date: { date: !0 },
          dateISO: { dateISO: !0 },
          number: { number: !0 },
          digits: { digits: !0 },
          creditcard: { creditcard: !0 },
        },
        addClassRules: function (e, i) {
          e.constructor === String
            ? (this.classRuleSettings[e] = i)
            : t.extend(this.classRuleSettings, e);
        },
        classRules: function (e) {
          var i = {},
            n = t(e).attr("class");
          return (
            n &&
              t.each(n.split(" "), function () {
                this in t.validator.classRuleSettings &&
                  t.extend(i, t.validator.classRuleSettings[this]);
              }),
            i
          );
        },
        normalizeAttributeRule: function (t, e, i, n) {
          /min|max/.test(i) &&
            (null === e || /number|range|text/.test(e)) &&
            ((n = Number(n)), isNaN(n) && (n = void 0)),
            n || 0 === n ? (t[i] = n) : e === i && "range" !== e && (t[i] = !0);
        },
        attributeRules: function (e) {
          var i,
            n,
            s = {},
            o = t(e),
            r = e.getAttribute("type");
          for (i in t.validator.methods)
            "required" === i
              ? ("" === (n = e.getAttribute(i)) && (n = !0), (n = !!n))
              : (n = o.attr(i)),
              this.normalizeAttributeRule(s, r, i, n);
          return (
            s.maxlength &&
              /-1|2147483647|524288/.test(s.maxlength) &&
              delete s.maxlength,
            s
          );
        },
        dataRules: function (e) {
          var i,
            n,
            s = {},
            o = t(e),
            r = e.getAttribute("type");
          for (i in t.validator.methods)
            (n = o.data(
              "rule" + i.charAt(0).toUpperCase() + i.substring(1).toLowerCase()
            )),
              this.normalizeAttributeRule(s, r, i, n);
          return s;
        },
        staticRules: function (e) {
          var i = {},
            n = t.data(e.form, "validator");
          return (
            n.settings.rules &&
              (i = t.validator.normalizeRule(n.settings.rules[e.name]) || {}),
            i
          );
        },
        normalizeRules: function (e, i) {
          return (
            t.each(e, function (n, s) {
              if (!1 !== s) {
                if (s.param || s.depends) {
                  var o = !0;
                  switch (typeof s.depends) {
                    case "string":
                      o = !!t(s.depends, i.form).length;
                      break;
                    case "function":
                      o = s.depends.call(i, i);
                  }
                  o ? (e[n] = void 0 === s.param || s.param) : delete e[n];
                }
              } else delete e[n];
            }),
            t.each(e, function (n, s) {
              e[n] = t.isFunction(s) ? s(i) : s;
            }),
            t.each(["minlength", "maxlength"], function () {
              e[this] && (e[this] = Number(e[this]));
            }),
            t.each(["rangelength", "range"], function () {
              var i;
              e[this] &&
                (t.isArray(e[this])
                  ? (e[this] = [Number(e[this][0]), Number(e[this][1])])
                  : "string" == typeof e[this] &&
                    ((i = e[this].replace(/[\[\]]/g, "").split(/[\s,]+/)),
                    (e[this] = [Number(i[0]), Number(i[1])])));
            }),
            t.validator.autoCreateRanges &&
              (null != e.min &&
                null != e.max &&
                ((e.range = [e.min, e.max]), delete e.min, delete e.max),
              null != e.minlength &&
                null != e.maxlength &&
                ((e.rangelength = [e.minlength, e.maxlength]),
                delete e.minlength,
                delete e.maxlength)),
            e
          );
        },
        normalizeRule: function (e) {
          if ("string" == typeof e) {
            var i = {};
            t.each(e.split(/\s/), function () {
              i[this] = !0;
            }),
              (e = i);
          }
          return e;
        },
        addMethod: function (e, i, n) {
          (t.validator.methods[e] = i),
            (t.validator.messages[e] =
              void 0 !== n ? n : t.validator.messages[e]),
            i.length < 3 &&
              t.validator.addClassRules(e, t.validator.normalizeRule(e));
        },
        methods: {
          required: function (e, i, n) {
            if (!this.depend(n, i)) return "dependency-mismatch";
            if ("select" === i.nodeName.toLowerCase()) {
              var s = t(i).val();
              return s && s.length > 0;
            }
            return this.checkable(i) ? this.getLength(e, i) > 0 : e.length > 0;
          },
          email: function (t, e) {
            return (
              this.optional(e) ||
              /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(
                t
              )
            );
          },
          url: function (t, e) {
            return (
              this.optional(e) ||
              /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[\/?#]\S*)?$/i.test(
                t
              )
            );
          },
          date: function (t, e) {
            return (
              this.optional(e) || !/Invalid|NaN/.test(new Date(t).toString())
            );
          },
          dateISO: function (t, e) {
            return (
              this.optional(e) ||
              /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(
                t
              )
            );
          },
          number: function (t, e) {
            return (
              this.optional(e) ||
              /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(t)
            );
          },
          digits: function (t, e) {
            return this.optional(e) || /^\d+$/.test(t);
          },
          creditcard: function (t, e) {
            if (this.optional(e)) return "dependency-mismatch";
            if (/[^0-9 \-]+/.test(t)) return !1;
            var i,
              n,
              s = 0,
              o = 0,
              r = !1;
            if ((t = t.replace(/\D/g, "")).length < 13 || t.length > 19)
              return !1;
            for (i = t.length - 1; i >= 0; i--)
              (n = t.charAt(i)),
                (o = parseInt(n, 10)),
                r && (o *= 2) > 9 && (o -= 9),
                (s += o),
                (r = !r);
            return s % 10 == 0;
          },
          minlength: function (e, i, n) {
            var s = t.isArray(e) ? e.length : this.getLength(e, i);
            return this.optional(i) || s >= n;
          },
          maxlength: function (e, i, n) {
            var s = t.isArray(e) ? e.length : this.getLength(e, i);
            return this.optional(i) || n >= s;
          },
          rangelength: function (e, i, n) {
            var s = t.isArray(e) ? e.length : this.getLength(e, i);
            return this.optional(i) || (s >= n[0] && s <= n[1]);
          },
          min: function (t, e, i) {
            return this.optional(e) || t >= i;
          },
          max: function (t, e, i) {
            return this.optional(e) || i >= t;
          },
          range: function (t, e, i) {
            return this.optional(e) || (t >= i[0] && t <= i[1]);
          },
          equalTo: function (e, i, n) {
            var s = t(n);
            return (
              this.settings.onfocusout &&
                s
                  .off(".validate-equalTo")
                  .on("blur.validate-equalTo", function () {
                    t(i).valid();
                  }),
              e === s.val()
            );
          },
          remote: function (e, i, n) {
            if (this.optional(i)) return "dependency-mismatch";
            var s,
              o,
              r = this.previousValue(i);
            return (
              this.settings.messages[i.name] ||
                (this.settings.messages[i.name] = {}),
              (r.originalMessage = this.settings.messages[i.name].remote),
              (this.settings.messages[i.name].remote = r.message),
              (n = ("string" == typeof n && { url: n }) || n),
              r.old === e
                ? r.valid
                : ((r.old = e),
                  (s = this),
                  this.startRequest(i),
                  ((o = {})[i.name] = e),
                  t.ajax(
                    t.extend(
                      !0,
                      {
                        mode: "abort",
                        port: "validate" + i.name,
                        dataType: "json",
                        data: o,
                        context: s.currentForm,
                        success: function (n) {
                          var o,
                            a,
                            l,
                            c = !0 === n || "true" === n;
                          (s.settings.messages[i.name].remote =
                            r.originalMessage),
                            c
                              ? ((l = s.formSubmitted),
                                s.prepareElement(i),
                                (s.formSubmitted = l),
                                s.successList.push(i),
                                delete s.invalid[i.name],
                                s.showErrors())
                              : ((o = {}),
                                (a = n || s.defaultMessage(i, "remote")),
                                (o[i.name] = r.message =
                                  t.isFunction(a) ? a(e) : a),
                                (s.invalid[i.name] = !0),
                                s.showErrors(o)),
                            (r.valid = c),
                            s.stopRequest(i, c);
                        },
                      },
                      n
                    )
                  ),
                  "pending")
            );
          },
        },
      });
    var e,
      i = {};
    t.ajaxPrefilter
      ? t.ajaxPrefilter(function (t, e, n) {
          var s = t.port;
          "abort" === t.mode && (i[s] && i[s].abort(), (i[s] = n));
        })
      : ((e = t.ajax),
        (t.ajax = function (n) {
          var s = ("mode" in n ? n : t.ajaxSettings).mode,
            o = ("port" in n ? n : t.ajaxSettings).port;
          return "abort" === s
            ? (i[o] && i[o].abort(), (i[o] = e.apply(this, arguments)), i[o])
            : e.apply(this, arguments);
        }));
  }),
  (function (t, e, i) {
    function n(e, i) {
      (this.element = e),
        (this.settings = t.extend({}, s, i)),
        this.settings.duplicate ||
          i.hasOwnProperty("removeIds") ||
          (this.settings.removeIds = !1),
        (this._defaults = s),
        (this._name = o),
        this.init();
    }
    var s = {
        label: "MENU",
        duplicate: !0,
        duration: 200,
        easingOpen: "swing",
        easingClose: "swing",
        closedSymbol: "&#9658;",
        openedSymbol: "&#9660;",
        prependTo: "body",
        appendTo: "",
        parentTag: "a",
        closeOnClick: !1,
        allowParentLinks: !1,
        nestedParentLinks: !0,
        showChildren: !1,
        removeIds: !0,
        removeClasses: !1,
        removeStyles: !1,
        brand: "",
        animations: "jquery",
        init: function () {},
        beforeOpen: function () {},
        beforeClose: function () {},
        afterOpen: function () {},
        afterClose: function () {},
      },
      o = "slicknav",
      r = "slicknav";
    (n.prototype.init = function () {
      var i,
        n,
        s = this,
        o = t(this.element),
        a = this.settings;
      if (
        (a.duplicate ? (s.mobileNav = o.clone()) : (s.mobileNav = o),
        a.removeIds &&
          (s.mobileNav.removeAttr("id"),
          s.mobileNav.find("*").each(function (e, i) {
            t(i).removeAttr("id");
          })),
        a.removeClasses &&
          (s.mobileNav.removeAttr("class"),
          s.mobileNav.find("*").each(function (e, i) {
            t(i).removeAttr("class");
          })),
        a.removeStyles &&
          (s.mobileNav.removeAttr("style"),
          s.mobileNav.find("*").each(function (e, i) {
            t(i).removeAttr("style");
          })),
        (i = r + "_icon"),
        "" === a.label && (i += " " + r + "_no-text"),
        "a" == a.parentTag && (a.parentTag = 'a href="#"'),
        s.mobileNav.attr("class", r + "_nav"),
        (n = t('<div class="' + r + '_menu"></div>')),
        "" !== a.brand)
      ) {
        var l = t('<div class="' + r + '_brand">' + a.brand + "</div>");
        t(n).append(l);
      }
      (s.btn = t(
        [
          "<" +
            a.parentTag +
            ' aria-haspopup="true" role="button" tabindex="0" class="' +
            r +
            "_btn " +
            r +
            '_collapsed">',
          '<span class="' + r + '_menutxt">' + a.label + "</span>",
          '<span class="' + i + '">',
          '<span class="' + r + '_icon-bar"></span>',
          '<span class="' + r + '_icon-bar"></span>',
          '<span class="' + r + '_icon-bar"></span>',
          "</span>",
          "</" + a.parentTag + ">",
        ].join("")
      )),
        t(n).append(s.btn),
        "" !== a.appendTo ? t(a.appendTo).append(n) : t(a.prependTo).prepend(n),
        n.append(s.mobileNav);
      var c = s.mobileNav.find("li");
      t(c).each(function () {
        var e = t(this),
          i = {};
        if (
          ((i.children = e.children("ul").attr("role", "menu")),
          e.data("menu", i),
          i.children.length > 0)
        ) {
          var n = e.contents(),
            o = !1,
            l = [];
          t(n).each(function () {
            return (
              !t(this).is("ul") &&
              (l.push(this), void (t(this).is("a") && (o = !0)))
            );
          });
          var c = t(
            "<" +
              a.parentTag +
              ' role="menuitem" aria-haspopup="true" tabindex="-1" class="' +
              r +
              '_item"/>'
          );
          a.allowParentLinks && !a.nestedParentLinks && o
            ? t(l)
                .wrapAll('<span class="' + r + "_parent-link " + r + '_row"/>')
                .parent()
            : t(l)
                .wrapAll(c)
                .parent()
                .addClass(r + "_row"),
            a.showChildren
              ? e.addClass(r + "_open")
              : e.addClass(r + "_collapsed"),
            e.addClass(r + "_parent");
          var d = t(
            '<span class="' +
              r +
              '_arrow">' +
              (a.showChildren ? a.openedSymbol : a.closedSymbol) +
              "</span>"
          );
          a.allowParentLinks &&
            !a.nestedParentLinks &&
            o &&
            (d = d.wrap(c).parent()),
            t(l).last().after(d);
        } else 0 === e.children().length && e.addClass(r + "_txtnode");
        e
          .children("a")
          .attr("role", "menuitem")
          .click(function (e) {
            a.closeOnClick &&
              !t(e.target)
                .parent()
                .closest("li")
                .hasClass(r + "_parent") &&
              t(s.btn).click();
          }),
          a.closeOnClick &&
            a.allowParentLinks &&
            (e
              .children("a")
              .children("a")
              .click(function (e) {
                t(s.btn).click();
              }),
            e
              .find("." + r + "_parent-link a:not(." + r + "_item)")
              .click(function (e) {
                t(s.btn).click();
              }));
      }),
        t(c).each(function () {
          var e = t(this).data("menu");
          a.showChildren || s._visibilityToggle(e.children, null, !1, null, !0);
        }),
        s._visibilityToggle(s.mobileNav, null, !1, "init", !0),
        s.mobileNav.attr("role", "menu"),
        t(e).mousedown(function () {
          s._outlines(!1);
        }),
        t(e).keyup(function () {
          s._outlines(!0);
        }),
        t(s.btn).click(function (t) {
          t.preventDefault(), s._menuToggle();
        }),
        s.mobileNav.on("click", "." + r + "_item", function (e) {
          e.preventDefault(), s._itemClick(t(this));
        }),
        t(s.btn).keydown(function (e) {
          var i = e || event;
          switch (i.keyCode) {
            case 13:
            case 32:
            case 40:
              e.preventDefault(),
                (40 === i.keyCode && t(s.btn).hasClass(r + "_open")) ||
                  s._menuToggle(),
                t(s.btn).next().find('[role="menuitem"]').first().focus();
          }
        }),
        s.mobileNav.on("keydown", "." + r + "_item", function (e) {
          switch ((e || event).keyCode) {
            case 13:
              e.preventDefault(), s._itemClick(t(e.target));
              break;
            case 39:
              e.preventDefault(),
                t(e.target)
                  .parent()
                  .hasClass(r + "_collapsed") && s._itemClick(t(e.target)),
                t(e.target).next().find('[role="menuitem"]').first().focus();
          }
        }),
        s.mobileNav.on("keydown", '[role="menuitem"]', function (e) {
          switch ((e || event).keyCode) {
            case 40:
              e.preventDefault();
              var i =
                (o = (n = t(e.target)
                  .parent()
                  .parent()
                  .children()
                  .children('[role="menuitem"]:visible')).index(e.target)) + 1;
              n.length <= i && (i = 0), n.eq(i).focus();
              break;
            case 38:
              e.preventDefault();
              var n,
                o = (n = t(e.target)
                  .parent()
                  .parent()
                  .children()
                  .children('[role="menuitem"]:visible')).index(e.target);
              n.eq(o - 1).focus();
              break;
            case 37:
              if (
                (e.preventDefault(),
                t(e.target)
                  .parent()
                  .parent()
                  .parent()
                  .hasClass(r + "_open"))
              ) {
                var a = t(e.target).parent().parent().prev();
                a.focus(), s._itemClick(a);
              } else
                t(e.target)
                  .parent()
                  .parent()
                  .hasClass(r + "_nav") && (s._menuToggle(), t(s.btn).focus());
              break;
            case 27:
              e.preventDefault(), s._menuToggle(), t(s.btn).focus();
          }
        }),
        a.allowParentLinks &&
          a.nestedParentLinks &&
          t("." + r + "_item a").click(function (t) {
            t.stopImmediatePropagation();
          });
    }),
      (n.prototype._menuToggle = function (t) {
        var e = this,
          i = e.btn,
          n = e.mobileNav;
        i.hasClass(r + "_collapsed")
          ? (i.removeClass(r + "_collapsed"), i.addClass(r + "_open"))
          : (i.removeClass(r + "_open"), i.addClass(r + "_collapsed")),
          i.addClass(r + "_animating"),
          e._visibilityToggle(n, i.parent(), !0, i);
      }),
      (n.prototype._itemClick = function (t) {
        var e = this,
          i = e.settings,
          n = t.data("menu");
        n ||
          (((n = {}).arrow = t.children("." + r + "_arrow")),
          (n.ul = t.next("ul")),
          (n.parent = t.parent()),
          n.parent.hasClass(r + "_parent-link") &&
            ((n.parent = t.parent().parent()), (n.ul = t.parent().next("ul"))),
          t.data("menu", n)),
          n.parent.hasClass(r + "_collapsed")
            ? (n.arrow.html(i.openedSymbol),
              n.parent.removeClass(r + "_collapsed"),
              n.parent.addClass(r + "_open"),
              n.parent.addClass(r + "_animating"),
              e._visibilityToggle(n.ul, n.parent, !0, t))
            : (n.arrow.html(i.closedSymbol),
              n.parent.addClass(r + "_collapsed"),
              n.parent.removeClass(r + "_open"),
              n.parent.addClass(r + "_animating"),
              e._visibilityToggle(n.ul, n.parent, !0, t));
      }),
      (n.prototype._visibilityToggle = function (e, i, n, s, o) {
        function a(e, i) {
          t(e).removeClass(r + "_animating"),
            t(i).removeClass(r + "_animating"),
            o || d.afterOpen(e);
        }
        function l(i, n) {
          e.attr("aria-hidden", "true"),
            h.attr("tabindex", "-1"),
            c._setVisAttr(e, !0),
            e.hide(),
            t(i).removeClass(r + "_animating"),
            t(n).removeClass(r + "_animating"),
            o ? "init" == i && d.init() : d.afterClose(i);
        }
        var c = this,
          d = c.settings,
          h = c._getActionItems(e),
          u = 0;
        n && (u = d.duration),
          e.hasClass(r + "_hidden")
            ? (e.removeClass(r + "_hidden"),
              o || d.beforeOpen(s),
              "jquery" === d.animations
                ? e.stop(!0, !0).slideDown(u, d.easingOpen, function () {
                    a(s, i);
                  })
                : "velocity" === d.animations &&
                  e.velocity("finish").velocity("slideDown", {
                    duration: u,
                    easing: d.easingOpen,
                    complete: function () {
                      a(s, i);
                    },
                  }),
              e.attr("aria-hidden", "false"),
              h.attr("tabindex", "0"),
              c._setVisAttr(e, !1))
            : (e.addClass(r + "_hidden"),
              o || d.beforeClose(s),
              "jquery" === d.animations
                ? e
                    .stop(!0, !0)
                    .slideUp(u, this.settings.easingClose, function () {
                      l(s, i);
                    })
                : "velocity" === d.animations &&
                  e.velocity("finish").velocity("slideUp", {
                    duration: u,
                    easing: d.easingClose,
                    complete: function () {
                      l(s, i);
                    },
                  }));
      }),
      (n.prototype._setVisAttr = function (e, i) {
        var n = this,
          s = e
            .children("li")
            .children("ul")
            .not("." + r + "_hidden");
        i
          ? s.each(function () {
              var e = t(this);
              e.attr("aria-hidden", "true"),
                n._getActionItems(e).attr("tabindex", "-1"),
                n._setVisAttr(e, i);
            })
          : s.each(function () {
              var e = t(this);
              e.attr("aria-hidden", "false"),
                n._getActionItems(e).attr("tabindex", "0"),
                n._setVisAttr(e, i);
            });
      }),
      (n.prototype._getActionItems = function (t) {
        var e = t.data("menu");
        if (!e) {
          e = {};
          var i = t.children("li"),
            n = i.find("a");
          (e.links = n.add(i.find("." + r + "_item"))), t.data("menu", e);
        }
        return e.links;
      }),
      (n.prototype._outlines = function (e) {
        e
          ? t("." + r + "_item, ." + r + "_btn").css("outline", "")
          : t("." + r + "_item, ." + r + "_btn").css("outline", "none");
      }),
      (n.prototype.toggle = function () {
        this._menuToggle();
      }),
      (n.prototype.open = function () {
        this.btn.hasClass(r + "_collapsed") && this._menuToggle();
      }),
      (n.prototype.close = function () {
        this.btn.hasClass(r + "_open") && this._menuToggle();
      }),
      (t.fn[o] = function (e) {
        var i,
          s = arguments;
        return void 0 === e || "object" == typeof e
          ? this.each(function () {
              t.data(this, "plugin_" + o) ||
                t.data(this, "plugin_" + o, new n(this, e));
            })
          : "string" == typeof e && "_" !== e[0] && "init" !== e
          ? (this.each(function () {
              var r = t.data(this, "plugin_" + o);
              r instanceof n &&
                "function" == typeof r[e] &&
                (i = r[e].apply(r, Array.prototype.slice.call(s, 1)));
            }),
            void 0 !== i ? i : this)
          : void 0;
      });
  })(jQuery, document, window),
  (function (t) {
    "use strict";
    "function" == typeof define && define.amd
      ? define(["jquery"], t)
      : "undefined" != typeof exports
      ? (module.exports = t(require("jquery")))
      : t(jQuery);
  })(function (t) {
    "use strict";
    var e = window.Slick || {};
    ((e = (function () {
      var e = 0;
      return function (i, n) {
        var s,
          o = this;
        (o.defaults = {
          accessibility: !0,
          adaptiveHeight: !1,
          appendArrows: t(i),
          appendDots: t(i),
          arrows: !0,
          asNavFor: null,
          prevArrow:
            '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
          nextArrow:
            '<button class="slick-next" aria-label="Next" type="button">Next</button>',
          autoplay: !1,
          autoplaySpeed: 3e3,
          centerMode: !1,
          centerPadding: "50px",
          cssEase: "ease",
          customPaging: function (e, i) {
            return t('<button type="button" />').text(i + 1);
          },
          dots: !1,
          dotsClass: "slick-dots",
          draggable: !0,
          easing: "linear",
          edgeFriction: 0.35,
          fade: !1,
          focusOnSelect: !1,
          focusOnChange: !1,
          infinite: !0,
          initialSlide: 0,
          lazyLoad: "ondemand",
          mobileFirst: !1,
          pauseOnHover: !0,
          pauseOnFocus: !0,
          pauseOnDotsHover: !1,
          respondTo: "window",
          responsive: null,
          rows: 1,
          rtl: !1,
          slide: "",
          slidesPerRow: 1,
          slidesToShow: 1,
          slidesToScroll: 1,
          speed: 500,
          swipe: !0,
          swipeToSlide: !1,
          touchMove: !0,
          touchThreshold: 5,
          useCSS: !0,
          useTransform: !0,
          variableWidth: !1,
          vertical: !1,
          verticalSwiping: !1,
          waitForAnimate: !0,
          zIndex: 1e3,
        }),
          (o.initials = {
            animating: !1,
            dragging: !1,
            autoPlayTimer: null,
            currentDirection: 0,
            currentLeft: null,
            currentSlide: 0,
            direction: 1,
            $dots: null,
            listWidth: null,
            listHeight: null,
            loadIndex: 0,
            $nextArrow: null,
            $prevArrow: null,
            scrolling: !1,
            slideCount: null,
            slideWidth: null,
            $slideTrack: null,
            $slides: null,
            sliding: !1,
            slideOffset: 0,
            swipeLeft: null,
            swiping: !1,
            $list: null,
            touchObject: {},
            transformsEnabled: !1,
            unslicked: !1,
          }),
          t.extend(o, o.initials),
          (o.activeBreakpoint = null),
          (o.animType = null),
          (o.animProp = null),
          (o.breakpoints = []),
          (o.breakpointSettings = []),
          (o.cssTransitions = !1),
          (o.focussed = !1),
          (o.interrupted = !1),
          (o.hidden = "hidden"),
          (o.paused = !0),
          (o.positionProp = null),
          (o.respondTo = null),
          (o.rowCount = 1),
          (o.shouldClick = !0),
          (o.$slider = t(i)),
          (o.$slidesCache = null),
          (o.transformType = null),
          (o.transitionType = null),
          (o.visibilityChange = "visibilitychange"),
          (o.windowWidth = 0),
          (o.windowTimer = null),
          (s = t(i).data("slick") || {}),
          (o.options = t.extend({}, o.defaults, n, s)),
          (o.currentSlide = o.options.initialSlide),
          (o.originalSettings = o.options),
          void 0 !== document.mozHidden
            ? ((o.hidden = "mozHidden"),
              (o.visibilityChange = "mozvisibilitychange"))
            : void 0 !== document.webkitHidden &&
              ((o.hidden = "webkitHidden"),
              (o.visibilityChange = "webkitvisibilitychange")),
          (o.autoPlay = t.proxy(o.autoPlay, o)),
          (o.autoPlayClear = t.proxy(o.autoPlayClear, o)),
          (o.autoPlayIterator = t.proxy(o.autoPlayIterator, o)),
          (o.changeSlide = t.proxy(o.changeSlide, o)),
          (o.clickHandler = t.proxy(o.clickHandler, o)),
          (o.selectHandler = t.proxy(o.selectHandler, o)),
          (o.setPosition = t.proxy(o.setPosition, o)),
          (o.swipeHandler = t.proxy(o.swipeHandler, o)),
          (o.dragHandler = t.proxy(o.dragHandler, o)),
          (o.keyHandler = t.proxy(o.keyHandler, o)),
          (o.instanceUid = e++),
          (o.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/),
          o.registerBreakpoints(),
          o.init(!0);
      };
    })()).prototype.activateADA = function () {
      this.$slideTrack
        .find(".slick-active")
        .attr({ "aria-hidden": "false" })
        .find("a, input, button, select")
        .attr({ tabindex: "0" });
    }),
      (e.prototype.addSlide = e.prototype.slickAdd =
        function (e, i, n) {
          var s = this;
          if ("boolean" == typeof i) (n = i), (i = null);
          else if (i < 0 || i >= s.slideCount) return !1;
          s.unload(),
            "number" == typeof i
              ? 0 === i && 0 === s.$slides.length
                ? t(e).appendTo(s.$slideTrack)
                : n
                ? t(e).insertBefore(s.$slides.eq(i))
                : t(e).insertAfter(s.$slides.eq(i))
              : !0 === n
              ? t(e).prependTo(s.$slideTrack)
              : t(e).appendTo(s.$slideTrack),
            (s.$slides = s.$slideTrack.children(this.options.slide)),
            s.$slideTrack.children(this.options.slide).detach(),
            s.$slideTrack.append(s.$slides),
            s.$slides.each(function (e, i) {
              t(i).attr("data-slick-index", e);
            }),
            (s.$slidesCache = s.$slides),
            s.reinit();
        }),
      (e.prototype.animateHeight = function () {
        var t = this;
        if (
          1 === t.options.slidesToShow &&
          !0 === t.options.adaptiveHeight &&
          !1 === t.options.vertical
        ) {
          var e = t.$slides.eq(t.currentSlide).outerHeight(!0);
          t.$list.animate({ height: e }, t.options.speed);
        }
      }),
      (e.prototype.animateSlide = function (e, i) {
        var n = {},
          s = this;
        s.animateHeight(),
          !0 === s.options.rtl && !1 === s.options.vertical && (e = -e),
          !1 === s.transformsEnabled
            ? !1 === s.options.vertical
              ? s.$slideTrack.animate(
                  { left: e },
                  s.options.speed,
                  s.options.easing,
                  i
                )
              : s.$slideTrack.animate(
                  { top: e },
                  s.options.speed,
                  s.options.easing,
                  i
                )
            : !1 === s.cssTransitions
            ? (!0 === s.options.rtl && (s.currentLeft = -s.currentLeft),
              t({ animStart: s.currentLeft }).animate(
                { animStart: e },
                {
                  duration: s.options.speed,
                  easing: s.options.easing,
                  step: function (t) {
                    (t = Math.ceil(t)),
                      !1 === s.options.vertical
                        ? ((n[s.animType] = "translate(" + t + "px, 0px)"),
                          s.$slideTrack.css(n))
                        : ((n[s.animType] = "translate(0px," + t + "px)"),
                          s.$slideTrack.css(n));
                  },
                  complete: function () {
                    i && i.call();
                  },
                }
              ))
            : (s.applyTransition(),
              (e = Math.ceil(e)),
              !1 === s.options.vertical
                ? (n[s.animType] = "translate3d(" + e + "px, 0px, 0px)")
                : (n[s.animType] = "translate3d(0px," + e + "px, 0px)"),
              s.$slideTrack.css(n),
              i &&
                setTimeout(function () {
                  s.disableTransition(), i.call();
                }, s.options.speed));
      }),
      (e.prototype.getNavTarget = function () {
        var e = this.options.asNavFor;
        return e && null !== e && (e = t(e).not(this.$slider)), e;
      }),
      (e.prototype.asNavFor = function (e) {
        var i = this.getNavTarget();
        null !== i &&
          "object" == typeof i &&
          i.each(function () {
            var i = t(this).slick("getSlick");
            i.unslicked || i.slideHandler(e, !0);
          });
      }),
      (e.prototype.applyTransition = function (t) {
        var e = this,
          i = {};
        !1 === e.options.fade
          ? (i[e.transitionType] =
              e.transformType +
              " " +
              e.options.speed +
              "ms " +
              e.options.cssEase)
          : (i[e.transitionType] =
              "opacity " + e.options.speed + "ms " + e.options.cssEase),
          !1 === e.options.fade ? e.$slideTrack.css(i) : e.$slides.eq(t).css(i);
      }),
      (e.prototype.autoPlay = function () {
        var t = this;
        t.autoPlayClear(),
          t.slideCount > t.options.slidesToShow &&
            (t.autoPlayTimer = setInterval(
              t.autoPlayIterator,
              t.options.autoplaySpeed
            ));
      }),
      (e.prototype.autoPlayClear = function () {
        this.autoPlayTimer && clearInterval(this.autoPlayTimer);
      }),
      (e.prototype.autoPlayIterator = function () {
        var t = this,
          e = t.currentSlide + t.options.slidesToScroll;
        t.paused ||
          t.interrupted ||
          t.focussed ||
          (!1 === t.options.infinite &&
            (1 === t.direction && t.currentSlide + 1 === t.slideCount - 1
              ? (t.direction = 0)
              : 0 === t.direction &&
                ((e = t.currentSlide - t.options.slidesToScroll),
                t.currentSlide - 1 == 0 && (t.direction = 1))),
          t.slideHandler(e));
      }),
      (e.prototype.buildArrows = function () {
        var e = this;
        !0 === e.options.arrows &&
          ((e.$prevArrow = t(e.options.prevArrow).addClass("slick-arrow")),
          (e.$nextArrow = t(e.options.nextArrow).addClass("slick-arrow")),
          e.slideCount > e.options.slidesToShow
            ? (e.$prevArrow
                .removeClass("slick-hidden")
                .removeAttr("aria-hidden tabindex"),
              e.$nextArrow
                .removeClass("slick-hidden")
                .removeAttr("aria-hidden tabindex"),
              e.htmlExpr.test(e.options.prevArrow) &&
                e.$prevArrow.prependTo(e.options.appendArrows),
              e.htmlExpr.test(e.options.nextArrow) &&
                e.$nextArrow.appendTo(e.options.appendArrows),
              !0 !== e.options.infinite &&
                e.$prevArrow
                  .addClass("slick-disabled")
                  .attr("aria-disabled", "true"))
            : e.$prevArrow
                .add(e.$nextArrow)
                .addClass("slick-hidden")
                .attr({ "aria-disabled": "true", tabindex: "-1" }));
      }),
      (e.prototype.buildDots = function () {
        var e,
          i,
          n = this;
        if (!0 === n.options.dots) {
          for (
            n.$slider.addClass("slick-dotted"),
              i = t("<ul />").addClass(n.options.dotsClass),
              e = 0;
            e <= n.getDotCount();
            e += 1
          )
            i.append(
              t("<li />").append(n.options.customPaging.call(this, n, e))
            );
          (n.$dots = i.appendTo(n.options.appendDots)),
            n.$dots.find("li").first().addClass("slick-active");
        }
      }),
      (e.prototype.buildOut = function () {
        var e = this;
        (e.$slides = e.$slider
          .children(e.options.slide + ":not(.slick-cloned)")
          .addClass("slick-slide")),
          (e.slideCount = e.$slides.length),
          e.$slides.each(function (e, i) {
            t(i)
              .attr("data-slick-index", e)
              .data("originalStyling", t(i).attr("style") || "");
          }),
          e.$slider.addClass("slick-slider"),
          (e.$slideTrack =
            0 === e.slideCount
              ? t('<div class="slick-track"/>').appendTo(e.$slider)
              : e.$slides.wrapAll('<div class="slick-track"/>').parent()),
          (e.$list = e.$slideTrack.wrap('<div class="slick-list"/>').parent()),
          e.$slideTrack.css("opacity", 0),
          (!0 !== e.options.centerMode && !0 !== e.options.swipeToSlide) ||
            (e.options.slidesToScroll = 1),
          t("img[data-lazy]", e.$slider).not("[src]").addClass("slick-loading"),
          e.setupInfinite(),
          e.buildArrows(),
          e.buildDots(),
          e.updateDots(),
          e.setSlideClasses(
            "number" == typeof e.currentSlide ? e.currentSlide : 0
          ),
          !0 === e.options.draggable && e.$list.addClass("draggable");
      }),
      (e.prototype.buildRows = function () {
        var t,
          e,
          i,
          n,
          s,
          o,
          r,
          a = this;
        if (
          ((n = document.createDocumentFragment()),
          (o = a.$slider.children()),
          a.options.rows > 1)
        ) {
          for (
            r = a.options.slidesPerRow * a.options.rows,
              s = Math.ceil(o.length / r),
              t = 0;
            t < s;
            t++
          ) {
            var l = document.createElement("div");
            for (e = 0; e < a.options.rows; e++) {
              var c = document.createElement("div");
              for (i = 0; i < a.options.slidesPerRow; i++) {
                var d = t * r + (e * a.options.slidesPerRow + i);
                o.get(d) && c.appendChild(o.get(d));
              }
              l.appendChild(c);
            }
            n.appendChild(l);
          }
          a.$slider.empty().append(n),
            a.$slider
              .children()
              .children()
              .children()
              .css({
                width: 100 / a.options.slidesPerRow + "%",
                display: "inline-block",
              });
        }
      }),
      (e.prototype.checkResponsive = function (e, i) {
        var n,
          s,
          o,
          r = this,
          a = !1,
          l = r.$slider.width(),
          c = window.innerWidth || t(window).width();
        if (
          ("window" === r.respondTo
            ? (o = c)
            : "slider" === r.respondTo
            ? (o = l)
            : "min" === r.respondTo && (o = Math.min(c, l)),
          r.options.responsive &&
            r.options.responsive.length &&
            null !== r.options.responsive)
        ) {
          for (n in ((s = null), r.breakpoints))
            r.breakpoints.hasOwnProperty(n) &&
              (!1 === r.originalSettings.mobileFirst
                ? o < r.breakpoints[n] && (s = r.breakpoints[n])
                : o > r.breakpoints[n] && (s = r.breakpoints[n]));
          null !== s
            ? null !== r.activeBreakpoint
              ? (s !== r.activeBreakpoint || i) &&
                ((r.activeBreakpoint = s),
                "unslick" === r.breakpointSettings[s]
                  ? r.unslick(s)
                  : ((r.options = t.extend(
                      {},
                      r.originalSettings,
                      r.breakpointSettings[s]
                    )),
                    !0 === e && (r.currentSlide = r.options.initialSlide),
                    r.refresh(e)),
                (a = s))
              : ((r.activeBreakpoint = s),
                "unslick" === r.breakpointSettings[s]
                  ? r.unslick(s)
                  : ((r.options = t.extend(
                      {},
                      r.originalSettings,
                      r.breakpointSettings[s]
                    )),
                    !0 === e && (r.currentSlide = r.options.initialSlide),
                    r.refresh(e)),
                (a = s))
            : null !== r.activeBreakpoint &&
              ((r.activeBreakpoint = null),
              (r.options = r.originalSettings),
              !0 === e && (r.currentSlide = r.options.initialSlide),
              r.refresh(e),
              (a = s)),
            e || !1 === a || r.$slider.trigger("breakpoint", [r, a]);
        }
      }),
      (e.prototype.changeSlide = function (e, i) {
        var n,
          s,
          o = this,
          r = t(e.currentTarget);
        switch (
          (r.is("a") && e.preventDefault(),
          r.is("li") || (r = r.closest("li")),
          (n =
            o.slideCount % o.options.slidesToScroll != 0
              ? 0
              : (o.slideCount - o.currentSlide) % o.options.slidesToScroll),
          e.data.message)
        ) {
          case "previous":
            (s =
              0 === n ? o.options.slidesToScroll : o.options.slidesToShow - n),
              o.slideCount > o.options.slidesToShow &&
                o.slideHandler(o.currentSlide - s, !1, i);
            break;
          case "next":
            (s = 0 === n ? o.options.slidesToScroll : n),
              o.slideCount > o.options.slidesToShow &&
                o.slideHandler(o.currentSlide + s, !1, i);
            break;
          case "index":
            var a =
              0 === e.data.index
                ? 0
                : e.data.index || r.index() * o.options.slidesToScroll;
            o.slideHandler(o.checkNavigable(a), !1, i),
              r.children().trigger("focus");
            break;
          default:
            return;
        }
      }),
      (e.prototype.checkNavigable = function (t) {
        var e, i;
        if (((i = 0), t > (e = this.getNavigableIndexes())[e.length - 1]))
          t = e[e.length - 1];
        else
          for (var n in e) {
            if (t < e[n]) {
              t = i;
              break;
            }
            i = e[n];
          }
        return t;
      }),
      (e.prototype.cleanUpEvents = function () {
        var e = this;
        e.options.dots &&
          null !== e.$dots &&
          (t("li", e.$dots)
            .off("click.slick", e.changeSlide)
            .off("mouseenter.slick", t.proxy(e.interrupt, e, !0))
            .off("mouseleave.slick", t.proxy(e.interrupt, e, !1)),
          !0 === e.options.accessibility &&
            e.$dots.off("keydown.slick", e.keyHandler)),
          e.$slider.off("focus.slick blur.slick"),
          !0 === e.options.arrows &&
            e.slideCount > e.options.slidesToShow &&
            (e.$prevArrow && e.$prevArrow.off("click.slick", e.changeSlide),
            e.$nextArrow && e.$nextArrow.off("click.slick", e.changeSlide),
            !0 === e.options.accessibility &&
              (e.$prevArrow && e.$prevArrow.off("keydown.slick", e.keyHandler),
              e.$nextArrow && e.$nextArrow.off("keydown.slick", e.keyHandler))),
          e.$list.off("touchstart.slick mousedown.slick", e.swipeHandler),
          e.$list.off("touchmove.slick mousemove.slick", e.swipeHandler),
          e.$list.off("touchend.slick mouseup.slick", e.swipeHandler),
          e.$list.off("touchcancel.slick mouseleave.slick", e.swipeHandler),
          e.$list.off("click.slick", e.clickHandler),
          t(document).off(e.visibilityChange, e.visibility),
          e.cleanUpSlideEvents(),
          !0 === e.options.accessibility &&
            e.$list.off("keydown.slick", e.keyHandler),
          !0 === e.options.focusOnSelect &&
            t(e.$slideTrack).children().off("click.slick", e.selectHandler),
          t(window).off(
            "orientationchange.slick.slick-" + e.instanceUid,
            e.orientationChange
          ),
          t(window).off("resize.slick.slick-" + e.instanceUid, e.resize),
          t("[draggable!=true]", e.$slideTrack).off(
            "dragstart",
            e.preventDefault
          ),
          t(window).off("load.slick.slick-" + e.instanceUid, e.setPosition);
      }),
      (e.prototype.cleanUpSlideEvents = function () {
        var e = this;
        e.$list.off("mouseenter.slick", t.proxy(e.interrupt, e, !0)),
          e.$list.off("mouseleave.slick", t.proxy(e.interrupt, e, !1));
      }),
      (e.prototype.cleanUpRows = function () {
        var t,
          e = this;
        e.options.rows > 1 &&
          ((t = e.$slides.children().children()).removeAttr("style"),
          e.$slider.empty().append(t));
      }),
      (e.prototype.clickHandler = function (t) {
        !1 === this.shouldClick &&
          (t.stopImmediatePropagation(),
          t.stopPropagation(),
          t.preventDefault());
      }),
      (e.prototype.destroy = function (e) {
        var i = this;
        i.autoPlayClear(),
          (i.touchObject = {}),
          i.cleanUpEvents(),
          t(".slick-cloned", i.$slider).detach(),
          i.$dots && i.$dots.remove(),
          i.$prevArrow &&
            i.$prevArrow.length &&
            (i.$prevArrow
              .removeClass("slick-disabled slick-arrow slick-hidden")
              .removeAttr("aria-hidden aria-disabled tabindex")
              .css("display", ""),
            i.htmlExpr.test(i.options.prevArrow) && i.$prevArrow.remove()),
          i.$nextArrow &&
            i.$nextArrow.length &&
            (i.$nextArrow
              .removeClass("slick-disabled slick-arrow slick-hidden")
              .removeAttr("aria-hidden aria-disabled tabindex")
              .css("display", ""),
            i.htmlExpr.test(i.options.nextArrow) && i.$nextArrow.remove()),
          i.$slides &&
            (i.$slides
              .removeClass(
                "slick-slide slick-active slick-center slick-visible slick-current"
              )
              .removeAttr("aria-hidden")
              .removeAttr("data-slick-index")
              .each(function () {
                t(this).attr("style", t(this).data("originalStyling"));
              }),
            i.$slideTrack.children(this.options.slide).detach(),
            i.$slideTrack.detach(),
            i.$list.detach(),
            i.$slider.append(i.$slides)),
          i.cleanUpRows(),
          i.$slider.removeClass("slick-slider"),
          i.$slider.removeClass("slick-initialized"),
          i.$slider.removeClass("slick-dotted"),
          (i.unslicked = !0),
          e || i.$slider.trigger("destroy", [i]);
      }),
      (e.prototype.disableTransition = function (t) {
        var e = this,
          i = {};
        (i[e.transitionType] = ""),
          !1 === e.options.fade ? e.$slideTrack.css(i) : e.$slides.eq(t).css(i);
      }),
      (e.prototype.fadeSlide = function (t, e) {
        var i = this;
        !1 === i.cssTransitions
          ? (i.$slides.eq(t).css({ zIndex: i.options.zIndex }),
            i.$slides
              .eq(t)
              .animate({ opacity: 1 }, i.options.speed, i.options.easing, e))
          : (i.applyTransition(t),
            i.$slides.eq(t).css({ opacity: 1, zIndex: i.options.zIndex }),
            e &&
              setTimeout(function () {
                i.disableTransition(t), e.call();
              }, i.options.speed));
      }),
      (e.prototype.fadeSlideOut = function (t) {
        var e = this;
        !1 === e.cssTransitions
          ? e.$slides
              .eq(t)
              .animate(
                { opacity: 0, zIndex: e.options.zIndex - 2 },
                e.options.speed,
                e.options.easing
              )
          : (e.applyTransition(t),
            e.$slides.eq(t).css({ opacity: 0, zIndex: e.options.zIndex - 2 }));
      }),
      (e.prototype.filterSlides = e.prototype.slickFilter =
        function (t) {
          var e = this;
          null !== t &&
            ((e.$slidesCache = e.$slides),
            e.unload(),
            e.$slideTrack.children(this.options.slide).detach(),
            e.$slidesCache.filter(t).appendTo(e.$slideTrack),
            e.reinit());
        }),
      (e.prototype.focusHandler = function () {
        var e = this;
        e.$slider
          .off("focus.slick blur.slick")
          .on("focus.slick blur.slick", "*", function (i) {
            i.stopImmediatePropagation();
            var n = t(this);
            setTimeout(function () {
              e.options.pauseOnFocus &&
                ((e.focussed = n.is(":focus")), e.autoPlay());
            }, 0);
          });
      }),
      (e.prototype.getCurrent = e.prototype.slickCurrentSlide =
        function () {
          return this.currentSlide;
        }),
      (e.prototype.getDotCount = function () {
        var t = this,
          e = 0,
          i = 0,
          n = 0;
        if (!0 === t.options.infinite)
          if (t.slideCount <= t.options.slidesToShow) ++n;
          else
            for (; e < t.slideCount; )
              ++n,
                (e = i + t.options.slidesToScroll),
                (i +=
                  t.options.slidesToScroll <= t.options.slidesToShow
                    ? t.options.slidesToScroll
                    : t.options.slidesToShow);
        else if (!0 === t.options.centerMode) n = t.slideCount;
        else if (t.options.asNavFor)
          for (; e < t.slideCount; )
            ++n,
              (e = i + t.options.slidesToScroll),
              (i +=
                t.options.slidesToScroll <= t.options.slidesToShow
                  ? t.options.slidesToScroll
                  : t.options.slidesToShow);
        else
          n =
            1 +
            Math.ceil(
              (t.slideCount - t.options.slidesToShow) / t.options.slidesToScroll
            );
        return n - 1;
      }),
      (e.prototype.getLeft = function (t) {
        var e,
          i,
          n,
          s,
          o = this,
          r = 0;
        return (
          (o.slideOffset = 0),
          (i = o.$slides.first().outerHeight(!0)),
          !0 === o.options.infinite
            ? (o.slideCount > o.options.slidesToShow &&
                ((o.slideOffset = o.slideWidth * o.options.slidesToShow * -1),
                (s = -1),
                !0 === o.options.vertical &&
                  !0 === o.options.centerMode &&
                  (2 === o.options.slidesToShow
                    ? (s = -1.5)
                    : 1 === o.options.slidesToShow && (s = -2)),
                (r = i * o.options.slidesToShow * s)),
              o.slideCount % o.options.slidesToScroll != 0 &&
                t + o.options.slidesToScroll > o.slideCount &&
                o.slideCount > o.options.slidesToShow &&
                (t > o.slideCount
                  ? ((o.slideOffset =
                      (o.options.slidesToShow - (t - o.slideCount)) *
                      o.slideWidth *
                      -1),
                    (r =
                      (o.options.slidesToShow - (t - o.slideCount)) * i * -1))
                  : ((o.slideOffset =
                      (o.slideCount % o.options.slidesToScroll) *
                      o.slideWidth *
                      -1),
                    (r = (o.slideCount % o.options.slidesToScroll) * i * -1))))
            : t + o.options.slidesToShow > o.slideCount &&
              ((o.slideOffset =
                (t + o.options.slidesToShow - o.slideCount) * o.slideWidth),
              (r = (t + o.options.slidesToShow - o.slideCount) * i)),
          o.slideCount <= o.options.slidesToShow &&
            ((o.slideOffset = 0), (r = 0)),
          !0 === o.options.centerMode && o.slideCount <= o.options.slidesToShow
            ? (o.slideOffset =
                (o.slideWidth * Math.floor(o.options.slidesToShow)) / 2 -
                (o.slideWidth * o.slideCount) / 2)
            : !0 === o.options.centerMode && !0 === o.options.infinite
            ? (o.slideOffset +=
                o.slideWidth * Math.floor(o.options.slidesToShow / 2) -
                o.slideWidth)
            : !0 === o.options.centerMode &&
              ((o.slideOffset = 0),
              (o.slideOffset +=
                o.slideWidth * Math.floor(o.options.slidesToShow / 2))),
          (e =
            !1 === o.options.vertical
              ? t * o.slideWidth * -1 + o.slideOffset
              : t * i * -1 + r),
          !0 === o.options.variableWidth &&
            ((n =
              o.slideCount <= o.options.slidesToShow ||
              !1 === o.options.infinite
                ? o.$slideTrack.children(".slick-slide").eq(t)
                : o.$slideTrack
                    .children(".slick-slide")
                    .eq(t + o.options.slidesToShow)),
            (e =
              !0 === o.options.rtl
                ? n[0]
                  ? -1 * (o.$slideTrack.width() - n[0].offsetLeft - n.width())
                  : 0
                : n[0]
                ? -1 * n[0].offsetLeft
                : 0),
            !0 === o.options.centerMode &&
              ((n =
                o.slideCount <= o.options.slidesToShow ||
                !1 === o.options.infinite
                  ? o.$slideTrack.children(".slick-slide").eq(t)
                  : o.$slideTrack
                      .children(".slick-slide")
                      .eq(t + o.options.slidesToShow + 1)),
              (e =
                !0 === o.options.rtl
                  ? n[0]
                    ? -1 * (o.$slideTrack.width() - n[0].offsetLeft - n.width())
                    : 0
                  : n[0]
                  ? -1 * n[0].offsetLeft
                  : 0),
              (e += (o.$list.width() - n.outerWidth()) / 2))),
          e
        );
      }),
      (e.prototype.getOption = e.prototype.slickGetOption =
        function (t) {
          return this.options[t];
        }),
      (e.prototype.getNavigableIndexes = function () {
        var t,
          e = this,
          i = 0,
          n = 0,
          s = [];
        for (
          !1 === e.options.infinite
            ? (t = e.slideCount)
            : ((i = -1 * e.options.slidesToScroll),
              (n = -1 * e.options.slidesToScroll),
              (t = 2 * e.slideCount));
          i < t;

        )
          s.push(i),
            (i = n + e.options.slidesToScroll),
            (n +=
              e.options.slidesToScroll <= e.options.slidesToShow
                ? e.options.slidesToScroll
                : e.options.slidesToShow);
        return s;
      }),
      (e.prototype.getSlick = function () {
        return this;
      }),
      (e.prototype.getSlideCount = function () {
        var e,
          i,
          n = this;
        return (
          (i =
            !0 === n.options.centerMode
              ? n.slideWidth * Math.floor(n.options.slidesToShow / 2)
              : 0),
          !0 === n.options.swipeToSlide
            ? (n.$slideTrack.find(".slick-slide").each(function (s, o) {
                if (o.offsetLeft - i + t(o).outerWidth() / 2 > -1 * n.swipeLeft)
                  return (e = o), !1;
              }),
              Math.abs(t(e).attr("data-slick-index") - n.currentSlide) || 1)
            : n.options.slidesToScroll
        );
      }),
      (e.prototype.goTo = e.prototype.slickGoTo =
        function (t, e) {
          this.changeSlide(
            { data: { message: "index", index: parseInt(t) } },
            e
          );
        }),
      (e.prototype.init = function (e) {
        var i = this;
        t(i.$slider).hasClass("slick-initialized") ||
          (t(i.$slider).addClass("slick-initialized"),
          i.buildRows(),
          i.buildOut(),
          i.setProps(),
          i.startLoad(),
          i.loadSlider(),
          i.initializeEvents(),
          i.updateArrows(),
          i.updateDots(),
          i.checkResponsive(!0),
          i.focusHandler()),
          e && i.$slider.trigger("init", [i]),
          !0 === i.options.accessibility && i.initADA(),
          i.options.autoplay && ((i.paused = !1), i.autoPlay());
      }),
      (e.prototype.initADA = function () {
        var e = this,
          i = Math.ceil(e.slideCount / e.options.slidesToShow),
          n = e.getNavigableIndexes().filter(function (t) {
            return t >= 0 && t < e.slideCount;
          });
        e.$slides
          .add(e.$slideTrack.find(".slick-cloned"))
          .attr({ "aria-hidden": "true", tabindex: "-1" })
          .find("a, input, button, select")
          .attr({ tabindex: "-1" }),
          null !== e.$dots &&
            (e.$slides
              .not(e.$slideTrack.find(".slick-cloned"))
              .each(function (i) {
                var s = n.indexOf(i);
                t(this).attr({
                  role: "tabpanel",
                  id: "slick-slide" + e.instanceUid + i,
                  tabindex: -1,
                }),
                  -1 !== s &&
                    t(this).attr({
                      "aria-describedby":
                        "slick-slide-control" + e.instanceUid + s,
                    });
              }),
            e.$dots
              .attr("role", "tablist")
              .find("li")
              .each(function (s) {
                var o = n[s];
                t(this).attr({ role: "presentation" }),
                  t(this)
                    .find("button")
                    .first()
                    .attr({
                      role: "tab",
                      id: "slick-slide-control" + e.instanceUid + s,
                      "aria-controls": "slick-slide" + e.instanceUid + o,
                      "aria-label": s + 1 + " of " + i,
                      "aria-selected": null,
                      tabindex: "-1",
                    });
              })
              .eq(e.currentSlide)
              .find("button")
              .attr({ "aria-selected": "true", tabindex: "0" })
              .end());
        for (var s = e.currentSlide, o = s + e.options.slidesToShow; s < o; s++)
          e.$slides.eq(s).attr("tabindex", 0);
        e.activateADA();
      }),
      (e.prototype.initArrowEvents = function () {
        var t = this;
        !0 === t.options.arrows &&
          t.slideCount > t.options.slidesToShow &&
          (t.$prevArrow
            .off("click.slick")
            .on("click.slick", { message: "previous" }, t.changeSlide),
          t.$nextArrow
            .off("click.slick")
            .on("click.slick", { message: "next" }, t.changeSlide),
          !0 === t.options.accessibility &&
            (t.$prevArrow.on("keydown.slick", t.keyHandler),
            t.$nextArrow.on("keydown.slick", t.keyHandler)));
      }),
      (e.prototype.initDotEvents = function () {
        var e = this;
        !0 === e.options.dots &&
          (t("li", e.$dots).on(
            "click.slick",
            { message: "index" },
            e.changeSlide
          ),
          !0 === e.options.accessibility &&
            e.$dots.on("keydown.slick", e.keyHandler)),
          !0 === e.options.dots &&
            !0 === e.options.pauseOnDotsHover &&
            t("li", e.$dots)
              .on("mouseenter.slick", t.proxy(e.interrupt, e, !0))
              .on("mouseleave.slick", t.proxy(e.interrupt, e, !1));
      }),
      (e.prototype.initSlideEvents = function () {
        var e = this;
        e.options.pauseOnHover &&
          (e.$list.on("mouseenter.slick", t.proxy(e.interrupt, e, !0)),
          e.$list.on("mouseleave.slick", t.proxy(e.interrupt, e, !1)));
      }),
      (e.prototype.initializeEvents = function () {
        var e = this;
        e.initArrowEvents(),
          e.initDotEvents(),
          e.initSlideEvents(),
          e.$list.on(
            "touchstart.slick mousedown.slick",
            { action: "start" },
            e.swipeHandler
          ),
          e.$list.on(
            "touchmove.slick mousemove.slick",
            { action: "move" },
            e.swipeHandler
          ),
          e.$list.on(
            "touchend.slick mouseup.slick",
            { action: "end" },
            e.swipeHandler
          ),
          e.$list.on(
            "touchcancel.slick mouseleave.slick",
            { action: "end" },
            e.swipeHandler
          ),
          e.$list.on("click.slick", e.clickHandler),
          t(document).on(e.visibilityChange, t.proxy(e.visibility, e)),
          !0 === e.options.accessibility &&
            e.$list.on("keydown.slick", e.keyHandler),
          !0 === e.options.focusOnSelect &&
            t(e.$slideTrack).children().on("click.slick", e.selectHandler),
          t(window).on(
            "orientationchange.slick.slick-" + e.instanceUid,
            t.proxy(e.orientationChange, e)
          ),
          t(window).on(
            "resize.slick.slick-" + e.instanceUid,
            t.proxy(e.resize, e)
          ),
          t("[draggable!=true]", e.$slideTrack).on(
            "dragstart",
            e.preventDefault
          ),
          t(window).on("load.slick.slick-" + e.instanceUid, e.setPosition),
          t(e.setPosition);
      }),
      (e.prototype.initUI = function () {
        var t = this;
        !0 === t.options.arrows &&
          t.slideCount > t.options.slidesToShow &&
          (t.$prevArrow.show(), t.$nextArrow.show()),
          !0 === t.options.dots &&
            t.slideCount > t.options.slidesToShow &&
            t.$dots.show();
      }),
      (e.prototype.keyHandler = function (t) {
        var e = this;
        t.target.tagName.match("TEXTAREA|INPUT|SELECT") ||
          (37 === t.keyCode && !0 === e.options.accessibility
            ? e.changeSlide({
                data: { message: !0 === e.options.rtl ? "next" : "previous" },
              })
            : 39 === t.keyCode &&
              !0 === e.options.accessibility &&
              e.changeSlide({
                data: { message: !0 === e.options.rtl ? "previous" : "next" },
              }));
      }),
      (e.prototype.lazyLoad = function () {
        function e(e) {
          t("img[data-lazy]", e).each(function () {
            var e = t(this),
              i = t(this).attr("data-lazy"),
              n = t(this).attr("data-srcset"),
              s = t(this).attr("data-sizes") || o.$slider.attr("data-sizes"),
              r = document.createElement("img");
            (r.onload = function () {
              e.animate({ opacity: 0 }, 100, function () {
                n && (e.attr("srcset", n), s && e.attr("sizes", s)),
                  e.attr("src", i).animate({ opacity: 1 }, 200, function () {
                    e.removeAttr(
                      "data-lazy data-srcset data-sizes"
                    ).removeClass("slick-loading");
                  }),
                  o.$slider.trigger("lazyLoaded", [o, e, i]);
              });
            }),
              (r.onerror = function () {
                e
                  .removeAttr("data-lazy")
                  .removeClass("slick-loading")
                  .addClass("slick-lazyload-error"),
                  o.$slider.trigger("lazyLoadError", [o, e, i]);
              }),
              (r.src = i);
          });
        }
        var i,
          n,
          s,
          o = this;
        if (
          (!0 === o.options.centerMode
            ? !0 === o.options.infinite
              ? (s =
                  (n = o.currentSlide + (o.options.slidesToShow / 2 + 1)) +
                  o.options.slidesToShow +
                  2)
              : ((n = Math.max(
                  0,
                  o.currentSlide - (o.options.slidesToShow / 2 + 1)
                )),
                (s = o.options.slidesToShow / 2 + 1 + 2 + o.currentSlide))
            : ((n = o.options.infinite
                ? o.options.slidesToShow + o.currentSlide
                : o.currentSlide),
              (s = Math.ceil(n + o.options.slidesToShow)),
              !0 === o.options.fade &&
                (n > 0 && n--, s <= o.slideCount && s++)),
          (i = o.$slider.find(".slick-slide").slice(n, s)),
          "anticipated" === o.options.lazyLoad)
        )
          for (
            var r = n - 1, a = s, l = o.$slider.find(".slick-slide"), c = 0;
            c < o.options.slidesToScroll;
            c++
          )
            r < 0 && (r = o.slideCount - 1),
              (i = (i = i.add(l.eq(r))).add(l.eq(a))),
              r--,
              a++;
        e(i),
          o.slideCount <= o.options.slidesToShow
            ? e(o.$slider.find(".slick-slide"))
            : o.currentSlide >= o.slideCount - o.options.slidesToShow
            ? e(
                o.$slider.find(".slick-cloned").slice(0, o.options.slidesToShow)
              )
            : 0 === o.currentSlide &&
              e(
                o.$slider
                  .find(".slick-cloned")
                  .slice(-1 * o.options.slidesToShow)
              );
      }),
      (e.prototype.loadSlider = function () {
        var t = this;
        t.setPosition(),
          t.$slideTrack.css({ opacity: 1 }),
          t.$slider.removeClass("slick-loading"),
          t.initUI(),
          "progressive" === t.options.lazyLoad && t.progressiveLazyLoad();
      }),
      (e.prototype.next = e.prototype.slickNext =
        function () {
          this.changeSlide({ data: { message: "next" } });
        }),
      (e.prototype.orientationChange = function () {
        this.checkResponsive(), this.setPosition();
      }),
      (e.prototype.pause = e.prototype.slickPause =
        function () {
          this.autoPlayClear(), (this.paused = !0);
        }),
      (e.prototype.play = e.prototype.slickPlay =
        function () {
          var t = this;
          t.autoPlay(),
            (t.options.autoplay = !0),
            (t.paused = !1),
            (t.focussed = !1),
            (t.interrupted = !1);
        }),
      (e.prototype.postSlide = function (e) {
        var i = this;
        i.unslicked ||
          (i.$slider.trigger("afterChange", [i, e]),
          (i.animating = !1),
          i.slideCount > i.options.slidesToShow && i.setPosition(),
          (i.swipeLeft = null),
          i.options.autoplay && i.autoPlay(),
          !0 === i.options.accessibility &&
            (i.initADA(),
            i.options.focusOnChange &&
              t(i.$slides.get(i.currentSlide)).attr("tabindex", 0).focus()));
      }),
      (e.prototype.prev = e.prototype.slickPrev =
        function () {
          this.changeSlide({ data: { message: "previous" } });
        }),
      (e.prototype.preventDefault = function (t) {
        t.preventDefault();
      }),
      (e.prototype.progressiveLazyLoad = function (e) {
        e = e || 1;
        var i,
          n,
          s,
          o,
          r,
          a = this,
          l = t("img[data-lazy]", a.$slider);
        l.length
          ? ((i = l.first()),
            (n = i.attr("data-lazy")),
            (s = i.attr("data-srcset")),
            (o = i.attr("data-sizes") || a.$slider.attr("data-sizes")),
            ((r = document.createElement("img")).onload = function () {
              s && (i.attr("srcset", s), o && i.attr("sizes", o)),
                i
                  .attr("src", n)
                  .removeAttr("data-lazy data-srcset data-sizes")
                  .removeClass("slick-loading"),
                !0 === a.options.adaptiveHeight && a.setPosition(),
                a.$slider.trigger("lazyLoaded", [a, i, n]),
                a.progressiveLazyLoad();
            }),
            (r.onerror = function () {
              e < 3
                ? setTimeout(function () {
                    a.progressiveLazyLoad(e + 1);
                  }, 500)
                : (i
                    .removeAttr("data-lazy")
                    .removeClass("slick-loading")
                    .addClass("slick-lazyload-error"),
                  a.$slider.trigger("lazyLoadError", [a, i, n]),
                  a.progressiveLazyLoad());
            }),
            (r.src = n))
          : a.$slider.trigger("allImagesLoaded", [a]);
      }),
      (e.prototype.refresh = function (e) {
        var i,
          n,
          s = this;
        (n = s.slideCount - s.options.slidesToShow),
          !s.options.infinite && s.currentSlide > n && (s.currentSlide = n),
          s.slideCount <= s.options.slidesToShow && (s.currentSlide = 0),
          (i = s.currentSlide),
          s.destroy(!0),
          t.extend(s, s.initials, { currentSlide: i }),
          s.init(),
          e || s.changeSlide({ data: { message: "index", index: i } }, !1);
      }),
      (e.prototype.registerBreakpoints = function () {
        var e,
          i,
          n,
          s = this,
          o = s.options.responsive || null;
        if ("array" === t.type(o) && o.length) {
          for (e in ((s.respondTo = s.options.respondTo || "window"), o))
            if (((n = s.breakpoints.length - 1), o.hasOwnProperty(e))) {
              for (i = o[e].breakpoint; n >= 0; )
                s.breakpoints[n] &&
                  s.breakpoints[n] === i &&
                  s.breakpoints.splice(n, 1),
                  n--;
              s.breakpoints.push(i), (s.breakpointSettings[i] = o[e].settings);
            }
          s.breakpoints.sort(function (t, e) {
            return s.options.mobileFirst ? t - e : e - t;
          });
        }
      }),
      (e.prototype.reinit = function () {
        var e = this;
        (e.$slides = e.$slideTrack
          .children(e.options.slide)
          .addClass("slick-slide")),
          (e.slideCount = e.$slides.length),
          e.currentSlide >= e.slideCount &&
            0 !== e.currentSlide &&
            (e.currentSlide = e.currentSlide - e.options.slidesToScroll),
          e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0),
          e.registerBreakpoints(),
          e.setProps(),
          e.setupInfinite(),
          e.buildArrows(),
          e.updateArrows(),
          e.initArrowEvents(),
          e.buildDots(),
          e.updateDots(),
          e.initDotEvents(),
          e.cleanUpSlideEvents(),
          e.initSlideEvents(),
          e.checkResponsive(!1, !0),
          !0 === e.options.focusOnSelect &&
            t(e.$slideTrack).children().on("click.slick", e.selectHandler),
          e.setSlideClasses(
            "number" == typeof e.currentSlide ? e.currentSlide : 0
          ),
          e.setPosition(),
          e.focusHandler(),
          (e.paused = !e.options.autoplay),
          e.autoPlay(),
          e.$slider.trigger("reInit", [e]);
      }),
      (e.prototype.resize = function () {
        var e = this;
        t(window).width() !== e.windowWidth &&
          (clearTimeout(e.windowDelay),
          (e.windowDelay = window.setTimeout(function () {
            (e.windowWidth = t(window).width()),
              e.checkResponsive(),
              e.unslicked || e.setPosition();
          }, 50)));
      }),
      (e.prototype.removeSlide = e.prototype.slickRemove =
        function (t, e, i) {
          var n = this;
          if (
            ((t =
              "boolean" == typeof t
                ? !0 === (e = t)
                  ? 0
                  : n.slideCount - 1
                : !0 === e
                ? --t
                : t),
            n.slideCount < 1 || t < 0 || t > n.slideCount - 1)
          )
            return !1;
          n.unload(),
            !0 === i
              ? n.$slideTrack.children().remove()
              : n.$slideTrack.children(this.options.slide).eq(t).remove(),
            (n.$slides = n.$slideTrack.children(this.options.slide)),
            n.$slideTrack.children(this.options.slide).detach(),
            n.$slideTrack.append(n.$slides),
            (n.$slidesCache = n.$slides),
            n.reinit();
        }),
      (e.prototype.setCSS = function (t) {
        var e,
          i,
          n = this,
          s = {};
        !0 === n.options.rtl && (t = -t),
          (e = "left" == n.positionProp ? Math.ceil(t) + "px" : "0px"),
          (i = "top" == n.positionProp ? Math.ceil(t) + "px" : "0px"),
          (s[n.positionProp] = t),
          !1 === n.transformsEnabled
            ? n.$slideTrack.css(s)
            : ((s = {}),
              !1 === n.cssTransitions
                ? ((s[n.animType] = "translate(" + e + ", " + i + ")"),
                  n.$slideTrack.css(s))
                : ((s[n.animType] = "translate3d(" + e + ", " + i + ", 0px)"),
                  n.$slideTrack.css(s)));
      }),
      (e.prototype.setDimensions = function () {
        var t = this;
        !1 === t.options.vertical
          ? !0 === t.options.centerMode &&
            t.$list.css({ padding: "0px " + t.options.centerPadding })
          : (t.$list.height(
              t.$slides.first().outerHeight(!0) * t.options.slidesToShow
            ),
            !0 === t.options.centerMode &&
              t.$list.css({ padding: t.options.centerPadding + " 0px" })),
          (t.listWidth = t.$list.width()),
          (t.listHeight = t.$list.height()),
          !1 === t.options.vertical && !1 === t.options.variableWidth
            ? ((t.slideWidth = Math.ceil(t.listWidth / t.options.slidesToShow)),
              t.$slideTrack.width(
                Math.ceil(
                  t.slideWidth * t.$slideTrack.children(".slick-slide").length
                )
              ))
            : !0 === t.options.variableWidth
            ? t.$slideTrack.width(5e3 * t.slideCount)
            : ((t.slideWidth = Math.ceil(t.listWidth)),
              t.$slideTrack.height(
                Math.ceil(
                  t.$slides.first().outerHeight(!0) *
                    t.$slideTrack.children(".slick-slide").length
                )
              ));
        var e = t.$slides.first().outerWidth(!0) - t.$slides.first().width();
        !1 === t.options.variableWidth &&
          t.$slideTrack.children(".slick-slide").width(t.slideWidth - e);
      }),
      (e.prototype.setFade = function () {
        var e,
          i = this;
        i.$slides.each(function (n, s) {
          (e = i.slideWidth * n * -1),
            !0 === i.options.rtl
              ? t(s).css({
                  position: "relative",
                  right: e,
                  top: 0,
                  zIndex: i.options.zIndex - 2,
                  opacity: 0,
                })
              : t(s).css({
                  position: "relative",
                  left: e,
                  top: 0,
                  zIndex: i.options.zIndex - 2,
                  opacity: 0,
                });
        }),
          i.$slides
            .eq(i.currentSlide)
            .css({ zIndex: i.options.zIndex - 1, opacity: 1 });
      }),
      (e.prototype.setHeight = function () {
        var t = this;
        if (
          1 === t.options.slidesToShow &&
          !0 === t.options.adaptiveHeight &&
          !1 === t.options.vertical
        ) {
          var e = t.$slides.eq(t.currentSlide).outerHeight(!0);
          t.$list.css("height", e);
        }
      }),
      (e.prototype.setOption = e.prototype.slickSetOption =
        function () {
          var e,
            i,
            n,
            s,
            o,
            r = this,
            a = !1;
          if (
            ("object" === t.type(arguments[0])
              ? ((n = arguments[0]), (a = arguments[1]), (o = "multiple"))
              : "string" === t.type(arguments[0]) &&
                ((n = arguments[0]),
                (s = arguments[1]),
                (a = arguments[2]),
                "responsive" === arguments[0] &&
                "array" === t.type(arguments[1])
                  ? (o = "responsive")
                  : void 0 !== arguments[1] && (o = "single")),
            "single" === o)
          )
            r.options[n] = s;
          else if ("multiple" === o)
            t.each(n, function (t, e) {
              r.options[t] = e;
            });
          else if ("responsive" === o)
            for (i in s)
              if ("array" !== t.type(r.options.responsive))
                r.options.responsive = [s[i]];
              else {
                for (e = r.options.responsive.length - 1; e >= 0; )
                  r.options.responsive[e].breakpoint === s[i].breakpoint &&
                    r.options.responsive.splice(e, 1),
                    e--;
                r.options.responsive.push(s[i]);
              }
          a && (r.unload(), r.reinit());
        }),
      (e.prototype.setPosition = function () {
        var t = this;
        t.setDimensions(),
          t.setHeight(),
          !1 === t.options.fade
            ? t.setCSS(t.getLeft(t.currentSlide))
            : t.setFade(),
          t.$slider.trigger("setPosition", [t]);
      }),
      (e.prototype.setProps = function () {
        var t = this,
          e = document.body.style;
        (t.positionProp = !0 === t.options.vertical ? "top" : "left"),
          "top" === t.positionProp
            ? t.$slider.addClass("slick-vertical")
            : t.$slider.removeClass("slick-vertical"),
          (void 0 === e.WebkitTransition &&
            void 0 === e.MozTransition &&
            void 0 === e.msTransition) ||
            (!0 === t.options.useCSS && (t.cssTransitions = !0)),
          t.options.fade &&
            ("number" == typeof t.options.zIndex
              ? t.options.zIndex < 3 && (t.options.zIndex = 3)
              : (t.options.zIndex = t.defaults.zIndex)),
          void 0 !== e.OTransform &&
            ((t.animType = "OTransform"),
            (t.transformType = "-o-transform"),
            (t.transitionType = "OTransition"),
            void 0 === e.perspectiveProperty &&
              void 0 === e.webkitPerspective &&
              (t.animType = !1)),
          void 0 !== e.MozTransform &&
            ((t.animType = "MozTransform"),
            (t.transformType = "-moz-transform"),
            (t.transitionType = "MozTransition"),
            void 0 === e.perspectiveProperty &&
              void 0 === e.MozPerspective &&
              (t.animType = !1)),
          void 0 !== e.webkitTransform &&
            ((t.animType = "webkitTransform"),
            (t.transformType = "-webkit-transform"),
            (t.transitionType = "webkitTransition"),
            void 0 === e.perspectiveProperty &&
              void 0 === e.webkitPerspective &&
              (t.animType = !1)),
          void 0 !== e.msTransform &&
            ((t.animType = "msTransform"),
            (t.transformType = "-ms-transform"),
            (t.transitionType = "msTransition"),
            void 0 === e.msTransform && (t.animType = !1)),
          void 0 !== e.transform &&
            !1 !== t.animType &&
            ((t.animType = "transform"),
            (t.transformType = "transform"),
            (t.transitionType = "transition")),
          (t.transformsEnabled =
            t.options.useTransform && null !== t.animType && !1 !== t.animType);
      }),
      (e.prototype.setSlideClasses = function (t) {
        var e,
          i,
          n,
          s,
          o = this;
        if (
          ((i = o.$slider
            .find(".slick-slide")
            .removeClass("slick-active slick-center slick-current")
            .attr("aria-hidden", "true")),
          o.$slides.eq(t).addClass("slick-current"),
          !0 === o.options.centerMode)
        ) {
          var r = o.options.slidesToShow % 2 == 0 ? 1 : 0;
          (e = Math.floor(o.options.slidesToShow / 2)),
            !0 === o.options.infinite &&
              (t >= e && t <= o.slideCount - 1 - e
                ? o.$slides
                    .slice(t - e + r, t + e + 1)
                    .addClass("slick-active")
                    .attr("aria-hidden", "false")
                : ((n = o.options.slidesToShow + t),
                  i
                    .slice(n - e + 1 + r, n + e + 2)
                    .addClass("slick-active")
                    .attr("aria-hidden", "false")),
              0 === t
                ? i
                    .eq(i.length - 1 - o.options.slidesToShow)
                    .addClass("slick-center")
                : t === o.slideCount - 1 &&
                  i.eq(o.options.slidesToShow).addClass("slick-center")),
            o.$slides.eq(t).addClass("slick-center");
        } else
          t >= 0 && t <= o.slideCount - o.options.slidesToShow
            ? o.$slides
                .slice(t, t + o.options.slidesToShow)
                .addClass("slick-active")
                .attr("aria-hidden", "false")
            : i.length <= o.options.slidesToShow
            ? i.addClass("slick-active").attr("aria-hidden", "false")
            : ((s = o.slideCount % o.options.slidesToShow),
              (n = !0 === o.options.infinite ? o.options.slidesToShow + t : t),
              o.options.slidesToShow == o.options.slidesToScroll &&
              o.slideCount - t < o.options.slidesToShow
                ? i
                    .slice(n - (o.options.slidesToShow - s), n + s)
                    .addClass("slick-active")
                    .attr("aria-hidden", "false")
                : i
                    .slice(n, n + o.options.slidesToShow)
                    .addClass("slick-active")
                    .attr("aria-hidden", "false"));
        ("ondemand" !== o.options.lazyLoad &&
          "anticipated" !== o.options.lazyLoad) ||
          o.lazyLoad();
      }),
      (e.prototype.setupInfinite = function () {
        var e,
          i,
          n,
          s = this;
        if (
          (!0 === s.options.fade && (s.options.centerMode = !1),
          !0 === s.options.infinite &&
            !1 === s.options.fade &&
            ((i = null), s.slideCount > s.options.slidesToShow))
        ) {
          for (
            n =
              !0 === s.options.centerMode
                ? s.options.slidesToShow + 1
                : s.options.slidesToShow,
              e = s.slideCount;
            e > s.slideCount - n;
            e -= 1
          )
            (i = e - 1),
              t(s.$slides[i])
                .clone(!0)
                .attr("id", "")
                .attr("data-slick-index", i - s.slideCount)
                .prependTo(s.$slideTrack)
                .addClass("slick-cloned");
          for (e = 0; e < n + s.slideCount; e += 1)
            (i = e),
              t(s.$slides[i])
                .clone(!0)
                .attr("id", "")
                .attr("data-slick-index", i + s.slideCount)
                .appendTo(s.$slideTrack)
                .addClass("slick-cloned");
          s.$slideTrack
            .find(".slick-cloned")
            .find("[id]")
            .each(function () {
              t(this).attr("id", "");
            });
        }
      }),
      (e.prototype.interrupt = function (t) {
        t || this.autoPlay(), (this.interrupted = t);
      }),
      (e.prototype.selectHandler = function (e) {
        var i = this,
          n = t(e.target).is(".slick-slide")
            ? t(e.target)
            : t(e.target).parents(".slick-slide"),
          s = parseInt(n.attr("data-slick-index"));
        s || (s = 0),
          i.slideCount <= i.options.slidesToShow
            ? i.slideHandler(s, !1, !0)
            : i.slideHandler(s);
      }),
      (e.prototype.slideHandler = function (t, e, i) {
        var n,
          s,
          o,
          r,
          a,
          l = null,
          c = this;
        if (
          ((e = e || !1),
          !(
            (!0 === c.animating && !0 === c.options.waitForAnimate) ||
            (!0 === c.options.fade && c.currentSlide === t)
          ))
        )
          if (
            (!1 === e && c.asNavFor(t),
            (n = t),
            (l = c.getLeft(n)),
            (r = c.getLeft(c.currentSlide)),
            (c.currentLeft = null === c.swipeLeft ? r : c.swipeLeft),
            !1 === c.options.infinite &&
              !1 === c.options.centerMode &&
              (t < 0 || t > c.getDotCount() * c.options.slidesToScroll))
          )
            !1 === c.options.fade &&
              ((n = c.currentSlide),
              !0 !== i
                ? c.animateSlide(r, function () {
                    c.postSlide(n);
                  })
                : c.postSlide(n));
          else if (
            !1 === c.options.infinite &&
            !0 === c.options.centerMode &&
            (t < 0 || t > c.slideCount - c.options.slidesToScroll)
          )
            !1 === c.options.fade &&
              ((n = c.currentSlide),
              !0 !== i
                ? c.animateSlide(r, function () {
                    c.postSlide(n);
                  })
                : c.postSlide(n));
          else {
            if (
              (c.options.autoplay && clearInterval(c.autoPlayTimer),
              (s =
                n < 0
                  ? c.slideCount % c.options.slidesToScroll != 0
                    ? c.slideCount - (c.slideCount % c.options.slidesToScroll)
                    : c.slideCount + n
                  : n >= c.slideCount
                  ? c.slideCount % c.options.slidesToScroll != 0
                    ? 0
                    : n - c.slideCount
                  : n),
              (c.animating = !0),
              c.$slider.trigger("beforeChange", [c, c.currentSlide, s]),
              (o = c.currentSlide),
              (c.currentSlide = s),
              c.setSlideClasses(c.currentSlide),
              c.options.asNavFor &&
                (a = (a = c.getNavTarget()).slick("getSlick")).slideCount <=
                  a.options.slidesToShow &&
                a.setSlideClasses(c.currentSlide),
              c.updateDots(),
              c.updateArrows(),
              !0 === c.options.fade)
            )
              return (
                !0 !== i
                  ? (c.fadeSlideOut(o),
                    c.fadeSlide(s, function () {
                      c.postSlide(s);
                    }))
                  : c.postSlide(s),
                void c.animateHeight()
              );
            !0 !== i
              ? c.animateSlide(l, function () {
                  c.postSlide(s);
                })
              : c.postSlide(s);
          }
      }),
      (e.prototype.startLoad = function () {
        var t = this;
        !0 === t.options.arrows &&
          t.slideCount > t.options.slidesToShow &&
          (t.$prevArrow.hide(), t.$nextArrow.hide()),
          !0 === t.options.dots &&
            t.slideCount > t.options.slidesToShow &&
            t.$dots.hide(),
          t.$slider.addClass("slick-loading");
      }),
      (e.prototype.swipeDirection = function () {
        var t,
          e,
          i,
          n,
          s = this;
        return (
          (t = s.touchObject.startX - s.touchObject.curX),
          (e = s.touchObject.startY - s.touchObject.curY),
          (i = Math.atan2(e, t)),
          (n = Math.round((180 * i) / Math.PI)) < 0 && (n = 360 - Math.abs(n)),
          n <= 45 && n >= 0
            ? !1 === s.options.rtl
              ? "left"
              : "right"
            : n <= 360 && n >= 315
            ? !1 === s.options.rtl
              ? "left"
              : "right"
            : n >= 135 && n <= 225
            ? !1 === s.options.rtl
              ? "right"
              : "left"
            : !0 === s.options.verticalSwiping
            ? n >= 35 && n <= 135
              ? "down"
              : "up"
            : "vertical"
        );
      }),
      (e.prototype.swipeEnd = function (t) {
        var e,
          i,
          n = this;
        if (((n.dragging = !1), (n.swiping = !1), n.scrolling))
          return (n.scrolling = !1), !1;
        if (
          ((n.interrupted = !1),
          (n.shouldClick = !(n.touchObject.swipeLength > 10)),
          void 0 === n.touchObject.curX)
        )
          return !1;
        if (
          (!0 === n.touchObject.edgeHit &&
            n.$slider.trigger("edge", [n, n.swipeDirection()]),
          n.touchObject.swipeLength >= n.touchObject.minSwipe)
        ) {
          switch ((i = n.swipeDirection())) {
            case "left":
            case "down":
              (e = n.options.swipeToSlide
                ? n.checkNavigable(n.currentSlide + n.getSlideCount())
                : n.currentSlide + n.getSlideCount()),
                (n.currentDirection = 0);
              break;
            case "right":
            case "up":
              (e = n.options.swipeToSlide
                ? n.checkNavigable(n.currentSlide - n.getSlideCount())
                : n.currentSlide - n.getSlideCount()),
                (n.currentDirection = 1);
          }
          "vertical" != i &&
            (n.slideHandler(e),
            (n.touchObject = {}),
            n.$slider.trigger("swipe", [n, i]));
        } else
          n.touchObject.startX !== n.touchObject.curX &&
            (n.slideHandler(n.currentSlide), (n.touchObject = {}));
      }),
      (e.prototype.swipeHandler = function (t) {
        var e = this;
        if (
          !(
            !1 === e.options.swipe ||
            ("ontouchend" in document && !1 === e.options.swipe) ||
            (!1 === e.options.draggable && -1 !== t.type.indexOf("mouse"))
          )
        )
          switch (
            ((e.touchObject.fingerCount =
              t.originalEvent && void 0 !== t.originalEvent.touches
                ? t.originalEvent.touches.length
                : 1),
            (e.touchObject.minSwipe = e.listWidth / e.options.touchThreshold),
            !0 === e.options.verticalSwiping &&
              (e.touchObject.minSwipe =
                e.listHeight / e.options.touchThreshold),
            t.data.action)
          ) {
            case "start":
              e.swipeStart(t);
              break;
            case "move":
              e.swipeMove(t);
              break;
            case "end":
              e.swipeEnd(t);
          }
      }),
      (e.prototype.swipeMove = function (t) {
        var e,
          i,
          n,
          s,
          o,
          r,
          a = this;
        return (
          (o = void 0 !== t.originalEvent ? t.originalEvent.touches : null),
          !(!a.dragging || a.scrolling || (o && 1 !== o.length)) &&
            ((e = a.getLeft(a.currentSlide)),
            (a.touchObject.curX = void 0 !== o ? o[0].pageX : t.clientX),
            (a.touchObject.curY = void 0 !== o ? o[0].pageY : t.clientY),
            (a.touchObject.swipeLength = Math.round(
              Math.sqrt(Math.pow(a.touchObject.curX - a.touchObject.startX, 2))
            )),
            (r = Math.round(
              Math.sqrt(Math.pow(a.touchObject.curY - a.touchObject.startY, 2))
            )),
            !a.options.verticalSwiping && !a.swiping && r > 4
              ? ((a.scrolling = !0), !1)
              : (!0 === a.options.verticalSwiping &&
                  (a.touchObject.swipeLength = r),
                (i = a.swipeDirection()),
                void 0 !== t.originalEvent &&
                  a.touchObject.swipeLength > 4 &&
                  ((a.swiping = !0), t.preventDefault()),
                (s =
                  (!1 === a.options.rtl ? 1 : -1) *
                  (a.touchObject.curX > a.touchObject.startX ? 1 : -1)),
                !0 === a.options.verticalSwiping &&
                  (s = a.touchObject.curY > a.touchObject.startY ? 1 : -1),
                (n = a.touchObject.swipeLength),
                (a.touchObject.edgeHit = !1),
                !1 === a.options.infinite &&
                  ((0 === a.currentSlide && "right" === i) ||
                    (a.currentSlide >= a.getDotCount() && "left" === i)) &&
                  ((n = a.touchObject.swipeLength * a.options.edgeFriction),
                  (a.touchObject.edgeHit = !0)),
                !1 === a.options.vertical
                  ? (a.swipeLeft = e + n * s)
                  : (a.swipeLeft =
                      e + n * (a.$list.height() / a.listWidth) * s),
                !0 === a.options.verticalSwiping && (a.swipeLeft = e + n * s),
                !0 !== a.options.fade &&
                  !1 !== a.options.touchMove &&
                  (!0 === a.animating
                    ? ((a.swipeLeft = null), !1)
                    : void a.setCSS(a.swipeLeft))))
        );
      }),
      (e.prototype.swipeStart = function (t) {
        var e,
          i = this;
        if (
          ((i.interrupted = !0),
          1 !== i.touchObject.fingerCount ||
            i.slideCount <= i.options.slidesToShow)
        )
          return (i.touchObject = {}), !1;
        void 0 !== t.originalEvent &&
          void 0 !== t.originalEvent.touches &&
          (e = t.originalEvent.touches[0]),
          (i.touchObject.startX = i.touchObject.curX =
            void 0 !== e ? e.pageX : t.clientX),
          (i.touchObject.startY = i.touchObject.curY =
            void 0 !== e ? e.pageY : t.clientY),
          (i.dragging = !0);
      }),
      (e.prototype.unfilterSlides = e.prototype.slickUnfilter =
        function () {
          var t = this;
          null !== t.$slidesCache &&
            (t.unload(),
            t.$slideTrack.children(this.options.slide).detach(),
            t.$slidesCache.appendTo(t.$slideTrack),
            t.reinit());
        }),
      (e.prototype.unload = function () {
        var e = this;
        t(".slick-cloned", e.$slider).remove(),
          e.$dots && e.$dots.remove(),
          e.$prevArrow &&
            e.htmlExpr.test(e.options.prevArrow) &&
            e.$prevArrow.remove(),
          e.$nextArrow &&
            e.htmlExpr.test(e.options.nextArrow) &&
            e.$nextArrow.remove(),
          e.$slides
            .removeClass("slick-slide slick-active slick-visible slick-current")
            .attr("aria-hidden", "true")
            .css("width", "");
      }),
      (e.prototype.unslick = function (t) {
        var e = this;
        e.$slider.trigger("unslick", [e, t]), e.destroy();
      }),
      (e.prototype.updateArrows = function () {
        var t = this;
        Math.floor(t.options.slidesToShow / 2),
          !0 === t.options.arrows &&
            t.slideCount > t.options.slidesToShow &&
            !t.options.infinite &&
            (t.$prevArrow
              .removeClass("slick-disabled")
              .attr("aria-disabled", "false"),
            t.$nextArrow
              .removeClass("slick-disabled")
              .attr("aria-disabled", "false"),
            0 === t.currentSlide
              ? (t.$prevArrow
                  .addClass("slick-disabled")
                  .attr("aria-disabled", "true"),
                t.$nextArrow
                  .removeClass("slick-disabled")
                  .attr("aria-disabled", "false"))
              : t.currentSlide >= t.slideCount - t.options.slidesToShow &&
                !1 === t.options.centerMode
              ? (t.$nextArrow
                  .addClass("slick-disabled")
                  .attr("aria-disabled", "true"),
                t.$prevArrow
                  .removeClass("slick-disabled")
                  .attr("aria-disabled", "false"))
              : t.currentSlide >= t.slideCount - 1 &&
                !0 === t.options.centerMode &&
                (t.$nextArrow
                  .addClass("slick-disabled")
                  .attr("aria-disabled", "true"),
                t.$prevArrow
                  .removeClass("slick-disabled")
                  .attr("aria-disabled", "false")));
      }),
      (e.prototype.updateDots = function () {
        var t = this;
        null !== t.$dots &&
          (t.$dots.find("li").removeClass("slick-active").end(),
          t.$dots
            .find("li")
            .eq(Math.floor(t.currentSlide / t.options.slidesToScroll))
            .addClass("slick-active"));
      }),
      (e.prototype.visibility = function () {
        var t = this;
        t.options.autoplay &&
          (document[t.hidden] ? (t.interrupted = !0) : (t.interrupted = !1));
      }),
      (t.fn.slick = function () {
        var t,
          i,
          n = this,
          s = arguments[0],
          o = Array.prototype.slice.call(arguments, 1),
          r = n.length;
        for (t = 0; t < r; t++)
          if (
            ("object" == typeof s || void 0 === s
              ? (n[t].slick = new e(n[t], s))
              : (i = n[t].slick[s].apply(n[t].slick, o)),
            void 0 !== i)
          )
            return i;
        return n;
      });
  }),
  (function (t, e) {
    var i = t(window);
    function n() {
      return new Date(Date.UTC.apply(Date, arguments));
    }
    function s() {
      var t = new Date();
      return n(t.getFullYear(), t.getMonth(), t.getDate());
    }
    function o(t) {
      return function () {
        return this[t].apply(this, arguments);
      };
    }
    var r,
      a =
        ((r = {
          get: function (t) {
            return this.slice(t)[0];
          },
          contains: function (t) {
            for (var e = t && t.valueOf(), i = 0, n = this.length; i < n; i++)
              if (this[i].valueOf() === e) return i;
            return -1;
          },
          remove: function (t) {
            this.splice(t, 1);
          },
          replace: function (e) {
            e &&
              (t.isArray(e) || (e = [e]),
              this.clear(),
              this.push.apply(this, e));
          },
          clear: function () {
            this.splice(0);
          },
          copy: function () {
            var t = new a();
            return t.replace(this), t;
          },
        }),
        function () {
          var e = [];
          return e.push.apply(e, arguments), t.extend(e, r), e;
        }),
      l = function (e, i) {
        (this.dates = new a()),
          (this.viewDate = s()),
          (this.focusDate = null),
          this._process_options(i),
          (this.element = t(e)),
          (this.isInline = !1),
          (this.isInput = this.element.is("input")),
          (this.component =
            !!this.element.is(".date") &&
            this.element.find(".add-on, .input-group-addon, .btn")),
          (this.hasInput = this.component && this.element.find("input").length),
          this.component &&
            0 === this.component.length &&
            (this.component = !1),
          (this.picker = t(f.template)),
          this._buildEvents(),
          this._attachEvents(),
          this.isInline
            ? this.picker.addClass("datepicker-inline").appendTo(this.element)
            : this.picker.addClass("datepicker-dropdown dropdown-menu"),
          this.o.rtl && this.picker.addClass("datepicker-rtl"),
          (this.viewMode = this.o.startView),
          this.o.calendarWeeks &&
            this.picker.find("tfoot th.today").attr("colspan", function (t, e) {
              return parseInt(e) + 1;
            }),
          (this._allow_update = !1),
          this.setStartDate(this._o.startDate),
          this.setEndDate(this._o.endDate),
          this.setDaysOfWeekDisabled(this.o.daysOfWeekDisabled),
          this.fillDow(),
          this.fillMonths(),
          (this._allow_update = !0),
          this.update(),
          this.showMode(),
          this.isInline && this.show();
      };
    l.prototype = {
      constructor: l,
      _process_options: function (e) {
        this._o = t.extend({}, this._o, e);
        var i = (this.o = t.extend({}, this._o)),
          n = i.language;
        switch (
          (p[n] || ((n = n.split("-")[0]), p[n] || (n = h.language)),
          (i.language = n),
          i.startView)
        ) {
          case 2:
          case "decade":
            i.startView = 2;
            break;
          case 1:
          case "year":
            i.startView = 1;
            break;
          default:
            i.startView = 0;
        }
        switch (i.minViewMode) {
          case 1:
          case "months":
            i.minViewMode = 1;
            break;
          case 2:
          case "years":
            i.minViewMode = 2;
            break;
          default:
            i.minViewMode = 0;
        }
        (i.startView = Math.max(i.startView, i.minViewMode)),
          !0 !== i.multidate &&
            ((i.multidate = Number(i.multidate) || !1),
            !1 !== i.multidate
              ? (i.multidate = Math.max(0, i.multidate))
              : (i.multidate = 1)),
          (i.multidateSeparator = String(i.multidateSeparator)),
          (i.weekStart %= 7),
          (i.weekEnd = (i.weekStart + 6) % 7);
        var s = f.parseFormat(i.format);
        i.startDate !== -1 / 0 &&
          (i.startDate
            ? i.startDate instanceof Date
              ? (i.startDate = this._local_to_utc(this._zero_time(i.startDate)))
              : (i.startDate = f.parseDate(i.startDate, s, i.language))
            : (i.startDate = -1 / 0)),
          i.endDate !== 1 / 0 &&
            (i.endDate
              ? i.endDate instanceof Date
                ? (i.endDate = this._local_to_utc(this._zero_time(i.endDate)))
                : (i.endDate = f.parseDate(i.endDate, s, i.language))
              : (i.endDate = 1 / 0)),
          (i.daysOfWeekDisabled = i.daysOfWeekDisabled || []),
          t.isArray(i.daysOfWeekDisabled) ||
            (i.daysOfWeekDisabled = i.daysOfWeekDisabled.split(/[,\s]*/)),
          (i.daysOfWeekDisabled = t.map(i.daysOfWeekDisabled, function (t) {
            return parseInt(t, 10);
          }));
        var o = String(i.orientation).toLowerCase().split(/\s+/g),
          r = i.orientation.toLowerCase();
        if (
          ((o = t.grep(o, function (t) {
            return /^auto|left|right|top|bottom$/.test(t);
          })),
          (i.orientation = { x: "auto", y: "auto" }),
          r && "auto" !== r)
        )
          if (1 === o.length)
            switch (o[0]) {
              case "top":
              case "bottom":
                i.orientation.y = o[0];
                break;
              case "left":
              case "right":
                i.orientation.x = o[0];
            }
          else
            (r = t.grep(o, function (t) {
              return /^left|right$/.test(t);
            })),
              (i.orientation.x = r[0] || "auto"),
              (r = t.grep(o, function (t) {
                return /^top|bottom$/.test(t);
              })),
              (i.orientation.y = r[0] || "auto");
      },
      _events: [],
      _secondaryEvents: [],
      _applyEvents: function (t) {
        for (var e, i, n, s = 0; s < t.length; s++)
          (e = t[s][0]),
            2 === t[s].length
              ? ((i = void 0), (n = t[s][1]))
              : 3 === t[s].length && ((i = t[s][1]), (n = t[s][2])),
            e.on(n, i);
      },
      _unapplyEvents: function (t) {
        for (var e, i, n, s = 0; s < t.length; s++)
          (e = t[s][0]),
            2 === t[s].length
              ? ((n = void 0), (i = t[s][1]))
              : 3 === t[s].length && ((n = t[s][1]), (i = t[s][2])),
            e.off(i, n);
      },
      _buildEvents: function () {
        this.isInput
          ? (this._events = [
              [
                this.element,
                {
                  focus: t.proxy(this.show, this),
                  keyup: t.proxy(function (e) {
                    -1 ===
                      t.inArray(e.keyCode, [27, 37, 39, 38, 40, 32, 13, 9]) &&
                      this.update();
                  }, this),
                  keydown: t.proxy(this.keydown, this),
                },
              ],
            ])
          : this.component && this.hasInput
          ? (this._events = [
              [
                this.element.find("input"),
                {
                  focus: t.proxy(this.show, this),
                  keyup: t.proxy(function (e) {
                    -1 ===
                      t.inArray(e.keyCode, [27, 37, 39, 38, 40, 32, 13, 9]) &&
                      this.update();
                  }, this),
                  keydown: t.proxy(this.keydown, this),
                },
              ],
              [this.component, { click: t.proxy(this.show, this) }],
            ])
          : this.element.is("div")
          ? (this.isInline = !0)
          : (this._events = [
              [this.element, { click: t.proxy(this.show, this) }],
            ]),
          this._events.push(
            [
              this.element,
              "*",
              {
                blur: t.proxy(function (t) {
                  this._focused_from = t.target;
                }, this),
              },
            ],
            [
              this.element,
              {
                blur: t.proxy(function (t) {
                  this._focused_from = t.target;
                }, this),
              },
            ]
          ),
          (this._secondaryEvents = [
            [this.picker, { click: t.proxy(this.click, this) }],
            [t(window), { resize: t.proxy(this.place, this) }],
            [
              t(document),
              {
                "mousedown touchstart": t.proxy(function (t) {
                  this.element.is(t.target) ||
                    this.element.find(t.target).length ||
                    this.picker.is(t.target) ||
                    this.picker.find(t.target).length ||
                    this.hide();
                }, this),
              },
            ],
          ]);
      },
      _attachEvents: function () {
        this._detachEvents(), this._applyEvents(this._events);
      },
      _detachEvents: function () {
        this._unapplyEvents(this._events);
      },
      _attachSecondaryEvents: function () {
        this._detachSecondaryEvents(), this._applyEvents(this._secondaryEvents);
      },
      _detachSecondaryEvents: function () {
        this._unapplyEvents(this._secondaryEvents);
      },
      _trigger: function (e, i) {
        var n = i || this.dates.get(-1),
          s = this._utc_to_local(n);
        this.element.trigger({
          type: e,
          date: s,
          dates: t.map(this.dates, this._utc_to_local),
          format: t.proxy(function (t, e) {
            0 === arguments.length
              ? ((t = this.dates.length - 1), (e = this.o.format))
              : "string" == typeof t && ((e = t), (t = this.dates.length - 1)),
              (e = e || this.o.format);
            var i = this.dates.get(t);
            return f.formatDate(i, e, this.o.language);
          }, this),
        });
      },
      show: function () {
        this.isInline || this.picker.appendTo("body"),
          this.picker.show(),
          this.place(),
          this._attachSecondaryEvents(),
          this._trigger("show");
      },
      hide: function () {
        this.isInline ||
          (this.picker.is(":visible") &&
            ((this.focusDate = null),
            this.picker.hide().detach(),
            this._detachSecondaryEvents(),
            (this.viewMode = this.o.startView),
            this.showMode(),
            this.o.forceParse &&
              ((this.isInput && this.element.val()) ||
                (this.hasInput && this.element.find("input").val())) &&
              this.setValue(),
            this._trigger("hide")));
      },
      remove: function () {
        this.hide(),
          this._detachEvents(),
          this._detachSecondaryEvents(),
          this.picker.remove(),
          delete this.element.data().datepicker,
          this.isInput || delete this.element.data().date;
      },
      _utc_to_local: function (t) {
        return t && new Date(t.getTime() + 6e4 * t.getTimezoneOffset());
      },
      _local_to_utc: function (t) {
        return t && new Date(t.getTime() - 6e4 * t.getTimezoneOffset());
      },
      _zero_time: function (t) {
        return t && new Date(t.getFullYear(), t.getMonth(), t.getDate());
      },
      _zero_utc_time: function (t) {
        return (
          t &&
          new Date(
            Date.UTC(t.getUTCFullYear(), t.getUTCMonth(), t.getUTCDate())
          )
        );
      },
      getDates: function () {
        return t.map(this.dates, this._utc_to_local);
      },
      getUTCDates: function () {
        return t.map(this.dates, function (t) {
          return new Date(t);
        });
      },
      getDate: function () {
        return this._utc_to_local(this.getUTCDate());
      },
      getUTCDate: function () {
        return new Date(this.dates.get(-1));
      },
      setDates: function () {
        var e = t.isArray(arguments[0]) ? arguments[0] : arguments;
        this.update.apply(this, e),
          this._trigger("changeDate"),
          this.setValue();
      },
      setUTCDates: function () {
        var e = t.isArray(arguments[0]) ? arguments[0] : arguments;
        this.update.apply(this, t.map(e, this._utc_to_local)),
          this._trigger("changeDate"),
          this.setValue();
      },
      setDate: o("setDates"),
      setUTCDate: o("setUTCDates"),
      setValue: function () {
        var t = this.getFormattedDate();
        this.isInput
          ? this.element.val(t).change()
          : this.component && this.element.find("input").val(t).change();
      },
      getFormattedDate: function (e) {
        void 0 === e && (e = this.o.format);
        var i = this.o.language;
        return t
          .map(this.dates, function (t) {
            return f.formatDate(t, e, i);
          })
          .join(this.o.multidateSeparator);
      },
      setStartDate: function (t) {
        this._process_options({ startDate: t }),
          this.update(),
          this.updateNavArrows();
      },
      setEndDate: function (t) {
        this._process_options({ endDate: t }),
          this.update(),
          this.updateNavArrows();
      },
      setDaysOfWeekDisabled: function (t) {
        this._process_options({ daysOfWeekDisabled: t }),
          this.update(),
          this.updateNavArrows();
      },
      place: function () {
        if (!this.isInline) {
          var e = this.picker.outerWidth(),
            n = this.picker.outerHeight(),
            s = i.width(),
            o = i.height(),
            r = i.scrollTop(),
            a =
              parseInt(
                this.element
                  .parents()
                  .filter(function () {
                    return "auto" !== t(this).css("z-index");
                  })
                  .first()
                  .css("z-index")
              ) + 10,
            l = this.component
              ? this.component.parent().offset()
              : this.element.offset(),
            c = this.component
              ? this.component.outerHeight(!0)
              : this.element.outerHeight(!1),
            d = this.component
              ? this.component.outerWidth(!0)
              : this.element.outerWidth(!1),
            h = l.left,
            u = l.top;
          this.picker.removeClass(
            "datepicker-orient-top datepicker-orient-bottom datepicker-orient-right datepicker-orient-left"
          ),
            "auto" !== this.o.orientation.x
              ? (this.picker.addClass(
                  "datepicker-orient-" + this.o.orientation.x
                ),
                "right" === this.o.orientation.x && (h -= e - d))
              : (this.picker.addClass("datepicker-orient-left"),
                l.left < 0
                  ? (h -= l.left - 10)
                  : l.left + e > s && (h = s - e - 10));
          var p,
            f,
            m = this.o.orientation.y;
          "auto" === m &&
            ((p = -r + l.top - n),
            (f = r + o - (l.top + c + n)),
            (m = Math.max(p, f) === f ? "top" : "bottom")),
            this.picker.addClass("datepicker-orient-" + m),
            "top" === m
              ? (u += c)
              : (u -= n + parseInt(this.picker.css("padding-top"))),
            this.picker.css({ top: u, left: h, zIndex: a });
        }
      },
      _allow_update: !0,
      update: function () {
        if (this._allow_update) {
          var e = this.dates.copy(),
            i = [],
            n = !1;
          arguments.length
            ? (t.each(
                arguments,
                t.proxy(function (t, e) {
                  e instanceof Date && (e = this._local_to_utc(e)), i.push(e);
                }, this)
              ),
              (n = !0))
            : ((i =
                (i = this.isInput
                  ? this.element.val()
                  : this.element.data("date") ||
                    this.element.find("input").val()) && this.o.multidate
                  ? i.split(this.o.multidateSeparator)
                  : [i]),
              delete this.element.data().date),
            (i = t.map(
              i,
              t.proxy(function (t) {
                return f.parseDate(t, this.o.format, this.o.language);
              }, this)
            )),
            (i = t.grep(
              i,
              t.proxy(function (t) {
                return t < this.o.startDate || t > this.o.endDate || !t;
              }, this),
              !0
            )),
            this.dates.replace(i),
            this.dates.length
              ? (this.viewDate = new Date(this.dates.get(-1)))
              : this.viewDate < this.o.startDate
              ? (this.viewDate = new Date(this.o.startDate))
              : this.viewDate > this.o.endDate &&
                (this.viewDate = new Date(this.o.endDate)),
            n
              ? this.setValue()
              : i.length &&
                String(e) !== String(this.dates) &&
                this._trigger("changeDate"),
            !this.dates.length && e.length && this._trigger("clearDate"),
            this.fill();
        }
      },
      fillDow: function () {
        var t = this.o.weekStart,
          e = "<tr>";
        if (this.o.calendarWeeks) {
          var i = '<th class="cw">&nbsp;</th>';
          (e += i),
            this.picker
              .find(".datepicker-days thead tr:first-child")
              .prepend(i);
        }
        for (; t < this.o.weekStart + 7; )
          e +=
            '<th class="dow">' + p[this.o.language].daysMin[t++ % 7] + "</th>";
        (e += "</tr>"), this.picker.find(".datepicker-days thead").append(e);
      },
      fillMonths: function () {
        for (var t = "", e = 0; e < 12; )
          t +=
            '<span class="month">' +
            p[this.o.language].monthsShort[e++] +
            "</span>";
        this.picker.find(".datepicker-months td").html(t);
      },
      setRange: function (e) {
        e && e.length
          ? (this.range = t.map(e, function (t) {
              return t.valueOf();
            }))
          : delete this.range,
          this.fill();
      },
      getClassNames: function (e) {
        var i = [],
          n = this.viewDate.getUTCFullYear(),
          s = this.viewDate.getUTCMonth(),
          o = new Date();
        return (
          e.getUTCFullYear() < n ||
          (e.getUTCFullYear() === n && e.getUTCMonth() < s)
            ? i.push("old")
            : (e.getUTCFullYear() > n ||
                (e.getUTCFullYear() === n && e.getUTCMonth() > s)) &&
              i.push("new"),
          this.focusDate &&
            e.valueOf() === this.focusDate.valueOf() &&
            i.push("focused"),
          this.o.todayHighlight &&
            e.getUTCFullYear() === o.getFullYear() &&
            e.getUTCMonth() === o.getMonth() &&
            e.getUTCDate() === o.getDate() &&
            i.push("today"),
          -1 !== this.dates.contains(e) && i.push("active"),
          (e.valueOf() < this.o.startDate ||
            e.valueOf() > this.o.endDate ||
            -1 !== t.inArray(e.getUTCDay(), this.o.daysOfWeekDisabled)) &&
            i.push("disabled"),
          this.range &&
            (e > this.range[0] &&
              e < this.range[this.range.length - 1] &&
              i.push("range"),
            -1 !== t.inArray(e.valueOf(), this.range) && i.push("selected")),
          i
        );
      },
      fill: function () {
        var e,
          i = new Date(this.viewDate),
          s = i.getUTCFullYear(),
          o = i.getUTCMonth(),
          r =
            this.o.startDate !== -1 / 0
              ? this.o.startDate.getUTCFullYear()
              : -1 / 0,
          a =
            this.o.startDate !== -1 / 0
              ? this.o.startDate.getUTCMonth()
              : -1 / 0,
          l =
            this.o.endDate !== 1 / 0 ? this.o.endDate.getUTCFullYear() : 1 / 0,
          c = this.o.endDate !== 1 / 0 ? this.o.endDate.getUTCMonth() : 1 / 0,
          d = p[this.o.language].today || p.en.today || "",
          h = p[this.o.language].clear || p.en.clear || "";
        this.picker
          .find(".datepicker-days thead th.datepicker-switch")
          .text(p[this.o.language].months[o] + " " + s),
          this.picker
            .find("tfoot th.today")
            .text(d)
            .toggle(!1 !== this.o.todayBtn),
          this.picker
            .find("tfoot th.clear")
            .text(h)
            .toggle(!1 !== this.o.clearBtn),
          this.updateNavArrows(),
          this.fillMonths();
        var u = n(s, o - 1, 28),
          m = f.getDaysInMonth(u.getUTCFullYear(), u.getUTCMonth());
        u.setUTCDate(m),
          u.setUTCDate(m - ((u.getUTCDay() - this.o.weekStart + 7) % 7));
        var g = new Date(u);
        g.setUTCDate(g.getUTCDate() + 42), (g = g.valueOf());
        for (var v, y = []; u.valueOf() < g; ) {
          if (
            u.getUTCDay() === this.o.weekStart &&
            (y.push("<tr>"), this.o.calendarWeeks)
          ) {
            var _ = new Date(
                +u + ((this.o.weekStart - u.getUTCDay() - 7) % 7) * 864e5
              ),
              w = new Date(Number(_) + ((11 - _.getUTCDay()) % 7) * 864e5),
              b = new Date(
                Number((b = n(w.getUTCFullYear(), 0, 1))) +
                  ((11 - b.getUTCDay()) % 7) * 864e5
              ),
              C = (w - b) / 864e5 / 7 + 1;
            y.push('<td class="cw">' + C + "</td>");
          }
          if (
            ((v = this.getClassNames(u)).push("day"),
            this.o.beforeShowDay !== t.noop)
          ) {
            var k = this.o.beforeShowDay(this._utc_to_local(u));
            void 0 === k
              ? (k = {})
              : "boolean" == typeof k
              ? (k = { enabled: k })
              : "string" == typeof k && (k = { classes: k }),
              !1 === k.enabled && v.push("disabled"),
              k.classes && (v = v.concat(k.classes.split(/\s+/))),
              k.tooltip && (e = k.tooltip);
          }
          (v = t.unique(v)),
            y.push(
              '<td class="' +
                v.join(" ") +
                '"' +
                (e ? ' title="' + e + '"' : "") +
                ">" +
                u.getUTCDate() +
                "</td>"
            ),
            u.getUTCDay() === this.o.weekEnd && y.push("</tr>"),
            u.setUTCDate(u.getUTCDate() + 1);
        }
        this.picker.find(".datepicker-days tbody").empty().append(y.join(""));
        var D = this.picker
          .find(".datepicker-months")
          .find("th:eq(1)")
          .text(s)
          .end()
          .find("span")
          .removeClass("active");
        t.each(this.dates, function (t, e) {
          e.getUTCFullYear() === s && D.eq(e.getUTCMonth()).addClass("active");
        }),
          (s < r || s > l) && D.addClass("disabled"),
          s === r && D.slice(0, a).addClass("disabled"),
          s === l && D.slice(c + 1).addClass("disabled"),
          (y = ""),
          (s = 10 * parseInt(s / 10, 10));
        var T = this.picker
          .find(".datepicker-years")
          .find("th:eq(1)")
          .text(s + "-" + (s + 9))
          .end()
          .find("td");
        s -= 1;
        for (
          var x,
            S = t.map(this.dates, function (t) {
              return t.getUTCFullYear();
            }),
            E = -1;
          E < 11;
          E++
        )
          (x = ["year"]),
            -1 === E ? x.push("old") : 10 === E && x.push("new"),
            -1 !== t.inArray(s, S) && x.push("active"),
            (s < r || s > l) && x.push("disabled"),
            (y += '<span class="' + x.join(" ") + '">' + s + "</span>"),
            (s += 1);
        T.html(y);
      },
      updateNavArrows: function () {
        if (this._allow_update) {
          var t = new Date(this.viewDate),
            e = t.getUTCFullYear(),
            i = t.getUTCMonth();
          switch (this.viewMode) {
            case 0:
              this.o.startDate !== -1 / 0 &&
              e <= this.o.startDate.getUTCFullYear() &&
              i <= this.o.startDate.getUTCMonth()
                ? this.picker.find(".prev").css({ visibility: "hidden" })
                : this.picker.find(".prev").css({ visibility: "visible" }),
                this.o.endDate !== 1 / 0 &&
                e >= this.o.endDate.getUTCFullYear() &&
                i >= this.o.endDate.getUTCMonth()
                  ? this.picker.find(".next").css({ visibility: "hidden" })
                  : this.picker.find(".next").css({ visibility: "visible" });
              break;
            case 1:
            case 2:
              this.o.startDate !== -1 / 0 &&
              e <= this.o.startDate.getUTCFullYear()
                ? this.picker.find(".prev").css({ visibility: "hidden" })
                : this.picker.find(".prev").css({ visibility: "visible" }),
                this.o.endDate !== 1 / 0 && e >= this.o.endDate.getUTCFullYear()
                  ? this.picker.find(".next").css({ visibility: "hidden" })
                  : this.picker.find(".next").css({ visibility: "visible" });
          }
        }
      },
      click: function (e) {
        e.preventDefault();
        var i,
          s,
          o,
          r = t(e.target).closest("span, td, th");
        if (1 === r.length)
          switch (r[0].nodeName.toLowerCase()) {
            case "th":
              switch (r[0].className) {
                case "datepicker-switch":
                  this.showMode(1);
                  break;
                case "prev":
                case "next":
                  var a =
                    f.modes[this.viewMode].navStep *
                    ("prev" === r[0].className ? -1 : 1);
                  switch (this.viewMode) {
                    case 0:
                      (this.viewDate = this.moveMonth(this.viewDate, a)),
                        this._trigger("changeMonth", this.viewDate);
                      break;
                    case 1:
                    case 2:
                      (this.viewDate = this.moveYear(this.viewDate, a)),
                        1 === this.viewMode &&
                          this._trigger("changeYear", this.viewDate);
                  }
                  this.fill();
                  break;
                case "today":
                  var l = new Date();
                  (l = n(l.getFullYear(), l.getMonth(), l.getDate(), 0, 0, 0)),
                    this.showMode(-2);
                  var c = "linked" === this.o.todayBtn ? null : "view";
                  this._setDate(l, c);
                  break;
                case "clear":
                  var d;
                  this.isInput
                    ? (d = this.element)
                    : this.component && (d = this.element.find("input")),
                    d && d.val("").change(),
                    this.update(),
                    this._trigger("changeDate"),
                    this.o.autoclose && this.hide();
              }
              break;
            case "span":
              r.is(".disabled") ||
                (this.viewDate.setUTCDate(1),
                r.is(".month")
                  ? ((o = 1),
                    (s = r.parent().find("span").index(r)),
                    (i = this.viewDate.getUTCFullYear()),
                    this.viewDate.setUTCMonth(s),
                    this._trigger("changeMonth", this.viewDate),
                    1 === this.o.minViewMode && this._setDate(n(i, s, o)))
                  : ((o = 1),
                    (s = 0),
                    (i = parseInt(r.text(), 10) || 0),
                    this.viewDate.setUTCFullYear(i),
                    this._trigger("changeYear", this.viewDate),
                    2 === this.o.minViewMode && this._setDate(n(i, s, o))),
                this.showMode(-1),
                this.fill());
              break;
            case "td":
              r.is(".day") &&
                !r.is(".disabled") &&
                ((o = parseInt(r.text(), 10) || 1),
                (i = this.viewDate.getUTCFullYear()),
                (s = this.viewDate.getUTCMonth()),
                r.is(".old")
                  ? 0 === s
                    ? ((s = 11), (i -= 1))
                    : (s -= 1)
                  : r.is(".new") && (11 === s ? ((s = 0), (i += 1)) : (s += 1)),
                this._setDate(n(i, s, o)));
          }
        this.picker.is(":visible") &&
          this._focused_from &&
          t(this._focused_from).focus(),
          delete this._focused_from;
      },
      _toggle_multidate: function (t) {
        var e = this.dates.contains(t);
        if (
          (t
            ? -1 !== e
              ? this.dates.remove(e)
              : this.dates.push(t)
            : this.dates.clear(),
          "number" == typeof this.o.multidate)
        )
          for (; this.dates.length > this.o.multidate; ) this.dates.remove(0);
      },
      _setDate: function (t, e) {
        var i;
        (e && "date" !== e) || this._toggle_multidate(t && new Date(t)),
          (e && "view" !== e) || (this.viewDate = t && new Date(t)),
          this.fill(),
          this.setValue(),
          this._trigger("changeDate"),
          this.isInput
            ? (i = this.element)
            : this.component && (i = this.element.find("input")),
          i && i.change(),
          !this.o.autoclose || (e && "date" !== e) || this.hide();
      },
      moveMonth: function (t, e) {
        if (t) {
          if (!e) return t;
          var i,
            n,
            s = new Date(t.valueOf()),
            o = s.getUTCDate(),
            r = s.getUTCMonth(),
            a = Math.abs(e);
          if (((e = e > 0 ? 1 : -1), 1 === a))
            (n =
              -1 === e
                ? function () {
                    return s.getUTCMonth() === r;
                  }
                : function () {
                    return s.getUTCMonth() !== i;
                  }),
              (i = r + e),
              s.setUTCMonth(i),
              (i < 0 || i > 11) && (i = (i + 12) % 12);
          else {
            for (var l = 0; l < a; l++) s = this.moveMonth(s, e);
            (i = s.getUTCMonth()),
              s.setUTCDate(o),
              (n = function () {
                return i !== s.getUTCMonth();
              });
          }
          for (; n(); ) s.setUTCDate(--o), s.setUTCMonth(i);
          return s;
        }
      },
      moveYear: function (t, e) {
        return this.moveMonth(t, 12 * e);
      },
      dateWithinRange: function (t) {
        return t >= this.o.startDate && t <= this.o.endDate;
      },
      keydown: function (t) {
        if (this.picker.is(":not(:visible)")) 27 === t.keyCode && this.show();
        else {
          var e,
            i,
            n,
            o,
            r = !1,
            a = this.focusDate || this.viewDate;
          switch (t.keyCode) {
            case 27:
              this.focusDate
                ? ((this.focusDate = null),
                  (this.viewDate = this.dates.get(-1) || this.viewDate),
                  this.fill())
                : this.hide(),
                t.preventDefault();
              break;
            case 37:
            case 39:
              if (!this.o.keyboardNavigation) break;
              (e = 37 === t.keyCode ? -1 : 1),
                t.ctrlKey
                  ? ((i = this.moveYear(this.dates.get(-1) || s(), e)),
                    (n = this.moveYear(a, e)),
                    this._trigger("changeYear", this.viewDate))
                  : t.shiftKey
                  ? ((i = this.moveMonth(this.dates.get(-1) || s(), e)),
                    (n = this.moveMonth(a, e)),
                    this._trigger("changeMonth", this.viewDate))
                  : ((i = new Date(this.dates.get(-1) || s())).setUTCDate(
                      i.getUTCDate() + e
                    ),
                    (n = new Date(a)).setUTCDate(a.getUTCDate() + e)),
                this.dateWithinRange(i) &&
                  ((this.focusDate = this.viewDate = n),
                  this.setValue(),
                  this.fill(),
                  t.preventDefault());
              break;
            case 38:
            case 40:
              if (!this.o.keyboardNavigation) break;
              (e = 38 === t.keyCode ? -1 : 1),
                t.ctrlKey
                  ? ((i = this.moveYear(this.dates.get(-1) || s(), e)),
                    (n = this.moveYear(a, e)),
                    this._trigger("changeYear", this.viewDate))
                  : t.shiftKey
                  ? ((i = this.moveMonth(this.dates.get(-1) || s(), e)),
                    (n = this.moveMonth(a, e)),
                    this._trigger("changeMonth", this.viewDate))
                  : ((i = new Date(this.dates.get(-1) || s())).setUTCDate(
                      i.getUTCDate() + 7 * e
                    ),
                    (n = new Date(a)).setUTCDate(a.getUTCDate() + 7 * e)),
                this.dateWithinRange(i) &&
                  ((this.focusDate = this.viewDate = n),
                  this.setValue(),
                  this.fill(),
                  t.preventDefault());
              break;
            case 32:
              break;
            case 13:
              (a = this.focusDate || this.dates.get(-1) || this.viewDate),
                this._toggle_multidate(a),
                (r = !0),
                (this.focusDate = null),
                (this.viewDate = this.dates.get(-1) || this.viewDate),
                this.setValue(),
                this.fill(),
                this.picker.is(":visible") &&
                  (t.preventDefault(), this.o.autoclose && this.hide());
              break;
            case 9:
              (this.focusDate = null),
                (this.viewDate = this.dates.get(-1) || this.viewDate),
                this.fill(),
                this.hide();
          }
          r &&
            (this.dates.length
              ? this._trigger("changeDate")
              : this._trigger("clearDate"),
            this.isInput
              ? (o = this.element)
              : this.component && (o = this.element.find("input")),
            o && o.change());
        }
      },
      showMode: function (t) {
        t &&
          (this.viewMode = Math.max(
            this.o.minViewMode,
            Math.min(2, this.viewMode + t)
          )),
          this.picker
            .find(">div")
            .hide()
            .filter(".datepicker-" + f.modes[this.viewMode].clsName)
            .css("display", "block"),
          this.updateNavArrows();
      },
    };
    var c = function (e, i) {
      (this.element = t(e)),
        (this.inputs = t.map(i.inputs, function (t) {
          return t.jquery ? t[0] : t;
        })),
        delete i.inputs,
        t(this.inputs)
          .datepicker(i)
          .bind("changeDate", t.proxy(this.dateUpdated, this)),
        (this.pickers = t.map(this.inputs, function (e) {
          return t(e).data("datepicker");
        })),
        this.updateDates();
    };
    c.prototype = {
      updateDates: function () {
        (this.dates = t.map(this.pickers, function (t) {
          return t.getUTCDate();
        })),
          this.updateRanges();
      },
      updateRanges: function () {
        var e = t.map(this.dates, function (t) {
          return t.valueOf();
        });
        t.each(this.pickers, function (t, i) {
          i.setRange(e);
        });
      },
      dateUpdated: function (e) {
        if (!this.updating) {
          this.updating = !0;
          var i = t(e.target).data("datepicker").getUTCDate(),
            n = t.inArray(e.target, this.inputs),
            s = this.inputs.length;
          if (-1 !== n) {
            if (
              (t.each(this.pickers, function (t, e) {
                e.getUTCDate() || e.setUTCDate(i);
              }),
              i < this.dates[n])
            )
              for (; n >= 0 && i < this.dates[n]; )
                this.pickers[n--].setUTCDate(i);
            else if (i > this.dates[n])
              for (; n < s && i > this.dates[n]; )
                this.pickers[n++].setUTCDate(i);
            this.updateDates(), delete this.updating;
          }
        }
      },
      remove: function () {
        t.map(this.pickers, function (t) {
          t.remove();
        }),
          delete this.element.data().datepicker;
      },
    };
    var d = t.fn.datepicker;
    t.fn.datepicker = function (e) {
      var i,
        n = Array.apply(null, arguments);
      return (
        n.shift(),
        this.each(function () {
          var s = t(this),
            o = s.data("datepicker"),
            r = "object" == typeof e && e;
          if (!o) {
            var a = (function (e, i) {
                var n = t(e).data(),
                  s = {},
                  o = new RegExp("^" + i.toLowerCase() + "([A-Z])");
                function r(t, e) {
                  return e.toLowerCase();
                }
                for (var a in ((i = new RegExp("^" + i.toLowerCase())), n))
                  i.test(a) && (s[a.replace(o, r)] = n[a]);
                return s;
              })(this, "date"),
              d = (function (e) {
                var i = {};
                if (p[e] || ((e = e.split("-")[0]), p[e])) {
                  var n = p[e];
                  return (
                    t.each(u, function (t, e) {
                      e in n && (i[e] = n[e]);
                    }),
                    i
                  );
                }
              })(t.extend({}, h, a, r).language),
              f = t.extend({}, h, d, a, r);
            if (s.is(".input-daterange") || f.inputs) {
              var m = { inputs: f.inputs || s.find("input").toArray() };
              s.data("datepicker", (o = new c(this, t.extend(f, m))));
            } else s.data("datepicker", (o = new l(this, f)));
          }
          if (
            "string" == typeof e &&
            "function" == typeof o[e] &&
            void 0 !== (i = o[e].apply(o, n))
          )
            return !1;
        }),
        void 0 !== i ? i : this
      );
    };
    var h = (t.fn.datepicker.defaults = {
        autoclose: !1,
        beforeShowDay: t.noop,
        calendarWeeks: !1,
        clearBtn: !1,
        daysOfWeekDisabled: [],
        endDate: 1 / 0,
        forceParse: !0,
        format: "mm/dd/yyyy",
        keyboardNavigation: !0,
        language: "en",
        minViewMode: 0,
        multidate: !1,
        multidateSeparator: ",",
        orientation: "auto",
        rtl: !1,
        startDate: -1 / 0,
        startView: 0,
        todayBtn: !1,
        todayHighlight: !1,
        weekStart: 0,
      }),
      u = (t.fn.datepicker.locale_opts = ["format", "rtl", "weekStart"]);
    t.fn.datepicker.Constructor = l;
    var p = (t.fn.datepicker.dates = {
        en: {
          days: [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday",
          ],
          daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
          daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"],
          months: [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
          ],
          monthsShort: [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec",
          ],
          today: "Today",
          clear: "Clear",
        },
      }),
      f = {
        modes: [
          { clsName: "days", navFnc: "Month", navStep: 1 },
          { clsName: "months", navFnc: "FullYear", navStep: 1 },
          { clsName: "years", navFnc: "FullYear", navStep: 10 },
        ],
        isLeapYear: function (t) {
          return (t % 4 == 0 && t % 100 != 0) || t % 400 == 0;
        },
        getDaysInMonth: function (t, e) {
          return [
            31,
            f.isLeapYear(t) ? 29 : 28,
            31,
            30,
            31,
            30,
            31,
            31,
            30,
            31,
            30,
            31,
          ][e];
        },
        validParts: /dd?|DD?|mm?|MM?|yy(?:yy)?/g,
        nonpunctuation: /[^ -\/:-@\[\u3400-\u9fff-`{-~\t\n\r]+/g,
        parseFormat: function (t) {
          var e = t.replace(this.validParts, "\0").split("\0"),
            i = t.match(this.validParts);
          if (!e || !e.length || !i || 0 === i.length)
            throw new Error("Invalid date format.");
          return { separators: e, parts: i };
        },
        parseDate: function (e, i, s) {
          if (e) {
            if (e instanceof Date) return e;
            "string" == typeof i && (i = f.parseFormat(i));
            var o,
              r,
              a,
              c = /([\-+]\d+)([dmwy])/,
              d = e.match(/([\-+]\d+)([dmwy])/g);
            if (/^[\-+]\d+[dmwy]([\s,]+[\-+]\d+[dmwy])*$/.test(e)) {
              for (e = new Date(), a = 0; a < d.length; a++)
                switch (((o = c.exec(d[a])), (r = parseInt(o[1])), o[2])) {
                  case "d":
                    e.setUTCDate(e.getUTCDate() + r);
                    break;
                  case "m":
                    e = l.prototype.moveMonth.call(l.prototype, e, r);
                    break;
                  case "w":
                    e.setUTCDate(e.getUTCDate() + 7 * r);
                    break;
                  case "y":
                    e = l.prototype.moveYear.call(l.prototype, e, r);
                }
              return n(
                e.getUTCFullYear(),
                e.getUTCMonth(),
                e.getUTCDate(),
                0,
                0,
                0
              );
            }
            (d = (e && e.match(this.nonpunctuation)) || []), (e = new Date());
            var h,
              u,
              m = {},
              g = ["yyyy", "yy", "M", "MM", "m", "mm", "d", "dd"],
              v = {
                yyyy: function (t, e) {
                  return t.setUTCFullYear(e);
                },
                yy: function (t, e) {
                  return t.setUTCFullYear(2e3 + e);
                },
                m: function (t, e) {
                  if (isNaN(t)) return t;
                  for (e -= 1; e < 0; ) e += 12;
                  for (e %= 12, t.setUTCMonth(e); t.getUTCMonth() !== e; )
                    t.setUTCDate(t.getUTCDate() - 1);
                  return t;
                },
                d: function (t, e) {
                  return t.setUTCDate(e);
                },
              };
            (v.M = v.MM = v.mm = v.m),
              (v.dd = v.d),
              (e = n(e.getFullYear(), e.getMonth(), e.getDate(), 0, 0, 0));
            var y = i.parts.slice();
            if (
              (d.length !== y.length &&
                (y = t(y)
                  .filter(function (e, i) {
                    return -1 !== t.inArray(i, g);
                  })
                  .toArray()),
              d.length === y.length)
            ) {
              var _, w, b;
              for (a = 0, _ = y.length; a < _; a++) {
                if (((h = parseInt(d[a], 10)), (o = y[a]), isNaN(h)))
                  switch (o) {
                    case "MM":
                      (u = t(p[s].months).filter(C)),
                        (h = t.inArray(u[0], p[s].months) + 1);
                      break;
                    case "M":
                      (u = t(p[s].monthsShort).filter(C)),
                        (h = t.inArray(u[0], p[s].monthsShort) + 1);
                  }
                m[o] = h;
              }
              for (a = 0; a < g.length; a++)
                (b = g[a]) in m &&
                  !isNaN(m[b]) &&
                  ((w = new Date(e)), v[b](w, m[b]), isNaN(w) || (e = w));
            }
            return e;
          }
          function C() {
            var t = this.slice(0, d[a].length);
            return t === d[a].slice(0, t.length);
          }
        },
        formatDate: function (e, i, n) {
          if (!e) return "";
          "string" == typeof i && (i = f.parseFormat(i));
          var s = {
            d: e.getUTCDate(),
            D: p[n].daysShort[e.getUTCDay()],
            DD: p[n].days[e.getUTCDay()],
            m: e.getUTCMonth() + 1,
            M: p[n].monthsShort[e.getUTCMonth()],
            MM: p[n].months[e.getUTCMonth()],
            yy: e.getUTCFullYear().toString().substring(2),
            yyyy: e.getUTCFullYear(),
          };
          (s.dd = (s.d < 10 ? "0" : "") + s.d),
            (s.mm = (s.m < 10 ? "0" : "") + s.m),
            (e = []);
          for (
            var o = t.extend([], i.separators), r = 0, a = i.parts.length;
            r <= a;
            r++
          )
            o.length && e.push(o.shift()), e.push(s[i.parts[r]]);
          return e.join("");
        },
        headTemplate:
          '<thead><tr><th class="prev">&laquo;</th><th colspan="5" class="datepicker-switch"></th><th class="next">&raquo;</th></tr></thead>',
        contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>',
        footTemplate:
          '<tfoot><tr><th colspan="7" class="today"></th></tr><tr><th colspan="7" class="clear"></th></tr></tfoot>',
      };
    (f.template =
      '<div class="datepicker"><div class="datepicker-days"><table class=" table-condensed">' +
      f.headTemplate +
      "<tbody></tbody>" +
      f.footTemplate +
      '</table></div><div class="datepicker-months"><table class="table-condensed">' +
      f.headTemplate +
      f.contTemplate +
      f.footTemplate +
      '</table></div><div class="datepicker-years"><table class="table-condensed">' +
      f.headTemplate +
      f.contTemplate +
      f.footTemplate +
      "</table></div></div>"),
      (t.fn.datepicker.DPGlobal = f),
      (t.fn.datepicker.noConflict = function () {
        return (t.fn.datepicker = d), this;
      }),
      t(document).on(
        "focus.datepicker.data-api click.datepicker.data-api",
        '[data-provide="datepicker"]',
        function (e) {
          var i = t(this);
          i.data("datepicker") || (e.preventDefault(), i.datepicker("show"));
        }
      ),
      t(function () {
        t('[data-provide="datepicker-inline"]').datepicker();
      });
  })(window.jQuery),
  (function (t, e) {
    "object" == typeof exports && "undefined" != typeof module
      ? (module.exports = e())
      : "function" == typeof define && define.amd
      ? define(e)
      : (t.moment = e());
  })(this, function () {
    "use strict";
    function t() {
      return Xt.apply(null, arguments);
    }
    function e(t) {
      return (
        t instanceof Array ||
        "[object Array]" === Object.prototype.toString.call(t)
      );
    }
    function i(t) {
      return (
        null != t && "[object Object]" === Object.prototype.toString.call(t)
      );
    }
    function n(t) {
      return void 0 === t;
    }
    function s(t) {
      return (
        "number" == typeof t ||
        "[object Number]" === Object.prototype.toString.call(t)
      );
    }
    function o(t) {
      return (
        t instanceof Date ||
        "[object Date]" === Object.prototype.toString.call(t)
      );
    }
    function r(t, e) {
      var i,
        n = [];
      for (i = 0; i < t.length; ++i) n.push(e(t[i], i));
      return n;
    }
    function a(t, e) {
      return Object.prototype.hasOwnProperty.call(t, e);
    }
    function l(t, e) {
      for (var i in e) a(e, i) && (t[i] = e[i]);
      return (
        a(e, "toString") && (t.toString = e.toString),
        a(e, "valueOf") && (t.valueOf = e.valueOf),
        t
      );
    }
    function c(t, e, i, n) {
      return yt(t, e, i, n, !0).utc();
    }
    function d(t) {
      return (
        null == t._pf &&
          (t._pf = {
            empty: !1,
            unusedTokens: [],
            unusedInput: [],
            overflow: -2,
            charsLeftOver: 0,
            nullInput: !1,
            invalidMonth: null,
            invalidFormat: !1,
            userInvalidated: !1,
            iso: !1,
            parsedDateParts: [],
            meridiem: null,
            rfc2822: !1,
            weekdayMismatch: !1,
          }),
        t._pf
      );
    }
    function h(t) {
      if (null == t._isValid) {
        var e = d(t),
          i = Kt.call(e.parsedDateParts, function (t) {
            return null != t;
          }),
          n =
            !isNaN(t._d.getTime()) &&
            e.overflow < 0 &&
            !e.empty &&
            !e.invalidMonth &&
            !e.invalidWeekday &&
            !e.nullInput &&
            !e.invalidFormat &&
            !e.userInvalidated &&
            (!e.meridiem || (e.meridiem && i));
        if (
          (t._strict &&
            (n =
              n &&
              0 === e.charsLeftOver &&
              0 === e.unusedTokens.length &&
              void 0 === e.bigHour),
          null != Object.isFrozen && Object.isFrozen(t))
        )
          return n;
        t._isValid = n;
      }
      return t._isValid;
    }
    function u(t) {
      var e = c(NaN);
      return null != t ? l(d(e), t) : (d(e).userInvalidated = !0), e;
    }
    function p(t, e) {
      var i, s, o;
      if (
        (n(e._isAMomentObject) || (t._isAMomentObject = e._isAMomentObject),
        n(e._i) || (t._i = e._i),
        n(e._f) || (t._f = e._f),
        n(e._l) || (t._l = e._l),
        n(e._strict) || (t._strict = e._strict),
        n(e._tzm) || (t._tzm = e._tzm),
        n(e._isUTC) || (t._isUTC = e._isUTC),
        n(e._offset) || (t._offset = e._offset),
        n(e._pf) || (t._pf = d(e)),
        n(e._locale) || (t._locale = e._locale),
        Zt.length > 0)
      )
        for (i = 0; i < Zt.length; i++) n((o = e[(s = Zt[i])])) || (t[s] = o);
      return t;
    }
    function f(e) {
      p(this, e),
        (this._d = new Date(null != e._d ? e._d.getTime() : NaN)),
        this.isValid() || (this._d = new Date(NaN)),
        !1 === Jt && ((Jt = !0), t.updateOffset(this), (Jt = !1));
    }
    function m(t) {
      return t instanceof f || (null != t && null != t._isAMomentObject);
    }
    function g(t) {
      return t < 0 ? Math.ceil(t) || 0 : Math.floor(t);
    }
    function v(t) {
      var e = +t,
        i = 0;
      return 0 !== e && isFinite(e) && (i = g(e)), i;
    }
    function y(t, e, i) {
      var n,
        s = Math.min(t.length, e.length),
        o = Math.abs(t.length - e.length),
        r = 0;
      for (n = 0; n < s; n++)
        ((i && t[n] !== e[n]) || (!i && v(t[n]) !== v(e[n]))) && r++;
      return r + o;
    }
    function _(e) {
      !1 === t.suppressDeprecationWarnings &&
        "undefined" != typeof console &&
        console.warn &&
        console.warn("Deprecation warning: " + e);
    }
    function w(e, i) {
      var n = !0;
      return l(function () {
        if (
          (null != t.deprecationHandler && t.deprecationHandler(null, e), n)
        ) {
          for (var s, o = [], r = 0; r < arguments.length; r++) {
            if (((s = ""), "object" == typeof arguments[r])) {
              for (var a in ((s += "\n[" + r + "] "), arguments[0]))
                s += a + ": " + arguments[0][a] + ", ";
              s = s.slice(0, -2);
            } else s = arguments[r];
            o.push(s);
          }
          _(
            e +
              "\nArguments: " +
              Array.prototype.slice.call(o).join("") +
              "\n" +
              new Error().stack
          ),
            (n = !1);
        }
        return i.apply(this, arguments);
      }, i);
    }
    function b(e, i) {
      null != t.deprecationHandler && t.deprecationHandler(e, i),
        te[e] || (_(i), (te[e] = !0));
    }
    function C(t) {
      return (
        t instanceof Function ||
        "[object Function]" === Object.prototype.toString.call(t)
      );
    }
    function k(t, e) {
      var n,
        s = l({}, t);
      for (n in e)
        a(e, n) &&
          (i(t[n]) && i(e[n])
            ? ((s[n] = {}), l(s[n], t[n]), l(s[n], e[n]))
            : null != e[n]
            ? (s[n] = e[n])
            : delete s[n]);
      for (n in t) a(t, n) && !a(e, n) && i(t[n]) && (s[n] = l({}, s[n]));
      return s;
    }
    function D(t) {
      null != t && this.set(t);
    }
    function T(t, e) {
      var i = t.toLowerCase();
      ie[i] = ie[i + "s"] = ie[e] = t;
    }
    function x(t) {
      return "string" == typeof t ? ie[t] || ie[t.toLowerCase()] : void 0;
    }
    function S(t) {
      var e,
        i,
        n = {};
      for (i in t) a(t, i) && (e = x(i)) && (n[e] = t[i]);
      return n;
    }
    function E(t, e) {
      ne[t] = e;
    }
    function A(e, i) {
      return function (n) {
        return null != n
          ? ($(this, e, n), t.updateOffset(this, i), this)
          : M(this, e);
      };
    }
    function M(t, e) {
      return t.isValid() ? t._d["get" + (t._isUTC ? "UTC" : "") + e]() : NaN;
    }
    function $(t, e, i) {
      t.isValid() && t._d["set" + (t._isUTC ? "UTC" : "") + e](i);
    }
    function O(t, e, i) {
      var n = "" + Math.abs(t),
        s = e - n.length;
      return (
        (t >= 0 ? (i ? "+" : "") : "-") +
        Math.pow(10, Math.max(0, s)).toString().substr(1) +
        n
      );
    }
    function N(t, e, i, n) {
      var s = n;
      "string" == typeof n &&
        (s = function () {
          return this[n]();
        }),
        t && (ae[t] = s),
        e &&
          (ae[e[0]] = function () {
            return O(s.apply(this, arguments), e[1], e[2]);
          }),
        i &&
          (ae[i] = function () {
            return this.localeData().ordinal(s.apply(this, arguments), t);
          });
    }
    function I(t) {
      return t.match(/\[[\s\S]/)
        ? t.replace(/^\[|\]$/g, "")
        : t.replace(/\\/g, "");
    }
    function P(t, e) {
      return t.isValid()
        ? ((e = L(e, t.localeData())),
          (re[e] =
            re[e] ||
            (function (t) {
              var e,
                i,
                n = t.match(se);
              for (e = 0, i = n.length; e < i; e++)
                ae[n[e]] ? (n[e] = ae[n[e]]) : (n[e] = I(n[e]));
              return function (e) {
                var s,
                  o = "";
                for (s = 0; s < i; s++) o += C(n[s]) ? n[s].call(e, t) : n[s];
                return o;
              };
            })(e)),
          re[e](t))
        : t.localeData().invalidDate();
    }
    function L(t, e) {
      function i(t) {
        return e.longDateFormat(t) || t;
      }
      var n = 5;
      for (oe.lastIndex = 0; n >= 0 && oe.test(t); )
        (t = t.replace(oe, i)), (oe.lastIndex = 0), (n -= 1);
      return t;
    }
    function Y(t, e, i) {
      De[t] = C(e)
        ? e
        : function (t, n) {
            return t && i ? i : e;
          };
    }
    function H(t, e) {
      return a(De, t)
        ? De[t](e._strict, e._locale)
        : new RegExp(
            (function (t) {
              return j(
                t
                  .replace("\\", "")
                  .replace(
                    /\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g,
                    function (t, e, i, n, s) {
                      return e || i || n || s;
                    }
                  )
              );
            })(t)
          );
    }
    function j(t) {
      return t.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");
    }
    function W(t, e) {
      var i,
        n = e;
      for (
        "string" == typeof t && (t = [t]),
          s(e) &&
            (n = function (t, i) {
              i[e] = v(t);
            }),
          i = 0;
        i < t.length;
        i++
      )
        Te[t[i]] = n;
    }
    function R(t, e) {
      W(t, function (t, i, n, s) {
        (n._w = n._w || {}), e(t, n._w, n, s);
      });
    }
    function z(t, e, i) {
      null != e && a(Te, t) && Te[t](e, i._a, i, t);
    }
    function F(t, e) {
      return new Date(Date.UTC(t, e + 1, 0)).getUTCDate();
    }
    function U(t, e) {
      var i;
      if (!t.isValid()) return t;
      if ("string" == typeof e)
        if (/^\d+$/.test(e)) e = v(e);
        else if (!s((e = t.localeData().monthsParse(e)))) return t;
      return (
        (i = Math.min(t.date(), F(t.year(), e))),
        t._d["set" + (t._isUTC ? "UTC" : "") + "Month"](e, i),
        t
      );
    }
    function B(e) {
      return null != e
        ? (U(this, e), t.updateOffset(this, !0), this)
        : M(this, "Month");
    }
    function q() {
      function t(t, e) {
        return e.length - t.length;
      }
      var e,
        i,
        n = [],
        s = [],
        o = [];
      for (e = 0; e < 12; e++)
        (i = c([2e3, e])),
          n.push(this.monthsShort(i, "")),
          s.push(this.months(i, "")),
          o.push(this.months(i, "")),
          o.push(this.monthsShort(i, ""));
      for (n.sort(t), s.sort(t), o.sort(t), e = 0; e < 12; e++)
        (n[e] = j(n[e])), (s[e] = j(s[e]));
      for (e = 0; e < 24; e++) o[e] = j(o[e]);
      (this._monthsRegex = new RegExp("^(" + o.join("|") + ")", "i")),
        (this._monthsShortRegex = this._monthsRegex),
        (this._monthsStrictRegex = new RegExp("^(" + s.join("|") + ")", "i")),
        (this._monthsShortStrictRegex = new RegExp(
          "^(" + n.join("|") + ")",
          "i"
        ));
    }
    function V(t) {
      return G(t) ? 366 : 365;
    }
    function G(t) {
      return (t % 4 == 0 && t % 100 != 0) || t % 400 == 0;
    }
    function Q(t) {
      var e = new Date(Date.UTC.apply(null, arguments));
      return (
        t < 100 &&
          t >= 0 &&
          isFinite(e.getUTCFullYear()) &&
          e.setUTCFullYear(t),
        e
      );
    }
    function X(t, e, i) {
      var n = 7 + e - i;
      return (-(7 + Q(t, 0, n).getUTCDay() - e) % 7) + n - 1;
    }
    function K(t, e, i, n, s) {
      var o,
        r,
        a = 1 + 7 * (e - 1) + ((7 + i - n) % 7) + X(t, n, s);
      return (
        a <= 0
          ? (r = V((o = t - 1)) + a)
          : a > V(t)
          ? ((o = t + 1), (r = a - V(t)))
          : ((o = t), (r = a)),
        { year: o, dayOfYear: r }
      );
    }
    function Z(t, e, i) {
      var n,
        s,
        o = X(t.year(), e, i),
        r = Math.floor((t.dayOfYear() - o - 1) / 7) + 1;
      return (
        r < 1
          ? (n = r + J((s = t.year() - 1), e, i))
          : r > J(t.year(), e, i)
          ? ((n = r - J(t.year(), e, i)), (s = t.year() + 1))
          : ((s = t.year()), (n = r)),
        { week: n, year: s }
      );
    }
    function J(t, e, i) {
      var n = X(t, e, i),
        s = X(t + 1, e, i);
      return (V(t) - n + s) / 7;
    }
    function tt() {
      function t(t, e) {
        return e.length - t.length;
      }
      var e,
        i,
        n,
        s,
        o,
        r = [],
        a = [],
        l = [],
        d = [];
      for (e = 0; e < 7; e++)
        (i = c([2e3, 1]).day(e)),
          (n = this.weekdaysMin(i, "")),
          (s = this.weekdaysShort(i, "")),
          (o = this.weekdays(i, "")),
          r.push(n),
          a.push(s),
          l.push(o),
          d.push(n),
          d.push(s),
          d.push(o);
      for (r.sort(t), a.sort(t), l.sort(t), d.sort(t), e = 0; e < 7; e++)
        (a[e] = j(a[e])), (l[e] = j(l[e])), (d[e] = j(d[e]));
      (this._weekdaysRegex = new RegExp("^(" + d.join("|") + ")", "i")),
        (this._weekdaysShortRegex = this._weekdaysRegex),
        (this._weekdaysMinRegex = this._weekdaysRegex),
        (this._weekdaysStrictRegex = new RegExp("^(" + l.join("|") + ")", "i")),
        (this._weekdaysShortStrictRegex = new RegExp(
          "^(" + a.join("|") + ")",
          "i"
        )),
        (this._weekdaysMinStrictRegex = new RegExp(
          "^(" + r.join("|") + ")",
          "i"
        ));
    }
    function et() {
      return this.hours() % 12 || 12;
    }
    function it(t, e) {
      N(t, 0, 0, function () {
        return this.localeData().meridiem(this.hours(), this.minutes(), e);
      });
    }
    function nt(t, e) {
      return e._meridiemParse;
    }
    function st(t) {
      return t ? t.toLowerCase().replace("_", "-") : t;
    }
    function ot(t) {
      var e = null;
      if (!Ke[t] && "undefined" != typeof module && module && module.exports)
        try {
          (e = Ge._abbr), require("./locale/" + t), rt(e);
        } catch (t) {}
      return Ke[t];
    }
    function rt(t, e) {
      var i;
      return t && (i = n(e) ? lt(t) : at(t, e)) && (Ge = i), Ge._abbr;
    }
    function at(t, e) {
      if (null !== e) {
        var i = Xe;
        if (((e.abbr = t), null != Ke[t]))
          b(
            "defineLocaleOverride",
            "use moment.updateLocale(localeName, config) to change an existing locale. moment.defineLocale(localeName, config) should only be used for creating a new locale See http://momentjs.com/guides/#/warnings/define-locale/ for more info."
          ),
            (i = Ke[t]._config);
        else if (null != e.parentLocale) {
          if (null == Ke[e.parentLocale])
            return (
              Ze[e.parentLocale] || (Ze[e.parentLocale] = []),
              Ze[e.parentLocale].push({ name: t, config: e }),
              null
            );
          i = Ke[e.parentLocale]._config;
        }
        return (
          (Ke[t] = new D(k(i, e))),
          Ze[t] &&
            Ze[t].forEach(function (t) {
              at(t.name, t.config);
            }),
          rt(t),
          Ke[t]
        );
      }
      return delete Ke[t], null;
    }
    function lt(t) {
      var i;
      if ((t && t._locale && t._locale._abbr && (t = t._locale._abbr), !t))
        return Ge;
      if (!e(t)) {
        if ((i = ot(t))) return i;
        t = [t];
      }
      return (function (t) {
        for (var e, i, n, s, o = 0; o < t.length; ) {
          for (
            e = (s = st(t[o]).split("-")).length,
              i = (i = st(t[o + 1])) ? i.split("-") : null;
            e > 0;

          ) {
            if ((n = ot(s.slice(0, e).join("-")))) return n;
            if (i && i.length >= e && y(s, i, !0) >= e - 1) break;
            e--;
          }
          o++;
        }
        return null;
      })(t);
    }
    function ct(t) {
      var e,
        i = t._a;
      return (
        i &&
          -2 === d(t).overflow &&
          ((e =
            i[Se] < 0 || i[Se] > 11
              ? Se
              : i[Ee] < 1 || i[Ee] > F(i[xe], i[Se])
              ? Ee
              : i[Ae] < 0 ||
                i[Ae] > 24 ||
                (24 === i[Ae] && (0 !== i[Me] || 0 !== i[$e] || 0 !== i[Oe]))
              ? Ae
              : i[Me] < 0 || i[Me] > 59
              ? Me
              : i[$e] < 0 || i[$e] > 59
              ? $e
              : i[Oe] < 0 || i[Oe] > 999
              ? Oe
              : -1),
          d(t)._overflowDayOfYear && (e < xe || e > Ee) && (e = Ee),
          d(t)._overflowWeeks && -1 === e && (e = Ne),
          d(t)._overflowWeekday && -1 === e && (e = Ie),
          (d(t).overflow = e)),
        t
      );
    }
    function dt(t) {
      var e,
        i,
        n,
        s,
        o,
        r,
        a = t._i,
        l = Je.exec(a) || ti.exec(a);
      if (l) {
        for (d(t).iso = !0, e = 0, i = ii.length; e < i; e++)
          if (ii[e][1].exec(l[1])) {
            (s = ii[e][0]), (n = !1 !== ii[e][2]);
            break;
          }
        if (null == s) return void (t._isValid = !1);
        if (l[3]) {
          for (e = 0, i = ni.length; e < i; e++)
            if (ni[e][1].exec(l[3])) {
              o = (l[2] || " ") + ni[e][0];
              break;
            }
          if (null == o) return void (t._isValid = !1);
        }
        if (!n && null != o) return void (t._isValid = !1);
        if (l[4]) {
          if (!ei.exec(l[4])) return void (t._isValid = !1);
          r = "Z";
        }
        (t._f = s + (o || "") + (r || "")), mt(t);
      } else t._isValid = !1;
    }
    function ht(t) {
      var e,
        i,
        n,
        s,
        o,
        r,
        a,
        l = {
          " GMT": " +0000",
          " EDT": " -0400",
          " EST": " -0500",
          " CDT": " -0500",
          " CST": " -0600",
          " MDT": " -0600",
          " MST": " -0700",
          " PDT": " -0700",
          " PST": " -0800",
        };
      if (
        ((e = t._i
          .replace(/\([^\)]*\)|[\n\t]/g, " ")
          .replace(/(\s\s+)/g, " ")
          .replace(/^\s|\s$/g, "")),
        (i = oi.exec(e)))
      ) {
        if (
          ((n = i[1] ? "ddd" + (5 === i[1].length ? ", " : " ") : ""),
          (s = "D MMM " + (i[2].length > 10 ? "YYYY " : "YY ")),
          (o = "HH:mm" + (i[4] ? ":ss" : "")),
          i[1])
        ) {
          var c = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"][
            new Date(i[2]).getDay()
          ];
          if (i[1].substr(0, 3) !== c)
            return (d(t).weekdayMismatch = !0), void (t._isValid = !1);
        }
        switch (i[5].length) {
          case 2:
            r =
              0 === a
                ? " +0000"
                : ((a =
                    "YXWVUTSRQPONZABCDEFGHIKLM".indexOf(i[5][1].toUpperCase()) -
                    12) < 0
                    ? " -"
                    : " +") +
                  ("" + a).replace(/^-?/, "0").match(/..$/)[0] +
                  "00";
            break;
          case 4:
            r = l[i[5]];
            break;
          default:
            r = l[" GMT"];
        }
        (i[5] = r),
          (t._i = i.splice(1).join("")),
          (t._f = n + s + o + " ZZ"),
          mt(t),
          (d(t).rfc2822 = !0);
      } else t._isValid = !1;
    }
    function ut(t, e, i) {
      return null != t ? t : null != e ? e : i;
    }
    function pt(e) {
      var i = new Date(t.now());
      return e._useUTC
        ? [i.getUTCFullYear(), i.getUTCMonth(), i.getUTCDate()]
        : [i.getFullYear(), i.getMonth(), i.getDate()];
    }
    function ft(t) {
      var e,
        i,
        n,
        s,
        o = [];
      if (!t._d) {
        for (
          n = pt(t),
            t._w &&
              null == t._a[Ee] &&
              null == t._a[Se] &&
              (function (t) {
                var e, i, n, s, o, r, a, l;
                if (null != (e = t._w).GG || null != e.W || null != e.E)
                  (o = 1),
                    (r = 4),
                    (i = ut(e.GG, t._a[xe], Z(_t(), 1, 4).year)),
                    (n = ut(e.W, 1)),
                    ((s = ut(e.E, 1)) < 1 || s > 7) && (l = !0);
                else {
                  (o = t._locale._week.dow), (r = t._locale._week.doy);
                  var c = Z(_t(), o, r);
                  (i = ut(e.gg, t._a[xe], c.year)),
                    (n = ut(e.w, c.week)),
                    null != e.d
                      ? ((s = e.d) < 0 || s > 6) && (l = !0)
                      : null != e.e
                      ? ((s = e.e + o), (e.e < 0 || e.e > 6) && (l = !0))
                      : (s = o);
                }
                n < 1 || n > J(i, o, r)
                  ? (d(t)._overflowWeeks = !0)
                  : null != l
                  ? (d(t)._overflowWeekday = !0)
                  : ((a = K(i, n, s, o, r)),
                    (t._a[xe] = a.year),
                    (t._dayOfYear = a.dayOfYear));
              })(t),
            null != t._dayOfYear &&
              ((s = ut(t._a[xe], n[xe])),
              (t._dayOfYear > V(s) || 0 === t._dayOfYear) &&
                (d(t)._overflowDayOfYear = !0),
              (i = Q(s, 0, t._dayOfYear)),
              (t._a[Se] = i.getUTCMonth()),
              (t._a[Ee] = i.getUTCDate())),
            e = 0;
          e < 3 && null == t._a[e];
          ++e
        )
          t._a[e] = o[e] = n[e];
        for (; e < 7; e++)
          t._a[e] = o[e] = null == t._a[e] ? (2 === e ? 1 : 0) : t._a[e];
        24 === t._a[Ae] &&
          0 === t._a[Me] &&
          0 === t._a[$e] &&
          0 === t._a[Oe] &&
          ((t._nextDay = !0), (t._a[Ae] = 0)),
          (t._d = (
            t._useUTC
              ? Q
              : function (t, e, i, n, s, o, r) {
                  var a = new Date(t, e, i, n, s, o, r);
                  return (
                    t < 100 &&
                      t >= 0 &&
                      isFinite(a.getFullYear()) &&
                      a.setFullYear(t),
                    a
                  );
                }
          ).apply(null, o)),
          null != t._tzm && t._d.setUTCMinutes(t._d.getUTCMinutes() - t._tzm),
          t._nextDay && (t._a[Ae] = 24);
      }
    }
    function mt(e) {
      if (e._f !== t.ISO_8601)
        if (e._f !== t.RFC_2822) {
          (e._a = []), (d(e).empty = !0);
          var i,
            n,
            s,
            o,
            r,
            a = "" + e._i,
            l = a.length,
            c = 0;
          for (s = L(e._f, e._locale).match(se) || [], i = 0; i < s.length; i++)
            (o = s[i]),
              (n = (a.match(H(o, e)) || [])[0]) &&
                ((r = a.substr(0, a.indexOf(n))).length > 0 &&
                  d(e).unusedInput.push(r),
                (a = a.slice(a.indexOf(n) + n.length)),
                (c += n.length)),
              ae[o]
                ? (n ? (d(e).empty = !1) : d(e).unusedTokens.push(o),
                  z(o, n, e))
                : e._strict && !n && d(e).unusedTokens.push(o);
          (d(e).charsLeftOver = l - c),
            a.length > 0 && d(e).unusedInput.push(a),
            e._a[Ae] <= 12 &&
              !0 === d(e).bigHour &&
              e._a[Ae] > 0 &&
              (d(e).bigHour = void 0),
            (d(e).parsedDateParts = e._a.slice(0)),
            (d(e).meridiem = e._meridiem),
            (e._a[Ae] = (function (t, e, i) {
              var n;
              return null == i
                ? e
                : null != t.meridiemHour
                ? t.meridiemHour(e, i)
                : null != t.isPM
                ? ((n = t.isPM(i)) && e < 12 && (e += 12),
                  n || 12 !== e || (e = 0),
                  e)
                : e;
            })(e._locale, e._a[Ae], e._meridiem)),
            ft(e),
            ct(e);
        } else ht(e);
      else dt(e);
    }
    function gt(t) {
      var i = t._i,
        n = t._f;
      return (
        (t._locale = t._locale || lt(t._l)),
        null === i || (void 0 === n && "" === i)
          ? u({ nullInput: !0 })
          : ("string" == typeof i && (t._i = i = t._locale.preparse(i)),
            m(i)
              ? new f(ct(i))
              : (o(i)
                  ? (t._d = i)
                  : e(n)
                  ? (function (t) {
                      var e, i, n, s, o;
                      if (0 === t._f.length)
                        return (
                          (d(t).invalidFormat = !0), void (t._d = new Date(NaN))
                        );
                      for (s = 0; s < t._f.length; s++)
                        (o = 0),
                          (e = p({}, t)),
                          null != t._useUTC && (e._useUTC = t._useUTC),
                          (e._f = t._f[s]),
                          mt(e),
                          h(e) &&
                            ((o += d(e).charsLeftOver),
                            (o += 10 * d(e).unusedTokens.length),
                            (d(e).score = o),
                            (null == n || o < n) && ((n = o), (i = e)));
                      l(t, i || e);
                    })(t)
                  : n
                  ? mt(t)
                  : vt(t),
                h(t) || (t._d = null),
                t))
      );
    }
    function vt(a) {
      var l = a._i;
      n(l)
        ? (a._d = new Date(t.now()))
        : o(l)
        ? (a._d = new Date(l.valueOf()))
        : "string" == typeof l
        ? (function (e) {
            var i = si.exec(e._i);
            null !== i
              ? (e._d = new Date(+i[1]))
              : (dt(e),
                !1 === e._isValid &&
                  (delete e._isValid,
                  ht(e),
                  !1 === e._isValid &&
                    (delete e._isValid, t.createFromInputFallback(e))));
          })(a)
        : e(l)
        ? ((a._a = r(l.slice(0), function (t) {
            return parseInt(t, 10);
          })),
          ft(a))
        : i(l)
        ? (function (t) {
            if (!t._d) {
              var e = S(t._i);
              (t._a = r(
                [
                  e.year,
                  e.month,
                  e.day || e.date,
                  e.hour,
                  e.minute,
                  e.second,
                  e.millisecond,
                ],
                function (t) {
                  return t && parseInt(t, 10);
                }
              )),
                ft(t);
            }
          })(a)
        : s(l)
        ? (a._d = new Date(l))
        : t.createFromInputFallback(a);
    }
    function yt(t, n, s, o, r) {
      var a = {};
      return (
        (!0 !== s && !1 !== s) || ((o = s), (s = void 0)),
        ((i(t) &&
          (function (t) {
            var e;
            for (e in t) return !1;
            return !0;
          })(t)) ||
          (e(t) && 0 === t.length)) &&
          (t = void 0),
        (a._isAMomentObject = !0),
        (a._useUTC = a._isUTC = r),
        (a._l = s),
        (a._i = t),
        (a._f = n),
        (a._strict = o),
        (function (t) {
          var e = new f(ct(gt(a)));
          return e._nextDay && (e.add(1, "d"), (e._nextDay = void 0)), e;
        })()
      );
    }
    function _t(t, e, i, n) {
      return yt(t, e, i, n, !1);
    }
    function wt(t, i) {
      var n, s;
      if ((1 === i.length && e(i[0]) && (i = i[0]), !i.length)) return _t();
      for (n = i[0], s = 1; s < i.length; ++s)
        (i[s].isValid() && !i[s][t](n)) || (n = i[s]);
      return n;
    }
    function bt(t) {
      var e = S(t),
        i = e.year || 0,
        n = e.quarter || 0,
        s = e.month || 0,
        o = e.week || 0,
        r = e.day || 0,
        a = e.hour || 0,
        l = e.minute || 0,
        c = e.second || 0,
        d = e.millisecond || 0;
      (this._isValid = (function (t) {
        for (var e in t)
          if (-1 === li.indexOf(e) || (null != t[e] && isNaN(t[e]))) return !1;
        for (var i = !1, n = 0; n < li.length; ++n)
          if (t[li[n]]) {
            if (i) return !1;
            parseFloat(t[li[n]]) !== v(t[li[n]]) && (i = !0);
          }
        return !0;
      })(e)),
        (this._milliseconds = +d + 1e3 * c + 6e4 * l + 1e3 * a * 60 * 60),
        (this._days = +r + 7 * o),
        (this._months = +s + 3 * n + 12 * i),
        (this._data = {}),
        (this._locale = lt()),
        this._bubble();
    }
    function Ct(t) {
      return t instanceof bt;
    }
    function kt(t) {
      return t < 0 ? -1 * Math.round(-1 * t) : Math.round(t);
    }
    function Dt(t, e) {
      N(t, 0, 0, function () {
        var t = this.utcOffset(),
          i = "+";
        return (
          t < 0 && ((t = -t), (i = "-")),
          i + O(~~(t / 60), 2) + e + O(~~t % 60, 2)
        );
      });
    }
    function Tt(t, e) {
      var i = (e || "").match(t);
      if (null === i) return null;
      var n = ((i[i.length - 1] || []) + "").match(ci) || ["-", 0, 0],
        s = 60 * n[1] + v(n[2]);
      return 0 === s ? 0 : "+" === n[0] ? s : -s;
    }
    function xt(e, i) {
      var n, s;
      return i._isUTC
        ? ((n = i.clone()),
          (s = (m(e) || o(e) ? e.valueOf() : _t(e).valueOf()) - n.valueOf()),
          n._d.setTime(n._d.valueOf() + s),
          t.updateOffset(n, !1),
          n)
        : _t(e).local();
    }
    function St(t) {
      return 15 * -Math.round(t._d.getTimezoneOffset() / 15);
    }
    function Et() {
      return !!this.isValid() && this._isUTC && 0 === this._offset;
    }
    function At(t, e) {
      var i,
        n,
        o,
        r = t,
        l = null;
      return (
        Ct(t)
          ? (r = { ms: t._milliseconds, d: t._days, M: t._months })
          : s(t)
          ? ((r = {}), e ? (r[e] = t) : (r.milliseconds = t))
          : (l = di.exec(t))
          ? ((i = "-" === l[1] ? -1 : 1),
            (r = {
              y: 0,
              d: v(l[Ee]) * i,
              h: v(l[Ae]) * i,
              m: v(l[Me]) * i,
              s: v(l[$e]) * i,
              ms: v(kt(1e3 * l[Oe])) * i,
            }))
          : (l = hi.exec(t))
          ? ((i = "-" === l[1] ? -1 : 1),
            (r = {
              y: Mt(l[2], i),
              M: Mt(l[3], i),
              w: Mt(l[4], i),
              d: Mt(l[5], i),
              h: Mt(l[6], i),
              m: Mt(l[7], i),
              s: Mt(l[8], i),
            }))
          : null == r
          ? (r = {})
          : "object" == typeof r &&
            ("from" in r || "to" in r) &&
            ((o = (function (t, e) {
              var i;
              return t.isValid() && e.isValid()
                ? ((e = xt(e, t)),
                  t.isBefore(e)
                    ? (i = $t(t, e))
                    : (((i = $t(e, t)).milliseconds = -i.milliseconds),
                      (i.months = -i.months)),
                  i)
                : { milliseconds: 0, months: 0 };
            })(_t(r.from), _t(r.to))),
            ((r = {}).ms = o.milliseconds),
            (r.M = o.months)),
        (n = new bt(r)),
        Ct(t) && a(t, "_locale") && (n._locale = t._locale),
        n
      );
    }
    function Mt(t, e) {
      var i = t && parseFloat(t.replace(",", "."));
      return (isNaN(i) ? 0 : i) * e;
    }
    function $t(t, e) {
      var i = { milliseconds: 0, months: 0 };
      return (
        (i.months = e.month() - t.month() + 12 * (e.year() - t.year())),
        t.clone().add(i.months, "M").isAfter(e) && --i.months,
        (i.milliseconds = +e - +t.clone().add(i.months, "M")),
        i
      );
    }
    function Ot(t, e) {
      return function (i, n) {
        var s;
        return (
          null === n ||
            isNaN(+n) ||
            (b(
              e,
              "moment()." +
                e +
                "(period, number) is deprecated. Please use moment()." +
                e +
                "(number, period). See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info."
            ),
            (s = i),
            (i = n),
            (n = s)),
          Nt(this, At((i = "string" == typeof i ? +i : i), n), t),
          this
        );
      };
    }
    function Nt(e, i, n, s) {
      var o = i._milliseconds,
        r = kt(i._days),
        a = kt(i._months);
      e.isValid() &&
        ((s = null == s || s),
        o && e._d.setTime(e._d.valueOf() + o * n),
        r && $(e, "Date", M(e, "Date") + r * n),
        a && U(e, M(e, "Month") + a * n),
        s && t.updateOffset(e, r || a));
    }
    function It(t) {
      var e;
      return void 0 === t
        ? this._locale._abbr
        : (null != (e = lt(t)) && (this._locale = e), this);
    }
    function Pt() {
      return this._locale;
    }
    function Lt(t, e) {
      N(0, [t, t.length], 0, e);
    }
    function Yt(t, e, i, n, s) {
      var o;
      return null == t
        ? Z(this, n, s).year
        : (e > (o = J(t, n, s)) && (e = o),
          function (t, e, i, n, s) {
            var o = K(t, e, i, n, s),
              r = Q(o.year, 0, o.dayOfYear);
            return (
              this.year(r.getUTCFullYear()),
              this.month(r.getUTCMonth()),
              this.date(r.getUTCDate()),
              this
            );
          }.call(this, t, e, i, n, s));
    }
    function Ht(t, e) {
      e[Oe] = v(1e3 * ("0." + t));
    }
    function jt(t) {
      return t;
    }
    function Wt(t, e, i, n) {
      var s = lt(),
        o = c().set(n, e);
      return s[i](o, t);
    }
    function Rt(t, e, i) {
      if ((s(t) && ((e = t), (t = void 0)), (t = t || ""), null != e))
        return Wt(t, e, i, "month");
      var n,
        o = [];
      for (n = 0; n < 12; n++) o[n] = Wt(t, n, i, "month");
      return o;
    }
    function zt(t, e, i, n) {
      "boolean" == typeof t
        ? (s(e) && ((i = e), (e = void 0)), (e = e || ""))
        : ((i = e = t),
          (t = !1),
          s(e) && ((i = e), (e = void 0)),
          (e = e || ""));
      var o = lt(),
        r = t ? o._week.dow : 0;
      if (null != i) return Wt(e, (i + r) % 7, n, "day");
      var a,
        l = [];
      for (a = 0; a < 7; a++) l[a] = Wt(e, (a + r) % 7, n, "day");
      return l;
    }
    function Ft(t, e, i, n) {
      var s = At(e, i);
      return (
        (t._milliseconds += n * s._milliseconds),
        (t._days += n * s._days),
        (t._months += n * s._months),
        t._bubble()
      );
    }
    function Ut(t) {
      return t < 0 ? Math.floor(t) : Math.ceil(t);
    }
    function Bt(t) {
      return (4800 * t) / 146097;
    }
    function qt(t) {
      return (146097 * t) / 4800;
    }
    function Vt(t) {
      return function () {
        return this.as(t);
      };
    }
    function Gt(t) {
      return function () {
        return this.isValid() ? this._data[t] : NaN;
      };
    }
    function Qt() {
      if (!this.isValid()) return this.localeData().invalidDate();
      var t,
        e,
        i = Wi(this._milliseconds) / 1e3,
        n = Wi(this._days),
        s = Wi(this._months);
      (t = g(i / 60)), (e = g(t / 60)), (i %= 60), (t %= 60);
      var o = g(s / 12),
        r = (s %= 12),
        a = n,
        l = e,
        c = t,
        d = i,
        h = this.asSeconds();
      return h
        ? (h < 0 ? "-" : "") +
            "P" +
            (o ? o + "Y" : "") +
            (r ? r + "M" : "") +
            (a ? a + "D" : "") +
            (l || c || d ? "T" : "") +
            (l ? l + "H" : "") +
            (c ? c + "M" : "") +
            (d ? d + "S" : "")
        : "P0D";
    }
    var Xt,
      Kt = Array.prototype.some
        ? Array.prototype.some
        : function (t) {
            for (var e = Object(this), i = e.length >>> 0, n = 0; n < i; n++)
              if (n in e && t.call(this, e[n], n, e)) return !0;
            return !1;
          },
      Zt = (t.momentProperties = []),
      Jt = !1,
      te = {};
    (t.suppressDeprecationWarnings = !1), (t.deprecationHandler = null);
    var ee = Object.keys
        ? Object.keys
        : function (t) {
            var e,
              i = [];
            for (e in t) a(t, e) && i.push(e);
            return i;
          },
      ie = {},
      ne = {},
      se =
        /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g,
      oe = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g,
      re = {},
      ae = {},
      le = /\d/,
      ce = /\d\d/,
      de = /\d{3}/,
      he = /\d{4}/,
      ue = /[+-]?\d{6}/,
      pe = /\d\d?/,
      fe = /\d\d\d\d?/,
      me = /\d\d\d\d\d\d?/,
      ge = /\d{1,3}/,
      ve = /\d{1,4}/,
      ye = /[+-]?\d{1,6}/,
      _e = /\d+/,
      we = /[+-]?\d+/,
      be = /Z|[+-]\d\d:?\d\d/gi,
      Ce = /Z|[+-]\d\d(?::?\d\d)?/gi,
      ke =
        /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i,
      De = {},
      Te = {},
      xe = 0,
      Se = 1,
      Ee = 2,
      Ae = 3,
      Me = 4,
      $e = 5,
      Oe = 6,
      Ne = 7,
      Ie = 8,
      Pe = Array.prototype.indexOf
        ? Array.prototype.indexOf
        : function (t) {
            var e;
            for (e = 0; e < this.length; ++e) if (this[e] === t) return e;
            return -1;
          };
    N("M", ["MM", 2], "Mo", function () {
      return this.month() + 1;
    }),
      N("MMM", 0, 0, function (t) {
        return this.localeData().monthsShort(this, t);
      }),
      N("MMMM", 0, 0, function (t) {
        return this.localeData().months(this, t);
      }),
      T("month", "M"),
      E("month", 8),
      Y("M", pe),
      Y("MM", pe, ce),
      Y("MMM", function (t, e) {
        return e.monthsShortRegex(t);
      }),
      Y("MMMM", function (t, e) {
        return e.monthsRegex(t);
      }),
      W(["M", "MM"], function (t, e) {
        e[Se] = v(t) - 1;
      }),
      W(["MMM", "MMMM"], function (t, e, i, n) {
        var s = i._locale.monthsParse(t, n, i._strict);
        null != s ? (e[Se] = s) : (d(i).invalidMonth = t);
      });
    var Le = /D[oD]?(\[[^\[\]]*\]|\s)+MMMM?/,
      Ye =
        "January_February_March_April_May_June_July_August_September_October_November_December".split(
          "_"
        ),
      He = "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
      je = ke,
      We = ke;
    N("Y", 0, 0, function () {
      var t = this.year();
      return t <= 9999 ? "" + t : "+" + t;
    }),
      N(0, ["YY", 2], 0, function () {
        return this.year() % 100;
      }),
      N(0, ["YYYY", 4], 0, "year"),
      N(0, ["YYYYY", 5], 0, "year"),
      N(0, ["YYYYYY", 6, !0], 0, "year"),
      T("year", "y"),
      E("year", 1),
      Y("Y", we),
      Y("YY", pe, ce),
      Y("YYYY", ve, he),
      Y("YYYYY", ye, ue),
      Y("YYYYYY", ye, ue),
      W(["YYYYY", "YYYYYY"], xe),
      W("YYYY", function (e, i) {
        i[xe] = 2 === e.length ? t.parseTwoDigitYear(e) : v(e);
      }),
      W("YY", function (e, i) {
        i[xe] = t.parseTwoDigitYear(e);
      }),
      W("Y", function (t, e) {
        e[xe] = parseInt(t, 10);
      }),
      (t.parseTwoDigitYear = function (t) {
        return v(t) + (v(t) > 68 ? 1900 : 2e3);
      });
    var Re = A("FullYear", !0);
    N("w", ["ww", 2], "wo", "week"),
      N("W", ["WW", 2], "Wo", "isoWeek"),
      T("week", "w"),
      T("isoWeek", "W"),
      E("week", 5),
      E("isoWeek", 5),
      Y("w", pe),
      Y("ww", pe, ce),
      Y("W", pe),
      Y("WW", pe, ce),
      R(["w", "ww", "W", "WW"], function (t, e, i, n) {
        e[n.substr(0, 1)] = v(t);
      }),
      N("d", 0, "do", "day"),
      N("dd", 0, 0, function (t) {
        return this.localeData().weekdaysMin(this, t);
      }),
      N("ddd", 0, 0, function (t) {
        return this.localeData().weekdaysShort(this, t);
      }),
      N("dddd", 0, 0, function (t) {
        return this.localeData().weekdays(this, t);
      }),
      N("e", 0, 0, "weekday"),
      N("E", 0, 0, "isoWeekday"),
      T("day", "d"),
      T("weekday", "e"),
      T("isoWeekday", "E"),
      E("day", 11),
      E("weekday", 11),
      E("isoWeekday", 11),
      Y("d", pe),
      Y("e", pe),
      Y("E", pe),
      Y("dd", function (t, e) {
        return e.weekdaysMinRegex(t);
      }),
      Y("ddd", function (t, e) {
        return e.weekdaysShortRegex(t);
      }),
      Y("dddd", function (t, e) {
        return e.weekdaysRegex(t);
      }),
      R(["dd", "ddd", "dddd"], function (t, e, i, n) {
        var s = i._locale.weekdaysParse(t, n, i._strict);
        null != s ? (e.d = s) : (d(i).invalidWeekday = t);
      }),
      R(["d", "e", "E"], function (t, e, i, n) {
        e[n] = v(t);
      });
    var ze = "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split(
        "_"
      ),
      Fe = "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
      Ue = "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
      Be = ke,
      qe = ke,
      Ve = ke;
    N("H", ["HH", 2], 0, "hour"),
      N("h", ["hh", 2], 0, et),
      N("k", ["kk", 2], 0, function () {
        return this.hours() || 24;
      }),
      N("hmm", 0, 0, function () {
        return "" + et.apply(this) + O(this.minutes(), 2);
      }),
      N("hmmss", 0, 0, function () {
        return (
          "" + et.apply(this) + O(this.minutes(), 2) + O(this.seconds(), 2)
        );
      }),
      N("Hmm", 0, 0, function () {
        return "" + this.hours() + O(this.minutes(), 2);
      }),
      N("Hmmss", 0, 0, function () {
        return "" + this.hours() + O(this.minutes(), 2) + O(this.seconds(), 2);
      }),
      it("a", !0),
      it("A", !1),
      T("hour", "h"),
      E("hour", 13),
      Y("a", nt),
      Y("A", nt),
      Y("H", pe),
      Y("h", pe),
      Y("k", pe),
      Y("HH", pe, ce),
      Y("hh", pe, ce),
      Y("kk", pe, ce),
      Y("hmm", fe),
      Y("hmmss", me),
      Y("Hmm", fe),
      Y("Hmmss", me),
      W(["H", "HH"], Ae),
      W(["k", "kk"], function (t, e, i) {
        var n = v(t);
        e[Ae] = 24 === n ? 0 : n;
      }),
      W(["a", "A"], function (t, e, i) {
        (i._isPm = i._locale.isPM(t)), (i._meridiem = t);
      }),
      W(["h", "hh"], function (t, e, i) {
        (e[Ae] = v(t)), (d(i).bigHour = !0);
      }),
      W("hmm", function (t, e, i) {
        var n = t.length - 2;
        (e[Ae] = v(t.substr(0, n))),
          (e[Me] = v(t.substr(n))),
          (d(i).bigHour = !0);
      }),
      W("hmmss", function (t, e, i) {
        var n = t.length - 4,
          s = t.length - 2;
        (e[Ae] = v(t.substr(0, n))),
          (e[Me] = v(t.substr(n, 2))),
          (e[$e] = v(t.substr(s))),
          (d(i).bigHour = !0);
      }),
      W("Hmm", function (t, e, i) {
        var n = t.length - 2;
        (e[Ae] = v(t.substr(0, n))), (e[Me] = v(t.substr(n)));
      }),
      W("Hmmss", function (t, e, i) {
        var n = t.length - 4,
          s = t.length - 2;
        (e[Ae] = v(t.substr(0, n))),
          (e[Me] = v(t.substr(n, 2))),
          (e[$e] = v(t.substr(s)));
      });
    var Ge,
      Qe = A("Hours", !0),
      Xe = {
        calendar: {
          sameDay: "[Today at] LT",
          nextDay: "[Tomorrow at] LT",
          nextWeek: "dddd [at] LT",
          lastDay: "[Yesterday at] LT",
          lastWeek: "[Last] dddd [at] LT",
          sameElse: "L",
        },
        longDateFormat: {
          LTS: "h:mm:ss A",
          LT: "h:mm A",
          L: "MM/DD/YYYY",
          LL: "MMMM D, YYYY",
          LLL: "MMMM D, YYYY h:mm A",
          LLLL: "dddd, MMMM D, YYYY h:mm A",
        },
        invalidDate: "Invalid date",
        ordinal: "%d",
        dayOfMonthOrdinalParse: /\d{1,2}/,
        relativeTime: {
          future: "in %s",
          past: "%s ago",
          s: "a few seconds",
          ss: "%d seconds",
          m: "a minute",
          mm: "%d minutes",
          h: "an hour",
          hh: "%d hours",
          d: "a day",
          dd: "%d days",
          M: "a month",
          MM: "%d months",
          y: "a year",
          yy: "%d years",
        },
        months: Ye,
        monthsShort: He,
        week: { dow: 0, doy: 6 },
        weekdays: ze,
        weekdaysMin: Ue,
        weekdaysShort: Fe,
        meridiemParse: /[ap]\.?m?\.?/i,
      },
      Ke = {},
      Ze = {},
      Je =
        /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/,
      ti =
        /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/,
      ei = /Z|[+-]\d\d(?::?\d\d)?/,
      ii = [
        ["YYYYYY-MM-DD", /[+-]\d{6}-\d\d-\d\d/],
        ["YYYY-MM-DD", /\d{4}-\d\d-\d\d/],
        ["GGGG-[W]WW-E", /\d{4}-W\d\d-\d/],
        ["GGGG-[W]WW", /\d{4}-W\d\d/, !1],
        ["YYYY-DDD", /\d{4}-\d{3}/],
        ["YYYY-MM", /\d{4}-\d\d/, !1],
        ["YYYYYYMMDD", /[+-]\d{10}/],
        ["YYYYMMDD", /\d{8}/],
        ["GGGG[W]WWE", /\d{4}W\d{3}/],
        ["GGGG[W]WW", /\d{4}W\d{2}/, !1],
        ["YYYYDDD", /\d{7}/],
      ],
      ni = [
        ["HH:mm:ss.SSSS", /\d\d:\d\d:\d\d\.\d+/],
        ["HH:mm:ss,SSSS", /\d\d:\d\d:\d\d,\d+/],
        ["HH:mm:ss", /\d\d:\d\d:\d\d/],
        ["HH:mm", /\d\d:\d\d/],
        ["HHmmss.SSSS", /\d\d\d\d\d\d\.\d+/],
        ["HHmmss,SSSS", /\d\d\d\d\d\d,\d+/],
        ["HHmmss", /\d\d\d\d\d\d/],
        ["HHmm", /\d\d\d\d/],
        ["HH", /\d\d/],
      ],
      si = /^\/?Date\((\-?\d+)/i,
      oi =
        /^((?:Mon|Tue|Wed|Thu|Fri|Sat|Sun),?\s)?(\d?\d\s(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(?:\d\d)?\d\d\s)(\d\d:\d\d)(\:\d\d)?(\s(?:UT|GMT|[ECMP][SD]T|[A-IK-Za-ik-z]|[+-]\d{4}))$/;
    (t.createFromInputFallback = w(
      "value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), which is not reliable across all browsers and versions. Non RFC2822/ISO date formats are discouraged and will be removed in an upcoming major release. Please refer to http://momentjs.com/guides/#/warnings/js-date/ for more info.",
      function (t) {
        t._d = new Date(t._i + (t._useUTC ? " UTC" : ""));
      }
    )),
      (t.ISO_8601 = function () {}),
      (t.RFC_2822 = function () {});
    var ri = w(
        "moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/",
        function () {
          var t = _t.apply(null, arguments);
          return this.isValid() && t.isValid() ? (t < this ? this : t) : u();
        }
      ),
      ai = w(
        "moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/",
        function () {
          var t = _t.apply(null, arguments);
          return this.isValid() && t.isValid() ? (t > this ? this : t) : u();
        }
      ),
      li = [
        "year",
        "quarter",
        "month",
        "week",
        "day",
        "hour",
        "minute",
        "second",
        "millisecond",
      ];
    Dt("Z", ":"),
      Dt("ZZ", ""),
      Y("Z", Ce),
      Y("ZZ", Ce),
      W(["Z", "ZZ"], function (t, e, i) {
        (i._useUTC = !0), (i._tzm = Tt(Ce, t));
      });
    var ci = /([\+\-]|\d\d)/gi;
    t.updateOffset = function () {};
    var di = /^(\-)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)(\.\d*)?)?$/,
      hi =
        /^(-)?P(?:(-?[0-9,.]*)Y)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)W)?(?:(-?[0-9,.]*)D)?(?:T(?:(-?[0-9,.]*)H)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)S)?)?$/;
    (At.fn = bt.prototype),
      (At.invalid = function () {
        return At(NaN);
      });
    var ui = Ot(1, "add"),
      pi = Ot(-1, "subtract");
    (t.defaultFormat = "YYYY-MM-DDTHH:mm:ssZ"),
      (t.defaultFormatUtc = "YYYY-MM-DDTHH:mm:ss[Z]");
    var fi = w(
      "moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.",
      function (t) {
        return void 0 === t ? this.localeData() : this.locale(t);
      }
    );
    N(0, ["gg", 2], 0, function () {
      return this.weekYear() % 100;
    }),
      N(0, ["GG", 2], 0, function () {
        return this.isoWeekYear() % 100;
      }),
      Lt("gggg", "weekYear"),
      Lt("ggggg", "weekYear"),
      Lt("GGGG", "isoWeekYear"),
      Lt("GGGGG", "isoWeekYear"),
      T("weekYear", "gg"),
      T("isoWeekYear", "GG"),
      E("weekYear", 1),
      E("isoWeekYear", 1),
      Y("G", we),
      Y("g", we),
      Y("GG", pe, ce),
      Y("gg", pe, ce),
      Y("GGGG", ve, he),
      Y("gggg", ve, he),
      Y("GGGGG", ye, ue),
      Y("ggggg", ye, ue),
      R(["gggg", "ggggg", "GGGG", "GGGGG"], function (t, e, i, n) {
        e[n.substr(0, 2)] = v(t);
      }),
      R(["gg", "GG"], function (e, i, n, s) {
        i[s] = t.parseTwoDigitYear(e);
      }),
      N("Q", 0, "Qo", "quarter"),
      T("quarter", "Q"),
      E("quarter", 7),
      Y("Q", le),
      W("Q", function (t, e) {
        e[Se] = 3 * (v(t) - 1);
      }),
      N("D", ["DD", 2], "Do", "date"),
      T("date", "D"),
      E("date", 9),
      Y("D", pe),
      Y("DD", pe, ce),
      Y("Do", function (t, e) {
        return t
          ? e._dayOfMonthOrdinalParse || e._ordinalParse
          : e._dayOfMonthOrdinalParseLenient;
      }),
      W(["D", "DD"], Ee),
      W("Do", function (t, e) {
        e[Ee] = v(t.match(pe)[0]);
      });
    var mi = A("Date", !0);
    N("DDD", ["DDDD", 3], "DDDo", "dayOfYear"),
      T("dayOfYear", "DDD"),
      E("dayOfYear", 4),
      Y("DDD", ge),
      Y("DDDD", de),
      W(["DDD", "DDDD"], function (t, e, i) {
        i._dayOfYear = v(t);
      }),
      N("m", ["mm", 2], 0, "minute"),
      T("minute", "m"),
      E("minute", 14),
      Y("m", pe),
      Y("mm", pe, ce),
      W(["m", "mm"], Me);
    var gi = A("Minutes", !1);
    N("s", ["ss", 2], 0, "second"),
      T("second", "s"),
      E("second", 15),
      Y("s", pe),
      Y("ss", pe, ce),
      W(["s", "ss"], $e);
    var vi,
      yi = A("Seconds", !1);
    for (
      N("S", 0, 0, function () {
        return ~~(this.millisecond() / 100);
      }),
        N(0, ["SS", 2], 0, function () {
          return ~~(this.millisecond() / 10);
        }),
        N(0, ["SSS", 3], 0, "millisecond"),
        N(0, ["SSSS", 4], 0, function () {
          return 10 * this.millisecond();
        }),
        N(0, ["SSSSS", 5], 0, function () {
          return 100 * this.millisecond();
        }),
        N(0, ["SSSSSS", 6], 0, function () {
          return 1e3 * this.millisecond();
        }),
        N(0, ["SSSSSSS", 7], 0, function () {
          return 1e4 * this.millisecond();
        }),
        N(0, ["SSSSSSSS", 8], 0, function () {
          return 1e5 * this.millisecond();
        }),
        N(0, ["SSSSSSSSS", 9], 0, function () {
          return 1e6 * this.millisecond();
        }),
        T("millisecond", "ms"),
        E("millisecond", 16),
        Y("S", ge, le),
        Y("SS", ge, ce),
        Y("SSS", ge, de),
        vi = "SSSS";
      vi.length <= 9;
      vi += "S"
    )
      Y(vi, _e);
    for (vi = "S"; vi.length <= 9; vi += "S") W(vi, Ht);
    var _i = A("Milliseconds", !1);
    N("z", 0, 0, "zoneAbbr"), N("zz", 0, 0, "zoneName");
    var wi = f.prototype;
    (wi.add = ui),
      (wi.calendar = function (e, i) {
        var n = e || _t(),
          s = xt(n, this).startOf("day"),
          o = t.calendarFormat(this, s) || "sameElse",
          r = i && (C(i[o]) ? i[o].call(this, n) : i[o]);
        return this.format(r || this.localeData().calendar(o, this, _t(n)));
      }),
      (wi.clone = function () {
        return new f(this);
      }),
      (wi.diff = function (t, e, i) {
        var n, s, o, r;
        return this.isValid() && (n = xt(t, this)).isValid()
          ? ((s = 6e4 * (n.utcOffset() - this.utcOffset())),
            "year" === (e = x(e)) || "month" === e || "quarter" === e
              ? ((r = (function (t, e) {
                  var i = 12 * (e.year() - t.year()) + (e.month() - t.month()),
                    n = t.clone().add(i, "months");
                  return (
                    -(
                      i +
                      (e - n < 0
                        ? (e - n) / (n - t.clone().add(i - 1, "months"))
                        : (e - n) / (t.clone().add(i + 1, "months") - n))
                    ) || 0
                  );
                })(this, n)),
                "quarter" === e ? (r /= 3) : "year" === e && (r /= 12))
              : ((o = this - n),
                (r =
                  "second" === e
                    ? o / 1e3
                    : "minute" === e
                    ? o / 6e4
                    : "hour" === e
                    ? o / 36e5
                    : "day" === e
                    ? (o - s) / 864e5
                    : "week" === e
                    ? (o - s) / 6048e5
                    : o)),
            i ? r : g(r))
          : NaN;
      }),
      (wi.endOf = function (t) {
        return void 0 === (t = x(t)) || "millisecond" === t
          ? this
          : ("date" === t && (t = "day"),
            this.startOf(t)
              .add(1, "isoWeek" === t ? "week" : t)
              .subtract(1, "ms"));
      }),
      (wi.format = function (e) {
        e || (e = this.isUtc() ? t.defaultFormatUtc : t.defaultFormat);
        var i = P(this, e);
        return this.localeData().postformat(i);
      }),
      (wi.from = function (t, e) {
        return this.isValid() && ((m(t) && t.isValid()) || _t(t).isValid())
          ? At({ to: this, from: t }).locale(this.locale()).humanize(!e)
          : this.localeData().invalidDate();
      }),
      (wi.fromNow = function (t) {
        return this.from(_t(), t);
      }),
      (wi.to = function (t, e) {
        return this.isValid() && ((m(t) && t.isValid()) || _t(t).isValid())
          ? At({ from: this, to: t }).locale(this.locale()).humanize(!e)
          : this.localeData().invalidDate();
      }),
      (wi.toNow = function (t) {
        return this.to(_t(), t);
      }),
      (wi.get = function (t) {
        return C(this[(t = x(t))]) ? this[t]() : this;
      }),
      (wi.invalidAt = function () {
        return d(this).overflow;
      }),
      (wi.isAfter = function (t, e) {
        var i = m(t) ? t : _t(t);
        return (
          !(!this.isValid() || !i.isValid()) &&
          ("millisecond" === (e = x(n(e) ? "millisecond" : e))
            ? this.valueOf() > i.valueOf()
            : i.valueOf() < this.clone().startOf(e).valueOf())
        );
      }),
      (wi.isBefore = function (t, e) {
        var i = m(t) ? t : _t(t);
        return (
          !(!this.isValid() || !i.isValid()) &&
          ("millisecond" === (e = x(n(e) ? "millisecond" : e))
            ? this.valueOf() < i.valueOf()
            : this.clone().endOf(e).valueOf() < i.valueOf())
        );
      }),
      (wi.isBetween = function (t, e, i, n) {
        return (
          ("(" === (n = n || "()")[0]
            ? this.isAfter(t, i)
            : !this.isBefore(t, i)) &&
          (")" === n[1] ? this.isBefore(e, i) : !this.isAfter(e, i))
        );
      }),
      (wi.isSame = function (t, e) {
        var i,
          n = m(t) ? t : _t(t);
        return (
          !(!this.isValid() || !n.isValid()) &&
          ("millisecond" === (e = x(e || "millisecond"))
            ? this.valueOf() === n.valueOf()
            : ((i = n.valueOf()),
              this.clone().startOf(e).valueOf() <= i &&
                i <= this.clone().endOf(e).valueOf()))
        );
      }),
      (wi.isSameOrAfter = function (t, e) {
        return this.isSame(t, e) || this.isAfter(t, e);
      }),
      (wi.isSameOrBefore = function (t, e) {
        return this.isSame(t, e) || this.isBefore(t, e);
      }),
      (wi.isValid = function () {
        return h(this);
      }),
      (wi.lang = fi),
      (wi.locale = It),
      (wi.localeData = Pt),
      (wi.max = ai),
      (wi.min = ri),
      (wi.parsingFlags = function () {
        return l({}, d(this));
      }),
      (wi.set = function (t, e) {
        if ("object" == typeof t)
          for (
            var i = (function (t) {
                var e = [];
                for (var i in t) e.push({ unit: i, priority: ne[i] });
                return (
                  e.sort(function (t, e) {
                    return t.priority - e.priority;
                  }),
                  e
                );
              })((t = S(t))),
              n = 0;
            n < i.length;
            n++
          )
            this[i[n].unit](t[i[n].unit]);
        else if (C(this[(t = x(t))])) return this[t](e);
        return this;
      }),
      (wi.startOf = function (t) {
        switch ((t = x(t))) {
          case "year":
            this.month(0);
          case "quarter":
          case "month":
            this.date(1);
          case "week":
          case "isoWeek":
          case "day":
          case "date":
            this.hours(0);
          case "hour":
            this.minutes(0);
          case "minute":
            this.seconds(0);
          case "second":
            this.milliseconds(0);
        }
        return (
          "week" === t && this.weekday(0),
          "isoWeek" === t && this.isoWeekday(1),
          "quarter" === t && this.month(3 * Math.floor(this.month() / 3)),
          this
        );
      }),
      (wi.subtract = pi),
      (wi.toArray = function () {
        var t = this;
        return [
          t.year(),
          t.month(),
          t.date(),
          t.hour(),
          t.minute(),
          t.second(),
          t.millisecond(),
        ];
      }),
      (wi.toObject = function () {
        var t = this;
        return {
          years: t.year(),
          months: t.month(),
          date: t.date(),
          hours: t.hours(),
          minutes: t.minutes(),
          seconds: t.seconds(),
          milliseconds: t.milliseconds(),
        };
      }),
      (wi.toDate = function () {
        return new Date(this.valueOf());
      }),
      (wi.toISOString = function () {
        if (!this.isValid()) return null;
        var t = this.clone().utc();
        return t.year() < 0 || t.year() > 9999
          ? P(t, "YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]")
          : C(Date.prototype.toISOString)
          ? this.toDate().toISOString()
          : P(t, "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]");
      }),
      (wi.inspect = function () {
        if (!this.isValid()) return "moment.invalid(/* " + this._i + " */)";
        var t = "moment",
          e = "";
        this.isLocal() ||
          ((t = 0 === this.utcOffset() ? "moment.utc" : "moment.parseZone"),
          (e = "Z"));
        var i = "[" + t + '("]',
          n = 0 <= this.year() && this.year() <= 9999 ? "YYYY" : "YYYYYY",
          s = e + '[")]';
        return this.format(i + n + "-MM-DD[T]HH:mm:ss.SSS" + s);
      }),
      (wi.toJSON = function () {
        return this.isValid() ? this.toISOString() : null;
      }),
      (wi.toString = function () {
        return this.clone()
          .locale("en")
          .format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ");
      }),
      (wi.unix = function () {
        return Math.floor(this.valueOf() / 1e3);
      }),
      (wi.valueOf = function () {
        return this._d.valueOf() - 6e4 * (this._offset || 0);
      }),
      (wi.creationData = function () {
        return {
          input: this._i,
          format: this._f,
          locale: this._locale,
          isUTC: this._isUTC,
          strict: this._strict,
        };
      }),
      (wi.year = Re),
      (wi.isLeapYear = function () {
        return G(this.year());
      }),
      (wi.weekYear = function (t) {
        return Yt.call(
          this,
          t,
          this.week(),
          this.weekday(),
          this.localeData()._week.dow,
          this.localeData()._week.doy
        );
      }),
      (wi.isoWeekYear = function (t) {
        return Yt.call(this, t, this.isoWeek(), this.isoWeekday(), 1, 4);
      }),
      (wi.quarter = wi.quarters =
        function (t) {
          return null == t
            ? Math.ceil((this.month() + 1) / 3)
            : this.month(3 * (t - 1) + (this.month() % 3));
        }),
      (wi.month = B),
      (wi.daysInMonth = function () {
        return F(this.year(), this.month());
      }),
      (wi.week = wi.weeks =
        function (t) {
          var e = this.localeData().week(this);
          return null == t ? e : this.add(7 * (t - e), "d");
        }),
      (wi.isoWeek = wi.isoWeeks =
        function (t) {
          var e = Z(this, 1, 4).week;
          return null == t ? e : this.add(7 * (t - e), "d");
        }),
      (wi.weeksInYear = function () {
        var t = this.localeData()._week;
        return J(this.year(), t.dow, t.doy);
      }),
      (wi.isoWeeksInYear = function () {
        return J(this.year(), 1, 4);
      }),
      (wi.date = mi),
      (wi.day = wi.days =
        function (t) {
          if (!this.isValid()) return null != t ? this : NaN;
          var e = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
          return null != t
            ? ((t = (function (t, e) {
                return "string" != typeof t
                  ? t
                  : isNaN(t)
                  ? "number" == typeof (t = e.weekdaysParse(t))
                    ? t
                    : null
                  : parseInt(t, 10);
              })(t, this.localeData())),
              this.add(t - e, "d"))
            : e;
        }),
      (wi.weekday = function (t) {
        if (!this.isValid()) return null != t ? this : NaN;
        var e = (this.day() + 7 - this.localeData()._week.dow) % 7;
        return null == t ? e : this.add(t - e, "d");
      }),
      (wi.isoWeekday = function (t) {
        if (!this.isValid()) return null != t ? this : NaN;
        if (null != t) {
          var e = (function (t, e) {
            return "string" == typeof t
              ? e.weekdaysParse(t) % 7 || 7
              : isNaN(t)
              ? null
              : t;
          })(t, this.localeData());
          return this.day(this.day() % 7 ? e : e - 7);
        }
        return this.day() || 7;
      }),
      (wi.dayOfYear = function (t) {
        var e =
          Math.round(
            (this.clone().startOf("day") - this.clone().startOf("year")) / 864e5
          ) + 1;
        return null == t ? e : this.add(t - e, "d");
      }),
      (wi.hour = wi.hours = Qe),
      (wi.minute = wi.minutes = gi),
      (wi.second = wi.seconds = yi),
      (wi.millisecond = wi.milliseconds = _i),
      (wi.utcOffset = function (e, i, n) {
        var s,
          o = this._offset || 0;
        if (!this.isValid()) return null != e ? this : NaN;
        if (null != e) {
          if ("string" == typeof e) {
            if (null === (e = Tt(Ce, e))) return this;
          } else Math.abs(e) < 16 && !n && (e *= 60);
          return (
            !this._isUTC && i && (s = St(this)),
            (this._offset = e),
            (this._isUTC = !0),
            null != s && this.add(s, "m"),
            o !== e &&
              (!i || this._changeInProgress
                ? Nt(this, At(e - o, "m"), 1, !1)
                : this._changeInProgress ||
                  ((this._changeInProgress = !0),
                  t.updateOffset(this, !0),
                  (this._changeInProgress = null))),
            this
          );
        }
        return this._isUTC ? o : St(this);
      }),
      (wi.utc = function (t) {
        return this.utcOffset(0, t);
      }),
      (wi.local = function (t) {
        return (
          this._isUTC &&
            (this.utcOffset(0, t),
            (this._isUTC = !1),
            t && this.subtract(St(this), "m")),
          this
        );
      }),
      (wi.parseZone = function () {
        if (null != this._tzm) this.utcOffset(this._tzm, !1, !0);
        else if ("string" == typeof this._i) {
          var t = Tt(be, this._i);
          null != t ? this.utcOffset(t) : this.utcOffset(0, !0);
        }
        return this;
      }),
      (wi.hasAlignedHourOffset = function (t) {
        return (
          !!this.isValid() &&
          ((t = t ? _t(t).utcOffset() : 0), (this.utcOffset() - t) % 60 == 0)
        );
      }),
      (wi.isDST = function () {
        return (
          this.utcOffset() > this.clone().month(0).utcOffset() ||
          this.utcOffset() > this.clone().month(5).utcOffset()
        );
      }),
      (wi.isLocal = function () {
        return !!this.isValid() && !this._isUTC;
      }),
      (wi.isUtcOffset = function () {
        return !!this.isValid() && this._isUTC;
      }),
      (wi.isUtc = Et),
      (wi.isUTC = Et),
      (wi.zoneAbbr = function () {
        return this._isUTC ? "UTC" : "";
      }),
      (wi.zoneName = function () {
        return this._isUTC ? "Coordinated Universal Time" : "";
      }),
      (wi.dates = w("dates accessor is deprecated. Use date instead.", mi)),
      (wi.months = w("months accessor is deprecated. Use month instead", B)),
      (wi.years = w("years accessor is deprecated. Use year instead", Re)),
      (wi.zone = w(
        "moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/",
        function (t, e) {
          return null != t
            ? ("string" != typeof t && (t = -t), this.utcOffset(t, e), this)
            : -this.utcOffset();
        }
      )),
      (wi.isDSTShifted = w(
        "isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information",
        function () {
          if (!n(this._isDSTShifted)) return this._isDSTShifted;
          var t = {};
          if ((p(t, this), (t = gt(t))._a)) {
            var e = t._isUTC ? c(t._a) : _t(t._a);
            this._isDSTShifted = this.isValid() && y(t._a, e.toArray()) > 0;
          } else this._isDSTShifted = !1;
          return this._isDSTShifted;
        }
      ));
    var bi = D.prototype;
    (bi.calendar = function (t, e, i) {
      var n = this._calendar[t] || this._calendar.sameElse;
      return C(n) ? n.call(e, i) : n;
    }),
      (bi.longDateFormat = function (t) {
        var e = this._longDateFormat[t],
          i = this._longDateFormat[t.toUpperCase()];
        return e || !i
          ? e
          : ((this._longDateFormat[t] = i.replace(
              /MMMM|MM|DD|dddd/g,
              function (t) {
                return t.slice(1);
              }
            )),
            this._longDateFormat[t]);
      }),
      (bi.invalidDate = function () {
        return this._invalidDate;
      }),
      (bi.ordinal = function (t) {
        return this._ordinal.replace("%d", t);
      }),
      (bi.preparse = jt),
      (bi.postformat = jt),
      (bi.relativeTime = function (t, e, i, n) {
        var s = this._relativeTime[i];
        return C(s) ? s(t, e, i, n) : s.replace(/%d/i, t);
      }),
      (bi.pastFuture = function (t, e) {
        var i = this._relativeTime[t > 0 ? "future" : "past"];
        return C(i) ? i(e) : i.replace(/%s/i, e);
      }),
      (bi.set = function (t) {
        var e, i;
        for (i in t) C((e = t[i])) ? (this[i] = e) : (this["_" + i] = e);
        (this._config = t),
          (this._dayOfMonthOrdinalParseLenient = new RegExp(
            (this._dayOfMonthOrdinalParse.source || this._ordinalParse.source) +
              "|" +
              /\d{1,2}/.source
          ));
      }),
      (bi.months = function (t, i) {
        return t
          ? e(this._months)
            ? this._months[t.month()]
            : this._months[
                (this._months.isFormat || Le).test(i) ? "format" : "standalone"
              ][t.month()]
          : e(this._months)
          ? this._months
          : this._months.standalone;
      }),
      (bi.monthsShort = function (t, i) {
        return t
          ? e(this._monthsShort)
            ? this._monthsShort[t.month()]
            : this._monthsShort[Le.test(i) ? "format" : "standalone"][t.month()]
          : e(this._monthsShort)
          ? this._monthsShort
          : this._monthsShort.standalone;
      }),
      (bi.monthsParse = function (t, e, i) {
        var n, s, o;
        if (this._monthsParseExact)
          return function (t, e, i) {
            var n,
              s,
              o,
              r = t.toLocaleLowerCase();
            if (!this._monthsParse)
              for (
                this._monthsParse = [],
                  this._longMonthsParse = [],
                  this._shortMonthsParse = [],
                  n = 0;
                n < 12;
                ++n
              )
                (o = c([2e3, n])),
                  (this._shortMonthsParse[n] = this.monthsShort(
                    o,
                    ""
                  ).toLocaleLowerCase()),
                  (this._longMonthsParse[n] = this.months(
                    o,
                    ""
                  ).toLocaleLowerCase());
            return i
              ? "MMM" === e
                ? -1 !== (s = Pe.call(this._shortMonthsParse, r))
                  ? s
                  : null
                : -1 !== (s = Pe.call(this._longMonthsParse, r))
                ? s
                : null
              : "MMM" === e
              ? -1 !== (s = Pe.call(this._shortMonthsParse, r))
                ? s
                : -1 !== (s = Pe.call(this._longMonthsParse, r))
                ? s
                : null
              : -1 !== (s = Pe.call(this._longMonthsParse, r))
              ? s
              : -1 !== (s = Pe.call(this._shortMonthsParse, r))
              ? s
              : null;
          }.call(this, t, e, i);
        for (
          this._monthsParse ||
            ((this._monthsParse = []),
            (this._longMonthsParse = []),
            (this._shortMonthsParse = [])),
            n = 0;
          n < 12;
          n++
        ) {
          if (
            ((s = c([2e3, n])),
            i &&
              !this._longMonthsParse[n] &&
              ((this._longMonthsParse[n] = new RegExp(
                "^" + this.months(s, "").replace(".", "") + "$",
                "i"
              )),
              (this._shortMonthsParse[n] = new RegExp(
                "^" + this.monthsShort(s, "").replace(".", "") + "$",
                "i"
              ))),
            i ||
              this._monthsParse[n] ||
              ((o = "^" + this.months(s, "") + "|^" + this.monthsShort(s, "")),
              (this._monthsParse[n] = new RegExp(o.replace(".", ""), "i"))),
            i && "MMMM" === e && this._longMonthsParse[n].test(t))
          )
            return n;
          if (i && "MMM" === e && this._shortMonthsParse[n].test(t)) return n;
          if (!i && this._monthsParse[n].test(t)) return n;
        }
      }),
      (bi.monthsRegex = function (t) {
        return this._monthsParseExact
          ? (a(this, "_monthsRegex") || q.call(this),
            t ? this._monthsStrictRegex : this._monthsRegex)
          : (a(this, "_monthsRegex") || (this._monthsRegex = We),
            this._monthsStrictRegex && t
              ? this._monthsStrictRegex
              : this._monthsRegex);
      }),
      (bi.monthsShortRegex = function (t) {
        return this._monthsParseExact
          ? (a(this, "_monthsRegex") || q.call(this),
            t ? this._monthsShortStrictRegex : this._monthsShortRegex)
          : (a(this, "_monthsShortRegex") || (this._monthsShortRegex = je),
            this._monthsShortStrictRegex && t
              ? this._monthsShortStrictRegex
              : this._monthsShortRegex);
      }),
      (bi.week = function (t) {
        return Z(t, this._week.dow, this._week.doy).week;
      }),
      (bi.firstDayOfYear = function () {
        return this._week.doy;
      }),
      (bi.firstDayOfWeek = function () {
        return this._week.dow;
      }),
      (bi.weekdays = function (t, i) {
        return t
          ? e(this._weekdays)
            ? this._weekdays[t.day()]
            : this._weekdays[
                this._weekdays.isFormat.test(i) ? "format" : "standalone"
              ][t.day()]
          : e(this._weekdays)
          ? this._weekdays
          : this._weekdays.standalone;
      }),
      (bi.weekdaysMin = function (t) {
        return t ? this._weekdaysMin[t.day()] : this._weekdaysMin;
      }),
      (bi.weekdaysShort = function (t) {
        return t ? this._weekdaysShort[t.day()] : this._weekdaysShort;
      }),
      (bi.weekdaysParse = function (t, e, i) {
        var n, s, o;
        if (this._weekdaysParseExact)
          return function (t, e, i) {
            var n,
              s,
              o,
              r = t.toLocaleLowerCase();
            if (!this._weekdaysParse)
              for (
                this._weekdaysParse = [],
                  this._shortWeekdaysParse = [],
                  this._minWeekdaysParse = [],
                  n = 0;
                n < 7;
                ++n
              )
                (o = c([2e3, 1]).day(n)),
                  (this._minWeekdaysParse[n] = this.weekdaysMin(
                    o,
                    ""
                  ).toLocaleLowerCase()),
                  (this._shortWeekdaysParse[n] = this.weekdaysShort(
                    o,
                    ""
                  ).toLocaleLowerCase()),
                  (this._weekdaysParse[n] = this.weekdays(
                    o,
                    ""
                  ).toLocaleLowerCase());
            return i
              ? "dddd" === e
                ? -1 !== (s = Pe.call(this._weekdaysParse, r))
                  ? s
                  : null
                : "ddd" === e
                ? -1 !== (s = Pe.call(this._shortWeekdaysParse, r))
                  ? s
                  : null
                : -1 !== (s = Pe.call(this._minWeekdaysParse, r))
                ? s
                : null
              : "dddd" === e
              ? -1 !== (s = Pe.call(this._weekdaysParse, r))
                ? s
                : -1 !== (s = Pe.call(this._shortWeekdaysParse, r))
                ? s
                : -1 !== (s = Pe.call(this._minWeekdaysParse, r))
                ? s
                : null
              : "ddd" === e
              ? -1 !== (s = Pe.call(this._shortWeekdaysParse, r))
                ? s
                : -1 !== (s = Pe.call(this._weekdaysParse, r))
                ? s
                : -1 !== (s = Pe.call(this._minWeekdaysParse, r))
                ? s
                : null
              : -1 !== (s = Pe.call(this._minWeekdaysParse, r))
              ? s
              : -1 !== (s = Pe.call(this._weekdaysParse, r))
              ? s
              : -1 !== (s = Pe.call(this._shortWeekdaysParse, r))
              ? s
              : null;
          }.call(this, t, e, i);
        for (
          this._weekdaysParse ||
            ((this._weekdaysParse = []),
            (this._minWeekdaysParse = []),
            (this._shortWeekdaysParse = []),
            (this._fullWeekdaysParse = [])),
            n = 0;
          n < 7;
          n++
        ) {
          if (
            ((s = c([2e3, 1]).day(n)),
            i &&
              !this._fullWeekdaysParse[n] &&
              ((this._fullWeekdaysParse[n] = new RegExp(
                "^" + this.weekdays(s, "").replace(".", ".?") + "$",
                "i"
              )),
              (this._shortWeekdaysParse[n] = new RegExp(
                "^" + this.weekdaysShort(s, "").replace(".", ".?") + "$",
                "i"
              )),
              (this._minWeekdaysParse[n] = new RegExp(
                "^" + this.weekdaysMin(s, "").replace(".", ".?") + "$",
                "i"
              ))),
            this._weekdaysParse[n] ||
              ((o =
                "^" +
                this.weekdays(s, "") +
                "|^" +
                this.weekdaysShort(s, "") +
                "|^" +
                this.weekdaysMin(s, "")),
              (this._weekdaysParse[n] = new RegExp(o.replace(".", ""), "i"))),
            i && "dddd" === e && this._fullWeekdaysParse[n].test(t))
          )
            return n;
          if (i && "ddd" === e && this._shortWeekdaysParse[n].test(t)) return n;
          if (i && "dd" === e && this._minWeekdaysParse[n].test(t)) return n;
          if (!i && this._weekdaysParse[n].test(t)) return n;
        }
      }),
      (bi.weekdaysRegex = function (t) {
        return this._weekdaysParseExact
          ? (a(this, "_weekdaysRegex") || tt.call(this),
            t ? this._weekdaysStrictRegex : this._weekdaysRegex)
          : (a(this, "_weekdaysRegex") || (this._weekdaysRegex = Be),
            this._weekdaysStrictRegex && t
              ? this._weekdaysStrictRegex
              : this._weekdaysRegex);
      }),
      (bi.weekdaysShortRegex = function (t) {
        return this._weekdaysParseExact
          ? (a(this, "_weekdaysRegex") || tt.call(this),
            t ? this._weekdaysShortStrictRegex : this._weekdaysShortRegex)
          : (a(this, "_weekdaysShortRegex") || (this._weekdaysShortRegex = qe),
            this._weekdaysShortStrictRegex && t
              ? this._weekdaysShortStrictRegex
              : this._weekdaysShortRegex);
      }),
      (bi.weekdaysMinRegex = function (t) {
        return this._weekdaysParseExact
          ? (a(this, "_weekdaysRegex") || tt.call(this),
            t ? this._weekdaysMinStrictRegex : this._weekdaysMinRegex)
          : (a(this, "_weekdaysMinRegex") || (this._weekdaysMinRegex = Ve),
            this._weekdaysMinStrictRegex && t
              ? this._weekdaysMinStrictRegex
              : this._weekdaysMinRegex);
      }),
      (bi.isPM = function (t) {
        return "p" === (t + "").toLowerCase().charAt(0);
      }),
      (bi.meridiem = function (t, e, i) {
        return t > 11 ? (i ? "pm" : "PM") : i ? "am" : "AM";
      }),
      rt("en", {
        dayOfMonthOrdinalParse: /\d{1,2}(th|st|nd|rd)/,
        ordinal: function (t) {
          var e = t % 10;
          return (
            t +
            (1 === v((t % 100) / 10)
              ? "th"
              : 1 === e
              ? "st"
              : 2 === e
              ? "nd"
              : 3 === e
              ? "rd"
              : "th")
          );
        },
      }),
      (t.lang = w("moment.lang is deprecated. Use moment.locale instead.", rt)),
      (t.langData = w(
        "moment.langData is deprecated. Use moment.localeData instead.",
        lt
      ));
    var Ci = Math.abs,
      ki = Vt("ms"),
      Di = Vt("s"),
      Ti = Vt("m"),
      xi = Vt("h"),
      Si = Vt("d"),
      Ei = Vt("w"),
      Ai = Vt("M"),
      Mi = Vt("y"),
      $i = Gt("milliseconds"),
      Oi = Gt("seconds"),
      Ni = Gt("minutes"),
      Ii = Gt("hours"),
      Pi = Gt("days"),
      Li = Gt("months"),
      Yi = Gt("years"),
      Hi = Math.round,
      ji = { ss: 44, s: 45, m: 45, h: 22, d: 26, M: 11 },
      Wi = Math.abs,
      Ri = bt.prototype;
    return (
      (Ri.isValid = function () {
        return this._isValid;
      }),
      (Ri.abs = function () {
        var t = this._data;
        return (
          (this._milliseconds = Ci(this._milliseconds)),
          (this._days = Ci(this._days)),
          (this._months = Ci(this._months)),
          (t.milliseconds = Ci(t.milliseconds)),
          (t.seconds = Ci(t.seconds)),
          (t.minutes = Ci(t.minutes)),
          (t.hours = Ci(t.hours)),
          (t.months = Ci(t.months)),
          (t.years = Ci(t.years)),
          this
        );
      }),
      (Ri.add = function (t, e) {
        return Ft(this, t, e, 1);
      }),
      (Ri.subtract = function (t, e) {
        return Ft(this, t, e, -1);
      }),
      (Ri.as = function (t) {
        if (!this.isValid()) return NaN;
        var e,
          i,
          n = this._milliseconds;
        if ("month" === (t = x(t)) || "year" === t)
          return (
            (e = this._days + n / 864e5),
            (i = this._months + Bt(e)),
            "month" === t ? i : i / 12
          );
        switch (((e = this._days + Math.round(qt(this._months))), t)) {
          case "week":
            return e / 7 + n / 6048e5;
          case "day":
            return e + n / 864e5;
          case "hour":
            return 24 * e + n / 36e5;
          case "minute":
            return 1440 * e + n / 6e4;
          case "second":
            return 86400 * e + n / 1e3;
          case "millisecond":
            return Math.floor(864e5 * e) + n;
          default:
            throw new Error("Unknown unit " + t);
        }
      }),
      (Ri.asMilliseconds = ki),
      (Ri.asSeconds = Di),
      (Ri.asMinutes = Ti),
      (Ri.asHours = xi),
      (Ri.asDays = Si),
      (Ri.asWeeks = Ei),
      (Ri.asMonths = Ai),
      (Ri.asYears = Mi),
      (Ri.valueOf = function () {
        return this.isValid()
          ? this._milliseconds +
              864e5 * this._days +
              (this._months % 12) * 2592e6 +
              31536e6 * v(this._months / 12)
          : NaN;
      }),
      (Ri._bubble = function () {
        var t,
          e,
          i,
          n,
          s,
          o = this._milliseconds,
          r = this._days,
          a = this._months,
          l = this._data;
        return (
          (o >= 0 && r >= 0 && a >= 0) ||
            (o <= 0 && r <= 0 && a <= 0) ||
            ((o += 864e5 * Ut(qt(a) + r)), (r = 0), (a = 0)),
          (l.milliseconds = o % 1e3),
          (t = g(o / 1e3)),
          (l.seconds = t % 60),
          (e = g(t / 60)),
          (l.minutes = e % 60),
          (i = g(e / 60)),
          (l.hours = i % 24),
          (r += g(i / 24)),
          (a += s = g(Bt(r))),
          (r -= Ut(qt(s))),
          (n = g(a / 12)),
          (a %= 12),
          (l.days = r),
          (l.months = a),
          (l.years = n),
          this
        );
      }),
      (Ri.get = function (t) {
        return (t = x(t)), this.isValid() ? this[t + "s"]() : NaN;
      }),
      (Ri.milliseconds = $i),
      (Ri.seconds = Oi),
      (Ri.minutes = Ni),
      (Ri.hours = Ii),
      (Ri.days = Pi),
      (Ri.weeks = function () {
        return g(this.days() / 7);
      }),
      (Ri.months = Li),
      (Ri.years = Yi),
      (Ri.humanize = function (t) {
        if (!this.isValid()) return this.localeData().invalidDate();
        var e = this.localeData(),
          i = (function (t, e, i) {
            var n = At(t).abs(),
              s = Hi(n.as("s")),
              o = Hi(n.as("m")),
              r = Hi(n.as("h")),
              a = Hi(n.as("d")),
              l = Hi(n.as("M")),
              c = Hi(n.as("y")),
              d = (s <= ji.ss && ["s", s]) ||
                (s < ji.s && ["ss", s]) ||
                (o <= 1 && ["m"]) ||
                (o < ji.m && ["mm", o]) ||
                (r <= 1 && ["h"]) ||
                (r < ji.h && ["hh", r]) ||
                (a <= 1 && ["d"]) ||
                (a < ji.d && ["dd", a]) ||
                (l <= 1 && ["M"]) ||
                (l < ji.M && ["MM", l]) ||
                (c <= 1 && ["y"]) || ["yy", c];
            return (
              (d[2] = e),
              (d[3] = +t > 0),
              (d[4] = i),
              function (t, e, i, n, s) {
                return s.relativeTime(e || 1, !!i, t, n);
              }.apply(null, d)
            );
          })(this, !t, e);
        return t && (i = e.pastFuture(+this, i)), e.postformat(i);
      }),
      (Ri.toISOString = Qt),
      (Ri.toString = Qt),
      (Ri.toJSON = Qt),
      (Ri.locale = It),
      (Ri.localeData = Pt),
      (Ri.toIsoString = w(
        "toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)",
        Qt
      )),
      (Ri.lang = fi),
      N("X", 0, 0, "unix"),
      N("x", 0, 0, "valueOf"),
      Y("x", we),
      Y("X", /[+-]?\d+(\.\d{1,3})?/),
      W("X", function (t, e, i) {
        i._d = new Date(1e3 * parseFloat(t, 10));
      }),
      W("x", function (t, e, i) {
        i._d = new Date(v(t));
      }),
      (t.version = "2.18.1"),
      (Xt = _t),
      (t.fn = wi),
      (t.min = function () {
        return wt("isBefore", [].slice.call(arguments, 0));
      }),
      (t.max = function () {
        return wt("isAfter", [].slice.call(arguments, 0));
      }),
      (t.now = function () {
        return Date.now ? Date.now() : +new Date();
      }),
      (t.utc = c),
      (t.unix = function (t) {
        return _t(1e3 * t);
      }),
      (t.months = function (t, e) {
        return Rt(t, e, "months");
      }),
      (t.isDate = o),
      (t.locale = rt),
      (t.invalid = u),
      (t.duration = At),
      (t.isMoment = m),
      (t.weekdays = function (t, e, i) {
        return zt(t, e, i, "weekdays");
      }),
      (t.parseZone = function () {
        return _t.apply(null, arguments).parseZone();
      }),
      (t.localeData = lt),
      (t.isDuration = Ct),
      (t.monthsShort = function (t, e) {
        return Rt(t, e, "monthsShort");
      }),
      (t.weekdaysMin = function (t, e, i) {
        return zt(t, e, i, "weekdaysMin");
      }),
      (t.defineLocale = at),
      (t.updateLocale = function (t, e) {
        if (null != e) {
          var i,
            n = Xe;
          null != Ke[t] && (n = Ke[t]._config),
            ((i = new D((e = k(n, e)))).parentLocale = Ke[t]),
            (Ke[t] = i),
            rt(t);
        } else
          null != Ke[t] &&
            (null != Ke[t].parentLocale
              ? (Ke[t] = Ke[t].parentLocale)
              : null != Ke[t] && delete Ke[t]);
        return Ke[t];
      }),
      (t.locales = function () {
        return ee(Ke);
      }),
      (t.weekdaysShort = function (t, e, i) {
        return zt(t, e, i, "weekdaysShort");
      }),
      (t.normalizeUnits = x),
      (t.relativeTimeRounding = function (t) {
        return void 0 === t ? Hi : "function" == typeof t && ((Hi = t), !0);
      }),
      (t.relativeTimeThreshold = function (t, e) {
        return (
          void 0 !== ji[t] &&
          (void 0 === e
            ? ji[t]
            : ((ji[t] = e), "s" === t && (ji.ss = e - 1), !0))
        );
      }),
      (t.calendarFormat = function (t, e) {
        var i = t.diff(e, "days", !0);
        return i < -6
          ? "sameElse"
          : i < -1
          ? "lastWeek"
          : i < 0
          ? "lastDay"
          : i < 1
          ? "sameDay"
          : i < 2
          ? "nextDay"
          : i < 7
          ? "nextWeek"
          : "sameElse";
      }),
      (t.prototype = wi),
      t
    );
  }),
  (function (t, e) {
    if ("function" == typeof define && define.amd)
      define(["moment", "jquery"], function (t, i) {
        return (
          i.fn || (i.fn = {}),
          "function" != typeof t &&
            t.hasOwnProperty("default") &&
            (t = t.default),
          e(t, i)
        );
      });
    else if ("object" == typeof module && module.exports) {
      var i = "undefined" != typeof window ? window.jQuery : void 0;
      i || (i = require("jquery")).fn || (i.fn = {});
      var n =
        "undefined" != typeof window && void 0 !== window.moment
          ? window.moment
          : require("moment");
      module.exports = e(n, i);
    } else t.daterangepicker = e(t.moment, t.jQuery);
  })(this, function (t, e) {
    var i = function (i, n, s) {
      if (
        ((this.parentEl = "body"),
        (this.element = e(i)),
        (this.startDate = t().startOf("day")),
        (this.endDate = t().endOf("day")),
        (this.minDate = !1),
        (this.maxDate = !1),
        (this.maxSpan = !1),
        (this.autoApply = !1),
        (this.singleDatePicker = !1),
        (this.showDropdowns = !1),
        (this.minYear = t().subtract(100, "year").format("YYYY")),
        (this.maxYear = t().add(100, "year").format("YYYY")),
        (this.showWeekNumbers = !1),
        (this.showISOWeekNumbers = !1),
        (this.showCustomRangeLabel = !0),
        (this.timePicker = !1),
        (this.timePicker24Hour = !1),
        (this.timePickerIncrement = 1),
        (this.timePickerSeconds = !1),
        (this.linkedCalendars = !0),
        (this.autoUpdateInput = !0),
        (this.alwaysShowCalendars = !1),
        (this.ranges = {}),
        (this.opens = "right"),
        this.element.hasClass("pull-right") && (this.opens = "left"),
        (this.drops = "down"),
        this.element.hasClass("dropup") && (this.drops = "up"),
        (this.buttonClasses = "btn btn-sm"),
        (this.applyButtonClasses = "btn-primary"),
        (this.cancelButtonClasses = "btn-default"),
        (this.locale = {
          direction: "ltr",
          format: t.localeData().longDateFormat("L"),
          separator: " - ",
          applyLabel: "Apply",
          cancelLabel: "Cancel",
          weekLabel: "W",
          customRangeLabel: "Custom Range",
          daysOfWeek: t.weekdaysMin(),
          monthNames: t.monthsShort(),
          firstDay: t.localeData().firstDayOfWeek(),
        }),
        (this.callback = function () {}),
        (this.isShowing = !1),
        (this.leftCalendar = {}),
        (this.rightCalendar = {}),
        ("object" == typeof n && null !== n) || (n = {}),
        "string" == typeof (n = e.extend(this.element.data(), n)).template ||
          n.template instanceof e ||
          (n.template =
            '<div class="daterangepicker"><div class="ranges"></div><div class="drp-calendar left"><div class="calendar-table"></div><div class="calendar-time"></div></div><div class="drp-calendar right"><div class="calendar-table"></div><div class="calendar-time"></div></div><div class="drp-buttons"><span class="drp-selected"></span><button class="cancelBtn" type="button"></button><button class="applyBtn" disabled="disabled" type="button"></button> </div></div>'),
        (this.parentEl =
          n.parentEl && e(n.parentEl).length
            ? e(n.parentEl)
            : e(this.parentEl)),
        (this.container = e(n.template).appendTo(this.parentEl)),
        "object" == typeof n.locale &&
          ("string" == typeof n.locale.direction &&
            (this.locale.direction = n.locale.direction),
          "string" == typeof n.locale.format &&
            (this.locale.format = n.locale.format),
          "string" == typeof n.locale.separator &&
            (this.locale.separator = n.locale.separator),
          "object" == typeof n.locale.daysOfWeek &&
            (this.locale.daysOfWeek = n.locale.daysOfWeek.slice()),
          "object" == typeof n.locale.monthNames &&
            (this.locale.monthNames = n.locale.monthNames.slice()),
          "number" == typeof n.locale.firstDay &&
            (this.locale.firstDay = n.locale.firstDay),
          "string" == typeof n.locale.applyLabel &&
            (this.locale.applyLabel = n.locale.applyLabel),
          "string" == typeof n.locale.cancelLabel &&
            (this.locale.cancelLabel = n.locale.cancelLabel),
          "string" == typeof n.locale.weekLabel &&
            (this.locale.weekLabel = n.locale.weekLabel),
          "string" == typeof n.locale.customRangeLabel))
      ) {
        (u = document.createElement("textarea")).innerHTML =
          n.locale.customRangeLabel;
        var o = u.value;
        this.locale.customRangeLabel = o;
      }
      if (
        (this.container.addClass(this.locale.direction),
        "string" == typeof n.startDate &&
          (this.startDate = t(n.startDate, this.locale.format)),
        "string" == typeof n.endDate &&
          (this.endDate = t(n.endDate, this.locale.format)),
        "string" == typeof n.minDate &&
          (this.minDate = t(n.minDate, this.locale.format)),
        "string" == typeof n.maxDate &&
          (this.maxDate = t(n.maxDate, this.locale.format)),
        "object" == typeof n.startDate && (this.startDate = t(n.startDate)),
        "object" == typeof n.endDate && (this.endDate = t(n.endDate)),
        "object" == typeof n.minDate && (this.minDate = t(n.minDate)),
        "object" == typeof n.maxDate && (this.maxDate = t(n.maxDate)),
        this.minDate &&
          this.startDate.isBefore(this.minDate) &&
          (this.startDate = this.minDate.clone()),
        this.maxDate &&
          this.endDate.isAfter(this.maxDate) &&
          (this.endDate = this.maxDate.clone()),
        "string" == typeof n.applyButtonClasses &&
          (this.applyButtonClasses = n.applyButtonClasses),
        "string" == typeof n.applyClass &&
          (this.applyButtonClasses = n.applyClass),
        "string" == typeof n.cancelButtonClasses &&
          (this.cancelButtonClasses = n.cancelButtonClasses),
        "string" == typeof n.cancelClass &&
          (this.cancelButtonClasses = n.cancelClass),
        "object" == typeof n.maxSpan && (this.maxSpan = n.maxSpan),
        "object" == typeof n.dateLimit && (this.maxSpan = n.dateLimit),
        "string" == typeof n.opens && (this.opens = n.opens),
        "string" == typeof n.drops && (this.drops = n.drops),
        "boolean" == typeof n.showWeekNumbers &&
          (this.showWeekNumbers = n.showWeekNumbers),
        "boolean" == typeof n.showISOWeekNumbers &&
          (this.showISOWeekNumbers = n.showISOWeekNumbers),
        "string" == typeof n.buttonClasses &&
          (this.buttonClasses = n.buttonClasses),
        "object" == typeof n.buttonClasses &&
          (this.buttonClasses = n.buttonClasses.join(" ")),
        "boolean" == typeof n.showDropdowns &&
          (this.showDropdowns = n.showDropdowns),
        "number" == typeof n.minYear && (this.minYear = n.minYear),
        "number" == typeof n.maxYear && (this.maxYear = n.maxYear),
        "boolean" == typeof n.showCustomRangeLabel &&
          (this.showCustomRangeLabel = n.showCustomRangeLabel),
        "boolean" == typeof n.singleDatePicker &&
          ((this.singleDatePicker = n.singleDatePicker),
          this.singleDatePicker && (this.endDate = this.startDate.clone())),
        "boolean" == typeof n.timePicker && (this.timePicker = n.timePicker),
        "boolean" == typeof n.timePickerSeconds &&
          (this.timePickerSeconds = n.timePickerSeconds),
        "number" == typeof n.timePickerIncrement &&
          (this.timePickerIncrement = n.timePickerIncrement),
        "boolean" == typeof n.timePicker24Hour &&
          (this.timePicker24Hour = n.timePicker24Hour),
        "boolean" == typeof n.autoApply && (this.autoApply = n.autoApply),
        "boolean" == typeof n.autoUpdateInput &&
          (this.autoUpdateInput = n.autoUpdateInput),
        "boolean" == typeof n.linkedCalendars &&
          (this.linkedCalendars = n.linkedCalendars),
        "function" == typeof n.isInvalidDate &&
          (this.isInvalidDate = n.isInvalidDate),
        "function" == typeof n.isCustomDate &&
          (this.isCustomDate = n.isCustomDate),
        "boolean" == typeof n.alwaysShowCalendars &&
          (this.alwaysShowCalendars = n.alwaysShowCalendars),
        0 != this.locale.firstDay)
      )
        for (var r = this.locale.firstDay; r > 0; )
          this.locale.daysOfWeek.push(this.locale.daysOfWeek.shift()), r--;
      var a, l, c;
      if (
        void 0 === n.startDate &&
        void 0 === n.endDate &&
        e(this.element).is(":text")
      ) {
        var d = e(this.element).val(),
          h = d.split(this.locale.separator);
        (a = l = null),
          2 == h.length
            ? ((a = t(h[0], this.locale.format)),
              (l = t(h[1], this.locale.format)))
            : this.singleDatePicker &&
              "" !== d &&
              ((a = t(d, this.locale.format)), (l = t(d, this.locale.format))),
          null !== a &&
            null !== l &&
            (this.setStartDate(a), this.setEndDate(l));
      }
      if ("object" == typeof n.ranges) {
        for (c in n.ranges) {
          (a =
            "string" == typeof n.ranges[c][0]
              ? t(n.ranges[c][0], this.locale.format)
              : t(n.ranges[c][0])),
            (l =
              "string" == typeof n.ranges[c][1]
                ? t(n.ranges[c][1], this.locale.format)
                : t(n.ranges[c][1])),
            this.minDate &&
              a.isBefore(this.minDate) &&
              (a = this.minDate.clone());
          var u,
            p = this.maxDate;
          this.maxSpan &&
            p &&
            a.clone().add(this.maxSpan).isAfter(p) &&
            (p = a.clone().add(this.maxSpan)),
            p && l.isAfter(p) && (l = p.clone()),
            (this.minDate &&
              l.isBefore(this.minDate, this.timepicker ? "minute" : "day")) ||
              (p && a.isAfter(p, this.timepicker ? "minute" : "day")) ||
              (((u = document.createElement("textarea")).innerHTML = c),
              (o = u.value),
              (this.ranges[o] = [a, l]));
        }
        var f = "<ul>";
        for (c in this.ranges)
          f += '<li data-range-key="' + c + '">' + c + "</li>";
        this.showCustomRangeLabel &&
          (f +=
            '<li data-range-key="' +
            this.locale.customRangeLabel +
            '">' +
            this.locale.customRangeLabel +
            "</li>"),
          (f += "</ul>"),
          this.container.find(".ranges").prepend(f);
      }
      "function" == typeof s && (this.callback = s),
        this.timePicker ||
          ((this.startDate = this.startDate.startOf("day")),
          (this.endDate = this.endDate.endOf("day")),
          this.container.find(".calendar-time").hide()),
        this.timePicker && this.autoApply && (this.autoApply = !1),
        this.autoApply && this.container.addClass("auto-apply"),
        "object" == typeof n.ranges && this.container.addClass("show-ranges"),
        this.singleDatePicker &&
          (this.container.addClass("single"),
          this.container.find(".drp-calendar.left").addClass("single"),
          this.container.find(".drp-calendar.left").show(),
          this.container.find(".drp-calendar.right").hide(),
          !this.timePicker &&
            this.autoApply &&
            this.container.addClass("auto-apply")),
        ((void 0 === n.ranges && !this.singleDatePicker) ||
          this.alwaysShowCalendars) &&
          this.container.addClass("show-calendar"),
        this.container.addClass("opens" + this.opens),
        this.container
          .find(".applyBtn, .cancelBtn")
          .addClass(this.buttonClasses),
        this.applyButtonClasses.length &&
          this.container.find(".applyBtn").addClass(this.applyButtonClasses),
        this.cancelButtonClasses.length &&
          this.container.find(".cancelBtn").addClass(this.cancelButtonClasses),
        this.container.find(".applyBtn").html(this.locale.applyLabel),
        this.container.find(".cancelBtn").html(this.locale.cancelLabel),
        this.container
          .find(".drp-calendar")
          .on("click.daterangepicker", ".prev", e.proxy(this.clickPrev, this))
          .on("click.daterangepicker", ".next", e.proxy(this.clickNext, this))
          .on(
            "mousedown.daterangepicker",
            "td.available",
            e.proxy(this.clickDate, this)
          )
          .on(
            "mouseenter.daterangepicker",
            "td.available",
            e.proxy(this.hoverDate, this)
          )
          .on(
            "change.daterangepicker",
            "select.yearselect",
            e.proxy(this.monthOrYearChanged, this)
          )
          .on(
            "change.daterangepicker",
            "select.monthselect",
            e.proxy(this.monthOrYearChanged, this)
          )
          .on(
            "change.daterangepicker",
            "select.hourselect,select.minuteselect,select.secondselect,select.ampmselect",
            e.proxy(this.timeChanged, this)
          ),
        this.container
          .find(".ranges")
          .on("click.daterangepicker", "li", e.proxy(this.clickRange, this)),
        this.container
          .find(".drp-buttons")
          .on(
            "click.daterangepicker",
            "button.applyBtn",
            e.proxy(this.clickApply, this)
          )
          .on(
            "click.daterangepicker",
            "button.cancelBtn",
            e.proxy(this.clickCancel, this)
          ),
        this.element.is("input") || this.element.is("button")
          ? this.element.on({
              "click.daterangepicker": e.proxy(this.show, this),
              "focus.daterangepicker": e.proxy(this.show, this),
              "keyup.daterangepicker": e.proxy(this.elementChanged, this),
              "keydown.daterangepicker": e.proxy(this.keydown, this),
            })
          : (this.element.on(
              "click.daterangepicker",
              e.proxy(this.toggle, this)
            ),
            this.element.on(
              "keydown.daterangepicker",
              e.proxy(this.toggle, this)
            )),
        this.updateElement();
    };
    return (
      (i.prototype = {
        constructor: i,
        setStartDate: function (e) {
          "string" == typeof e && (this.startDate = t(e, this.locale.format)),
            "object" == typeof e && (this.startDate = t(e)),
            this.timePicker || (this.startDate = this.startDate.startOf("day")),
            this.timePicker &&
              this.timePickerIncrement &&
              this.startDate.minute(
                Math.round(this.startDate.minute() / this.timePickerIncrement) *
                  this.timePickerIncrement
              ),
            this.minDate &&
              this.startDate.isBefore(this.minDate) &&
              ((this.startDate = this.minDate.clone()),
              this.timePicker &&
                this.timePickerIncrement &&
                this.startDate.minute(
                  Math.round(
                    this.startDate.minute() / this.timePickerIncrement
                  ) * this.timePickerIncrement
                )),
            this.maxDate &&
              this.startDate.isAfter(this.maxDate) &&
              ((this.startDate = this.maxDate.clone()),
              this.timePicker &&
                this.timePickerIncrement &&
                this.startDate.minute(
                  Math.floor(
                    this.startDate.minute() / this.timePickerIncrement
                  ) * this.timePickerIncrement
                )),
            this.isShowing || this.updateElement(),
            this.updateMonthsInView();
        },
        setEndDate: function (e) {
          "string" == typeof e && (this.endDate = t(e, this.locale.format)),
            "object" == typeof e && (this.endDate = t(e)),
            this.timePicker || (this.endDate = this.endDate.endOf("day")),
            this.timePicker &&
              this.timePickerIncrement &&
              this.endDate.minute(
                Math.round(this.endDate.minute() / this.timePickerIncrement) *
                  this.timePickerIncrement
              ),
            this.endDate.isBefore(this.startDate) &&
              (this.endDate = this.startDate.clone()),
            this.maxDate &&
              this.endDate.isAfter(this.maxDate) &&
              (this.endDate = this.maxDate.clone()),
            this.maxSpan &&
              this.startDate.clone().add(this.maxSpan).isBefore(this.endDate) &&
              (this.endDate = this.startDate.clone().add(this.maxSpan)),
            (this.previousRightTime = this.endDate.clone()),
            this.container
              .find(".drp-selected")
              .html(
                this.startDate.format(this.locale.format) +
                  this.locale.separator +
                  this.endDate.format(this.locale.format)
              ),
            this.isShowing || this.updateElement(),
            this.updateMonthsInView();
        },
        isInvalidDate: function () {
          return !1;
        },
        isCustomDate: function () {
          return !1;
        },
        updateView: function () {
          this.timePicker &&
            (this.renderTimePicker("left"),
            this.renderTimePicker("right"),
            this.endDate
              ? this.container
                  .find(".right .calendar-time select")
                  .prop("disabled", !1)
                  .removeClass("disabled")
              : this.container
                  .find(".right .calendar-time select")
                  .prop("disabled", !0)
                  .addClass("disabled")),
            this.endDate &&
              this.container
                .find(".drp-selected")
                .html(
                  this.startDate.format(this.locale.format) +
                    this.locale.separator +
                    this.endDate.format(this.locale.format)
                ),
            this.updateMonthsInView(),
            this.updateCalendars(),
            this.updateFormInputs();
        },
        updateMonthsInView: function () {
          if (this.endDate) {
            if (
              !this.singleDatePicker &&
              this.leftCalendar.month &&
              this.rightCalendar.month &&
              (this.startDate.format("YYYY-MM") ==
                this.leftCalendar.month.format("YYYY-MM") ||
                this.startDate.format("YYYY-MM") ==
                  this.rightCalendar.month.format("YYYY-MM")) &&
              (this.endDate.format("YYYY-MM") ==
                this.leftCalendar.month.format("YYYY-MM") ||
                this.endDate.format("YYYY-MM") ==
                  this.rightCalendar.month.format("YYYY-MM"))
            )
              return;
            (this.leftCalendar.month = this.startDate.clone().date(2)),
              this.linkedCalendars ||
              (this.endDate.month() == this.startDate.month() &&
                this.endDate.year() == this.startDate.year())
                ? (this.rightCalendar.month = this.startDate
                    .clone()
                    .date(2)
                    .add(1, "month"))
                : (this.rightCalendar.month = this.endDate.clone().date(2));
          } else
            this.leftCalendar.month.format("YYYY-MM") !=
              this.startDate.format("YYYY-MM") &&
              this.rightCalendar.month.format("YYYY-MM") !=
                this.startDate.format("YYYY-MM") &&
              ((this.leftCalendar.month = this.startDate.clone().date(2)),
              (this.rightCalendar.month = this.startDate
                .clone()
                .date(2)
                .add(1, "month")));
          this.maxDate &&
            this.linkedCalendars &&
            !this.singleDatePicker &&
            this.rightCalendar.month > this.maxDate &&
            ((this.rightCalendar.month = this.maxDate.clone().date(2)),
            (this.leftCalendar.month = this.maxDate
              .clone()
              .date(2)
              .subtract(1, "month")));
        },
        updateCalendars: function () {
          var t, e, i, n;
          this.timePicker &&
            (this.endDate
              ? ((t = parseInt(
                  this.container.find(".left .hourselect").val(),
                  10
                )),
                (e = parseInt(
                  this.container.find(".left .minuteselect").val(),
                  10
                )),
                isNaN(e) &&
                  (e = parseInt(
                    this.container
                      .find(".left .minuteselect option:last")
                      .val(),
                    10
                  )),
                (i = this.timePickerSeconds
                  ? parseInt(
                      this.container.find(".left .secondselect").val(),
                      10
                    )
                  : 0),
                this.timePicker24Hour ||
                  ("PM" ===
                    (n = this.container.find(".left .ampmselect").val()) &&
                    t < 12 &&
                    (t += 12),
                  "AM" === n && 12 === t && (t = 0)))
              : ((t = parseInt(
                  this.container.find(".right .hourselect").val(),
                  10
                )),
                (e = parseInt(
                  this.container.find(".right .minuteselect").val(),
                  10
                )),
                isNaN(e) &&
                  (e = parseInt(
                    this.container
                      .find(".right .minuteselect option:last")
                      .val(),
                    10
                  )),
                (i = this.timePickerSeconds
                  ? parseInt(
                      this.container.find(".right .secondselect").val(),
                      10
                    )
                  : 0),
                this.timePicker24Hour ||
                  ("PM" ===
                    (n = this.container.find(".right .ampmselect").val()) &&
                    t < 12 &&
                    (t += 12),
                  "AM" === n && 12 === t && (t = 0))),
            this.leftCalendar.month.hour(t).minute(e).second(i),
            this.rightCalendar.month.hour(t).minute(e).second(i)),
            this.renderCalendar("left"),
            this.renderCalendar("right"),
            this.container.find(".ranges li").removeClass("active"),
            null != this.endDate && this.calculateChosenLabel();
        },
        renderCalendar: function (i) {
          var n,
            s = (n =
              "left" == i
                ? this.leftCalendar
                : this.rightCalendar).month.month(),
            o = n.month.year(),
            r = n.month.hour(),
            a = n.month.minute(),
            l = n.month.second(),
            c = t([o, s]).daysInMonth(),
            d = t([o, s, 1]),
            h = t([o, s, c]),
            u = t(d).subtract(1, "month").month(),
            p = t(d).subtract(1, "month").year(),
            f = t([p, u]).daysInMonth(),
            m = d.day();
          ((n = []).firstDay = d), (n.lastDay = h);
          for (var g = 0; g < 6; g++) n[g] = [];
          var v = f - m + this.locale.firstDay + 1;
          v > f && (v -= 7), m == this.locale.firstDay && (v = f - 6);
          for (
            var y = t([p, u, v, 12, a, l]), _ = ((g = 0), 0), w = 0;
            g < 42;
            g++, _++, y = t(y).add(24, "hour")
          )
            g > 0 && _ % 7 == 0 && ((_ = 0), w++),
              (n[w][_] = y.clone().hour(r).minute(a).second(l)),
              y.hour(12),
              this.minDate &&
                n[w][_].format("YYYY-MM-DD") ==
                  this.minDate.format("YYYY-MM-DD") &&
                n[w][_].isBefore(this.minDate) &&
                "left" == i &&
                (n[w][_] = this.minDate.clone()),
              this.maxDate &&
                n[w][_].format("YYYY-MM-DD") ==
                  this.maxDate.format("YYYY-MM-DD") &&
                n[w][_].isAfter(this.maxDate) &&
                "right" == i &&
                (n[w][_] = this.maxDate.clone());
          "left" == i
            ? (this.leftCalendar.calendar = n)
            : (this.rightCalendar.calendar = n);
          var b = "left" == i ? this.minDate : this.startDate,
            C = this.maxDate,
            k =
              ("left" == i ? this.startDate : this.endDate,
              this.locale.direction,
              '<table class="table-condensed">');
          (k += "<thead>"),
            (k += "<tr>"),
            (this.showWeekNumbers || this.showISOWeekNumbers) &&
              (k += "<th></th>"),
            (b && !b.isBefore(n.firstDay)) ||
            (this.linkedCalendars && "left" != i)
              ? (k += "<th></th>")
              : (k += '<th class="prev available"><span></span></th>');
          var D =
            this.locale.monthNames[n[1][1].month()] + n[1][1].format(" YYYY");
          if (this.showDropdowns) {
            for (
              var T = n[1][1].month(),
                x = n[1][1].year(),
                S = (C && C.year()) || this.maxYear,
                E = (b && b.year()) || this.minYear,
                A = x == E,
                M = x == S,
                $ = '<select class="monthselect">',
                O = 0;
              O < 12;
              O++
            )
              (!A || (b && O >= b.month())) && (!M || (C && O <= C.month()))
                ? ($ +=
                    "<option value='" +
                    O +
                    "'" +
                    (O === T ? " selected='selected'" : "") +
                    ">" +
                    this.locale.monthNames[O] +
                    "</option>")
                : ($ +=
                    "<option value='" +
                    O +
                    "'" +
                    (O === T ? " selected='selected'" : "") +
                    " disabled='disabled'>" +
                    this.locale.monthNames[O] +
                    "</option>");
            $ += "</select>";
            for (var N = '<select class="yearselect">', I = E; I <= S; I++)
              N +=
                '<option value="' +
                I +
                '"' +
                (I === x ? ' selected="selected"' : "") +
                ">" +
                I +
                "</option>";
            D = $ + (N += "</select>");
          }
          if (
            ((k += '<th colspan="5" class="month">' + D + "</th>"),
            (C && !C.isAfter(n.lastDay)) ||
            (this.linkedCalendars && "right" != i && !this.singleDatePicker)
              ? (k += "<th></th>")
              : (k += '<th class="next available"><span></span></th>'),
            (k += "</tr>"),
            (k += "<tr>"),
            (this.showWeekNumbers || this.showISOWeekNumbers) &&
              (k += '<th class="week">' + this.locale.weekLabel + "</th>"),
            e.each(this.locale.daysOfWeek, function (t, e) {
              k += "<th>" + e + "</th>";
            }),
            (k += "</tr>"),
            (k += "</thead>"),
            (k += "<tbody>"),
            null == this.endDate && this.maxSpan)
          ) {
            var P = this.startDate.clone().add(this.maxSpan).endOf("day");
            (C && !P.isBefore(C)) || (C = P);
          }
          for (w = 0; w < 6; w++) {
            for (
              k += "<tr>",
                this.showWeekNumbers
                  ? (k += '<td class="week">' + n[w][0].week() + "</td>")
                  : this.showISOWeekNumbers &&
                    (k += '<td class="week">' + n[w][0].isoWeek() + "</td>"),
                _ = 0;
              _ < 7;
              _++
            ) {
              var L = [];
              n[w][_].isSame(new Date(), "day") && L.push("today"),
                n[w][_].isoWeekday() > 5 && L.push("weekend"),
                n[w][_].month() != n[1][1].month() && L.push("off", "ends"),
                this.minDate &&
                  n[w][_].isBefore(this.minDate, "day") &&
                  L.push("off", "disabled"),
                C && n[w][_].isAfter(C, "day") && L.push("off", "disabled"),
                this.isInvalidDate(n[w][_]) && L.push("off", "disabled"),
                n[w][_].format("YYYY-MM-DD") ==
                  this.startDate.format("YYYY-MM-DD") &&
                  L.push("active", "start-date"),
                null != this.endDate &&
                  n[w][_].format("YYYY-MM-DD") ==
                    this.endDate.format("YYYY-MM-DD") &&
                  L.push("active", "end-date"),
                null != this.endDate &&
                  n[w][_] > this.startDate &&
                  n[w][_] < this.endDate &&
                  L.push("in-range");
              var Y = this.isCustomDate(n[w][_]);
              !1 !== Y &&
                ("string" == typeof Y
                  ? L.push(Y)
                  : Array.prototype.push.apply(L, Y));
              var H = "",
                j = !1;
              for (g = 0; g < L.length; g++)
                (H += L[g] + " "), "disabled" == L[g] && (j = !0);
              j || (H += "available"),
                (k +=
                  '<td class="' +
                  H.replace(/^\s+|\s+$/g, "") +
                  '" data-title="r' +
                  w +
                  "c" +
                  _ +
                  '">' +
                  n[w][_].date() +
                  "</td>");
            }
            k += "</tr>";
          }
          (k += "</tbody>"),
            (k += "</table>"),
            this.container
              .find(".drp-calendar." + i + " .calendar-table")
              .html(k);
        },
        renderTimePicker: function (t) {
          if ("right" != t || this.endDate) {
            var e,
              i,
              n,
              s = this.maxDate;
            if (
              (!this.maxSpan ||
                (this.maxDate &&
                  !this.startDate
                    .clone()
                    .add(this.maxSpan)
                    .isBefore(this.maxDate)) ||
                (s = this.startDate.clone().add(this.maxSpan)),
              "left" == t)
            )
              (i = this.startDate.clone()), (n = this.minDate);
            else if ("right" == t) {
              (i = this.endDate.clone()), (n = this.startDate);
              var o = this.container.find(".drp-calendar.right .calendar-time");
              if (
                "" != o.html() &&
                (i.hour(
                  isNaN(i.hour())
                    ? o.find(".hourselect option:selected").val()
                    : i.hour()
                ),
                i.minute(
                  isNaN(i.minute())
                    ? o.find(".minuteselect option:selected").val()
                    : i.minute()
                ),
                i.second(
                  isNaN(i.second())
                    ? o.find(".secondselect option:selected").val()
                    : i.second()
                ),
                !this.timePicker24Hour)
              ) {
                var r = o.find(".ampmselect option:selected").val();
                "PM" === r && i.hour() < 12 && i.hour(i.hour() + 12),
                  "AM" === r && 12 === i.hour() && i.hour(0);
              }
              i.isBefore(this.startDate) && (i = this.startDate.clone()),
                s && i.isAfter(s) && (i = s.clone());
            }
            e = '<select class="hourselect">';
            for (
              var a = this.timePicker24Hour ? 0 : 1,
                l = this.timePicker24Hour ? 23 : 12,
                c = a;
              c <= l;
              c++
            ) {
              var d = c;
              this.timePicker24Hour ||
                (d =
                  i.hour() >= 12 ? (12 == c ? 12 : c + 12) : 12 == c ? 0 : c);
              var h = i.clone().hour(d),
                u = !1;
              n && h.minute(59).isBefore(n) && (u = !0),
                s && h.minute(0).isAfter(s) && (u = !0),
                d != i.hour() || u
                  ? (e += u
                      ? '<option value="' +
                        c +
                        '" disabled="disabled" class="disabled">' +
                        c +
                        "</option>"
                      : '<option value="' + c + '">' + c + "</option>")
                  : (e +=
                      '<option value="' +
                      c +
                      '" selected="selected">' +
                      c +
                      "</option>");
            }
            for (
              e += "</select> ", e += ': <select class="minuteselect">', c = 0;
              c < 60;
              c += this.timePickerIncrement
            ) {
              var p = c < 10 ? "0" + c : c;
              (h = i.clone().minute(c)),
                (u = !1),
                n && h.second(59).isBefore(n) && (u = !0),
                s && h.second(0).isAfter(s) && (u = !0),
                i.minute() != c || u
                  ? (e += u
                      ? '<option value="' +
                        c +
                        '" disabled="disabled" class="disabled">' +
                        p +
                        "</option>"
                      : '<option value="' + c + '">' + p + "</option>")
                  : (e +=
                      '<option value="' +
                      c +
                      '" selected="selected">' +
                      p +
                      "</option>");
            }
            if (((e += "</select> "), this.timePickerSeconds)) {
              for (e += ': <select class="secondselect">', c = 0; c < 60; c++)
                (p = c < 10 ? "0" + c : c),
                  (h = i.clone().second(c)),
                  (u = !1),
                  n && h.isBefore(n) && (u = !0),
                  s && h.isAfter(s) && (u = !0),
                  i.second() != c || u
                    ? (e += u
                        ? '<option value="' +
                          c +
                          '" disabled="disabled" class="disabled">' +
                          p +
                          "</option>"
                        : '<option value="' + c + '">' + p + "</option>")
                    : (e +=
                        '<option value="' +
                        c +
                        '" selected="selected">' +
                        p +
                        "</option>");
              e += "</select> ";
            }
            if (!this.timePicker24Hour) {
              e += '<select class="ampmselect">';
              var f = "",
                m = "";
              n &&
                i.clone().hour(12).minute(0).second(0).isBefore(n) &&
                (f = ' disabled="disabled" class="disabled"'),
                s &&
                  i.clone().hour(0).minute(0).second(0).isAfter(s) &&
                  (m = ' disabled="disabled" class="disabled"'),
                i.hour() >= 12
                  ? (e +=
                      '<option value="AM"' +
                      f +
                      '>AM</option><option value="PM" selected="selected"' +
                      m +
                      ">PM</option>")
                  : (e +=
                      '<option value="AM" selected="selected"' +
                      f +
                      '>AM</option><option value="PM"' +
                      m +
                      ">PM</option>"),
                (e += "</select>");
            }
            this.container
              .find(".drp-calendar." + t + " .calendar-time")
              .html(e);
          }
        },
        updateFormInputs: function () {
          this.singleDatePicker ||
          (this.endDate &&
            (this.startDate.isBefore(this.endDate) ||
              this.startDate.isSame(this.endDate)))
            ? this.container.find("button.applyBtn").prop("disabled", !1)
            : this.container.find("button.applyBtn").prop("disabled", !0);
        },
        move: function () {
          var t,
            i = { top: 0, left: 0 },
            n = this.drops,
            s = e(window).width();
          switch (
            (this.parentEl.is("body") ||
              ((i = {
                top: this.parentEl.offset().top - this.parentEl.scrollTop(),
                left: this.parentEl.offset().left - this.parentEl.scrollLeft(),
              }),
              (s = this.parentEl[0].clientWidth + this.parentEl.offset().left)),
            n)
          ) {
            case "auto":
              (t =
                this.element.offset().top +
                this.element.outerHeight() -
                i.top) +
                this.container.outerHeight() >=
                this.parentEl[0].scrollHeight &&
                ((t =
                  this.element.offset().top -
                  this.container.outerHeight() -
                  i.top),
                (n = "up"));
              break;
            case "up":
              t =
                this.element.offset().top -
                this.container.outerHeight() -
                i.top;
              break;
            default:
              t =
                this.element.offset().top + this.element.outerHeight() - i.top;
          }
          this.container.css({ top: 0, left: 0, right: "auto" });
          var o = this.container.outerWidth();
          if (
            (this.container.toggleClass("drop-up", "up" == n),
            "left" == this.opens)
          ) {
            var r = s - this.element.offset().left - this.element.outerWidth();
            o + r > e(window).width()
              ? this.container.css({ top: t, right: "auto", left: 9 })
              : this.container.css({ top: t, right: r, left: "auto" });
          } else if ("center" == this.opens)
            (a =
              this.element.offset().left -
              i.left +
              this.element.outerWidth() / 2 -
              o / 2) < 0
              ? this.container.css({ top: t, right: "auto", left: 9 })
              : a + o > e(window).width()
              ? this.container.css({ top: t, left: "auto", right: 0 })
              : this.container.css({ top: t, left: a, right: "auto" });
          else {
            var a;
            (a = this.element.offset().left - i.left) + o > e(window).width()
              ? this.container.css({ top: t, left: "auto", right: 0 })
              : this.container.css({ top: t, left: a, right: "auto" });
          }
        },
        show: function (t) {
          this.isShowing ||
            ((this._outsideClickProxy = e.proxy(function (t) {
              this.outsideClick(t);
            }, this)),
            e(document)
              .on("mousedown.daterangepicker", this._outsideClickProxy)
              .on("touchend.daterangepicker", this._outsideClickProxy)
              .on(
                "click.daterangepicker",
                "[data-toggle=dropdown]",
                this._outsideClickProxy
              )
              .on("focusin.daterangepicker", this._outsideClickProxy),
            e(window).on(
              "resize.daterangepicker",
              e.proxy(function (t) {
                this.move(t);
              }, this)
            ),
            (this.oldStartDate = this.startDate.clone()),
            (this.oldEndDate = this.endDate.clone()),
            (this.previousRightTime = this.endDate.clone()),
            this.updateView(),
            this.container.show(),
            this.move(),
            this.element.trigger("show.daterangepicker", this),
            (this.isShowing = !0));
        },
        hide: function (t) {
          this.isShowing &&
            (this.endDate ||
              ((this.startDate = this.oldStartDate.clone()),
              (this.endDate = this.oldEndDate.clone())),
            (this.startDate.isSame(this.oldStartDate) &&
              this.endDate.isSame(this.oldEndDate)) ||
              this.callback(
                this.startDate.clone(),
                this.endDate.clone(),
                this.chosenLabel
              ),
            this.updateElement(),
            e(document).off(".daterangepicker"),
            e(window).off(".daterangepicker"),
            this.container.hide(),
            this.element.trigger("hide.daterangepicker", this),
            (this.isShowing = !1));
        },
        toggle: function (t) {
          this.isShowing ? this.hide() : this.show();
        },
        outsideClick: function (t) {
          var i = e(t.target);
          "focusin" == t.type ||
            i.closest(this.element).length ||
            i.closest(this.container).length ||
            i.closest(".calendar-table").length ||
            (this.hide(),
            this.element.trigger("outsideClick.daterangepicker", this));
        },
        showCalendars: function () {
          this.container.addClass("show-calendar"),
            this.move(),
            this.element.trigger("showCalendar.daterangepicker", this);
        },
        hideCalendars: function () {
          this.container.removeClass("show-calendar"),
            this.element.trigger("hideCalendar.daterangepicker", this);
        },
        clickRange: function (t) {
          var e = t.target.getAttribute("data-range-key");
          if (((this.chosenLabel = e), e == this.locale.customRangeLabel))
            this.showCalendars();
          else {
            var i = this.ranges[e];
            (this.startDate = i[0]),
              (this.endDate = i[1]),
              this.timePicker ||
                (this.startDate.startOf("day"), this.endDate.endOf("day")),
              this.alwaysShowCalendars || this.hideCalendars(),
              this.clickApply();
          }
        },
        clickPrev: function (t) {
          e(t.target).parents(".drp-calendar").hasClass("left")
            ? (this.leftCalendar.month.subtract(1, "month"),
              this.linkedCalendars &&
                this.rightCalendar.month.subtract(1, "month"))
            : this.rightCalendar.month.subtract(1, "month"),
            this.updateCalendars();
        },
        clickNext: function (t) {
          e(t.target).parents(".drp-calendar").hasClass("left")
            ? this.leftCalendar.month.add(1, "month")
            : (this.rightCalendar.month.add(1, "month"),
              this.linkedCalendars && this.leftCalendar.month.add(1, "month")),
            this.updateCalendars();
        },
        hoverDate: function (t) {
          if (e(t.target).hasClass("available")) {
            var i = e(t.target).attr("data-title"),
              n = i.substr(1, 1),
              s = i.substr(3, 1),
              o = e(t.target).parents(".drp-calendar").hasClass("left")
                ? this.leftCalendar.calendar[n][s]
                : this.rightCalendar.calendar[n][s],
              r = this.leftCalendar,
              a = this.rightCalendar,
              l = this.startDate;
            this.endDate ||
              this.container
                .find(".drp-calendar tbody td")
                .each(function (t, i) {
                  if (!e(i).hasClass("week")) {
                    var n = e(i).attr("data-title"),
                      s = n.substr(1, 1),
                      c = n.substr(3, 1),
                      d = e(i).parents(".drp-calendar").hasClass("left")
                        ? r.calendar[s][c]
                        : a.calendar[s][c];
                    (d.isAfter(l) && d.isBefore(o)) || d.isSame(o, "day")
                      ? e(i).addClass("in-range")
                      : e(i).removeClass("in-range");
                  }
                });
          }
        },
        clickDate: function (t) {
          if (e(t.target).hasClass("available")) {
            var i = e(t.target).attr("data-title"),
              n = i.substr(1, 1),
              s = i.substr(3, 1),
              o = e(t.target).parents(".drp-calendar").hasClass("left")
                ? this.leftCalendar.calendar[n][s]
                : this.rightCalendar.calendar[n][s];
            if (this.endDate || o.isBefore(this.startDate, "day")) {
              if (this.timePicker) {
                var r = parseInt(
                  this.container.find(".left .hourselect").val(),
                  10
                );
                this.timePicker24Hour ||
                  ("PM" ===
                    (c = this.container.find(".left .ampmselect").val()) &&
                    r < 12 &&
                    (r += 12),
                  "AM" === c && 12 === r && (r = 0));
                var a = parseInt(
                  this.container.find(".left .minuteselect").val(),
                  10
                );
                isNaN(a) &&
                  (a = parseInt(
                    this.container
                      .find(".left .minuteselect option:last")
                      .val(),
                    10
                  ));
                var l = this.timePickerSeconds
                  ? parseInt(
                      this.container.find(".left .secondselect").val(),
                      10
                    )
                  : 0;
                o = o.clone().hour(r).minute(a).second(l);
              }
              (this.endDate = null), this.setStartDate(o.clone());
            } else if (!this.endDate && o.isBefore(this.startDate))
              this.setEndDate(this.startDate.clone());
            else {
              var c;
              this.timePicker &&
                ((r = parseInt(
                  this.container.find(".right .hourselect").val(),
                  10
                )),
                this.timePicker24Hour ||
                  ("PM" ===
                    (c = this.container.find(".right .ampmselect").val()) &&
                    r < 12 &&
                    (r += 12),
                  "AM" === c && 12 === r && (r = 0)),
                (a = parseInt(
                  this.container.find(".right .minuteselect").val(),
                  10
                )),
                isNaN(a) &&
                  (a = parseInt(
                    this.container
                      .find(".right .minuteselect option:last")
                      .val(),
                    10
                  )),
                (l = this.timePickerSeconds
                  ? parseInt(
                      this.container.find(".right .secondselect").val(),
                      10
                    )
                  : 0),
                (o = o.clone().hour(r).minute(a).second(l))),
                this.setEndDate(o.clone()),
                this.autoApply &&
                  (this.calculateChosenLabel(), this.clickApply());
            }
            this.singleDatePicker &&
              (this.setEndDate(this.startDate),
              !this.timePicker && this.autoApply && this.clickApply()),
              this.updateView(),
              t.stopPropagation();
          }
        },
        calculateChosenLabel: function () {
          var t = !0,
            e = 0;
          for (var i in this.ranges) {
            if (this.timePicker) {
              var n = this.timePickerSeconds
                ? "YYYY-MM-DD HH:mm:ss"
                : "YYYY-MM-DD HH:mm";
              if (
                this.startDate.format(n) == this.ranges[i][0].format(n) &&
                this.endDate.format(n) == this.ranges[i][1].format(n)
              ) {
                (t = !1),
                  (this.chosenLabel = this.container
                    .find(".ranges li:eq(" + e + ")")
                    .addClass("active")
                    .attr("data-range-key"));
                break;
              }
            } else if (
              this.startDate.format("YYYY-MM-DD") ==
                this.ranges[i][0].format("YYYY-MM-DD") &&
              this.endDate.format("YYYY-MM-DD") ==
                this.ranges[i][1].format("YYYY-MM-DD")
            ) {
              (t = !1),
                (this.chosenLabel = this.container
                  .find(".ranges li:eq(" + e + ")")
                  .addClass("active")
                  .attr("data-range-key"));
              break;
            }
            e++;
          }
          t &&
            (this.showCustomRangeLabel
              ? (this.chosenLabel = this.container
                  .find(".ranges li:last")
                  .addClass("active")
                  .attr("data-range-key"))
              : (this.chosenLabel = null),
            this.showCalendars());
        },
        clickApply: function (t) {
          this.hide(), this.element.trigger("apply.daterangepicker", this);
        },
        clickCancel: function (t) {
          (this.startDate = this.oldStartDate),
            (this.endDate = this.oldEndDate),
            this.hide(),
            this.element.trigger("cancel.daterangepicker", this);
        },
        monthOrYearChanged: function (t) {
          var i = e(t.target).closest(".drp-calendar").hasClass("left"),
            n = i ? "left" : "right",
            s = this.container.find(".drp-calendar." + n),
            o = parseInt(s.find(".monthselect").val(), 10),
            r = s.find(".yearselect").val();
          i ||
            ((r < this.startDate.year() ||
              (r == this.startDate.year() && o < this.startDate.month())) &&
              ((o = this.startDate.month()), (r = this.startDate.year()))),
            this.minDate &&
              (r < this.minDate.year() ||
                (r == this.minDate.year() && o < this.minDate.month())) &&
              ((o = this.minDate.month()), (r = this.minDate.year())),
            this.maxDate &&
              (r > this.maxDate.year() ||
                (r == this.maxDate.year() && o > this.maxDate.month())) &&
              ((o = this.maxDate.month()), (r = this.maxDate.year())),
            i
              ? (this.leftCalendar.month.month(o).year(r),
                this.linkedCalendars &&
                  (this.rightCalendar.month = this.leftCalendar.month
                    .clone()
                    .add(1, "month")))
              : (this.rightCalendar.month.month(o).year(r),
                this.linkedCalendars &&
                  (this.leftCalendar.month = this.rightCalendar.month
                    .clone()
                    .subtract(1, "month"))),
            this.updateCalendars();
        },
        timeChanged: function (t) {
          var i = e(t.target).closest(".drp-calendar"),
            n = i.hasClass("left"),
            s = parseInt(i.find(".hourselect").val(), 10),
            o = parseInt(i.find(".minuteselect").val(), 10);
          isNaN(o) &&
            (o = parseInt(i.find(".minuteselect option:last").val(), 10));
          var r = this.timePickerSeconds
            ? parseInt(i.find(".secondselect").val(), 10)
            : 0;
          if (!this.timePicker24Hour) {
            var a = i.find(".ampmselect").val();
            "PM" === a && s < 12 && (s += 12),
              "AM" === a && 12 === s && (s = 0);
          }
          if (n) {
            var l = this.startDate.clone();
            l.hour(s),
              l.minute(o),
              l.second(r),
              this.setStartDate(l),
              this.singleDatePicker
                ? (this.endDate = this.startDate.clone())
                : this.endDate &&
                  this.endDate.format("YYYY-MM-DD") == l.format("YYYY-MM-DD") &&
                  this.endDate.isBefore(l) &&
                  this.setEndDate(l.clone());
          } else if (this.endDate) {
            var c = this.endDate.clone();
            c.hour(s), c.minute(o), c.second(r), this.setEndDate(c);
          }
          this.updateCalendars(),
            this.updateFormInputs(),
            this.renderTimePicker("left"),
            this.renderTimePicker("right");
        },
        elementChanged: function () {
          if (this.element.is("input") && this.element.val().length) {
            var e = this.element.val().split(this.locale.separator),
              i = null,
              n = null;
            2 === e.length &&
              ((i = t(e[0], this.locale.format)),
              (n = t(e[1], this.locale.format))),
              (this.singleDatePicker || null === i || null === n) &&
                (n = i = t(this.element.val(), this.locale.format)),
              i.isValid() &&
                n.isValid() &&
                (this.setStartDate(i), this.setEndDate(n), this.updateView());
          }
        },
        keydown: function (t) {
          (9 !== t.keyCode && 13 !== t.keyCode) || this.hide(),
            27 === t.keyCode &&
              (t.preventDefault(), t.stopPropagation(), this.hide());
        },
        updateElement: function () {
          if (this.element.is("input") && this.autoUpdateInput) {
            var t = this.startDate.format(this.locale.format);
            this.singleDatePicker ||
              (t +=
                this.locale.separator +
                this.endDate.format(this.locale.format)),
              t !== this.element.val() && this.element.val(t).trigger("change");
          }
        },
        remove: function () {
          this.container.remove(),
            this.element.off(".daterangepicker"),
            this.element.removeData();
        },
      }),
      (e.fn.daterangepicker = function (t, n) {
        var s = e.extend(!0, {}, e.fn.daterangepicker.defaultOptions, t);
        return (
          this.each(function () {
            var t = e(this);
            t.data("daterangepicker") && t.data("daterangepicker").remove(),
              t.data("daterangepicker", new i(t, s, n));
          }),
          this
        );
      }),
      i
    );
  }),
  jQuery.validator.setDefaults({
    errorPlacement: function (t, e) {
      e.parent(".input-group").length
        ? t.insertAfter(e.parent())
        : e.parent(".radio-inline").length
        ? t.insertAfter(e.parent().parent())
        : t.insertAfter(e);
    },
    errorElement: "div",
    errorClass: "help-block",
  }),
  $(".subscribe_block").validate(),
  $(".login_form").validate(),
  $(".signup_form").validate(),
  $(".contact_forms").validate(),
  (function (t) {
    "use strict";
    t(window).on("scroll", function () {
      t(window).scrollTop() < 400
        ? (t("#sticky-header").removeClass("sticky"),
          t("#back-top").fadeIn(500))
        : (t("#sticky-header").addClass("sticky"), t("#back-top").fadeIn(500));
    }),
      t(document).ready(function () {
        new WOW().init(),
          document.getElementById("default-select") && t("select").niceSelect();
      }),
      t(document).ready(function () {
        t("select").niceSelect();
      });
  })(jQuery),
  (function (t, e) {
    "use strict";
    var i = t.jQuery || t.Zepto,
      n = 0,
      s = !1;
    function o(n, o, r, a, l) {
      var c = 0,
        d = -1,
        h = -1,
        u = !1,
        p = "afterLoad",
        f = "load",
        m = "error",
        g = "img",
        v = "src",
        y = "srcset",
        _ = "sizes",
        w = "background-image";
      function b() {
        var e, s, c, p;
        (u = t.devicePixelRatio > 1),
          (r = C(r)),
          o.delay >= 0 &&
            setTimeout(function () {
              k(!0);
            }, o.delay),
          (o.delay < 0 || o.combined) &&
            ((a.e =
              ((e = o.throttle),
              (s = function (t) {
                "resize" === t.type && (d = h = -1), k(t.all);
              }),
              (p = 0),
              function (t, i) {
                var r = +new Date() - p;
                function a() {
                  (p = +new Date()), s.call(n, t);
                }
                c && clearTimeout(c),
                  r > e || !o.enableThrottle || i
                    ? a()
                    : (c = setTimeout(a, e - r));
              })),
            (a.a = function (t) {
              (t = C(t)), r.push.apply(r, t);
            }),
            (a.g = function () {
              return (r = i(r).filter(function () {
                return !i(this).data(o.loadedName);
              }));
            }),
            (a.f = function (t) {
              for (var e = 0; e < t.length; e++) {
                var i = r.filter(function () {
                  return this === t[e];
                });
                i.length && k(!1, i);
              }
            }),
            k(),
            i(o.appendScroll).on("scroll." + l + " resize." + l, a.e));
      }
      function C(t) {
        for (
          var s = o.defaultImage,
            r = o.placeholder,
            a = o.imageBase,
            l = o.srcsetAttribute,
            c = o.loaderAttribute,
            d = o._f || {},
            h = 0,
            u = (t = i(t)
              .filter(function () {
                var t = i(this),
                  n = x(this);
                return (
                  !t.data(o.handledName) &&
                  (t.attr(o.attribute) || t.attr(l) || t.attr(c) || d[n] !== e)
                );
              })
              .data("plugin_" + o.name, n)).length;
          h < u;
          h++
        ) {
          var p = i(t[h]),
            f = x(t[h]),
            m = p.attr(o.imageBaseAttribute) || a;
          f === g && m && p.attr(l) && p.attr(l, S(p.attr(l), m)),
            d[f] === e || p.attr(c) || p.attr(c, d[f]),
            f === g && s && !p.attr(v)
              ? p.attr(v, s)
              : f === g ||
                !r ||
                (p.css(w) && "none" !== p.css(w)) ||
                p.css(w, "url('" + r + "')");
        }
        return t;
      }
      function k(t, e) {
        if (r.length) {
          for (
            var s = e || r,
              a = !1,
              l = o.imageBase || "",
              c = o.srcsetAttribute,
              d = o.handledName,
              h = 0;
            h < s.length;
            h++
          )
            if (t || e || T(s[h])) {
              var u = i(s[h]),
                p = x(s[h]),
                f = u.attr(o.attribute),
                m = u.attr(o.imageBaseAttribute) || l,
                _ = u.attr(o.loaderAttribute);
              u.data(d) ||
                (o.visibleOnly && !u.is(":visible")) ||
                !(
                  ((f || u.attr(c)) &&
                    ((p === g &&
                      (m + f !== u.attr(v) || u.attr(c) !== u.attr(y))) ||
                      (p !== g && m + f !== u.css(w)))) ||
                  _
                ) ||
                ((a = !0), u.data(d, !0), D(u, p, m, _));
            }
          a &&
            (r = i(r).filter(function () {
              return !i(this).data(d);
            }));
        } else o.autoDestroy && n.destroy();
      }
      function D(t, e, n, s) {
        ++c;
        var r = function () {
          A("onError", t), E(), (r = i.noop);
        };
        A("beforeLoad", t);
        var a = o.attribute,
          l = o.srcsetAttribute,
          d = o.sizesAttribute,
          h = o.retinaAttribute,
          b = o.removeAttribute,
          C = o.loadedName,
          k = t.attr(h);
        if (s) {
          var D = function () {
            b && t.removeAttr(o.loaderAttribute),
              t.data(C, !0),
              A(p, t),
              setTimeout(E, 1),
              (D = i.noop);
          };
          t.off(m).one(m, r).one(f, D),
            A(s, t, function (e) {
              e ? (t.off(f), D()) : (t.off(m), r());
            }) || t.trigger(m);
        } else {
          var T = i(new Image());
          T.one(m, r).one(f, function () {
            t.hide(),
              e === g
                ? t.attr(_, T.attr(_)).attr(y, T.attr(y)).attr(v, T.attr(v))
                : t.css(w, "url('" + T.attr(v) + "')"),
              t[o.effect](o.effectTime),
              b &&
                (t.removeAttr(
                  a + " " + l + " " + h + " " + o.imageBaseAttribute
                ),
                d !== _ && t.removeAttr(d)),
              t.data(C, !0),
              A(p, t),
              T.remove(),
              E();
          });
          var x = (u && k ? k : t.attr(a)) || "";
          T.attr(_, t.attr(d))
            .attr(y, t.attr(l))
            .attr(v, x ? n + x : null),
            T.complete && T.trigger(f);
        }
      }
      function T(e) {
        var n = e.getBoundingClientRect(),
          s = o.scrollDirection,
          r = o.threshold,
          a = (h >= 0 ? h : (h = i(t).height())) + r > n.top && -r < n.bottom,
          l = (d >= 0 ? d : (d = i(t).width())) + r > n.left && -r < n.right;
        return "vertical" === s ? a : "horizontal" === s ? l : a && l;
      }
      function x(t) {
        return t.tagName.toLowerCase();
      }
      function S(t, e) {
        if (e) {
          var i = t.split(",");
          t = "";
          for (var n = 0, s = i.length; n < s; n++)
            t += e + i[n].trim() + (n !== s - 1 ? "," : "");
        }
        return t;
      }
      function E() {
        --c, r.length || c || A("onFinishedAll");
      }
      function A(t, e, i) {
        return !!(t = o[t]) && (t.apply(n, [].slice.call(arguments, 1)), !0);
      }
      "event" === o.bind || s ? b() : i(t).on(f + "." + l, b);
    }
    function r(s, r) {
      var a = this,
        l = i.extend({}, a.config, r),
        c = {},
        d = l.name + "-" + ++n;
      return (
        (a.config = function (t, i) {
          return i === e ? l[t] : ((l[t] = i), a);
        }),
        (a.addItems = function (t) {
          return c.a && c.a("string" === i.type(t) ? i(t) : t), a;
        }),
        (a.getItems = function () {
          return c.g ? c.g() : {};
        }),
        (a.update = function (t) {
          return c.e && c.e({}, !t), a;
        }),
        (a.force = function (t) {
          return c.f && c.f("string" === i.type(t) ? i(t) : t), a;
        }),
        (a.loadAll = function () {
          return c.e && c.e({ all: !0 }, !0), a;
        }),
        (a.destroy = function () {
          return (
            i(l.appendScroll).off("." + d, c.e), i(t).off("." + d), (c = {}), e
          );
        }),
        o(a, l, s, c, d),
        l.chainable ? s : a
      );
    }
    (i.fn.Lazy = i.fn.lazy =
      function (t) {
        return new r(this, t);
      }),
      (i.Lazy = i.lazy =
        function (t, n, s) {
          if ((i.isFunction(n) && ((s = n), (n = [])), i.isFunction(s))) {
            (t = i.isArray(t) ? t : [t]), (n = i.isArray(n) ? n : [n]);
            for (
              var o = r.prototype.config,
                a = o._f || (o._f = {}),
                l = 0,
                c = t.length;
              l < c;
              l++
            )
              (o[t[l]] === e || i.isFunction(o[t[l]])) && (o[t[l]] = s);
            for (var d = 0, h = n.length; d < h; d++) a[n[d]] = t[0];
          }
        }),
      (r.prototype.config = {
        name: "lazy",
        chainable: !0,
        autoDestroy: !0,
        bind: "load",
        threshold: 500,
        visibleOnly: !1,
        appendScroll: t,
        scrollDirection: "both",
        imageBase: null,
        defaultImage:
          "data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==",
        placeholder: null,
        delay: -1,
        combined: !1,
        attribute: "data-src",
        srcsetAttribute: "data-srcset",
        sizesAttribute: "data-sizes",
        retinaAttribute: "data-retina",
        loaderAttribute: "data-loader",
        imageBaseAttribute: "data-imagebase",
        removeAttribute: !0,
        handledName: "handled",
        loadedName: "loaded",
        effect: "show",
        effectTime: 0,
        enableThrottle: !0,
        throttle: 250,
        beforeLoad: e,
        afterLoad: e,
        onError: e,
        onFinishedAll: e,
      }),
      i(t).on("load", function () {
        s = !0;
      });
  })(window),
  (function (t) {
    function e(e, i, n, s) {
      var o;
      ("POST" !== (s = s ? s.toUpperCase() : "GET") && "PUT" !== s) ||
        !e.config("ajaxCreateData") ||
        (o = e.config("ajaxCreateData").apply(e, [i])),
        t.ajax({
          url: i.attr("data-src"),
          type: "POST" === s || "PUT" === s ? s : "GET",
          data: o,
          dataType: i.attr("data-type") || "html",
          success: function (t) {
            i.html(t),
              n(!0),
              e.config("removeAttribute") &&
                i.removeAttr("data-src data-method data-type");
          },
          error: function () {
            n(!1);
          },
        });
    }
    t.lazy("ajax", function (t, i) {
      e(this, t, i, t.attr("data-method"));
    }),
      t.lazy("get", function (t, i) {
        e(this, t, i, "GET");
      }),
      t.lazy("post", function (t, i) {
        e(this, t, i, "POST");
      }),
      t.lazy("put", function (t, i) {
        e(this, t, i, "PUT");
      });
  })(window.jQuery || window.Zepto),
  (function (t) {
    t.lazy(["av", "audio", "video"], ["audio", "video"], function (e, i) {
      var n = e[0].tagName.toLowerCase();
      if ("audio" === n || "video" === n) {
        var s = e.find("data-src"),
          o = e.find("data-track"),
          r = 0,
          a = function () {
            ++r === s.length && i(!1);
          },
          l = function () {
            var e = t(this),
              i = e[0].tagName.toLowerCase(),
              n = e.prop("attributes"),
              s = t("data-src" === i ? "<source>" : "<track>");
            "data-src" === i && s.one("error", a),
              t.each(n, function (t, e) {
                s.attr(e.name, e.value);
              }),
              e.replaceWith(s);
          };
        e
          .one("loadedmetadata", function () {
            i(!0);
          })
          .off("load error")
          .attr("poster", e.attr("data-poster")),
          s.length
            ? s.each(l)
            : e.attr("data-src")
            ? (t.each(e.attr("data-src").split(","), function (i, n) {
                var s = n.split("|");
                e.append(
                  t("<source>")
                    .one("error", a)
                    .attr({ src: s[0].trim(), type: s[1].trim() })
                );
              }),
              this.config("removeAttribute") && e.removeAttr("data-src"))
            : i(!1),
          o.length && o.each(l);
      } else i(!1);
    });
  })(window.jQuery || window.Zepto),
  (function (t) {
    t.lazy(["frame", "iframe"], "iframe", function (e, i) {
      var n = this;
      if ("iframe" === e[0].tagName.toLowerCase()) {
        var s = e.attr("data-error-detect");
        "true" !== s && "1" !== s
          ? (e.attr("src", e.attr("data-src")),
            n.config("removeAttribute") &&
              e.removeAttr("data-src data-error-detect"))
          : t.ajax({
              url: e.attr("data-src"),
              dataType: "html",
              crossDomain: !0,
              xhrFields: { withCredentials: !0 },
              success: function (t) {
                e.html(t).attr("src", e.attr("data-src")),
                  n.config("removeAttribute") &&
                    e.removeAttr("data-src data-error-detect");
              },
              error: function () {
                i(!1);
              },
            });
      } else i(!1);
    });
  })(window.jQuery || window.Zepto),
  (function (t) {
    t.lazy("noop", function () {}),
      t.lazy("noop-success", function (t, e) {
        e(!0);
      }),
      t.lazy("noop-error", function (t, e) {
        e(!1);
      });
  })(window.jQuery || window.Zepto),
  (function (t) {
    var e = "data-src";
    function i(i, n, o) {
      var r = i.prop("attributes"),
        a = t("<" + n + ">");
      return (
        t.each(r, function (t, i) {
          ("srcset" !== i.name && i.name !== e) || (i.value = s(i.value, o)),
            a.attr(i.name, i.value);
        }),
        i.replaceWith(a),
        a
      );
    }
    function n(e, i, n) {
      var s = t("<img>")
        .one("load", function () {
          n(!0);
        })
        .one("error", function () {
          n(!1);
        })
        .appendTo(e)
        .attr("src", i);
      s.complete && s.load();
    }
    function s(t, e) {
      if (e) {
        var i = t.split(",");
        t = "";
        for (var n = 0, s = i.length; n < s; n++)
          t += e + i[n].trim() + (n !== s - 1 ? "," : "");
      }
      return t;
    }
    t.lazy(["pic", "picture"], ["picture"], function (o, r) {
      if ("picture" === o[0].tagName.toLowerCase()) {
        var a = o.find(e),
          l = o.find("data-img"),
          c = this.config("imageBase") || "";
        a.length
          ? (a.each(function () {
              i(t(this), "source", c);
            }),
            1 === l.length
              ? ((l = i(l, "img", c))
                  .on("load", function () {
                    r(!0);
                  })
                  .on("error", function () {
                    r(!1);
                  }),
                l.attr("src", l.attr(e)),
                this.config("removeAttribute") && l.removeAttr(e))
              : o.attr(e)
              ? (n(o, c + o.attr(e), r),
                this.config("removeAttribute") && o.removeAttr(e))
              : r(!1))
          : o.attr("data-srcset")
          ? (t("<source>")
              .attr({
                media: o.attr("data-media"),
                sizes: o.attr("data-sizes"),
                type: o.attr("data-type"),
                srcset: s(o.attr("data-srcset"), c),
              })
              .appendTo(o),
            n(o, c + o.attr(e), r),
            this.config("removeAttribute") &&
              o.removeAttr(e + " data-srcset data-media data-sizes data-type"))
          : r(!1);
      } else r(!1);
    });
  })(window.jQuery || window.Zepto),
  (window.jQuery || window.Zepto).lazy(
    ["js", "javascript", "script"],
    "script",
    function (t, e) {
      "script" === t[0].tagName.toLowerCase()
        ? (t.attr("src", t.attr("data-src")),
          this.config("removeAttribute") && t.removeAttr("data-src"))
        : e(!1);
    }
  ),
  (window.jQuery || window.Zepto).lazy("vimeo", function (t, e) {
    "iframe" === t[0].tagName.toLowerCase()
      ? (t.attr("src", "https://player.vimeo.com/video/" + t.attr("data-src")),
        this.config("removeAttribute") && t.removeAttr("data-src"))
      : e(!1);
  }),
  (window.jQuery || window.Zepto).lazy(["yt", "youtube"], function (t, e) {
    if ("iframe" === t[0].tagName.toLowerCase()) {
      var i = /1|true/.test(t.attr("data-nocookie"));
      t.attr(
        "src",
        "https://www.youtube" +
          (i ? "-nocookie" : "") +
          ".com/embed/" +
          t.attr("data-src") +
          "?rel=0&amp;showinfo=0"
      ),
        this.config("removeAttribute") && t.removeAttr("data-src");
    } else e(!1);
  }),
  $(document).ready(function () {
    $(".work-slider").owlCarousel({
      stagePadding: 50,
      margin: 0,
      items: 3,
      itemsDesktop: [1199, 3],
      itemsDesktopSmall: [980, 2],
      itemsMobile: [600, 1],
      navigation: !0,
      navigationText: ["", ""],
      pagination: !0,
      autoPlay: !0,
      responsiveClass: !0,
      navText: [
        '<svg style="transform: rotate(180deg)" width="46" height="53" viewBox="0 0 46 53" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M21 0.999999L45 25M45 25L21 52M45 25L9.40799e-05 25" stroke="#34343E"/></svg>',
        '<svg width="46" height="53" viewBox="0 0 46 53" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M21 0.999999L45 25M45 25L21 52M45 25L9.40799e-05 25" stroke="#34343E"/></svg>',
      ],
      navContainer: ".main-content .custom-nav",
      responsive: {
        0: { items: 1, nav: !1, stagePadding: 20 },
        767: { items: 2, loop: !1, stagePadding: 120, nav: !0 },
        980: { items: 2, loop: !1, stagePadding: 120, nav: !0 },
        1024: { items: 3, loop: !1, stagePadding: 120, nav: !0 },
        1100: { items: 4, loop: !1, stagePadding: 120, nav: !0 },
      },
    });
  }),
  $(document).ready(function () {
    $(".product-slider").owlCarousel({
      items: 3,
      itemsDesktop: [1199, 3],
      margin: 0,
      itemsDesktopSmall: [980, 2],
      itemsMobile: [600, 1],
      navigation: !0,
      navigationText: ["", ""],
      pagination: !0,
      autoPlay: !0,
      responsiveClass: !0,
      navText: [
        '<svg  style="transform: rotate(180deg)" width="88" height="88" viewBox="0 0 88 88" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M44 22L66 44M66 44L44 68.75M66 44L24.7501 44" stroke="white"/></svg>',
        '<svg width="88" height="88" viewBox="0 0 88 88" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M44 22L66 44M66 44L44 68.75M66 44L24.7501 44" stroke="white"/></svg>',
      ],
      navContainer: ".main-content2 .custom-nav",
      responsive: {
        0: { items: 1, stagePadding: 40, margin: 20, nav: !1 },
        600: { items: 2, stagePadding: 85, nav: !0 },
        1000: { items: 3, loop: !1, stagePadding: 85, nav: !0 },
      },
    });
  }),
  $(document).ready(function () {
    $(".recommend-slider").owlCarousel({
      items: 4,
      itemsDesktop: [1199, 4],
      margin: 16,
      itemsDesktopSmall: [980, 2],
      itemsMobile: [600, 1],
      navigation: !0,
      navigationText: ["", ""],
      pagination: !0,
      autoPlay: !0,
      responsiveClass: !0,
      navText: [
        '<svg width="11" height="8" viewBox="0 0 11 8" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M4.0715 7.21436L1.21436 4.35721M1.21436 4.35721L4.0715 1.14293M1.21436 4.35721L10.5001 4.35721" stroke="black"/></svg>',
        '<svg width="11" height="8" viewBox="0 0 11 8" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M6.9285 0.785645L9.78564 3.64279M9.78564 3.64279L6.9285 6.85707M9.78564 3.64279L0.499931 3.64279" stroke="white"/></svg>',
      ],
      navContainer: ".content_recommend .custom-nav",
      responsive: {
        0: { items: 1, nav: !0 },
        600: { items: 2, nav: !0 },
        1000: { items: 4, loop: !1, nav: !0 },
      },
    });
  }),
  $(document).ready(function () {
    $(".block-instagram").owlCarousel({
      items: 3,
      itemsDesktop: [1199, 3],
      itemsDesktopSmall: [980, 2],
      itemsMobile: [600, 1],
      navigation: !0,
      margin: 40,
      navigationText: ["", ""],
      pagination: !0,
      autoPlay: !0,
      responsiveClass: !0,
      responsive: {
        0: { items: 1, nav: !1 },
        600: { items: 2, nav: !1, stagePadding: 200 },
        1000: { items: 3, nav: !1, loop: !1, stagePadding: 200 },
      },
    });
  }),
  $(document).ready(function () {
    "use strict";
    if (
      ($(".menu > ul > li:has( > ul)").addClass("menu-dropdown-icon"),
      $(".menu > ul > li > ul:not(:has(ul))").addClass("normal-sub"),
      $(".menu > ul").before('<a href="#" class="menu-mobile"></a>'),
      $(".menu > ul > li").hover(function (t) {
        $(window).width() > 943 &&
          ($(this).children("ul").stop(!0, !1).fadeToggle(150),
          t.preventDefault());
      }),
      $(".menu > ul > li").click(function () {
        $(window).width() <= 943 && $(this).children("ul").fadeToggle(150);
      }),
      $(".menu-mobile").click(function (t) {
        $(".menu > ul").toggleClass("show-on-mobile"), t.preventDefault();
      }),
      $(".back-to-top").length)
    ) {
      var t = function () {
        $(window).scrollTop();
      };
      t(),
        $(window).on("scroll", function () {
          t();
        }),
        $(".back-to-top").on("click", function (t) {
          t.preventDefault(),
            $("html,body").stop().animate({ scrollTop: 0 }, 1e3);
        });
    }
  }),
  $(document).ready(preloderFunction()),
  $(function () {
    $(".hero__title")
      .unbind()
      .on("click", function () {
        $("html, body").animate(
          { scrollTop: $(this).offset().top },
          600,
          "linear"
        );
      });
  }),
  $(function () {
    let t = $(".menuBtn"),
      e = $(".fullscreen_menu"),
      i = document.querySelector(".left"),
      n = $(".img_cont"),
      s = document.querySelector(".img_cont"),
      o = $(".right"),
      r = $(".nav-menus-wrapper"),
      a = $(".cart_header"),
      l = $(".border_header"),
      c = $(".nav-brand"),
      d = $(".menuBtn"),
      h = $(".sign_in"),
      u = document.querySelectorAll(".right .link_m"),
      p = [
        "url(./img/link0menu.png)",
        "url(./img/link1menu.png)",
        "url(./img/link2menu.png)",
        "url(./img/link3menu.png)",
        "url(./img/link4menu.png)",
      ],
      f = [
        "url(./img/link01menu.png)",
        "url(./img/link11menu.png)",
        "url(./img/link21menu.png)",
        "url(./img/link31menu.png)",
        "url(./img/link41menu.png)",
      ],
      m = !0;
    t.click(function (t) {
      (m = !m)
        ? (e.css("z-index", "-1"),
          e.css("background", "none"),
          r.css("opacity", "1"),
          r.css("z-index", "9"),
          a.css("opacity", "1"),
          a.css("z-index", "7"),
          h.css("z-index", "992"),
          h.css("opacity", "1"),
          $(".bgmenu").fadeOut(100),
          l.removeClass("no_border"),
          c.removeClass("open_mn"),
          d.removeClass("open_mn"),
          (i.style.transform = "translateY(100vh)"),
          n.removeClass("anim_img"),
          o.css("transform", "translateY(-100vh)"),
          o.removeClass("anim_list"),
          $("body").css("overflow", "auto"),
          $(".header-area .main-header-area").removeClass("header_onopen"))
        : (e.css("opacity", "1"),
          e.css("z-index", "10"),
          e.css(
            "background",
            "linear-gradient(180deg, #F1CEDF 0%, #F5D7E7 32.94%, #FBE6F6 57.81%, #BFC3CD 100%)"
          ),
          e.css(
            "background",
            " linear-gradient(180deg, #F1CEDF 0%, #F5D7E7 32.94%, #FBE6F6 57.81%, #BFC3CD 100%)"
          ),
          e.css("background-size", "100% 100%"),
          (i.style.transform = "translateY(0)"),
          o.css("transform", "translateY(0)"),
          o.addClass("anim_list"),
          n.addClass("anim_img"),
          l.addClass("no_border"),
          c.addClass("open_mn"),
          d.addClass("open_mn"),
          $("body").css("overflow", "hidden"),
          r.css("opacity", "0"),
          r.css("z-index", "-1"),
          a.css("opacity", "0"),
          a.css("z-index", "-1"),
          h.css("z-index", "-1"),
          h.css("opacity", "0"),
          $(".main-header-area").hasClass("sticky") &&
            $(".main-header-area").addClass("header_onopen")),
        (i.style.background = "url(./img/link1menu.png)");
    });
    for (let t = 0; t < u.length; t++)
      u[t].addEventListener("mousemove", () => {
        (i.style.background = p[t]), (s.style.background = f[t]);
      });
  }),
  $(document).ready(function () {
    $(".sticky").hasClass("header_onopen") &&
      $(this).parent().find(".fullscreen_menu ").css("cursor", "default");
  }),
  $(document).ready(function () {
    var t = $("#sticky-header").offset().top;
    $(window).scroll(function () {
      var e = $(window).scrollTop();
      t < e
        ? ($(".fullscreen_menu ").addClass("def_cursor"),
          $(".cursor-dot").addClass("cursor_menu"))
        : ($(".fullscreen_menu ").removeClass("def_cursor"),
          $(".cursor-dot").removeClass("cursor_menu"));
    });
  }),
  $(document).ready(function () {
    var t = document.querySelector(".main-nav li.active"),
      e = document.querySelector(".active_underline");
    o(t, e);
    var i = document.querySelector(".main-nav ul"),
      n = document.querySelectorAll(".main-nav ul li");
    i.addEventListener("mouseover", function (t) {
      var i = t.target.offsetWidth,
        n = t.target.offsetLeft;
      (e.style.left = n + "px"), (e.style.width = i + "px");
    }),
      i.addEventListener("mouseout", function (i) {
        e.style.left = t.offsetLeft + "px";
      });
    var s = document.querySelectorAll(".main-nav a");
    function o(t, e) {
      (e.style.left = t.offsetLeft + "px"),
        (e.style.width = t.offsetWidth + "px");
    }
    Array.from(s).forEach((i) => {
      i.addEventListener("click", function (i) {
        Array.from(n).forEach((t) => {
          t.classList.remove("active");
        }),
          i.target.parentNode.classList.add("active"),
          (t = document.querySelector(".main-nav li.active")),
          (e = document.querySelector(".active_underline")),
          o(t, e);
      });
    });
  }),
  $(window).on("load resize scroll", function () {
    $(".bg-static").each(function () {
      var t = $(window).scrollTop() - $(this).offset().top;
      $(this).find(".bg-move").css({ left: t });
    });
  }),
  $(function () {
    $(".lazy").lazy();
  }),
  $(document).ready(function () {
    $(".form_si_su")
      .find("input, textarea")
      .on("keyup blur focus", function (t) {
        var e = $(this),
          i = e.prev("label");
        "keyup" === t.type
          ? "" === e.val()
            ? i.removeClass("active highlight")
            : i.addClass("active highlight")
          : "blur" === t.type
          ? "" === e.val()
            ? i.removeClass("active highlight")
            : i.removeClass("highlight")
          : "focus" === t.type &&
            ("" === e.val()
              ? i.removeClass("highlight")
              : "" !== e.val() && i.addClass("highlight"));
      }),
      $(".form_contact")
        .find("input, textarea")
        .on("keyup blur focus", function (t) {
          var e = $(this),
            i = e.prev("label");
          "keyup" === t.type
            ? "" === e.val()
              ? i.removeClass("active highlight")
              : i.addClass("active highlight")
            : "blur" === t.type
            ? "" === e.val()
              ? i.removeClass("active highlight")
              : i.removeClass("highlight")
            : "focus" === t.type &&
              ("" === e.val()
                ? i.removeClass("highlight")
                : "" !== e.val() && i.addClass("highlight"));
        }),
      $(".tab_formlinks a").on("click", function (t) {
        t.preventDefault(),
          $(this).parent().addClass("active"),
          $(this).parent().siblings().removeClass("active"),
          (target = $(this).attr("href")),
          $(".tab-cont_forms > div").not(target).hide(),
          $(target).fadeIn(600);
      }),
      $(".tab_product a").on("click", function (t) {
        t.preventDefault(),
          $(this).parent().addClass("active"),
          $(this).parent().siblings().removeClass("active"),
          (target = $(this).attr("href")),
          $(".tab_product-content > div").not(target).hide(),
          $(target).fadeIn(600);
      }),
      $(".links_cvabout > li > a").click(function () {
        return (
          $("html, body").animate(
            { scrollTop: $($(this).attr("href")).offset().top },
            500
          ),
          !1
        );
      }),
      $(".btn_filter").click(function () {
        $(".content_filter").stop().slideUp(500),
          $(this).next(".content_filter").stop().slideToggle(500),
          $(this).toggleClass("close_filter");
      }),
      $(".delete_filter").click(function () {
        $(this).fadeOut("slow", function () {
          $(this).parent().remove();
        });
      });
  }),
  $(document).ready(function () {
    var t = $(".product-photos-side .swiper-wrapper").slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        dots: !1,
        infinite: !1,
        arrows: !1,
        adaptiveHeight: !1,
        vertical: !0,
        swipe: !0,
        focusOnSelect: !0,
        verticalSwiping: !0,
      }),
      e = $(".product-photos-main .swiper-wrapper").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: !1,
        infinite: !0,
        adaptiveHeight: !0,
        autoplay: !1,
        touchThreshold: 10,
        touchMove: !0,
        arrows: !1,
      }),
      i = $(".product-gallery-full-screen .swiper-wrapper").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: !1,
        touchThreshold: 10,
        infinite: !0,
        touchMove: !0,
        adaptiveHeight: !0,
        autoplay: !1,
        arrows: !1,
        fade: !0,
      });
    t[0] &&
      $(
        t.slick("getSlick").$slides.get(e.slick("getSlick").slickCurrentSlide())
      ).addClass("selected"),
      $(e).on("beforeChange", function (e, i, n, s) {
        $(t.slick("getSlick").$slides.get(n)).removeClass("selected"),
          $(t.slick("getSlick").$slides.get(s)).addClass("selected"),
          t.slick("getSlick").goTo(s);
      });
    var n,
      s = !1,
      o = $(".gallery-nav .fullscreen")[0],
      r = $(".product-gallery-full-screen");
    function a() {
      (s = !1),
        $(".fullscreen").removeClass("leavefs"),
        null !== document.fullscreenElement &&
          (document.exitFullscreen
            ? document.exitFullscreen()
            : document.mozCancelFullScreen
            ? document.mozCancelFullScreen()
            : document.webkitExitFullscreen && document.webkitExitFullscreen());
    }
    function l() {
      a(),
        r.removeClass("opened"),
        $(".navigation").removeClass("z_in0"),
        $(".main-header-area").removeClass("z_in0");
    }
    var c = $(".product-photos-main .slick-slide");
    c.on("mousedown", function () {
      c.on("mouseup mousemove", function t(e) {
        if ("mouseup" === e.type) {
          var n = $(this).data("slick-index");
          i.slick("slickGoTo", n),
            r.addClass("opened"),
            $(".navigation").addClass("z_in0"),
            $(".main-header-area").addClass("z_in0");
        }
        c.off("mouseup mousemove", t);
      });
    });
    var d = $(".product-photos-side .slick-slide");
    d.on("mousedown", function () {
      d.on("mouseup mousemove", function t(i) {
        if ("mouseup" === i.type) {
          var n = $(this).data("slick-index");
          e.slick("slickGoTo", n);
        }
        d.off("mouseup mousemove", t);
      });
    }),
      $(".gallery-nav .close").on("click", function () {
        l();
      }),
      i.on("mousewheel", function (t, e) {
        l();
      }),
      $(".product-gallery-full-screen img").on("mousemove click", function () {
        $(".product-gallery-full-screen .gallery-nav").css("opacity", ".65");
        try {
          clearTimeout(n);
        } catch (t) {}
        n = setTimeout(function () {
          $(".product-gallery-full-screen .gallery-nav").css("opacity", "0");
        }, 3e3);
      }),
      $(".product-gallery-full-screen .swiper-button-next").on(
        "click",
        function () {
          $(i).slick("slickNext");
        }
      ),
      $(".product-gallery-full-screen .swiper-button-prev").on(
        "click",
        function () {
          $(i).slick("slickPrev");
        }
      ),
      $(o).on("click", function () {
        var t;
        s
          ? a()
          : ((s = !0),
            $(".fullscreen").addClass("leavefs"),
            (t = r[0]),
            r[0].requestFullscreen
              ? r[0].requestFullscreen.call(t)
              : [
                  "webkitRequestFullScreen",
                  "mozRequestFullscreen",
                  "msRequestFullScreen",
                ].forEach(function (e) {
                  (r[0].requestFullscreen || r[0][e]).call(t);
                }));
      });
  }),
  $(function () {
    moment.updateLocale("en", {
      weekdaysMin: ["S", "M", "T", "W", "T", "F", "S"],
    }),
      $("#date_rent").daterangepicker({
        autoApply: !0,
        opens: "left",
        linkedCalendars: !1,
        minDate: new Date(),
        locale: { monthNames: moment.months(), format: "DD/MM/YYYY" },
      });
  }),
  $(document).ready(function () {
    $(".remove_r").click(function (t) {
      !(function (t) {
        t.animate({ opacity: "0" }, 150, function () {
          t.animate({ height: "0px" }, 150, function () {
            t.remove();
          });
        });
      })($(this).parent().parent());
    }),
      $(".add_card").click(function () {
        $(".popup_addcard").addClass("visable");
      }),
      $(".close_card").click(function () {
        return $(".popup_addcard").removeClass("visable"), !1;
      });
  }),
  $(function () {
    $(".card_date").datepicker({
      autoclose: !0,
      todayHighlight: !0,
      format: "mm/yy",
      viewMode: "months",
      minViewMode: "months",
    });
  }),
  $(function () {
    ({
      delay: 8,
      _x: 0,
      _y: 0,
      endX: window.innerWidth / 2,
      endY: window.innerHeight / 2,
      cursorVisible: !0,
      cursorEnlarged: !1,
      $dot: document.querySelector(".cursor-dot"),
      $outline: document.querySelector(".cursor-dot-outline"),
      init: function () {
        (this.dotSize = this.$dot.offsetWidth),
          (this.outlineSize = this.$outline.offsetWidth),
          this.setupEventListeners(),
          this.animateDotOutline();
      },
      setupEventListeners: function () {
        var t = this;
        document.querySelectorAll("a").forEach(function (e) {
          e.addEventListener("mouseover", function () {
            (t.cursorEnlarged = !0), t.toggleCursorSize();
          }),
            e.addEventListener("mouseout", function () {
              (t.cursorEnlarged = !1), t.toggleCursorSize();
            });
        }),
          document.addEventListener("mousedown", function () {
            (t.cursorEnlarged = !0), t.toggleCursorSize();
          }),
          document.addEventListener("mouseup", function () {
            (t.cursorEnlarged = !1), t.toggleCursorSize();
          }),
          document.addEventListener("mousemove", function (e) {
            (t.cursorVisible = !0),
              t.toggleCursorVisibility(),
              (t.endX = e.pageX),
              (t.endY = e.pageY),
              (t.$dot.style.top = t.endY + "px"),
              (t.$dot.style.left = t.endX + "px");
          }),
          document.addEventListener("mouseenter", function (e) {
            (t.cursorVisible = !0),
              t.toggleCursorVisibility(),
              (t.$dot.style.opacity = 1),
              (t.$outline.style.opacity = 1);
          }),
          document.addEventListener("mouseleave", function (e) {
            (t.cursorVisible = !0),
              t.toggleCursorVisibility(),
              (t.$dot.style.opacity = 0),
              (t.$outline.style.opacity = 0);
          });
      },
      animateDotOutline: function () {
        (this._x += (this.endX - this._x) / this.delay),
          (this._y += (this.endY - this._y) / this.delay),
          (this.$outline.style.top = this._y + "px"),
          (this.$outline.style.left = this._x + "px"),
          requestAnimationFrame(this.animateDotOutline.bind(this));
      },
      toggleCursorSize: function () {
        this.cursorEnlarged
          ? ((this.$dot.style.transform = "translate(-50%, -50%) scale(0.75)"),
            (this.$outline.style.transform =
              "translate(-50%, -50%) scale(1.5)"))
          : ((this.$dot.style.transform = "translate(-50%, -50%) scale(1)"),
            (this.$outline.style.transform = "translate(-50%, -50%) scale(1)"));
      },
      toggleCursorVisibility: function () {
        this.cursorVisible
          ? ((this.$dot.style.opacity = 1), (this.$outline.style.opacity = 1))
          : ((this.$dot.style.opacity = 0), (this.$outline.style.opacity = 0));
      },
    }.init());
  }),
  (function (t, e, i, n) {
    (t.navigation = function (s, o) {
      var r = {
          responsive: !0,
          mobileBreakpoint: 991,
          showDuration: 200,
          hideDuration: 200,
          showDelayDuration: 0,
          hideDelayDuration: 0,
          submenuTrigger: "hover",
          effect: "fade",
          submenuIndicator: !0,
          submenuIndicatorTrigger: !1,
          hideSubWhenGoOut: !0,
          visibleSubmenusOnMobile: !1,
          fixed: !1,
          overlay: !0,
          overlayColor: "rgba(0, 0, 0, 0.5)",
          hidden: !1,
          hiddenOnMobile: !1,
          offCanvasSide: "left",
          offCanvasCloseButton: !0,
          animationOnShow: "",
          animationOnHide: "",
          onInit: function () {},
          onLandscape: function () {},
          onPortrait: function () {},
          onShowOffCanvas: function () {},
          onHideOffCanvas: function () {},
        },
        a = this,
        l = Number.MAX_VALUE,
        c = 1,
        d = "click.nav touchstart.nav",
        h = "mouseenter focusin",
        u = "mouseleave focusout";
      (a.settings = {}),
        t(s),
        t((s = s)).find(".nav-search").length > 0 &&
          t(s).find(".nav-search").find("form"),
        (a.init = function () {
          (a.settings = t.extend({}, r, o)),
            a.settings.offCanvasCloseButton && t(s).find(".nav-menus-wrapper"),
            "right" == a.settings.offCanvasSide &&
              t(s)
                .find(".nav-menus-wrapper")
                .addClass("nav-menus-wrapper-right"),
            a.settings.hidden &&
              (t(s).addClass("navigation-hidden"),
              (a.settings.mobileBreakpoint = 99999)),
            f(),
            a.settings.fixed && t(s).addClass("navigation-fixed"),
            t(s)
              .find(".nav-toggle")
              .on("click touchstart", function (t) {
                t.stopPropagation(),
                  t.preventDefault(),
                  a.showOffcanvas(),
                  o !== n && a.callback("onShowOffCanvas");
              }),
            t(s)
              .find(".nav-menus-wrapper-close-button")
              .on("click touchstart", function () {
                a.hideOffcanvas(), o !== n && a.callback("onHideOffCanvas");
              }),
            t(s)
              .find(".nav-search-button, .nav-search-close-button")
              .on("click touchstart keydown", function (e) {
                e.stopPropagation(), e.preventDefault();
                var i = e.keyCode || e.which;
                "click" === e.type ||
                "touchstart" === e.type ||
                ("keydown" === e.type && 13 == i)
                  ? a.toggleSearch()
                  : 9 == i && t(e.target).blur();
              }),
            t(s).find(".megamenu-tabs").length > 0 && w(),
            t(e).resize(function () {
              a.initNavigationMode(v()), _(), a.settings.hiddenOnMobile && m();
            }),
            a.initNavigationMode(v()),
            a.settings.hiddenOnMobile && m(),
            o !== n && a.callback("onInit");
        });
      var p = function () {
          t(s).find(".nav-submenu").hide(0),
            t(s).find("li").removeClass("focus");
        },
        f = function () {
          t(s)
            .find("li")
            .each(function () {
              t(this).children(".nav-dropdown,.megamenu-panel").length > 0 &&
                (t(this)
                  .children(".nav-dropdown,.megamenu-panel")
                  .addClass("nav-submenu"),
                a.settings.submenuIndicator &&
                  t(this)
                    .children("a")
                    .append("<span class='submenu-indicator'></span>"));
            });
        },
        m = function () {
          t(s).hasClass("navigation-portrait")
            ? t(s).addClass("navigation-hidden")
            : t(s).removeClass("navigation-hidden");
        };
      (a.showSubmenu = function (e, i) {
        v() > a.settings.mobileBreakpoint &&
          t(s).find(".nav-search").find("form"),
          "fade" == i
            ? t(e)
                .children(".nav-submenu")
                .stop(!0, !0)
                .delay(a.settings.showDelayDuration)
                .fadeIn(a.settings.showDuration)
                .removeClass(a.settings.animationOnHide)
                .addClass(a.settings.animationOnShow)
            : t(e)
                .children(".nav-submenu")
                .stop(!0, !0)
                .delay(a.settings.showDelayDuration)
                .slideDown(a.settings.showDuration)
                .removeClass(a.settings.animationOnHide)
                .addClass(a.settings.animationOnShow),
          t(e).addClass("focus");
      }),
        (a.hideSubmenu = function (e, i) {
          "fade" == i
            ? t(e)
                .find(".nav-submenu")
                .stop(!0, !0)
                .delay(a.settings.hideDelayDuration)
                .fadeOut(a.settings.hideDuration)
                .removeClass(a.settings.animationOnShow)
                .addClass(a.settings.animationOnHide)
            : t(e)
                .find(".nav-submenu")
                .stop(!0, !0)
                .delay(a.settings.hideDelayDuration)
                .slideUp(a.settings.hideDuration)
                .removeClass(a.settings.animationOnShow)
                .addClass(a.settings.animationOnHide),
            t(e).removeClass("focus").find(".focus").removeClass("focus");
        });
      var g = function () {
        t("body").removeClass("no-scroll"),
          a.settings.overlay &&
            t(s)
              .find(".nav-overlay-panel")
              .fadeOut(400, function () {
                t(this).remove();
              });
      };
      (a.showOffcanvas = function () {
        t("body").addClass("no-scroll"),
          a.settings.overlay &&
            (t(s).append("<div class='nav-overlay-panel'></div>"),
            t(s)
              .find(".nav-overlay-panel")
              .css("background-color", a.settings.overlayColor)
              .fadeIn(300)
              .on("click touchstart", function (t) {
                a.hideOffcanvas();
              })),
          "left" == a.settings.offCanvasSide
            ? t(s)
                .find(".nav-menus-wrapper")
                .css("transition-property", "left")
                .addClass("nav-menus-wrapper-open")
            : t(s)
                .find(".nav-menus-wrapper")
                .css("transition-property", "right")
                .addClass("nav-menus-wrapper-open"),
          $("#sticky-header").addClass("height_all");
      }),
        (a.hideOffcanvas = function () {
          t(s)
            .find(".nav-menus-wrapper")
            .removeClass("nav-menus-wrapper-open")
            .on(
              "webkitTransitionEnd moztransitionend transitionend oTransitionEnd",
              function () {
                t(s)
                  .find(".nav-menus-wrapper")
                  .css("transition-property", "none")
                  .off(),
                  $("#sticky-header").removeClass("height_all");
              }
            ),
            g();
        }),
        (a.toggleOffcanvas = function () {
          v() <= a.settings.mobileBreakpoint &&
            (t(s).find(".nav-menus-wrapper").hasClass("nav-menus-wrapper-open")
              ? (a.hideOffcanvas(), o !== n && a.callback("onHideOffCanvas"))
              : (a.showOffcanvas(), o !== n && a.callback("onShowOffCanvas")));
        }),
        (a.initNavigationMode = function (e) {
          a.settings.responsive
            ? (e <= a.settings.mobileBreakpoint &&
                l > a.settings.mobileBreakpoint &&
                (t(s)
                  .addClass("navigation-portrait")
                  .removeClass("navigation-landscape"),
                C(),
                o !== n && a.callback("onPortrait")),
              e > a.settings.mobileBreakpoint &&
                c <= a.settings.mobileBreakpoint &&
                (t(s)
                  .addClass("navigation-landscape")
                  .removeClass("navigation-portrait"),
                b(),
                g(),
                a.hideOffcanvas(),
                o !== n && a.callback("onLandscape")),
              (l = e),
              (c = e))
            : (t(s).addClass("navigation-landscape"),
              b(),
              o !== n && a.callback("onLandscape"));
        });
      var v = function () {
          return (
            e.innerWidth || i.documentElement.clientWidth || i.body.clientWidth
          );
        },
        y = function () {
          t(s).find(".nav-menu").find("li, a").off(d).off(h).off(u);
        },
        _ = function () {
          if (v() > a.settings.mobileBreakpoint) {
            var e = t(s).outerWidth(!0);
            t(s)
              .find(".nav-menu")
              .children("li")
              .children(".nav-submenu")
              .each(function () {
                t(this).parent().position().left + t(this).outerWidth() > e
                  ? t(this).css("right", 0)
                  : t(this).css("right", "auto");
              });
          }
        },
        w = function () {
          function e(e) {
            var i = t(e).children(".megamenu-tabs-nav").children("li"),
              n = t(e).children(".megamenu-tabs-pane");
            t(i).on("click.tabs touchstart.tabs", function (e) {
              e.stopPropagation(),
                e.preventDefault(),
                t(i).removeClass("active"),
                t(this).addClass("active"),
                t(n).hide(0).removeClass("active"),
                t(n[t(this).index()]).show(0).addClass("active");
            });
          }
          if (t(s).find(".megamenu-tabs").length > 0)
            for (var i = t(s).find(".megamenu-tabs"), n = 0; n < i.length; n++)
              e(i[n]);
        },
        b = function () {
          y(),
            p(),
            navigator.userAgent.match(/Mobi/i) ||
            navigator.maxTouchPoints > 0 ||
            "click" == a.settings.submenuTrigger
              ? t(s)
                  .find(".nav-menu, .nav-dropdown")
                  .children("li")
                  .children("a")
                  .on(d, function (i) {
                    if (
                      (a.hideSubmenu(
                        t(this).parent("li").siblings("li"),
                        a.settings.effect
                      ),
                      t(this)
                        .closest(".nav-menu")
                        .siblings(".nav-menu")
                        .find(".nav-submenu")
                        .fadeOut(a.settings.hideDuration),
                      t(this).siblings(".nav-submenu").length > 0)
                    ) {
                      if (
                        (i.stopPropagation(),
                        i.preventDefault(),
                        "none" ==
                          t(this).siblings(".nav-submenu").css("display"))
                      )
                        return (
                          a.showSubmenu(
                            t(this).parent("li"),
                            a.settings.effect
                          ),
                          _(),
                          !1
                        );
                      if (
                        (a.hideSubmenu(t(this).parent("li"), a.settings.effect),
                        "_blank" === t(this).attr("target") ||
                          "blank" === t(this).attr("target"))
                      )
                        e.open(t(this).attr("href"));
                      else {
                        if (
                          "#" === t(this).attr("href") ||
                          "" === t(this).attr("href") ||
                          "javascript:void(0)" === t(this).attr("href")
                        )
                          return !1;
                        e.location.href = t(this).attr("href");
                      }
                    }
                  })
              : t(s)
                  .find(".nav-menu")
                  .find("li")
                  .on(h, function () {
                    a.showSubmenu(this, a.settings.effect), _();
                  })
                  .on(u, function () {
                    a.hideSubmenu(this, a.settings.effect);
                  }),
            a.settings.hideSubWhenGoOut &&
              t("html").on("click.body touchstart.body", function (e) {
                0 === t(e.target).closest(".navigation").length &&
                  (t(s).find(".nav-submenu").fadeOut(),
                  t(s).find(".focus").removeClass("focus"),
                  t(s));
              });
        },
        C = function () {
          y(),
            p(),
            a.settings.visibleSubmenusOnMobile
              ? t(s).find(".nav-submenu").show(0)
              : (t(s)
                  .find(".submenu-indicator")
                  .removeClass("submenu-indicator-up"),
                a.settings.submenuIndicator &&
                a.settings.submenuIndicatorTrigger
                  ? t(s)
                      .find(".submenu-indicator")
                      .on(d, function (e) {
                        return (
                          e.stopPropagation(),
                          e.preventDefault(),
                          a.hideSubmenu(
                            t(this).parent("a").parent("li").siblings("li"),
                            "slide"
                          ),
                          a.hideSubmenu(
                            t(this)
                              .closest(".nav-menu")
                              .siblings(".nav-menu")
                              .children("li"),
                            "slide"
                          ),
                          "none" ==
                          t(this)
                            .parent("a")
                            .siblings(".nav-submenu")
                            .css("display")
                            ? (t(this).addClass("submenu-indicator-up"),
                              t(this)
                                .parent("a")
                                .parent("li")
                                .siblings("li")
                                .find(".submenu-indicator")
                                .removeClass("submenu-indicator-up"),
                              t(this)
                                .closest(".nav-menu")
                                .siblings(".nav-menu")
                                .find(".submenu-indicator")
                                .removeClass("submenu-indicator-up"),
                              a.showSubmenu(
                                t(this).parent("a").parent("li"),
                                "slide"
                              ),
                              !1)
                            : (t(this)
                                .parent("a")
                                .parent("li")
                                .find(".submenu-indicator")
                                .removeClass("submenu-indicator-up"),
                              void a.hideSubmenu(
                                t(this).parent("a").parent("li"),
                                "slide"
                              ))
                        );
                      })
                  : t(s)
                      .find(".nav-menu, .nav-dropdown")
                      .children("li")
                      .children("a")
                      .on(d, function (i) {
                        if (
                          (i.stopPropagation(),
                          i.preventDefault(),
                          a.hideSubmenu(
                            t(this).parent("li").siblings("li"),
                            a.settings.effect
                          ),
                          a.hideSubmenu(
                            t(this)
                              .closest(".nav-menu")
                              .siblings(".nav-menu")
                              .children("li"),
                            "slide"
                          ),
                          "none" ==
                            t(this).siblings(".nav-submenu").css("display"))
                        )
                          return (
                            t(this)
                              .children(".submenu-indicator")
                              .addClass("submenu-indicator-up"),
                            t(this)
                              .parent("li")
                              .siblings("li")
                              .find(".submenu-indicator")
                              .removeClass("submenu-indicator-up"),
                            t(this)
                              .closest(".nav-menu")
                              .siblings(".nav-menu")
                              .find(".submenu-indicator")
                              .removeClass("submenu-indicator-up"),
                            a.showSubmenu(t(this).parent("li"), "slide"),
                            !1
                          );
                        if (
                          (t(this)
                            .parent("li")
                            .find(".submenu-indicator")
                            .removeClass("submenu-indicator-up"),
                          a.hideSubmenu(t(this).parent("li"), "slide"),
                          "_blank" === t(this).attr("target") ||
                            "blank" === t(this).attr("target"))
                        )
                          e.open(t(this).attr("href"));
                        else {
                          if (
                            "#" === t(this).attr("href") ||
                            "" === t(this).attr("href") ||
                            "javascript:void(0)" === t(this).attr("href")
                          )
                            return !1;
                          e.location.href = t(this).attr("href");
                        }
                      }));
        };
      (a.callback = function (t) {
        o[t] !== n && o[t].call(s);
      }),
        a.init();
    }),
      (t.fn.navigation = function (e) {
        return this.each(function () {
          if (n === t(this).data("navigation")) {
            var i = new t.navigation(this, e);
            t(this).data("navigation", i);
          }
        });
      });
  })(jQuery, window, document),
  (function (t) {
    "use strict";
    t(window),
      t.fn.navigation &&
        (t("#navigation1").navigation(),
        t("#always-hidden-nav").navigation({ hidden: !0 }));
  })(jQuery),
  $(document).on(
    {
      mouseenter: function () {
        $(".navigation").addClass("bg_pink"),
          $(".border_header").addClass("open_menu"),
          $("#sticky-header").addClass("bg_pink"),
          $(".header-area").addClass("bg_pink"),
          $(".logo_header").addClass("svg_hover_header"),
          $(".cart_icon").addClass("icon_hover_header"),
          $(".someicon").addClass("color_black"),
          $(".link_shop").addClass("color_black"),
          $(".sign_intxt").addClass("color_black"),
          $(".menuBtn").addClass("bg_icon_menu"),
          $(".cart_header").addClass("cart_hover_header"),
          $(".langue_header li a").addClass("black_color_langue"),
          $(".langue_hover .active")
            .addClass("after_before")
            .append('<span class="left_part">(</span>')
            .prepend('<span class="right_part">)</span>');
      },
      mouseleave: function () {
        $(".navigation").removeClass("bg_pink"),
          $(".border_header").removeClass("open_menu"),
          $("#sticky-header").removeClass("bg_pink"),
          $(".header-area").removeClass("bg_pink"),
          $(".logo_header").removeClass("svg_hover_header"),
          $(".cart_icon").removeClass("icon_hover_header"),
          $(".someicon").removeClass("color_black"),
          $(".link_shop").removeClass("color_black"),
          $(".sign_intxt").removeClass("color_black"),
          $(".menuBtn").removeClass("bg_icon_menu"),
          $(".cart_header").removeClass("cart_hover_header"),
          $(".langue_header li a").removeClass("black_color_langue"),
          $(".langue_hover .active").find(".left_part").remove(),
          $(".langue_hover .active").find(".right_part").remove();
      },
    },
    ".someicon"
  ),
  $(document).on(
    {
      mouseenter: function () {
        $(".navigation").addClass("bg_pink"),
          $(".border_header").addClass("open_menu"),
          $("#sticky-header").addClass("bg_pink"),
          $(".header-area").addClass("bg_pink"),
          $(".logo_header").addClass("svg_hover_header"),
          $(".cart_icon").addClass("icon_hover_header"),
          $(".someicon").addClass("color_black"),
          $(".link_shop").addClass("color_black"),
          $(".sign_intxt").addClass("color_black"),
          $(".menuBtn").addClass("bg_icon_menu"),
          $(".cart_header").addClass("cart_hover_header"),
          $(".langue_header li a").addClass("black_color_langue"),
          $(".langue_hover .active")
            .addClass("after_before")
            .append('<span class="left_part">(</span>')
            .prepend('<span class="right_part">)</span>');
      },
      mouseleave: function () {
        $(".navigation").removeClass("bg_pink"),
          $(".border_header").removeClass("open_menu"),
          $("#sticky-header").removeClass("bg_pink"),
          $(".header-area").removeClass("bg_pink"),
          $(".logo_header").removeClass("svg_hover_header"),
          $(".cart_icon").removeClass("icon_hover_header"),
          $(".someicon").removeClass("color_black"),
          $(".link_shop").removeClass("color_black"),
          $(".sign_intxt").removeClass("color_black"),
          $(".menuBtn").removeClass("bg_icon_menu"),
          $(".cart_header").removeClass("cart_hover_header"),
          $(".langue_header li a").removeClass("black_color_langue"),
          $(".langue_hover .active").find(".left_part").remove(),
          $(".langue_hover .active").find(".right_part").remove();
      },
    },
    ".megamenu-panel"
  ),
  jQuery(document).ready(function () {
    $(".search_bar").on("click", function (t) {
      t.preventDefault(),
        $(".top-search-form").slideDown("slow", function () {
          $(".top-search-form").show(), $(".searchClose").show();
        }),
        setTimeout(function () {
          $(".top-search-form .search-field").focus();
        }, 50);
    });
  }),
  $(document).mouseup(function (t) {
    var e = $(".top-search-form");
    $(".searchClose").on("click", function (t) {
      $(".top-search-form").slideUp("slow", function () {
        e.hide();
      });
    });
  });
