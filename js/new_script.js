$(".accordion-toggle").on("click", function () {
  let is_mobile = false;
  if (window.innerWidth <= 990) {
    is_mobile = true;
  } else {
    is_mobile = false;
  }
  $(window).on("resize", function () {
    if (window.innerWidth <= 990) {
      is_mobile = true;
    } else {
      is_mobile = false;
      $(".accordion-content").hide();
    }
  });
  if (is_mobile) {
    // console.log($(this).next());
    $(this).next().toggleClass("open");
    $(this)
      .next()
      .next(".btn_close_submenu")
      .on("click", function () {
        $(this).parent().removeClass("open");
      });
    if ($(".menuBtn").hasClass("open_mn")) {
      $(".menuBtn").on("click", function () {
        $(".accordion-content").removeClass("open");
      });
    }
  } else {
    $(".accordion-content").hide();
  }
});
$(".btn_close_submenu").on("click", function () {
  $(this).parent("li").parent(".menu-submenu").removeClass("open");
});
$(".open_popupsignin").on("click", function () {
  $(".popup_signin").toggleClass("open_popupSiSo");
});
$(".close_si_so").on("click", function (e) {
  e.preventDefault();
  $(".popup_signin").removeClass("open_popupSiSo");
});
$(document).ready(() => {
  if (window.innerWidth <= 767) {
    $(".btn_filter").on("click", function (e) {
      $(".header_cover").toggleClass("pos_header");
    });
  }
});
$(".slider_product_mobile").slick({
  infinite: true,
  dots: true,
  arrows: false,
  autoplay: true,
  autoplaySpeed: 3000,
  fade: true,
  fadeSpeed: 1000,
});
$(window).resize(function () {});

$(".tabs-nav li:first-child").addClass("active");
$(".tab-content").hide();
$(".tab-content:first").show();

// Click function
$(".tabs-nav li").click(function () {
  $(".tabs-nav li").removeClass("active");
  $(this).addClass("active");
  $(".tab-content").hide();

  var activeTab = $(this).find("a").attr("href");
  $(activeTab).fadeIn();
  return false;
});

$(document).ready(function () {
  $(".dress_rental").owlCarousel({
    items: 1,
    itemsDesktop: [1199, 1],
    margin: 0,
    itemsDesktopSmall: [980, 1],
    itemsMobile: [600, 1],
    navigation: !0,
    navigationText: ["", ""],
    pagination: !0,
    autoPlay: !0,
    responsiveClass: !0,
    navText: [
      // '<svg  style="transform: rotate(180deg)" width="88" height="88" viewBox="0 0 88 88" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M44 22L66 44M66 44L44 68.75M66 44L24.7501 44" stroke="white"/></svg>',
      '<svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M21.9993 11.3329L32.666 21.9995M32.666 21.9995L21.9993 33.9995M32.666 21.9995L12.6661 21.9995" stroke="white"/></svg>',
      '<svg width="44" height="44" viewBox="0 0 44 44" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M21.9993 11.3329L32.666 21.9995M32.666 21.9995L21.9993 33.9995M32.666 21.9995L12.6661 21.9995" stroke="white"/></svg>',
    ],
    navContainer: " .main-content_dress .custom-nav",
    responsive: {
      0: { items: 1, margin: 20, nav: !0 },
      600: { items: 1, nav: !0 },
      1000: { items: 1, loop: !1, nav: !0 },
    },
  });
});
